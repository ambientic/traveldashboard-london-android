package traveldashboard.utils;

import org.ibicoop.init.IbicoopInit;
import org.ibicoop.platform.sysinfo.SystemInfoNative;

import android.content.Context;

public class TravelDashboardUtils {

	public static void startIbicoop(Context context) {
		if (!IbicoopInit.getInstance().isStarted()) {
			String IBICOOP_USERNAME = "TravelDashboardLondon" + System.currentTimeMillis() + "Android@ibicoop.org";
			String IBICOOP_PASSWORD = "TravelDashboardLondon";
			
			SystemInfoNative sysInfo = new SystemInfoNative(
					context.getApplicationContext());
			Object[] initObjects = { context.getApplicationContext(),
					IBICOOP_USERNAME, IBICOOP_PASSWORD,
					sysInfo.getDeviceUniqueId() };
			IbicoopInit.getInstance().start(initObjects);
		}
	}

	private TravelDashboardUtils() {
	}
}
