package traveldashboard.utils;

public final class IntentConstants {
	public static final String PREFERENCE_FOLDER_NAME = "TravelDashboard";
	public static final String PREFERENCE_DIR = "org.theresis.cimst.storage.repo";
	public static final String PREFERENCE_URL = "org.traveldashboard.prefs";
	public static final String PREFERENCE_ENTITY = "transportPreference";
	
	
	public static final String EXTRA_MESSAGE = "traveldashboardlondon.app.EXTRA_MESSAGE";
	
	public static final String CITY = "traveldashboardlondon.app.CITY";
	public static final String STOP_ID = "traveldashboardlondon.app.STOP_ID";
	public static final String STOP_NAME = "traveldashboardlondon.app.STOP_NAME";
	
	public static final String ROUTE_ID = "traveldashboardlondon.app.ROUTE_ID";
	public static final String ROUTE_NAME = "traveldashboardlondon.app.ROUTE_NAME";	
	public static final String ROUTE_DIRECTION = "traveldashboardlondon.app.ROUTE_DIRECTION";	
	
	public static final String LATITUDE = "traveldashboardlondon.app.LATITUDE";
	public static final String LONGITUDE = "traveldashboardlondon.app.LONGITUDE";
	
	public static final String CURRENT_TIME = "traveldashboardlondon.app.CURRENT_TIME";
	
	public static final String CROWDEDNESS_LEVEL = "traveldashboardlondon.app.CROWDEDNESS_LEVEL";
	public static final String CROWD_QUERY_TIME = "traveldashboardlondon.app.CROWD_QUERY_TIME";	
	
	public static final String TUBE_STATION_GSON = "traveldashboardlondon.app.TUBE_STATION_GSON";
	public static final String BUS_STATION_GSON = "traveldashboardlondon.app.BUS_STATION_GSON";
	public static final String TRAM_STATION_GSON = "traveldashboardlondon.app.TRAM_STATION_GSON";
}
