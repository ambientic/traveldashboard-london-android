package traveldashboard.utils;

import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationManager;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiSender;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;

import com.google.gson.Gson;

import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.data.core.CrowdSourcedData;
import traveldashboard.data.core.CrowdSourcedPullCallback;
import traveldashboard.data.core.CrowdSourcedRequest;
import traveldashboard.data.core.CrowdSourcedValue;
import traveldashboard.data.core.HotSpot;
import traveldashboard.data.core.NameUtils;
import android.content.Context;
import android.os.AsyncTask;

/**
 * 
 * This class is to asynchronously pull crowdSourced data from server on Android.
 * 
 * @author cknguyen
 *
 * @param <H>
 * 			hotSpot information such as TubeStation, Tube 1, etc.
 * @param <V>
 * 			crowdSourced data value
 */
public class TravelDashboardCommonPullTask <C extends CrowdSourcedData<H, V>, H extends HotSpot, V extends CrowdSourcedValue, R extends CrowdSourcedRequest>
        extends AsyncTask<Void, Void, Boolean> {

	private static final String POSTFIX_USER_NAME = "@ibicoop.org";


	private Class<C> clazz;
	private R csRequest;
	private CrowdSourcedPullCallback<C, H, V> callback;
	private C resultObj;
	
	private IbiSender sender;
	private Context context;

	public TravelDashboardCommonPullTask(Context context, Class<C> clazz,
			R csRequest, CrowdSourcedPullCallback<C, H, V> callback) {

		this.clazz = clazz;
		this.csRequest = csRequest;
		this.callback = callback;
		this.context = context;
	}
	
	@Override
	protected Boolean doInBackground(Void... arg0) {
		if (isCancelled()) return false;

		String clazzName = NameUtils.getClazzName(clazz);
		if (clazzName == null) return false;
		
		
		try {
			TravelDashboardUtils.startIbicoop(context);

			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopTask for " + clazzName + ": Create sender!");

			IBIURL uriLocal = new IBIURL("ibiurl",
					clazzName + POSTFIX_USER_NAME,
					clazzName + String.valueOf(System.currentTimeMillis()),
					IbicoopDataConstants.APPLICATION, NameUtils.getService(clazzName),
					NameUtils.getPath(clazzName));

			IBIURL uriRemote = new IBIURL("ibiurl", IbicoopDataConstants.USER,
					IbicoopDataConstants.TERMINAL, IbicoopDataConstants.APPLICATION,
					NameUtils.getService(clazzName),
					NameUtils.getPath(clazzName));

			CommunicationOptions options = new CommunicationOptions();
			options.setCommunicationMode(new CommunicationMode(
					CommunicationConstants.MODE_PROXY));

			CommunicationManager comm = IbicoopInit.getInstance()
					.getCommunicationManager();

			sender = comm.createSender(uriLocal, uriRemote, options,
					null);

			NetworkMessage requestMessage = new NetworkMessageXml();
			requestMessage.setMsgOperation(NameUtils.getOperationName(clazzName));
			requestMessage.setMsgType(NetworkMessage.MSG_TYPE_REQUEST);

			Gson gson = new Gson();
			String str = gson.toJson(csRequest);
			if (str != null) {
				requestMessage.addPayload("0", str);
			}

			NetworkMessage responseMessage = new NetworkMessageXml();

			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopTask for " + clazzName + ": Going to send request!");

			boolean ok = responseMessage.decode(sender
					.sendRequestResponse(requestMessage.encode()));
			sender.stop();

			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopTask for " + clazzName + ": Stop sender!");

			if (!ok) return false;

			String wrappedStr = responseMessage.getPayload("0");

			resultObj = gson.fromJson(wrappedStr, clazz);

			return true;

		} catch(Exception e) {
			if (sender != null) sender.stop();
			if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
		}

		return false;
			
	}

	@Override
	protected void onPostExecute(Boolean result) {
		
		if (result) {
			if (callback != null) {
				if (resultObj != null) {
				    callback.update(resultObj);
				} else {
					callback.failed();
				}
			}
		} else {
			if (callback != null) callback.failed();
		}
	}

}
