package traveldashboard.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.Queue;

import org.ibicoop.broker.common.BrokerConstants;
import org.ibicoop.broker.common.BrokerManager;
import org.ibicoop.broker.common.IbiTopic;
import org.ibicoop.broker.common.IbiTopicEvent;
import org.ibicoop.broker.common.IbiTopicFilter;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;

import com.google.gson.Gson;

import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.data.core.CrowdSourcedData;
import traveldashboard.data.core.CrowdSourcedPushCallback;
import traveldashboard.data.core.CrowdSourcedValue;
import traveldashboard.data.core.HotSpot;
import traveldashboard.data.core.NameUtils;
import android.content.Context;
import android.os.AsyncTask;

/**
 * This class is to asynchronously push crowdSourced data to server. If there is
 * a problem somewhere, the class stores data as a cached file to restore it next time.
 *
 * @author cknguyen
 *
 *@param <C>
 * @param <H>
 * @param <V>
 */
public class TravelDashboardCommonPushTask<C extends CrowdSourcedData<H, V>, H extends HotSpot, V extends CrowdSourcedValue>
        extends AsyncTask<Void, Void, Boolean> {

	private static final String POSTFIX_PERSISTENT_PUSH_DATA = ".cached";
	
	private Queue<byte[]> persistentData;

	private Context context;
	private C crowdSourcedData;
	private CrowdSourcedPushCallback callback;
	private boolean successfull = false;

	public TravelDashboardCommonPushTask(Context context, C crowdSourcedData, CrowdSourcedPushCallback callback) {
		this.context = context;
		this.crowdSourcedData = crowdSourcedData;
		this.callback = callback;

		if (persistentData == null) {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopPushTask : Create persistentData");
			//Create persistentData
			readDataCached();
		}

		NetworkMessage pushMessage = new NetworkMessageXml();
		Gson gson = new Gson();
		String str = gson.toJson(crowdSourcedData);
		if (str != null) {
		    pushMessage.addPayload("0", str);
		}
		persistentData.add(pushMessage.encode());
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopPushTask : Insert crowd data into persistentData, " +
				" new persistentData size = " + persistentData.size());
	}

	@Override
	protected Boolean doInBackground(Void... arg0) {

		if(isCancelled()) return false;
		
		boolean publishSuccess = false;
		
		String clazzName = NameUtils.getClazzName(crowdSourcedData.getClass());
		if (clazzName == null) return false;
		
		try {
			TravelDashboardUtils.startIbicoop(context);

			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopPushTask : starting!");

			BrokerManager brokerManager = IbicoopInit.getInstance()
					.getBrokerManager(NameUtils.getBrokerName(clazzName));

			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopPushTask : Create topic list!");
			IbiTopicFilter topicFilter = brokerManager.createIbiTopicFilter();
			topicFilter.setName(NameUtils.getTopicName(clazzName));
			topicFilter.setType(NameUtils.getTopicType(clazzName));
			IbiTopic [] topicList = brokerManager.getTopics(topicFilter);

			String eventName = NameUtils.getEventName(clazzName);
			while (!persistentData.isEmpty()) {
				//Publish all saved Crowd data
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopPushTask : Publish data!");
				byte[] data = persistentData.peek();
				publishSuccess = false;

				for (IbiTopic topic : topicList) {
					IbiTopicEvent ibiEvent = brokerManager.createIbiEvent(eventName, eventName, data);

					int result = topic.publishEvent(ibiEvent);

					if (result == BrokerConstants.OK) {
						if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopPushTask : Publish event ok for topic = " + topic.getName() );
						successfull = true;
						publishSuccess = true;
					} else {
						if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IbicoopPushTask : Publish event not ok for topic = " + topic.getName());
					}
				}

				if (publishSuccess) {
					persistentData.remove(data);
					if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopPushTask : Publish success! Remove Crowd data from persistentData!" +
							" current size = " + persistentData.size());
				} else {
					if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IbicoopPushTask : Publish failed!");
					break;
				}
			}
		} catch (Exception e) {
			if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
		}
		
		writeDataCached();

		return publishSuccess;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopPushTask : ending!");
		
		if (callback != null) {
			if (successfull) {
				callback.success();
			} else {
				callback.failed();
			}
		}
	}


	/**
	 * Read Crowd event cached
	 */
	private synchronized void readDataCached() {
		FileInputStream fis = null;
		ObjectInputStream ois = null;
		try {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopPushTask : read crowd event cached!");
			// Load the persistent cache if empty
			File cacheDirectory = context.getCacheDir();
			if (cacheDirectory == null) {
				throw new IOException("The local cache is null! Cannot store the events file!");
			}
			
			File cacheFile = new File(cacheDirectory, getFileName());
			if (cacheFile.exists()) {
				fis = new FileInputStream(cacheFile);
				ois = new ObjectInputStream(fis);
				persistentData = (Queue<byte[]>) ois.readObject();
			}
			
		} catch (Exception exception) {
			if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IbicoopPushTask : Cannot create " + getFileName() 
					+ exception.getMessage());
		} finally {
			if (ois != null) {
				try {
					ois.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (persistentData == null) {
				persistentData = new LinkedList<byte[]>();
			} else {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopPushTask : got new data cached!");
			}
		}
	}
	

	private synchronized void writeDataCached() {
		FileOutputStream fos = null;
		ObjectOutputStream oos = null;
		try {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopPushTask : write data cached!");
			File cacheDirectory = context.getCacheDir();
			if (cacheDirectory == null) {
				throw new IOException("The local cache is null! Cannot store the events file!");
			}
			
			File cacheFile = new File(cacheDirectory, getFileName());
			
			if (cacheFile.exists()) {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopPushTask : delete previous data cached!");
				cacheFile.delete();
			}
			fos = new FileOutputStream(cacheFile);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(persistentData);
			oos.close();
			fos.close();
		} catch (Exception exception) {
			if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IbicoopPushTask : Cannot write persistentDataCache: " 
					+ exception.getMessage());
			if (oos != null) {
				try {
					oos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private String getFileName() {
		return crowdSourcedData.getHotSpot().getClass().getName()
				+ crowdSourcedData.getCrowdSourcedValue().getClass().getName() + POSTFIX_PERSISTENT_PUSH_DATA;
	}
}
