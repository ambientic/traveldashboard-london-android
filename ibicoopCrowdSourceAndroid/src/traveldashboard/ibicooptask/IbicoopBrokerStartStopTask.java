package traveldashboard.ibicooptask;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

import org.ibicoop.broker.common.BrokerConstants;
import org.ibicoop.broker.common.BrokerManager;
import org.ibicoop.broker.common.EventNotificationListener;
import org.ibicoop.broker.common.IbiResource;
import org.ibicoop.broker.common.IbiResourceEvent;
import org.ibicoop.broker.common.IbiTopic;
import org.ibicoop.broker.common.IbiTopicEvent;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;

import com.google.gson.Gson;

import traveldashboard.utils.IntentConstants;
import traveldashboard.data.TubeStation;
import traveldashboard.data.IbicoopDataConstants;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

public class IbicoopBrokerStartStopTask extends AsyncTask<Void, Void, Boolean> {
	private static final int BROKER_START_TIMEOUT_SECONDS = 30;
	private static AtomicBoolean isStarted = new AtomicBoolean(false);
	
	private Context context;
	private boolean stopRequested;
	private TSLContext tslApplicationContext;
	
	private Class<? extends IntentService> stationVoteIntentService;
	private Class<? extends IntentService> requestIntentService;
	

	public IbicoopBrokerStartStopTask(Context ctx, boolean isStopping, TSLContext tslApplicationContext,
			Class<? extends IntentService> stationVoteIntentService, Class<? extends IntentService> requestIntentService) {
		this.context = ctx;
		this.stopRequested = isStopping;
		this.tslApplicationContext = tslApplicationContext;
		this.stationVoteIntentService = stationVoteIntentService;
		this.requestIntentService = requestIntentService;
	}
	
	@Override
	protected Boolean doInBackground(Void... params) {
		// stopping iBICOOP
		try {
			if (stopRequested) {
				BrokerManager brokerManager = IbicoopInit.getInstance()
						.getBrokerManager(IbicoopDataConstants.BROKER_NAME);
				brokerManager.stopNotification();
				isStarted.set(false);
				return true;
			}
		} catch (Exception e) {
			if (IbicoopDataConstants.DEBUG_MODE)  e.printStackTrace();
			return false;
		}
		
		// iBICOOP was already started before
		if(isStarted.get()){
			return true;
		}

		// starting iBICOOP
		try {
			BrokerManager brokerManager = IbicoopInit.getInstance()
					.getBrokerManager(IbicoopDataConstants.BROKER_NAME);
			brokerManager.startNotification(eventListener);

			// Wait until the broker is started
			if (brokerManager.getBrokerStatus() < 0)
				return false;

			waitForBrokerStatus(brokerManager,
					BrokerConstants.STATUS_BROKER_STARTED_WITH_NOTIFICATION);
			
			isStarted.set(true);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	protected void onPostExecute(Boolean ok) {
		if (stopRequested)
			// GUI is probably not displayed, will not show any message
			return;

		if (!ok)
			if (IbicoopDataConstants.DEBUG_MODE)  Toast.makeText(context, "Failed to start iBICOOP Message Broker",
					Toast.LENGTH_LONG).show();
		else
			if (IbicoopDataConstants.DEBUG_MODE)  Toast.makeText(context, "Start iBICOOP Message Broker success",
					Toast.LENGTH_LONG).show();
	}
	
	/**
	 * Received an on-demand vote request notification
	 * @param forTopic
	 * @param event
	 */
	private void receivedOnDemandRequest(IbiTopic forTopic, IbiTopicEvent event){
		try{
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopBrokerStartStopTask: received on demand request for topic " + forTopic.getName());
			
			// message with wrong name
			if(!event.getName().equals(IbicoopDataConstants.OD_VOTE_EVENT_NAME))
				return;
			
			
			// message from this device. Ignore.
			if(tslApplicationContext.getPendingOnDemandRequests().contains(event.getDescription())){
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Message from this device! Return!");
				return;
			}
			
			// event from wrong topic
			//if(!appInstance.currentChannelTopicList.contains(forTopic)){
			//	Log.w(Constants.LOG_TAG, "Event from wrong topic, unsubscribing ... " + forTopic.getName());
			//	forTopic.unSubscribe();
			//	return;
			//}

			NetworkMessage eventMsg = NetworkMessageXml.readMessage(event.getData());
			String metroDesc = eventMsg.getPayload(IntentConstants.TUBE_STATION_GSON);
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("BrokerTask: metroDesc = " + metroDesc);
			Intent localNotif = new Intent(context, requestIntentService);
			localNotif.putExtra(IntentConstants.TUBE_STATION_GSON, metroDesc);
			context.startService(localNotif);
		} catch (NumberFormatException e){
			if (IbicoopDataConstants.DEBUG_MODE) System.err.println(""+e.getMessage());
		} catch (NullPointerException e){
			if (IbicoopDataConstants.DEBUG_MODE) System.err.println(""+e.getMessage());
		}
	}
	
	/**
	 * A vote was received for a station channel we are listening to
	 * @param forTopic
	 * @param event
	 */
	private void receivedStationVote(IbiTopic forTopic, IbiTopicEvent event){
		try{
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopBrokerStartStopTask: received station vote for topic " + forTopic.getName());
			
			// message with wrong name
			if(!event.getName().equals(IbicoopDataConstants.OD_RESPONSE_EVENT_NAME))
				return;
			
			// message from this device. Ignore.
			if(!tslApplicationContext.getCurrentMetroStationTopicList().containsKey(forTopic.getName()))
			{
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Event from wrong topic, unsubscribing ..."  + forTopic.getName());
				forTopic.unSubscribe();
				return;
			}
			
			TubeStation stop = tslApplicationContext.getCurrentMetroStationTopicList().get(forTopic.getName());
			
			int level = Integer.parseInt(event.getDescription());
			
			Intent localNotif = new Intent(context, stationVoteIntentService);
			Gson gson = new Gson();
			String metroDesc = gson.toJson(stop);
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("BrokerTask: Receive station vote: metroDesc = " + metroDesc);
			localNotif.putExtra(IntentConstants.TUBE_STATION_GSON, metroDesc);
			localNotif.putExtra(IbicoopDataConstants.OD_KEY_LEVEL, level);

			context.startService(localNotif);
			
			// we unsubscribe once we received a response
			tslApplicationContext.getCurrentMetroStationTopicList().remove(forTopic.getName());
			forTopic.unSubscribe();
		} catch (NumberFormatException e){
			if (IbicoopDataConstants.DEBUG_MODE) System.err.println(e.getMessage());
		} catch (NullPointerException e){
			if (IbicoopDataConstants.DEBUG_MODE) System.err.println(e.getMessage());
		}
	}

	private EventNotificationListener eventListener = new EventNotificationListener() {

		@Override
		public boolean acceptNotificationFrom(String eventPublisher) {
			return true;
		}

		@Override
		public void brokerStatus(String brokerName, int statusCode) {

		}

		@Override
		public void receiveEvent(IbiTopic forTopic, IbiTopicEvent event) {
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Received event from topic " + forTopic.getName());
			
			// message with wrong name
			if(event.getName().equals(IbicoopDataConstants.OD_VOTE_EVENT_NAME)){
				receivedOnDemandRequest(forTopic, event);
			} else if(event.getName().equals(IbicoopDataConstants.OD_RESPONSE_EVENT_NAME)) {
				receivedStationVote(forTopic, event);
			}
		}

		@Override
		public void receiveEvent(IbiResource forResource,
				IbiResourceEvent resourceEvent) {
			
		}

		@Override
		public void receiveEvent(IbiTopic arg0, IbiResourceEvent arg1) {
			// TODO Auto-generated method stub
			
		}


	};

	private void waitForBrokerStatus(BrokerManager brokerManager, int code)
			throws IOException {
		long brokerStartTime = System.currentTimeMillis();
		while (brokerManager.getBrokerStatus() != code) {
			try {
				if(isCancelled())
					throw new IOException("Broker cancelled!");
				Thread.sleep(250);
			} catch (InterruptedException e) {

			}			
			if (System.currentTimeMillis() - brokerStartTime >= BROKER_START_TIMEOUT_SECONDS * 1000) {
				if (IbicoopDataConstants.DEBUG_MODE) System.err.println("Broker timed-out!");
				throw new IOException();
			}
		}
	}
}