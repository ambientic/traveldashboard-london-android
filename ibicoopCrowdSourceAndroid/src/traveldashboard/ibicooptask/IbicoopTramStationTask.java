package traveldashboard.ibicooptask;

import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationManager;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiSender;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;

import traveldashboard.data.TramStationsCollection;
import traveldashboard.data.IbicoopDataConstants;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

/**
 * To get tram stations
 * @author khoo
 *
 */
public class IbicoopTramStationTask extends AsyncTask<Void, Void, Boolean> {

	private Context context;
	private IbicoopTramStationCallback callback;
	private String latitude;
	private String longitude;
	private String radius;
	private String tramStationMessage;
	private long initTime;
	private long finalTime;
	
	private IbiSender sender;
	
	public IbicoopTramStationTask(Context context, String latitude, String longitude, String radius, IbicoopTramStationCallback callback) {
		this.context = context;
		this.latitude = latitude;
		this.longitude = longitude;
		this.radius = radius;
		this.callback = callback;
		initTime = System.currentTimeMillis();
	}
	
	@Override
	protected Boolean doInBackground(Void... params) {
		
		if (isCancelled()) return false;
		
		try {
				if (!IbicoopInit.getInstance().isStarted()) {
					if (IbicoopDataConstants.DEBUG_MODE) System.err.println("Ibicoop not started!");
					return false;
				}
			
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopTramStationTask : Create sender!");
		
				IBIURL uriLocal = new IBIURL("ibiurl",
						IbicoopDataConstants.USER_TRAM_STATION,
						IbicoopDataConstants.TERMINAL_TRAM_STATION + String.valueOf(System.currentTimeMillis()),
						IbicoopDataConstants.APPLICATION, IbicoopDataConstants.TRAM_STATION_SERVICE,
						IbicoopDataConstants.PATH_GET_TRAM_STATION);
				
				IBIURL uriRemote = new IBIURL("ibiurl", IbicoopDataConstants.USER,
						IbicoopDataConstants.TERMINAL, IbicoopDataConstants.APPLICATION,
						IbicoopDataConstants.TRAM_STATION_SERVICE,
						IbicoopDataConstants.PATH_GET_TRAM_STATION);
		
				CommunicationOptions options = new CommunicationOptions();
				options.setCommunicationMode(new CommunicationMode(
						CommunicationConstants.MODE_PROXY));
		
				CommunicationManager comm = IbicoopInit.getInstance()
						.getCommunicationManager();
		
				sender = comm.createSender(uriLocal, uriRemote, options,
						null);
				
				NetworkMessage requestMessage = new NetworkMessageXml();
				requestMessage.setMsgOperation(IbicoopDataConstants.OPERATION_GET_TRAM_STATION);
				requestMessage.setMsgType(NetworkMessage.MSG_TYPE_REQUEST);
				
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_STOP_LAT, latitude);
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_STOP_LON, longitude);
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_RADIUS, radius);
		
				ResponseMessage responseMessage = new ResponseMessage();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopTramStationTask : Going to send request!");
				
				boolean ok = responseMessage.decode(sender
						.sendRequestResponse(requestMessage.encode()));
				sender.stop();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopTramStationTask : Stop sender!");
				
				if (!ok) return false;

				tramStationMessage = responseMessage.getPayload(IbicoopDataConstants.TRAM_STATION_MESSAGE);
				
				return true;
			
			} catch(Exception e) {
				if (sender != null) sender.stop();
				if (IbicoopDataConstants.DEBUG_MODE)  e.printStackTrace();
			}
			
			return false;	
	}

	
	@Override
	protected void onPostExecute(Boolean result) {
		
		if (result) {
			if (callback != null) { 
				TramStationsCollection stations = getTramStationsCollection();
				if (stations != null) callback.tramStationsCollectionInfo(stations);
				else callback.tramStationFailed();
			}
		} else {
			if (callback != null) callback.tramStationFailed();
		}
		
		finalTime =System.currentTimeMillis();
		if (IbicoopDataConstants.DEBUG_MODE) System.err.println("#############Execution time############");
		long duration = finalTime - initTime;
		if (IbicoopDataConstants.DEBUG_MODE) System.err.println("duration of tram station task = " + duration + " ms");
		if (IbicoopDataConstants.DEBUG_MODE) System.err.println("#############End Execution time############");
	}
	
	protected TramStationsCollection getTramStationsCollection() {
		TramStationsCollection stations = null;
		
		try {
			
			if (tramStationMessage == null) return stations;
			
			Gson gson = new Gson();
			stations = gson.fromJson(tramStationMessage, TramStationsCollection.class);
		} catch (Exception e) {
			if (IbicoopDataConstants.DEBUG_MODE)  e.printStackTrace();
		}
		
		return stations;
	}
}

