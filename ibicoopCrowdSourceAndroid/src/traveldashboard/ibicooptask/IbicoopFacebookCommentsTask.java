package traveldashboard.ibicooptask;

import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationManager;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiSender;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;

import traveldashboard.data.comment.TubeStationCommentsCollection;
import traveldashboard.data.IbicoopDataConstants;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

public class IbicoopFacebookCommentsTask extends AsyncTask<Void, Void, Boolean> {

	private Context context;
	
	private IbicoopFacebookCommentCallback callback;
	
	private String city;
	private String stopId;
	private int maxComments;
	
	private String facebookCommentMessage;
	
	private IbiSender sender;
	
	public IbicoopFacebookCommentsTask(Context context, String city, String stopId, int maxComments, IbicoopFacebookCommentCallback callback) {
		this.context = context;
		this.city = city;
		this.stopId = stopId;
		this.maxComments = maxComments;
		this.callback = callback;
	}
	
	@Override
	protected Boolean doInBackground(Void... params) {
		
		if (isCancelled()) return false;
		
		try {
			
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopFacebookCommentGetterTask : Create sender!");
			
				IBIURL uriLocal = new IBIURL("ibiurl",
						IbicoopDataConstants.USER_FACEBOOK_COMMENT,
						IbicoopDataConstants.TERMINAL_FACEBOOK_COMMENT,
						IbicoopDataConstants.APPLICATION, IbicoopDataConstants.FACEBOOK_COMMENT_SERVICE,
						IbicoopDataConstants.PATH_GET_FACEBOOK_COMMENT);
				
				IBIURL uriRemote = new IBIURL("ibiurl", IbicoopDataConstants.USER,
						IbicoopDataConstants.TERMINAL, IbicoopDataConstants.APPLICATION,
						IbicoopDataConstants.FACEBOOK_COMMENT_SERVICE,
						IbicoopDataConstants.PATH_GET_FACEBOOK_COMMENT);
				
		
				CommunicationOptions options = new CommunicationOptions();
				options.setCommunicationMode(new CommunicationMode(
						CommunicationConstants.MODE_PROXY));
		
				CommunicationManager comm = IbicoopInit.getInstance()
						.getCommunicationManager();
		
				sender = comm.createSender(uriLocal, uriRemote, options,
						null);
				
				NetworkMessage requestMessage = new NetworkMessageXml();
				
				requestMessage = new NetworkMessageXml();
				requestMessage.setMsgOperation(IbicoopDataConstants.OPERATION_GET_FACEBOOK_COMMENT);
				requestMessage.setMsgType(NetworkMessage.MSG_TYPE_REQUEST);
				
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_CITY_TYPE, city);
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_STOP_ID, stopId);
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_MAX_VALUES, String.valueOf(maxComments));
		
				ResponseMessage responseMessage = new ResponseMessage();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopFacebookCommentGetterTask : Going to send request!");
				
				boolean ok = responseMessage.decode(sender
						.sendRequestResponse(requestMessage.encode()));
				
				sender.stop();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopFacebookCommentGetterTask : Stop sender!");
				
				if (!ok) return false;

				facebookCommentMessage = responseMessage.getPayload(IbicoopDataConstants.FACEBOOK_COMMENT_MESSAGE);
				
				return true;
			
			} catch(Exception e) {
				if (sender != null) sender.stop();
				e.printStackTrace();
			}
			
			return false;
			
	}

	@Override
	protected void onPostExecute(Boolean result) {
		
		if (result) {
			if (callback != null) { 
				TubeStationCommentsCollection tubeStationCommentsCollection = getTubeStationFacebookCommentsCollection();
				if (tubeStationCommentsCollection != null) callback.tubeStationFacebookCommentsCollection(tubeStationCommentsCollection);
				else callback.tubeStationFacebookCommentsCollectionFailed();
			}
		} else {
			if (callback != null)  callback.tubeStationFacebookCommentsCollectionFailed();
		}
	}
	
	protected TubeStationCommentsCollection getTubeStationFacebookCommentsCollection() {
		TubeStationCommentsCollection tubeStationCommentsCollection = null;
		
		if (facebookCommentMessage != null) {
			Gson gson = new Gson();
			tubeStationCommentsCollection = gson.fromJson(facebookCommentMessage, TubeStationCommentsCollection.class);
		}
		
		return tubeStationCommentsCollection;
	}

}