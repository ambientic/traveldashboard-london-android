package traveldashboard.ibicooptask;


import java.io.InputStream;

import traveldashboard.data.LondonMetroDbManager;
import traveldashboard.data.IbicoopDataConstants;
import android.os.AsyncTask;

public class IbicoopAddAllLondonMetroStopsInDbTask  extends AsyncTask<Void, Void, Boolean>{

	//Variables
	private LondonMetroDbManager dbManager;
	private InputStream[] csvStationsInputStream;
	private AddAllLondonMetroStopsInDbTaskCallback callback;
	
	public interface AddAllLondonMetroStopsInDbTaskCallback {
		public void addAllStopsInDbOk();
		public void addAllStopsInDbFailed();	
	}
	
	public IbicoopAddAllLondonMetroStopsInDbTask(
			LondonMetroDbManager receivedDbManager, 
			InputStream[] receivedCsvStationInputStream,
			AddAllLondonMetroStopsInDbTaskCallback receivedCallback
			) {
		dbManager = receivedDbManager;
		csvStationsInputStream = receivedCsvStationInputStream;
		callback = receivedCallback;
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		
		if (isCancelled()) return false;
		
		boolean success = false;
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Add all london stops in db starting...");
		
		try {
			
			int tableCount = dbManager.getStopInfoDbCount();
			
			if (tableCount >= 254) {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Stops are already in database! count = " + tableCount + "! return!");
				return true;
			}
						
			dbManager.insertStopIntoDbByInputStreams(csvStationsInputStream);

			
			try {
				
				for (int i = 0; i < csvStationsInputStream.length; i++) {
					csvStationsInputStream[i].close();
				}
				
			} catch (Exception exception) {
				if (IbicoopDataConstants.DEBUG_MODE) System.err.println("Close csv input stream exception : " + exception.getMessage());
			}
			
			success = true;
			
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		
		return success;
	}
	
	@Override
	protected void onPostExecute(Boolean success) {
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Success = " + success);
		
		if (callback != null) {
			if (success) callback.addAllStopsInDbOk();
			else callback.addAllStopsInDbFailed();
		}
	}

}

