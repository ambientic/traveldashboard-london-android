package traveldashboard.ibicooptask;

import java.util.ArrayList;

import org.ibicoop.sensor.data.IbiSensorDataLocation;
import org.ibicoop.sensor.trigger.IbiSensorDataInRegionSystemTrigger;

import traveldashboard.data.IbicoopDataConstants;

import android.os.AsyncTask;

public class IbicoopRemoveLondonStopsFromRegionMonitoringTask extends AsyncTask<Void, Void, Boolean> {
	//In region trigger
	private IbiSensorDataInRegionSystemTrigger inRegionSystemTrigger;
	
	//Callback
	private RemoveLondonStopsFromRegionMonitoringCallback callback;
	
	private ArrayList<IbiSensorDataLocation> locationDatas;

	public IbicoopRemoveLondonStopsFromRegionMonitoringTask(
			IbiSensorDataInRegionSystemTrigger inRegionSystemTrigger,
			ArrayList<IbiSensorDataLocation> locationDatas,
			RemoveLondonStopsFromRegionMonitoringCallback callback
			) {
		this.inRegionSystemTrigger = inRegionSystemTrigger;
		this.locationDatas = new ArrayList<IbiSensorDataLocation>();
		this.locationDatas.addAll(locationDatas);
		this.callback = callback;
		locationDatas = new ArrayList<IbiSensorDataLocation>();
	}
	
    @Override
    protected Boolean doInBackground(Void... params) {
    	
		if (isCancelled()) return false;
    	if ((inRegionSystemTrigger == null) || (locationDatas.size() == 0)) return false;
    	
    	if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Remove all london stops from region monitoring task");
    	
    	int nbCount = 0;
    	    	
    	for (IbiSensorDataLocation locationData: locationDatas) {    		
            inRegionSystemTrigger.removeThreshold(locationData);            
            nbCount++;
    	}
    	
    	if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Nb london stops removed = " + nbCount);
    	
    	if (nbCount > 0) return true;
    	
        return false;
    }
    
    @Override
    protected void onPostExecute(Boolean result) {
    	if (callback != null) {
    		if (result) callback.removeStopsFromRegionMonitoringOK();
    		else callback.removeStopsFromRegionMonitoringFailed();
    	}
    }
    
    public interface RemoveLondonStopsFromRegionMonitoringCallback {
    	public void removeStopsFromRegionMonitoringOK();
    	public void removeStopsFromRegionMonitoringFailed();
    }
}
