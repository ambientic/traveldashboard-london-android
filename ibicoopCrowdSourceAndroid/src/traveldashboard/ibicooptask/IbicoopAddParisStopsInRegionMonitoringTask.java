package traveldashboard.ibicooptask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.ibicoop.sensor.data.IbiSensorDataLocation;
import org.ibicoop.sensor.trigger.IbiSensorDataInRegionSystemTrigger;
import org.ibicoop.sensor.unit.IbiSensorUnitLocation;

import traveldashboard.data.ParisStopInfo;
import traveldashboard.data.IbicoopDataConstants;
import android.os.AsyncTask;

/**
 * Add all stops in region monitoring task
 * @author khoo
 *
 */
public class IbicoopAddParisStopsInRegionMonitoringTask extends AsyncTask<Void, Void, Boolean> {
	
	//In region trigger
	private IbiSensorDataInRegionSystemTrigger inRegionSystemTrigger;
	
	//Callback
	private AddParisStopsInRegionMonitoringCallback callback;
	
	//Variables
	private ArrayList<ParisStopInfo> parisStopInfos;
	private ArrayList<IbiSensorDataLocation> locationDatas;
	private float monitoringRadius;
	
	
	//
	//
	//Mock location in INRIA
	private static final ArrayList<ParisStopInfo> mockStopInfos;
	
	static {
		mockStopInfos = new ArrayList<ParisStopInfo>();
		ParisStopInfo ambientic0 = new ParisStopInfo("ambientic0", "ambientic0", "ambientic0", "ambientic0", 48.83772, 2.10245,  0);
		mockStopInfos.add(ambientic0);
		//StopInfo ambientic1 = new StopInfo("ambientic1", "ambientic1", "ambientic1", "ambientic1", 48.83772,  2.10245, 1);
		//mockStopInfos.add(ambientic1);
		ParisStopInfo mainBuilding0 = new ParisStopInfo("mainBuilding0", "mainBuilding0", "mainBuilding0", "mainBuilding0", 48.83765, 2.10297, 0);
		mockStopInfos.add(mainBuilding0);
		//StopInfo mainBuilding1 = new StopInfo("mainBuilding1", "mainBuilding1", "mainBuilding1", "mainBuilding1", 48.83765, 2.10297, 1);
		//mockStopInfos.add(mainBuilding1);
		ParisStopInfo canteen0 = new ParisStopInfo("canteen0", "canteen0", "canteen0", "canteen0", 48.83765,  2.10297, 0);
		mockStopInfos.add(canteen0);
		//StopInfo canteen1 = new StopInfo("canteen1", "canteen1", "canteen1", "canteen1", 48.83765, 2.10297, 1);
		//mockStopInfos.add(canteen1);
		ParisStopInfo imara0 = new ParisStopInfo("imara0", "imara0", "imara0", "imara0", 48.83782,  2.10089, 0);
		mockStopInfos.add(imara0);
		//StopInfo imara1 = new StopInfo("imara1", "imara1", "imara1", "imara1", 48.83782,  2.10089,1);
		//mockStopInfos.add(imara1);
		ParisStopInfo entrance0 = new ParisStopInfo("entrance0", "entrance0", "entrance0", "entrance0", 48.83703, 2.10332, 0);
		mockStopInfos.add(entrance0);
		//StopInfo entrance1 = new StopInfo("entrance1", "entrance1", "entrance1", "entrance1", 48.83703, 2.10332,  1);
		//mockStopInfos.add(entrance1);
		ParisStopInfo fieldI0 = new ParisStopInfo("fieldI0", "fieldI0", "fieldI0", "fieldI0", 48.83709,  2.09976, 0);
		mockStopInfos.add(fieldI0);
		//StopInfo fieldI1 = new StopInfo("fieldI1", "fieldI1", "fieldI1", "fieldI1", 48.83709,  2.09976,1);
		//mockStopInfos.add(fieldI1);
		ParisStopInfo fieldII0 = new ParisStopInfo("fieldII0", "fieldII0", "fieldII0", "fieldII0", 48.83696, 2.09889,  0);
		mockStopInfos.add(fieldII0);
		//StopInfo fieldII1 = new StopInfo("fieldII1", "fieldII1", "fieldII1", "fieldII1", 48.83696,  2.09889, 1);		
		//mockStopInfos.add(fieldII1);
		ParisStopInfo fieldIII0 = new ParisStopInfo("fieldIII0", "fieldIII0", "fieldIII0", "fieldIII0", 48.83694, 2.09803, 0);
		mockStopInfos.add(fieldIII0);
		//StopInfo fieldIII1 = new StopInfo("fieldIII1", "fieldIII1", "fieldIII1", "fieldIII1", 48.83694,  2.09803, 1);	
		//mockStopInfos.add(fieldIII1);
		ParisStopInfo fieldIV0 = new ParisStopInfo("fieldIV0", "fieldIV0", "fieldIV0", "fieldIV0", 48.83652, 2.10017, 0);
		mockStopInfos.add(fieldIV0);
		//StopInfo fieldIV1 = new StopInfo("fieldIV1", "fieldIV1", "fieldIV1", "fieldIV1",  48.83652, 2.10017, 1);	
		//mockStopInfos.add(fieldIV1);
		ParisStopInfo fieldV0 = new ParisStopInfo("fieldV0", "fieldV0", "fieldV0", "fieldV0",  48.83634, 2.09890, 0);
		mockStopInfos.add(fieldV0);
		//StopInfo fieldV1 = new StopInfo("fieldV1", "fieldV1", "fieldV1", "fieldV1",  48.83634, 2.09890, 1);	
		//mockStopInfos.add(fieldV1);
		ParisStopInfo fieldVI0 = new ParisStopInfo("fieldVI0", "fieldVI0", "fieldVI0", "fieldVI0", 48.83627, 2.09769, 0);
		mockStopInfos.add(fieldVI0);
		//StopInfo fieldVI1 = new StopInfo("fieldVI1", "fieldVI1", "fieldVI1", "fieldVI1", 48.83627, 2.09769, 1);
		//mockStopInfos.add(fieldVI1);
	}
	
	public IbicoopAddParisStopsInRegionMonitoringTask(
			IbiSensorDataInRegionSystemTrigger inRegionSystemTrigger,
			ArrayList<ParisStopInfo> parisStopInfos,
			float monitoringRadius,
			AddParisStopsInRegionMonitoringCallback callback
			) {
		this.inRegionSystemTrigger = inRegionSystemTrigger;
		this.parisStopInfos = new ArrayList<ParisStopInfo>();
		this.parisStopInfos.addAll(parisStopInfos);
		//Add mock location into stop infos
		//this.parisStopInfos.addAll(mockStopInfos);
		this.monitoringRadius = monitoringRadius;
		this.callback = callback;
		locationDatas = new ArrayList<IbiSensorDataLocation>();
	}
	
    @Override
    protected Boolean doInBackground(Void... params) {
    	
		if (isCancelled()) return false;
    	if ((inRegionSystemTrigger == null) || (parisStopInfos.size() == 0)) return false;
    	
    	if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Add all paris stops in region monitoring task");
    	
    	int nbCount = 0;
    	    	
    	for (ParisStopInfo stop: parisStopInfos) {
            
    		Map<String, String> infoMap = new HashMap<String, String>();
            
            infoMap.put(IbicoopDataConstants.PARAM_VALUE_STOP_ID, stop.getStopId());
            infoMap.put(IbicoopDataConstants.PARAM_KEY_ROUTE_NAME, stop.getRouteName());
            infoMap.put(IbicoopDataConstants.PARAM_KEY_ROUTE_ID, stop.getRouteId());
            infoMap.put(IbicoopDataConstants.PARAM_KEY_STOP_NAME, stop.getStopName());
            infoMap.put(IbicoopDataConstants.PARAM_KEY_STOP_LON, String.valueOf(stop.getLongitude()));
            infoMap.put(IbicoopDataConstants.PARAM_KEY_STOP_LAT, String.valueOf(stop.getLatitude()));

            IbiSensorDataLocation ibiSensorData = new IbiSensorDataLocation(
            		stop.getLatitude(), 
            		stop.getLongitude(), 
            		0.0, 
            		0.0f, 
            		0.0f, 
            		System.currentTimeMillis(), 
            		monitoringRadius,
            		infoMap,
                    new IbiSensorUnitLocation());
            
            inRegionSystemTrigger.addThreshold(ibiSensorData);
            
            locationDatas.add(ibiSensorData);
            
            nbCount++;
    	}
    	
    	if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Nb stops added = " + nbCount);
    	
    	if (nbCount > 0) return true;
    	
        return false;
    }
    
    @Override
    protected void onPostExecute(Boolean result) {
    	if (callback != null) {
    		if (result) callback.addStopsInRegionMonitoringOK(locationDatas);
    		else callback.addStopsInRegionMonitoringFailed();
    	}
    }
    
    public interface AddParisStopsInRegionMonitoringCallback {
    	public void addStopsInRegionMonitoringOK(ArrayList<IbiSensorDataLocation> locationDatas);
    	public void addStopsInRegionMonitoringFailed();
    }
}
