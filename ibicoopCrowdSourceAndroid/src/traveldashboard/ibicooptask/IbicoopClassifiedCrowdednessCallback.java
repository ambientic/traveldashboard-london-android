package traveldashboard.ibicooptask;

import traveldashboard.data.crowd.TubeStationCrowd;

public interface IbicoopClassifiedCrowdednessCallback {
	public void crowdednessFailed();
	public void tubeStationCrowdInfo(TubeStationCrowd crowd);
}
