package traveldashboard.ibicooptask;

import traveldashboard.data.info.TramRealTimeInfosCollection;

/**
 * Ibicoop tram count down callback
 * @author khoo
 *
 */
public interface IbicoopTramCountdownCallback {
	public void tramCountdownFailed();
	public void tramRealtimeInfo(TramRealTimeInfosCollection info);
}
