package traveldashboard.ibicooptask;

import traveldashboard.data.comment.TubeStationCommentsCollection;

public interface IbicoopCommentCallback {
	public void commentFailed();
	public void tubeStationCommentsCollection(TubeStationCommentsCollection info);
}
