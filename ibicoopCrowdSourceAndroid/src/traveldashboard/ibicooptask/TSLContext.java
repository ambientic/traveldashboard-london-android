package traveldashboard.ibicooptask;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.ibicoop.broker.common.IbiTopic;

import traveldashboard.data.TubeStation;

public interface TSLContext {

	boolean checkIfUserIsLoggedIn();
	String currentSHA1EmailAddress();
	Set<String> getPendingOnDemandRequests();
	ConcurrentHashMap<String, TubeStation> getCurrentMetroStationTopicList();
	Set<IbiTopic> getCurrentChannelTopicList();
}
