package traveldashboard.ibicooptask;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CancellationException;

import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationManager;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiSender;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;
import org.json.JSONObject;

import com.google.gson.Gson;

import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.data.weather.Weather;
import android.content.Context;
import android.os.AsyncTask;

public class IbicoopWeatherTask extends AsyncTask<Void, Void, Boolean> {

	private Context context;
	private IbicoopWeatherCallback callback;
	private String latitude;
	private String longitude;
	private String weatherMessage;
	private long initTime;
	private long finalTime;
	
	private IbiSender sender;
	
	public IbicoopWeatherTask(Context context, String latitude, String longitude, IbicoopWeatherCallback callback) {
		this.context = context;
		this.latitude = latitude.trim();
		this.longitude = longitude.trim();
		this.callback = callback;
		initTime = System.currentTimeMillis();
	}
	
	@Override
	protected Boolean doInBackground(Void... params) {
		
		if (isCancelled()) return false;

		try {
				if (!IbicoopInit.getInstance().isStarted()) {
					if (IbicoopDataConstants.DEBUG_MODE) System.err.println("Ibicoop not started!");
					return false;
				}
			
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopWeatherGetterTask : Create sender!");
				
				IBIURL uriLocal = new IBIURL("ibiurl",
						IbicoopDataConstants.USER_WEATHER,
						IbicoopDataConstants.TERMINAL_WEATHER + String.valueOf(System.currentTimeMillis()),
						IbicoopDataConstants.APPLICATION, IbicoopDataConstants.WEATHER_SERVICE,
						IbicoopDataConstants.PATH_GET_WEATHER);
				
				IBIURL uriRemote = new IBIURL("ibiurl", IbicoopDataConstants.USER,
						IbicoopDataConstants.TERMINAL, IbicoopDataConstants.APPLICATION,
						IbicoopDataConstants.WEATHER_SERVICE,
						IbicoopDataConstants.PATH_GET_WEATHER);
		
				CommunicationOptions options = new CommunicationOptions();
				options.setCommunicationMode(new CommunicationMode(
						CommunicationConstants.MODE_PROXY));
		
				CommunicationManager comm = IbicoopInit.getInstance()
						.getCommunicationManager();
		
				sender = comm.createSender(uriLocal, uriRemote, options,
						null);
				
				NetworkMessage requestMessage = new NetworkMessageXml();
				requestMessage.setMsgOperation(IbicoopDataConstants.OPERATION_GET_WEATHER);
				requestMessage.setMsgType(NetworkMessage.MSG_TYPE_REQUEST);
				
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_STOP_LAT, latitude);
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_STOP_LON, longitude);	
		
				ResponseMessage responseMessage = new ResponseMessage();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopWeatherGetterTask : Going to send request!");
				
				boolean ok = responseMessage.decode(sender
						.sendRequestResponse(requestMessage.encode()));
				sender.stop();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopWeatherGetterTask : Stop sender!");
				
				if (!ok) return false;

				weatherMessage = responseMessage.getPayload(IbicoopDataConstants.WEATHER_MESSAGE);
				
				return true;
			
			} catch(Exception e) {
				if (sender != null) sender.stop();
				if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
			}
			
			return false;	
	}

	
	@Override
	protected void onPostExecute(Boolean result) {
		
		if (result) {
			if (callback != null) { 
				
				Weather weather = getWeather();
				
				if (weather != null) callback.weatherObject(weather);
				else callback.weatherFailed();
			}
		} else {
			if (callback != null) callback.weatherFailed();
		}
		
		finalTime =System.currentTimeMillis();
		if (IbicoopDataConstants.DEBUG_MODE) System.err.println("#############Execution time############");
		long duration = finalTime - initTime;
		if (IbicoopDataConstants.DEBUG_MODE) System.err.println("duration of weather task = " + duration + " ms");
		if (IbicoopDataConstants.DEBUG_MODE) System.err.println("#############End Execution time############");
	}
	
	protected Weather getWeather() {
		Weather weather = null;
		
		if (weatherMessage != null) {
			Gson gson = new Gson();
			weather = gson.fromJson(weatherMessage, Weather.class);
		}
		
		return weather;
	}
}
