package traveldashboard.ibicooptask;

import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationManager;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiSender;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;


import com.google.gson.Gson;

import traveldashboard.data.comment.TubeStationCommentsCollection;
import traveldashboard.data.IbicoopDataConstants;
import android.content.Context;
import android.os.AsyncTask;

public class IbicoopAllCommentsTask extends AsyncTask<Void, Void, Boolean> {

	private Context context;
	
	private IbicoopAllCommentsCallback callback;
	
	private String city;
	private String stopId;
	private String routeId;
	
	private String commentMessage;
	private String twitterMessage;
	private String facebookMessage;
	
	private int maxComments;
	
	private IbiSender sender;

	public IbicoopAllCommentsTask(Context context, 
			String city,
			String stopId,
			String routeId,
			int maxComments, 
			IbicoopAllCommentsCallback callback) {

		this.context = context;
		this.city = city;
		this.stopId = stopId;
		this.routeId = routeId;
		this.callback = callback;
		this.maxComments = maxComments;
		
	}
	
	@Override
	protected Boolean doInBackground(Void... params) {

		if (isCancelled()) return false;
		
		try  {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopAllCommentsTask : Create user comment sender!");
			
			IBIURL uriLocal = new IBIURL("ibiurl",
					IbicoopDataConstants.USER_TUBE_COMMENT,
					IbicoopDataConstants.TERMINAL_TUBE_COMMENT + String.valueOf(System.currentTimeMillis()),
					IbicoopDataConstants.APPLICATION, IbicoopDataConstants.TUBE_COMMENT_SERVICE,
					IbicoopDataConstants.PATH_GET_TUBE_COMMENT);
			
			IBIURL uriRemote = new IBIURL("ibiurl", IbicoopDataConstants.USER,
					IbicoopDataConstants.TERMINAL, IbicoopDataConstants.APPLICATION,
					IbicoopDataConstants.TUBE_COMMENT_SERVICE,
					IbicoopDataConstants.PATH_GET_TUBE_COMMENT);
	
			
			CommunicationOptions options = new CommunicationOptions();
			options.setCommunicationMode(new CommunicationMode(
					CommunicationConstants.MODE_PROXY));
	
			CommunicationManager comm = IbicoopInit.getInstance()
					.getCommunicationManager();
	
			sender = comm.createSender(uriLocal, uriRemote, options,
					null);
			
			NetworkMessage requestMessage = new NetworkMessageXml();
			requestMessage.setMsgOperation(IbicoopDataConstants.OPERATION_GET_TUBE_COMMENT);
			requestMessage.setMsgType(NetworkMessage.MSG_TYPE_REQUEST);
			
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_CITY_TYPE, city);
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_ROUTE_ID, routeId);
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_STOP_ID, stopId);
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_MAX_VALUES, String.valueOf(maxComments));
	
			ResponseMessage responseMessage = new ResponseMessage();
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopAllCommentsTask : Going to send user comment request!");
			
			boolean ok1 = false; 
			try {
				ok1 = responseMessage.decode(sender
				.sendRequestResponse(requestMessage.encode()));

				if (ok1) commentMessage = responseMessage.getPayload(IbicoopDataConstants.TUBE_COMMENT_MESSAGE);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			sender.stop();
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("\nIbicoopAllCommentsTask : stop user comment sender\n\n");
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopAllCommentsTask : create twitter comment sender");
			
			uriLocal = new IBIURL("ibiurl",
					IbicoopDataConstants.USER_TWITTER_COMMENT,
					IbicoopDataConstants.TERMINAL_TWITTER_COMMENT,
					IbicoopDataConstants.APPLICATION, IbicoopDataConstants.TWITTER_COMMENT_SERVICE,
					IbicoopDataConstants.PATH_GET_TWITTER_COMMENT);
			
			uriRemote = new IBIURL("ibiurl", IbicoopDataConstants.USER,
					IbicoopDataConstants.TERMINAL, IbicoopDataConstants.APPLICATION,
					IbicoopDataConstants.TWITTER_COMMENT_SERVICE,
					IbicoopDataConstants.PATH_GET_TWITTER_COMMENT);
	
			options = new CommunicationOptions();
			options.setCommunicationMode(new CommunicationMode(
					CommunicationConstants.MODE_PROXY));
	
			comm = IbicoopInit.getInstance()
					.getCommunicationManager();
	
			sender = comm.createSender(uriLocal, uriRemote, options,
					null);
			
			requestMessage = new NetworkMessageXml();
			requestMessage.setMsgOperation(IbicoopDataConstants.OPERATION_GET_TWITTER_COMMENT);
			requestMessage.setMsgType(NetworkMessage.MSG_TYPE_REQUEST);
			
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_CITY_TYPE, city);
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_STOP_ID, stopId);
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_MAX_VALUES, String.valueOf(maxComments));
	
	
			responseMessage = new ResponseMessage();
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopAllCommentGetterTask : Going to send twitter comment request!");
			
			boolean ok2 = false;
			
			try {
				ok2 = responseMessage.decode(sender
						.sendRequestResponse(requestMessage.encode()));
				
				if (ok2) twitterMessage = responseMessage.getPayload(IbicoopDataConstants.TWITTER_COMMENT_MESSAGE);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			sender.stop();
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("\nIbicoopAllCommentsTask : stop twitter comment sender\n\n");
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopAllCommentsTask : create facebook comment sender");
			
			uriLocal = new IBIURL("ibiurl",
					IbicoopDataConstants.USER_FACEBOOK_COMMENT,
					IbicoopDataConstants.TERMINAL_FACEBOOK_COMMENT,
					IbicoopDataConstants.APPLICATION, IbicoopDataConstants.FACEBOOK_COMMENT_SERVICE,
					IbicoopDataConstants.PATH_GET_FACEBOOK_COMMENT);
			
			uriRemote = new IBIURL("ibiurl", IbicoopDataConstants.USER,
					IbicoopDataConstants.TERMINAL, IbicoopDataConstants.APPLICATION,
					IbicoopDataConstants.FACEBOOK_COMMENT_SERVICE,
					IbicoopDataConstants.PATH_GET_FACEBOOK_COMMENT);
			
	
			options = new CommunicationOptions();
			options.setCommunicationMode(new CommunicationMode(
					CommunicationConstants.MODE_PROXY));
	
			comm = IbicoopInit.getInstance()
					.getCommunicationManager();
	
			sender = comm.createSender(uriLocal, uriRemote, options,
					null);
			
			requestMessage = new NetworkMessageXml();
			requestMessage.setMsgOperation(IbicoopDataConstants.OPERATION_GET_FACEBOOK_COMMENT);
			requestMessage.setMsgType(NetworkMessage.MSG_TYPE_REQUEST);
			
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_CITY_TYPE, city);
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_STOP_ID, stopId);
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_MAX_VALUES, String.valueOf(maxComments));
	
			responseMessage = new ResponseMessage();
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopAllCommentGetterTask : Going to send facebook comment request!");
			
			boolean ok3 = false;
			
			try {
				ok3 = responseMessage.decode(sender
						.sendRequestResponse(requestMessage.encode()));
				
				if (ok3) facebookMessage = responseMessage.getPayload(IbicoopDataConstants.FACEBOOK_COMMENT_MESSAGE);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			sender.stop();
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("\nIbicoopAllCommentGetterTask : Stop facebook comment sender!\n\n");
			
			return (ok1 || ok2 || ok3);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return false;
			
	}

	@Override
	protected void onPostExecute(Boolean result) {
		
		if (result) {
			if (callback != null) { 

				//Return user comments
				TubeStationCommentsCollection userCommentsCollection = getTubeStationUserCommentsCollection();
				
				if (userCommentsCollection != null) callback.tubeStationUserCommentsCollection(userCommentsCollection);
				else callback.tubeStationUserCommentsCollectionFailed();
				
				//Return facebook comments
				TubeStationCommentsCollection facebookCommentsCollection = getTubeStationFacebookCommentsCollection();
				
				if (facebookCommentsCollection != null) callback.tubeStationFacebookCommentsCollection(facebookCommentsCollection);
				else callback.tubeStationFacebookCommentsCollectionFailed();
				
				//Return twitter comments
				TubeStationCommentsCollection twitterCommentsCollection = getTubeStationTwitterCommentsCollection();
				
				if (twitterCommentsCollection != null) callback.tubeStationTwitterCommentsCollection(twitterCommentsCollection);
				else callback.tubeStationTwitterCommentsCollectionFailed();
			}
		} else {
			if (callback != null) callback.allCommentsFailed();
		}
	}
	
	protected TubeStationCommentsCollection getTubeStationUserCommentsCollection() {
		TubeStationCommentsCollection stationUserCommentsCollection = null;
		
		if (commentMessage != null) {
			Gson gson = new Gson();
			stationUserCommentsCollection = gson.fromJson(commentMessage, TubeStationCommentsCollection.class);
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("User comment = " + stationUserCommentsCollection.toString());
		}
		
		return stationUserCommentsCollection;
	}
	
	
	protected TubeStationCommentsCollection getTubeStationFacebookCommentsCollection() {
		TubeStationCommentsCollection stationFacebookCommentsCollection = null;
			
		if (facebookMessage != null) {
			Gson gson = new Gson();
			stationFacebookCommentsCollection = gson.fromJson(facebookMessage, TubeStationCommentsCollection.class);
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Facebook comment = " + stationFacebookCommentsCollection.toString());
		}
		
		return stationFacebookCommentsCollection;
	}
	
	protected TubeStationCommentsCollection getTubeStationTwitterCommentsCollection() {
		TubeStationCommentsCollection stationTwitterCommentsCollection = null;
		
		if (twitterMessage != null) {
			Gson gson = new Gson();
			stationTwitterCommentsCollection = gson.fromJson(twitterMessage, TubeStationCommentsCollection.class);
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Twitter comment = " + stationTwitterCommentsCollection.toString());
		}
		
		return stationTwitterCommentsCollection;
	}

}
