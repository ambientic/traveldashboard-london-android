package traveldashboard.ibicooptask;

import traveldashboard.data.BusStationsCollection;

public interface IbicoopBusStopCallback {
	public void busStopFailed();
	public void busStationsCollection(BusStationsCollection busStationsCollection);
}
