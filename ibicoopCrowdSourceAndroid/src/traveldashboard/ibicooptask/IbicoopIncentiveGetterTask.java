package traveldashboard.ibicooptask;

import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationManager;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiSender;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;

import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.data.incentive.Incentive;
import traveldashboard.data.incentive.IncentiveType;
import traveldashboard.data.noise.NoiseDatasCollection;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

public class IbicoopIncentiveGetterTask extends AsyncTask<Void, Void, Boolean> {

	private Context context;
	
	private IbicoopIncentiveCallback callback;

	private String city;
	private String stopId;
	private IncentiveType incentiveType;
	
	private String incentiveMessage;
	
	private IbiSender sender;

	public IbicoopIncentiveGetterTask(Context context,
			String city,
			String stopId,
			IncentiveType incentiveType,
			IbicoopIncentiveCallback incentiveCallback) {
		this.context = context;
		this.city = city;
		this.stopId = stopId;
		this.incentiveType = incentiveType;
		this.callback = incentiveCallback;
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		
		if (isCancelled()) return false;
		
		
		try {
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopIncentiveGetterTask: Creating sender");
			
			IBIURL uriLocal = new IBIURL("ibiurl",
					IbicoopDataConstants.USER_INCENTIVE,
					IbicoopDataConstants.TERMINAL_INCENTIVE + String.valueOf(System.currentTimeMillis()),
					IbicoopDataConstants.APPLICATION, IbicoopDataConstants.INCENTIVE_SERVICE,
					IbicoopDataConstants.PATH_GET_INCENTIVE);
			
			IBIURL uriRemote = new IBIURL("ibiurl", IbicoopDataConstants.USER,
					IbicoopDataConstants.TERMINAL, IbicoopDataConstants.APPLICATION,
					IbicoopDataConstants.INCENTIVE_SERVICE,
					IbicoopDataConstants.PATH_GET_INCENTIVE);

			CommunicationOptions options = new CommunicationOptions();
			options.setCommunicationMode(new CommunicationMode(
					CommunicationConstants.MODE_PROXY));

			CommunicationManager comm = IbicoopInit.getInstance()
					.getCommunicationManager();

			sender = comm.createSender(uriLocal, uriRemote, options,
					null);

			NetworkMessage requestMessage = new NetworkMessageXml();
			requestMessage.setMsgOperation(IbicoopDataConstants.OPERATION_GET_INCENTIVE);
			requestMessage.setMsgType(NetworkMessage.MSG_TYPE_REQUEST);
			
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_CITY_TYPE, city);
			requestMessage.addPayload(IbicoopDataConstants.PARAM_VALUE_STOP_ID, stopId);
			requestMessage.addPayload(IbicoopDataConstants.PARAM_INCENTIVE_TYPE, incentiveType.toString());

			ResponseMessage responseMessage = new ResponseMessage();
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopIncentiveGetter: Going to send request");
			
			boolean ok = responseMessage.decode(sender
					.sendRequestResponse(requestMessage.encode()));
			sender.stop();

			if (ok) {
				incentiveMessage = responseMessage.getPayload(IbicoopDataConstants.INCENTIVE_MESSAGE);
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopIncentiveGetter: Incentive message = " + incentiveMessage);
			} else {
				return false;
			}

			return true;
		} catch(Exception e) {
			if (sender != null) sender.stop();
			e.printStackTrace();
		}
		return false;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		
		if (result) {
			if (callback != null) { 
				Incentive incentive = getIncentive();
				
				if (incentive != null) callback.getIncentiveSuccess(incentive);
				else callback.getIncentiveFailed();
			}
		} else {
			if (callback != null) callback.getIncentiveFailed();
		}
	}

	protected Incentive getIncentive() {
		
		Incentive incentive = null;
		
		if (incentiveMessage != null) {
			Gson gson = new Gson();
			incentive = gson.fromJson(incentiveMessage, Incentive.class);
		}
		
		return incentive;
	}

}


