package traveldashboard.ibicooptask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ibicoop.adaptation.communication.RestartReceiverAction;
import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationManager;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiSender;
import org.ibicoop.communication.common.SenderListener;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;
import org.json.JSONArray;
import org.json.JSONObject;

import traveldashboard.data.BusStationsCollection;
import traveldashboard.data.TubeStationsCollection;
import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.data.status.TubeStatusCollection;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

public class IbicoopTubeStationTask extends AsyncTask<Void, Void, Boolean> {

	private Context context;
	private IbicoopTubeStationCallback callback;
	private String latitude;
	private String longitude;
	private String radius;
	private String tubeStationMessage;
	private long initTime;
	private long finalTime;
	
	private IbiSender sender;
	
	public IbicoopTubeStationTask(Context context, String latitude, String longitude, String radius, IbicoopTubeStationCallback callback) {
		this.context = context;
		this.latitude = latitude;
		this.longitude = longitude;
		this.radius = radius;
		this.callback = callback;
		initTime = System.currentTimeMillis();
	}
	
	@Override
	protected Boolean doInBackground(Void... params) {
		
		if (isCancelled()) return false;
		
		try {
				if (!IbicoopInit.getInstance().isStarted()) {
					if (IbicoopDataConstants.DEBUG_MODE) System.err.println("Ibicoop not started!");
					return false;
				}
			
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopTubeStationTask : Create sender!");
		
				IBIURL uriLocal = new IBIURL("ibiurl",
						IbicoopDataConstants.USER_TUBE_STATION,
						IbicoopDataConstants.TERMINAL_TUBE_STATION + String.valueOf(System.currentTimeMillis()),
						IbicoopDataConstants.APPLICATION, IbicoopDataConstants.TUBE_STATION_SERVICE,
						IbicoopDataConstants.PATH_GET_TUBE_STATION);
				
				IBIURL uriRemote = new IBIURL("ibiurl", IbicoopDataConstants.USER,
						IbicoopDataConstants.TERMINAL, IbicoopDataConstants.APPLICATION,
						IbicoopDataConstants.TUBE_STATION_SERVICE,
						IbicoopDataConstants.PATH_GET_TUBE_STATION);
		
				CommunicationOptions options = new CommunicationOptions();
				options.setCommunicationMode(new CommunicationMode(
						CommunicationConstants.MODE_PROXY));
		
				CommunicationManager comm = IbicoopInit.getInstance()
						.getCommunicationManager();
		
				sender = comm.createSender(uriLocal, uriRemote, options,
						null);
				
				NetworkMessage requestMessage = new NetworkMessageXml();
				requestMessage.setMsgOperation(IbicoopDataConstants.OPERATION_GET_TUBE_STATION);
				requestMessage.setMsgType(NetworkMessage.MSG_TYPE_REQUEST);
				
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_STOP_LAT, latitude);
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_STOP_LON, longitude);
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_RADIUS, radius);	
		
				ResponseMessage responseMessage = new ResponseMessage();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopTubeStationTask : Going to send request!");
				
				boolean ok = responseMessage.decode(sender
						.sendRequestResponse(requestMessage.encode()));
				sender.stop();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopTubeStationTask : Stop sender!");
				
				if (!ok) return false;

				tubeStationMessage = responseMessage.getPayload(IbicoopDataConstants.TUBE_STATION_MESSAGE);
				
				return true;
			
			} catch(Exception e) {
				if (sender != null) sender.stop();
				if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
			}
			
			return false;	
	}

	
	@Override
	protected void onPostExecute(Boolean result) {
		
		if (result) {
			if (callback != null) { 
				TubeStationsCollection stations = getTubeStationsCollection();			
				if (stations != null) callback.tubeStationsCollectionInfo(stations);
				else callback.tubeStationFailed();
			}
		} else {
			if (callback != null) callback.tubeStationFailed();
		}
		
		finalTime =System.currentTimeMillis();
		if (IbicoopDataConstants.DEBUG_MODE) System.err.println("#############Execution time############");
		long duration = finalTime - initTime;
		if (IbicoopDataConstants.DEBUG_MODE) System.err.println("duration of tube station task = " + duration + " ms");
		if (IbicoopDataConstants.DEBUG_MODE) System.err.println("#############End Execution time############");
	}
	
	protected TubeStationsCollection getTubeStationsCollection() {
		TubeStationsCollection stations = null;
		
		try {
			
			if (tubeStationMessage == null) return stations;
			
			Gson gson = new Gson();
			stations = gson.fromJson(tubeStationMessage, TubeStationsCollection.class);
			
		} catch (Exception e) {
			if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
		}
		
		return stations;
	}
	
}
