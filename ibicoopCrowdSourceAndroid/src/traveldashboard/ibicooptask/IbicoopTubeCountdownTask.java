package traveldashboard.ibicooptask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CancellationException;

import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationManager;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiSender;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;

import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.data.info.TubeRealTimeInfo;
import traveldashboard.data.info.TubeRealTimeInfosCollection;
import android.content.Context;
import android.os.AsyncTask;

public class IbicoopTubeCountdownTask extends AsyncTask<Void, Void, Boolean> {

	private Context context;
	
	private IbicoopTubeCountdownCallback callback;
	
	private String city;
	private String stopId;
	private String routeId;
	private int maxValues;

	private String tubeCountdownMessage;

	private List<TubeRealTimeInfosCollection> infoList;
	
	private IbiSender sender;

	public IbicoopTubeCountdownTask(Context context, String city, String stopId, String routeId, int maxValues, IbicoopTubeCountdownCallback callback) {
		this.context = context;
		
		this.city = city;
		this.stopId = stopId;
		this.routeId = routeId;
		this.maxValues = maxValues;
		this.callback = callback;
		this.infoList = new ArrayList<TubeRealTimeInfosCollection>();
	}
		
	@Override
	protected Boolean doInBackground(Void... params) {
		
		if (isCancelled()) return false;
		
		boolean ok =false;
		
		try {
			IBIURL uriLocal = null;
			IBIURL uriRemote = null;
			CommunicationOptions options = null;
			CommunicationManager comm = null;
				
			try {
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Create tube count down sender");
				
				uriLocal = new IBIURL("ibiurl",
						IbicoopDataConstants.USER_TUBE_COUNTDOWN,
						IbicoopDataConstants.TERMINAL_TUBE_COUNTDOWN + String.valueOf(System.currentTimeMillis() + routeId),
						IbicoopDataConstants.APPLICATION, IbicoopDataConstants.TUBE_COUNTDOWN_SERVICE,
						IbicoopDataConstants.PATH_GET_TUBE_COUNTDOWN);
				
				uriRemote = new IBIURL("ibiurl", IbicoopDataConstants.USER,
						IbicoopDataConstants.TERMINAL, IbicoopDataConstants.APPLICATION,
						IbicoopDataConstants.TUBE_COUNTDOWN_SERVICE,
						IbicoopDataConstants.PATH_GET_TUBE_COUNTDOWN);

				options = new CommunicationOptions();
				options.setCommunicationMode(new CommunicationMode(
						CommunicationConstants.MODE_PROXY));

				comm = IbicoopInit.getInstance()
						.getCommunicationManager();

				sender = comm.createSender(uriLocal, uriRemote, options,
						null);
				
				NetworkMessage requestMessage = new NetworkMessageXml();
				requestMessage.setMsgOperation(IbicoopDataConstants.OPERATION_GET_TUBE_COUNTDOWN);
				requestMessage.setMsgType(NetworkMessage.MSG_TYPE_REQUEST);
				
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_CITY_TYPE, city);
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_ROUTE_ID, routeId);
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_STOP_ID, stopId);
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_MAX_VALUES, String.valueOf(maxValues));
				
				ResponseMessage responseMessage = new ResponseMessage();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Send tube count down sender request");
				boolean okTempo = responseMessage.decode(sender
						.sendRequestResponse(requestMessage.encode()));
				
				tubeCountdownMessage = responseMessage.getPayload(IbicoopDataConstants.TUBE_COUNTDOWN_MESSAGE);
				
				infoList.add(getTubeRealTimeInfosCollection());
				
				ok = (ok || okTempo);
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Stop tube count down sender");
				sender.stop();
			}
			catch(Exception e) {
				if (sender != null) sender.stop();
				if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
			}

		if (!ok)
			return false;
		
			return true;
		} catch(Exception e) {
			if (sender != null) sender.stop();
			if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
		}
		return false;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		
		if (result) {
			if (callback != null) { 
				//Return the combinaison of all tube real time infos collection
				TubeRealTimeInfosCollection tubeRealTimeInfosCollection = combineAllTubeRealTimeInfosCollections();
				if (tubeRealTimeInfosCollection != null) callback.tubeRealtimeInfo(routeId, tubeRealTimeInfosCollection);
				else callback.tubeCountdownFailed(routeId);
			}
		} else {
			if (callback != null) callback.tubeCountdownFailed(routeId);
		}
	}

	/**
	 * Combine all tube real time infos collection
	 * @return
	 */
	protected TubeRealTimeInfosCollection combineAllTubeRealTimeInfosCollections() {
		
		TubeRealTimeInfosCollection tubeRealTimeInfosCollection = null;
		List<TubeRealTimeInfo> infos = new ArrayList<TubeRealTimeInfo>();
		
		for (int i = 0; i < infoList.size(); i++) {
			TubeRealTimeInfosCollection info = infoList.get(i);

			TubeRealTimeInfo[] infosTempo = new TubeRealTimeInfo[0];
			
			if (info != null) {
				infosTempo = info.getTubeRealTimeInfos();
			}
			
			for (int j = 0; j < infosTempo.length; j++) {
				infos.add(infosTempo[j]);
			}
		}
		
		TubeRealTimeInfo[] newInfos = new TubeRealTimeInfo[infos.size()];
		for (int k = 0; k < infos.size(); k++) {
			newInfos[k] = infos.get(k);
		}
		
		tubeRealTimeInfosCollection = new TubeRealTimeInfosCollection(newInfos);
		
		return tubeRealTimeInfosCollection;
	}
	
	protected synchronized TubeRealTimeInfosCollection getTubeRealTimeInfosCollection() {
		TubeRealTimeInfosCollection tubeRealTimeInfosCollection = null;
		
		if (tubeCountdownMessage != null) {
			Gson gson = new Gson();
			tubeRealTimeInfosCollection = gson.fromJson(tubeCountdownMessage, TubeRealTimeInfosCollection.class);
		}
		
		return tubeRealTimeInfosCollection;
	}
}
