package traveldashboard.ibicooptask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationManager;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiSender;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;
import org.ibicoop.utils.StringUtils;

import traveldashboard.data.ParisStopInfo;
import traveldashboard.data.IbicoopDataConstants;

import android.content.Context;
import android.os.AsyncTask;

/**
 * Get all stops
 * @author khoo
 *
 */
public class IbicoopParisMetroAllStopsTask extends AsyncTask<Void, Integer, ArrayList<ParisStopInfo>> {
	private IbicoopAllStopsReadyCallback callback;
	private Context context;
	private File stopsCache;
	private String city;
	private static ArrayList<ParisStopInfo> stopsStatic; // static list is cached

	private IbiSender sender;
	
	public IbicoopParisMetroAllStopsTask(Context context, String city, IbicoopAllStopsReadyCallback callback) {
		this.callback = callback;
		this.context = context;
		this.city = city;
		File cacheDirectory = this.context.getCacheDir();
		stopsCache = new File(cacheDirectory,
		                IbicoopDataConstants.METRO_STOPS_CACHE_FILE_NAME);
	}
	
	@Override
	protected ArrayList<ParisStopInfo> doInBackground(Void... params) {
		
		if (stopsStatic != null) {
		        return stopsStatic;
		}
		
		if (isCancelled()) return null;
		
		try {
		        if (stopsCache.exists()) {
		                // file exists, so we do not need network
		                InputStream is = new FileInputStream(stopsCache);
		                ByteArrayOutputStream bos = new ByteArrayOutputStream();
		                byte[] b = new byte[1024];
		                int bytesRead;
		                while ((bytesRead = is.read(b)) != -1) {
		                        bos.write(b, 0, bytesRead);
		                }
		                byte[] bytes = bos.toByteArray();
		                is.close();
		                bos.close();
		                return parseMessage(bytes);
		
		        } else {
			
	    			IBIURL uriLocal = new IBIURL("ibiurl",
	    					IbicoopDataConstants.USER_ALL_STOPS,
	    					IbicoopDataConstants.TERMINAL_ALL_STOPS + String.valueOf(System.currentTimeMillis()),
	    					IbicoopDataConstants.APPLICATION, IbicoopDataConstants.GET_ALL_STOPS_SERVICE,
	    					IbicoopDataConstants.PATH_GET_ALL_STOPS);
	    			IBIURL uriRemote = new IBIURL("ibiurl", IbicoopDataConstants.USER,
	    					IbicoopDataConstants.TERMINAL, IbicoopDataConstants.APPLICATION,
	    					IbicoopDataConstants.GET_ALL_STOPS_SERVICE,
	    					IbicoopDataConstants.PATH_GET_ALL_STOPS);
	    			
	    			CommunicationOptions options = new CommunicationOptions();
	    			options.setCommunicationMode(new CommunicationMode(
	    					CommunicationConstants.MODE_PROXY));
	    	
	    			CommunicationManager comm = IbicoopInit.getInstance()
	    					.getCommunicationManager();
	    	
	    			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopAllStopsTask: Create sender!");
	    			
	    			sender = comm.createSender(uriLocal, uriRemote, options, null);
	    			
	                NetworkMessage requestMessage = new NetworkMessageXml();
	                requestMessage.setMsgOperation(IbicoopDataConstants.OPERATION_GET_ALL_STOPS);
	                requestMessage.setMsgType(NetworkMessage.MSG_TYPE_REQUEST);
	                requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_CITY_TYPE, city);
	                //Route type 1 for metro
	                requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_ROUTE_TYPE, "1");
		                
		            byte[] responseData = sender.sendRequestResponse(requestMessage.encode());
		
	    			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopParisMetroAllStopsTask: stop sender!");
	    			if (IbicoopDataConstants.DEBUG_MODE) System.out.println(new String(responseData));
		            
		            sender.stop();
		            
		            if (responseData == null) {
		            	return null;
		            }
		
	                ArrayList<ParisStopInfo> tmpLst = parseMessage(responseData);
	                if (tmpLst == null) {
	                    return null;
	                }
	
	                // store data to file for later use
	                stopsCache.createNewFile();
	                OutputStream os = new FileOutputStream(stopsCache);
	                os.write(responseData);
	                os.flush();
	                os.close();
	
	                return tmpLst;
		        }
		} catch (Exception exception) {
		        if (IbicoopDataConstants.DEBUG_MODE) System.err.println(exception.getMessage());
		}
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	private ArrayList<ParisStopInfo> parseMessage(byte[] data) {
		// Parsing the response
		NetworkMessage responseMessage = new NetworkMessageXml();
		boolean result = responseMessage.decode(data);
	
		if (!result) {
			return null;
		}
		
		Vector<byte[]> stopIdVector = responseMessage.getKeyVector();
		HashMap<String, String> stopInfoMap = new HashMap<String, String>();
		List<String> stopIdList = new ArrayList<String>();
		int stopIdVectorSize = stopIdVector.size();
		
		for (int i = 0; i < stopIdVectorSize; i++) {
		        try {
	                String stopIdKey = new String(stopIdVector.elementAt(i),
	                                StringUtils.UTF8);
	                String stopIdData = responseMessage.getPayload(i);
	                stopInfoMap.put(stopIdKey, stopIdData);
	
	                if (stopIdData.equals("ID")) {
	                        stopIdList.add(stopIdKey);
	                }
		        } catch (Exception e) {
		                if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
		        }
		}
		
		ArrayList<ParisStopInfo> stops = new ArrayList<ParisStopInfo>();
		// Create StopInfo object
		for (String stopId : stopIdList) {
		        String keyWordName = stopId + "_Name";
		        String keyWordLat = stopId + "_Lat";
		        String keyWordLon = stopId + "_Lon";
		        String keyWordRouteId = stopId + "_RouteId";
		        String keyWordRouteName = stopId + "_RouteName";
		        String keyWordDirection = stopId + "_Direction";
		
		        String name = stopInfoMap.get(keyWordName);
		        String routeId = stopInfoMap.get(keyWordRouteId);
		        String routeName = stopInfoMap.get(keyWordRouteName);
		        double latitude = Double.parseDouble(stopInfoMap.get(keyWordLat));
		        double longitude = Double.parseDouble(stopInfoMap.get(keyWordLon));
		        int direction = Integer.parseInt(stopInfoMap.get(keyWordDirection));
		        stops.add(new ParisStopInfo(stopId, routeId, routeName, name, latitude,
		                        longitude, direction));
		}
		
		// Now sorting the stops in alphabetical order
		Collections.sort(stops, new Comparator<ParisStopInfo>() {
		        @Override
		        public int compare(ParisStopInfo o1, ParisStopInfo o2) {
		                return o1.getStopName().compareTo(o2.getStopName());
		        }
		});
		
		return stops;
	}
	
	@Override
	protected void onPostExecute(ArrayList<ParisStopInfo> result) {
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopParisMetroAllStopsTask: end!");
		
		if (isCancelled() || callback == null)
		        return;
		
		if (result != null) {
		        callback.allStopsReady(result);
		        stopsStatic = result;
		} else {
		        callback.allStopsFailed();
		}
	}
}
