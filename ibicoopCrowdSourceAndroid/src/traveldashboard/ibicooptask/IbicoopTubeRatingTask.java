package traveldashboard.ibicooptask;

import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationManager;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiSender;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;

import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.data.rating.TubeRating;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

public class IbicoopTubeRatingTask extends AsyncTask<Void, Void, Boolean> {

	private Context context;
	
	private IbicoopTubeRatingCallback callback;

	private String city;
	private String routeId;
	
	private String tubeRatingMessage;
	
	private IbiSender sender;
	
	public IbicoopTubeRatingTask(Context context, String city, String routeId,
			IbicoopTubeRatingCallback callback) {
		this.context = context;
		this.city = city;
		this.routeId = routeId;
		this.callback = callback;

	}

	@Override
	protected Boolean doInBackground(Void... params) {
		
		if (isCancelled()) return false;
		
		try {
				boolean ok = false;
							
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopTubeRatingTask : Create tube rating sender!");
				
				IBIURL uriLocal = new IBIURL("ibiurl",
						IbicoopDataConstants.USER_TUBE_RATING,
						IbicoopDataConstants.TERMINAL_TUBE_RATING  + String.valueOf(System.currentTimeMillis()),
						IbicoopDataConstants.APPLICATION, IbicoopDataConstants.TUBE_RATING_SERVICE,
						IbicoopDataConstants.PATH_GET_TUBE_RATING);
				
				IBIURL uriRemote =  new IBIURL("ibiurl", IbicoopDataConstants.USER,
						IbicoopDataConstants.TERMINAL, IbicoopDataConstants.APPLICATION,
						IbicoopDataConstants.TUBE_RATING_SERVICE,
						IbicoopDataConstants.PATH_GET_TUBE_RATING);
		
				CommunicationOptions options = new CommunicationOptions();
				options.setCommunicationMode(new CommunicationMode(
						CommunicationConstants.MODE_PROXY));
		
				CommunicationManager comm = IbicoopInit.getInstance()
						.getCommunicationManager();
		
				sender = comm.createSender(uriLocal, uriRemote, options,
						null);
				
				NetworkMessage requestMessage = new NetworkMessageXml();
				
				requestMessage.setMsgOperation(IbicoopDataConstants.OPERATION_GET_TUBE_RATING);
				
				requestMessage.setMsgType(NetworkMessage.MSG_TYPE_REQUEST);
				
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_CITY_TYPE, city);
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_ROUTE_ID, routeId);
				
				ResponseMessage responseMessage = new ResponseMessage();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopTubeRatingTask : Going to send tube rating request!");
				
				try {
					ok = responseMessage.decode(sender
							.sendRequestResponse(requestMessage.encode()));
					
					if (ok) tubeRatingMessage = responseMessage.getPayload(IbicoopDataConstants.TUBE_RATING_MESSAGE);
					
				} catch (Exception e) {
					if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
				}
				
				sender.stop();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopTubeRatingTask : Stop tube rating sender!");
				
				return ok;
			
			} catch(Exception e) {
				if (sender != null) sender.stop();
				if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
			}
			
			return false;
			
	}

	
	@Override
	protected void onPostExecute(Boolean result) {
		
		if (result) {
			if (callback != null) {				
				//Return tube rating object
				TubeRating rating =  processTubeRating();
				if (rating != null) callback.tubeRatingInfo(rating);
				else callback.tubeRatingFailed();
			}
		} else {
			if (callback != null) callback.tubeRatingFailed();
		}
	}
		
	protected TubeRating processTubeRating() {
		//Process tube rating
		TubeRating tubeRating = null;
		
		if ((tubeRatingMessage == null) || (tubeRatingMessage.equals("null"))) return tubeRating;
		
			Gson gson = new Gson();
			tubeRating= gson.fromJson(tubeRatingMessage, TubeRating.class);
		
		return tubeRating;
	}
}
