package traveldashboard.ibicooptask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ibicoop.sensor.data.IbiSensorDataLocation;
import org.ibicoop.sensor.trigger.IbiSensorDataInRegionSystemTrigger;
import org.ibicoop.sensor.unit.IbiSensorUnitLocation;

import traveldashboard.data.LondonMetroStopInfo;
import traveldashboard.data.ParisStopInfo;
import traveldashboard.ibicooptask.IbicoopAddParisStopsInRegionMonitoringTask.AddParisStopsInRegionMonitoringCallback;
import traveldashboard.data.IbicoopDataConstants;
import android.os.AsyncTask;

public class IbicoopAddLondonStopsInRegionMonitoringTask  extends AsyncTask<Void, Void, Boolean> {
	
	//In region trigger
	private IbiSensorDataInRegionSystemTrigger inRegionSystemTrigger;
	
	//Callback
	private AddLondonStopsInRegionMonitoringCallback callback;
	
	//Variables
	private ArrayList<LondonMetroStopInfo> londonStopInfos;
	private ArrayList<IbiSensorDataLocation> locationDatas;
	private float monitoringRadius;

	public IbicoopAddLondonStopsInRegionMonitoringTask (
			IbiSensorDataInRegionSystemTrigger inRegionSystemTrigger,
			ArrayList<LondonMetroStopInfo> londonStopInfos,
			float monitoringRadius,
			AddLondonStopsInRegionMonitoringCallback callback
			) {
		this.inRegionSystemTrigger = inRegionSystemTrigger;
		this.londonStopInfos = new ArrayList<LondonMetroStopInfo>();
		this.monitoringRadius = monitoringRadius;
		this.callback = callback;
		locationDatas = new ArrayList<IbiSensorDataLocation>();
	}
	
    @Override
    protected Boolean doInBackground(Void... params) {
    	
		if (isCancelled()) return false;
    	if ((inRegionSystemTrigger == null) || (londonStopInfos.size() == 0)) return false;
    	
    	if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Add all london stops in region monitoring task");
    	
    	int nbCount = 0;
    	    	
    	for (LondonMetroStopInfo stop: londonStopInfos) {
            
    		Map<String, String> infoMap = new HashMap<String, String>();
            
    		infoMap.put("id", stop.getStopName());
            infoMap.put("stc", stop.getStopId());
            infoMap.put("nlc", stop.getStopNlc());
            infoMap.put("lng", String.valueOf(stop.getLongitude()));
            infoMap.put("lat", String.valueOf(stop.getLatitude()));
            
            List<String> routeIds = stop.getRouteIds();
            HashMap<String, String> routeIdsWithNamesMap = stop.getRouteIdsWithNamesMap();
            HashMap<String, String> routeIdsWithDirectionsMap = stop.getRouteIdsWithDirectionsMap();
            
            for (int i = 0; i < routeIds.size(); i++) {
            	String routeId = routeIds.get(i);
            	String routeName = routeIdsWithNamesMap.get(routeId);
            	String routeDest = routeIdsWithDirectionsMap.get(routeId);
                infoMap.put("line" + (i + 1), routeId);
                infoMap.put("lineName" + (i + 1), routeName);
                infoMap.put("lineDirection" + (i + 1), routeDest);                
            }

            IbiSensorDataLocation ibiSensorData = new IbiSensorDataLocation(
            		stop.getLatitude(), 
            		stop.getLongitude(), 
            		0.0, 
            		0.0f, 
            		0.0f, 
            		System.currentTimeMillis(), 
            		monitoringRadius,
            		infoMap,
                    new IbiSensorUnitLocation());
            
            inRegionSystemTrigger.addThreshold(ibiSensorData);
            
            locationDatas.add(ibiSensorData);
            
            nbCount++;
    	}
    	
    	if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Nb stops added = " + nbCount);
    	
    	if (nbCount > 0) return true;
    	
        return false;
    }
    
    @Override
    protected void onPostExecute(Boolean result) {
    	if (callback != null) {
    		if (result) callback.addStopsInRegionMonitoringOK(locationDatas);
    		else callback.addStopsInRegionMonitoringFailed();
    	}
    }
    
    public interface AddLondonStopsInRegionMonitoringCallback {
    	public void addStopsInRegionMonitoringOK(ArrayList<IbiSensorDataLocation> locationDatas);
    	public void addStopsInRegionMonitoringFailed();
    }
}
