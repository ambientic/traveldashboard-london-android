package traveldashboard.ibicooptask;

import traveldashboard.data.noise.NoiseDatasCollection;

public interface IbicoopNoiseCallback {
	public void getNoiseFailed();
	public void getNoiseInfos(NoiseDatasCollection noiseDatasCollection);
}
