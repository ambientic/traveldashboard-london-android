package traveldashboard.ibicooptask;


import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationManager;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiSender;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;


import com.google.gson.Gson;

import traveldashboard.data.BikeStationsCollection;
import traveldashboard.data.IbicoopDataConstants;

import android.content.Context;
import android.os.AsyncTask;

public class IbicoopBikeGetterTask extends AsyncTask<Void, Void, Boolean> {

	private Context context;
	private IbicoopBikeCallback callback;
	private String latitude;
	private String longitude;
	private String radius;
	private String bikeMessage;
	private long initTime;
	private long finalTime;
	
	private IbiSender sender;
	
	public IbicoopBikeGetterTask(Context context, String latitude, String longitude, String radius, IbicoopBikeCallback callback) {
		this.context = context;
		this.latitude = latitude;
		this.longitude = longitude;
		this.radius = radius;
		this.callback = callback;
		initTime = System.currentTimeMillis();
	}
	
	@Override
	protected Boolean doInBackground(Void... params) {
		
		if (isCancelled()) return false;
		
		try {
		
				if (!IbicoopInit.getInstance().isStarted()) {
					if (IbicoopDataConstants.DEBUG_MODE) System.err.println("Ibicoop not started!");
					return false;
				}
			
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopBikeGetterTask : Create sender!");
			
				IBIURL uriLocal = new IBIURL("ibiurl",
						IbicoopDataConstants.USER_BIKE,
						IbicoopDataConstants.TERMINAL_BIKE + String.valueOf(System.currentTimeMillis()),
						IbicoopDataConstants.APPLICATION, IbicoopDataConstants.BIKE_SERVICE,
						IbicoopDataConstants.PATH_GET_BIKE);
				IBIURL uriRemote = new IBIURL("ibiurl", IbicoopDataConstants.USER,
						IbicoopDataConstants.TERMINAL, IbicoopDataConstants.APPLICATION,
						IbicoopDataConstants.BIKE_SERVICE,
						IbicoopDataConstants.PATH_GET_BIKE);
		
				CommunicationOptions options = new CommunicationOptions();
				options.setCommunicationMode(new CommunicationMode(
						CommunicationConstants.MODE_PROXY));
		
				CommunicationManager comm = IbicoopInit.getInstance()
						.getCommunicationManager();
		
				sender = comm.createSender(uriLocal, uriRemote, options,
						null);
				
				NetworkMessage requestMessage = new NetworkMessageXml();
				requestMessage.setMsgOperation(IbicoopDataConstants.OPERATION_GET_BIKE);
				requestMessage.setMsgType(NetworkMessage.MSG_TYPE_REQUEST);
				
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_STOP_LAT, latitude);
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_STOP_LON, longitude);
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_RADIUS, radius);	
		
				ResponseMessage responseMessage = new ResponseMessage();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopBikeGetterTask : Going to send request!");
					
				boolean ok = responseMessage.decode(sender
						.sendRequestResponse(requestMessage.encode()));
				sender.stop();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopBikeGetterTask : Stop sender!");
				
				if (!ok) return false;

				bikeMessage = responseMessage.getPayload(IbicoopDataConstants.BIKE_MESSAGE);
				
				return true;
			
			} catch(Exception e) {
				if (sender != null) sender.stop();
				if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
			}
			
			return false;
			
	}

	
	@Override
	protected void onPostExecute(Boolean result) {
		
		if (result) {
			if (callback != null) { 
				BikeStationsCollection bikeStationsCollection = getBikeStationsCollection();
				if (bikeStationsCollection != null) callback.bikeStationsCollection(bikeStationsCollection);
				else callback.bikeFailed();
			}
		} else {
			if (callback != null) callback.bikeFailed();
		}
		
		finalTime =System.currentTimeMillis();
		if (IbicoopDataConstants.DEBUG_MODE) System.err.println("#############Execution time############");
		long duration = finalTime - initTime;
		if (IbicoopDataConstants.DEBUG_MODE) System.err.println("duration of bike task = " + duration + " ms");
		if (IbicoopDataConstants.DEBUG_MODE) System.err.println("#############End Execution time############");
	}
	
	
	protected BikeStationsCollection getBikeStationsCollection() {
		BikeStationsCollection bikeStationsCollection = null;
		
		try {
			
			if (bikeMessage == null) return bikeStationsCollection;
			
			Gson gson = new Gson();
			bikeStationsCollection = gson.fromJson(bikeMessage, BikeStationsCollection.class);
			
		} catch (Exception e) {
			if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
		}
		
		return bikeStationsCollection;
	}

}
