package traveldashboard.ibicooptask;


import traveldashboard.data.BikeStationsCollection;

public interface IbicoopBikeCallback {
	public void bikeFailed();
	public void bikeStationsCollection(BikeStationsCollection bikeStationsCollection);
}
