package traveldashboard.ibicooptask;


import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationManager;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiSender;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;

import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.data.noise.NoiseDatasCollection;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

public class IbicoopNoiseGetterTask extends AsyncTask<Void, Void, Boolean> {

	private Context context;
	
	private IbicoopNoiseCallback callback;

	private long interval;
	
	private String city;
	private String stopId;
	private String routeId;
	
	private String noiseMessage;
	
	private IbiSender sender;

	public IbicoopNoiseGetterTask(Context context, String city, String stopId, String routeId, 
			long interval, IbicoopNoiseCallback noiseCallback) {
		this.context = context;
		this.city = city;
		this.stopId = stopId;
		this.routeId = routeId;
		this.interval = interval;
		this.callback = noiseCallback;
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		
		if (isCancelled()) return false;
		
		try {
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopNoiseGetterTask: Creating sender");
			
			IBIURL uriLocal = new IBIURL("ibiurl",
					IbicoopDataConstants.USER_NOISE,
					IbicoopDataConstants.TERMINAL_NOISE + String.valueOf(System.currentTimeMillis()),
					IbicoopDataConstants.APPLICATION, IbicoopDataConstants.NOISE_SERVICE,
					IbicoopDataConstants.PATH_GET_NOISE);
			
			IBIURL uriRemote = new IBIURL("ibiurl", IbicoopDataConstants.USER,
					IbicoopDataConstants.TERMINAL, IbicoopDataConstants.APPLICATION,
					IbicoopDataConstants.NOISE_SERVICE,
					IbicoopDataConstants.PATH_GET_NOISE);

			CommunicationOptions options = new CommunicationOptions();
			options.setCommunicationMode(new CommunicationMode(
					CommunicationConstants.MODE_PROXY));

			CommunicationManager comm = IbicoopInit.getInstance()
					.getCommunicationManager();

			sender = comm.createSender(uriLocal, uriRemote, options,
					null);

			NetworkMessage requestMessage = new NetworkMessageXml();
			requestMessage.setMsgOperation(IbicoopDataConstants.OPERATION_GET_NOISE);
			requestMessage.setMsgType(NetworkMessage.MSG_TYPE_REQUEST);
			
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_CITY_TYPE, city);
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_STOP_ID, stopId);
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_ROUTE_ID, routeId);
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_TIMESTAMP, String.valueOf(System.currentTimeMillis()));
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_INTERVAL, String.valueOf(interval));				

			ResponseMessage responseMessage = new ResponseMessage();
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopNoiseGetter: Going to send request");
			
			boolean ok = responseMessage.decode(sender
					.sendRequestResponse(requestMessage.encode()));
			sender.stop();

			if (ok) {
				noiseMessage = responseMessage.getPayload(IbicoopDataConstants.NOISE_MESSAGE);
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopNoiseGetter: Noise message = " + noiseMessage);
			} else {
				return false;
			}

			return true;
		} catch(Exception e) {
			if (sender != null) sender.stop();
			if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
		}
		return false;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		
		if (result) {
			if (callback != null) { 
				NoiseDatasCollection noiseDatasCollection = getNoiseDatasCollection();
				if (noiseDatasCollection != null) callback.getNoiseInfos(noiseDatasCollection);
				else callback.getNoiseFailed();
			}
		} else {
			if (callback != null) callback.getNoiseFailed();
		}
	}

	protected NoiseDatasCollection getNoiseDatasCollection() {
		
		NoiseDatasCollection noiseDatasCollection = null;
		
		if (noiseMessage != null) {
			Gson gson = new Gson();
			noiseDatasCollection = gson.fromJson(noiseMessage, NoiseDatasCollection.class);
		}
		
		return noiseDatasCollection;
	}

}

