package traveldashboard.ibicooptask;

import java.util.Vector;

import org.ibicoop.sdp.config.NetworkMessageXml;

class ResponseMessage extends NetworkMessageXml {
	public ResponseMessage() {
		super();
	}

	public Vector<byte[]> getKeyVector() {
		return (Vector<byte[]>) keyIndex;
	}
}
