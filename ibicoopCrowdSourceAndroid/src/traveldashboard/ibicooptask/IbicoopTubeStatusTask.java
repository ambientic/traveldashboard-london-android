package traveldashboard.ibicooptask;

import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationManager;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiSender;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;

import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.data.status.TubeStatus;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

public class IbicoopTubeStatusTask extends AsyncTask<Void, Void, Boolean> {

	private Context context;
	
	private IbicoopTubeStatusCallback callback;

	private String city;
	private String routeId;
	
	private String statusMessage;
	
	private IbiSender sender;
	
	public IbicoopTubeStatusTask(Context context, String city, String routeId,
			IbicoopTubeStatusCallback  callback) {
		this.context = context;
		this.city = city;
		this.routeId = routeId;
		this.callback = callback;
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		
		if (isCancelled()) return false;
		
		try {
				boolean ok1 = false;
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopTubeStatusTask : Create tube status sender!");
			
				IBIURL uriLocal = new IBIURL("ibiurl",
						IbicoopDataConstants.USER_TUBE_STATUS,
						IbicoopDataConstants.TERMINAL_TUBE_STATUS  + String.valueOf(System.currentTimeMillis()),
						IbicoopDataConstants.APPLICATION, IbicoopDataConstants.TUBE_STATUS_SERVICE,
						IbicoopDataConstants.PATH_GET_TUBE_STATUS);
				
				IBIURL uriRemote = new IBIURL("ibiurl", IbicoopDataConstants.USER,
						IbicoopDataConstants.TERMINAL, IbicoopDataConstants.APPLICATION,
						IbicoopDataConstants.TUBE_STATUS_SERVICE,
						IbicoopDataConstants.PATH_GET_TUBE_STATUS);
		
				CommunicationOptions options = new CommunicationOptions();
				options.setCommunicationMode(new CommunicationMode(
						CommunicationConstants.MODE_PROXY));
		
				CommunicationManager comm = IbicoopInit.getInstance()
						.getCommunicationManager();
		
				sender = comm.createSender(uriLocal, uriRemote, options,
						null);
				
				NetworkMessage requestMessage = new NetworkMessageXml();
				requestMessage.setMsgOperation(IbicoopDataConstants.OPERATION_GET_TUBE_STATUS);
				requestMessage.setMsgType(NetworkMessage.MSG_TYPE_REQUEST);	

				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_CITY_TYPE, city);				
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_ROUTE_ID, routeId);
				
				ResponseMessage responseMessage = new ResponseMessage();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopTubeStatusTask : Going to send tube status request!");
				
				try {
					ok1 = responseMessage.decode(sender
							.sendRequestResponse(requestMessage.encode()));
					
					if (ok1) statusMessage = responseMessage.getPayload(IbicoopDataConstants.TUBE_STATUS_MESSAGE);
				} catch (Exception e) {
					if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
				}

				sender.stop();
				
				return (ok1);
			
			} catch(Exception e) {
				if (sender != null) sender.stop();
				if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
			}
			
			return false;
			
	}

	
	@Override
	protected void onPostExecute(Boolean result) {
		
		if (result) {
			if (callback != null) {
				//Return tube status object
				TubeStatus status = processTubeStatus();
				if (status != null) callback.tubeStatusInfo(status);
				else callback.tubeStatusFailed();
			}
		} else {
			if (callback != null) callback.tubeStatusFailed();
		}
	}
	
	protected TubeStatus processTubeStatus() {
		//Process tube status
		TubeStatus tubeStatus = null;
		
		if ((statusMessage == null) || (statusMessage.equals("null"))) return tubeStatus; 
			
			Gson gson = new Gson();
			tubeStatus = gson.fromJson(statusMessage, TubeStatus.class);
		
		return tubeStatus;
	}
}
