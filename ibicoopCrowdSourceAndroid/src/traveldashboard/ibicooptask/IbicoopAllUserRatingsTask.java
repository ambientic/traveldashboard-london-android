package traveldashboard.ibicooptask;

import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationManager;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiSender;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;

import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.data.rating.TubeRating;
import traveldashboard.data.rating.TubeRatingsCollection;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

public class IbicoopAllUserRatingsTask extends AsyncTask<Void, Void, Boolean> {

	private Context context;
	
	private IbicoopAllRatingsCallback callback;

	private String city;
	
	private TSLContext tslApplicationContext;
	
	private String userRatingsMessage;
	
	private IbiSender sender;
	
	public IbicoopAllUserRatingsTask(Context context, String city, TSLContext tslApplicationContext, double latitude, double longitude, IbicoopAllRatingsCallback callback) {
		this.context = context;
		this.city = city;
		this.tslApplicationContext = tslApplicationContext;
		this.callback = callback;
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		
		if (isCancelled()) return false;
		
		if(!tslApplicationContext.checkIfUserIsLoggedIn()) return false;
		
		try {
				boolean ok = false;
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopAllUserRatingsTask : Create all user tube ratings sender!");
					
				IBIURL uriLocal = new IBIURL("ibiurl",
						IbicoopDataConstants.USER_ALL_USER_RATING,
						IbicoopDataConstants.TERMINAL_ALL_USER_RATING + String.valueOf(System.currentTimeMillis()),
						IbicoopDataConstants.APPLICATION, IbicoopDataConstants.ALL_USER_RATING_SERVICE,
						IbicoopDataConstants.PATH_GET_ALL_USER_RATING);
				
				IBIURL uriRemote = new IBIURL("ibiurl", IbicoopDataConstants.USER,
						IbicoopDataConstants.TERMINAL, IbicoopDataConstants.APPLICATION,
						IbicoopDataConstants.ALL_USER_RATING_SERVICE,
						IbicoopDataConstants.PATH_GET_ALL_USER_RATING);
			
				CommunicationOptions options = new CommunicationOptions();
				options.setCommunicationMode(new CommunicationMode(
						CommunicationConstants.MODE_PROXY));
			
				CommunicationManager comm = IbicoopInit.getInstance()
							.getCommunicationManager();
			
				sender = comm.createSender(uriLocal, uriRemote, options,
					null);
				
				NetworkMessage requestMessage = new NetworkMessageXml();
				
				requestMessage.setMsgOperation(IbicoopDataConstants.OPERATION_GET_ALL_USER_RATING);
				
				requestMessage.setMsgType(NetworkMessage.MSG_TYPE_REQUEST);
				
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_CITY_TYPE, city);
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_USER_NAME, tslApplicationContext.currentSHA1EmailAddress());
				
				ResponseMessage responseMessage = new ResponseMessage();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopAllUserRatingsTask : Going to send all user ratings request!");
				
				try {
					
					ok = responseMessage.decode(sender
							.sendRequestResponse(requestMessage.encode()));
					
					if (ok) userRatingsMessage = responseMessage.getPayload(IbicoopDataConstants.ALL_USER_RATING_MESSAGE);
					
				} catch (Exception e) {
					if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
				}
				
				sender.stop();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopAllUserRatingsTask : Stop all user ratings sender!");
				
				return ok;
			} catch(Exception e) {
				if (sender != null) sender.stop();
				if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
			}
			
			return false;
	}

	
	@Override
	protected void onPostExecute(Boolean result) {
		
		if (result) {
			if (callback != null) {
				TubeRatingsCollection ratingsCollection = processUserTubeRatings();
				if (ratingsCollection != null) callback.tubeRatingsCollectionInfo(ratingsCollection);
				else callback.tubeRatingFailed();
			}
		} else {
			if (callback != null) callback.tubeRatingFailed();
		}
	}
	
	protected TubeRatingsCollection processUserTubeRatings() {
		
		TubeRatingsCollection tubeRatingsCollection = null;
		
		if ((userRatingsMessage == null) || (userRatingsMessage == "null")) return tubeRatingsCollection;
			
		Gson gson = new Gson();
			
		tubeRatingsCollection = gson.fromJson(userRatingsMessage, TubeRatingsCollection.class);
		
		if (tubeRatingsCollection == null) return tubeRatingsCollection;
		
		//FIXME tubeRatingsCollection can be null, should test of userRating message equal to string message null
		TubeRating[] tubeRatings = tubeRatingsCollection.getTubeRatings();
		for (int i = 0; i < tubeRatings.length; i++) {
			TubeRating tubeRating = tubeRatings[i];
			//tslApplicationContext.submitRatingForLine(tubeRating.getTube().getId(), (float) tubeRating.getRating().getRatingLevel().getRating());			
		}
			
		return tubeRatingsCollection;
	}

}

