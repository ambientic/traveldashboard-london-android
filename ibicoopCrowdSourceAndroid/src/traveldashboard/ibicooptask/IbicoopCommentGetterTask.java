package traveldashboard.ibicooptask;

import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationManager;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiSender;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;


import com.google.gson.Gson;

import traveldashboard.data.comment.TubeStationCommentsCollection;
import traveldashboard.data.IbicoopDataConstants;
import android.content.Context;
import android.os.AsyncTask;

public class IbicoopCommentGetterTask extends AsyncTask<Void, Void, Boolean> {

	private Context context;
	
	private IbicoopCommentCallback callback;
	
	private String city;
	private String stopId;
	private String routeId;
	private int maxComments;
	private String commentMessage;
	
	private IbiSender sender;
	
	public IbicoopCommentGetterTask(Context context, String city, String stopId, String routeId, int maxComments, IbicoopCommentCallback callback) {
		this.context = context;
		this.city = city;
		this.stopId = stopId;
		this.routeId = routeId;
		this.maxComments = maxComments;
		this.callback = callback;
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		
		if (isCancelled()) return false;

		try {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopCommentGetterTask : Create sender!");
			
				IBIURL uriLocal = new IBIURL("ibiurl",
						IbicoopDataConstants.USER_TUBE_COMMENT,
						IbicoopDataConstants.TERMINAL_TUBE_COMMENT + String.valueOf(System.currentTimeMillis()),
						IbicoopDataConstants.APPLICATION, IbicoopDataConstants.TUBE_COMMENT_SERVICE,
						IbicoopDataConstants.PATH_GET_TUBE_COMMENT);
				
				IBIURL uriRemote = new IBIURL("ibiurl", IbicoopDataConstants.USER,
						IbicoopDataConstants.TERMINAL, IbicoopDataConstants.APPLICATION,
						IbicoopDataConstants.TUBE_COMMENT_SERVICE,
						IbicoopDataConstants.PATH_GET_TUBE_COMMENT);
		
				CommunicationOptions options = new CommunicationOptions();
				options.setCommunicationMode(new CommunicationMode(
						CommunicationConstants.MODE_PROXY));
		
				CommunicationManager comm = IbicoopInit.getInstance()
						.getCommunicationManager();
		
				sender = comm.createSender(uriLocal, uriRemote, options,
						null);
				
				NetworkMessage requestMessage = new NetworkMessageXml();
				requestMessage.setMsgOperation(IbicoopDataConstants.OPERATION_GET_TUBE_COMMENT);
				requestMessage.setMsgType(NetworkMessage.MSG_TYPE_REQUEST);
				
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_CITY_TYPE, city);
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_STOP_ID, stopId);
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_ROUTE_ID, routeId);
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_MAX_VALUES, String.valueOf(maxComments));
		
				ResponseMessage responseMessage = new ResponseMessage();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopCommentGetterTask : Going to send request!");
				
				boolean ok = responseMessage.decode(sender
						.sendRequestResponse(requestMessage.encode()));
				sender.stop();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopCommentGetterTask : Stop sender!");
				
				if (!ok) return false;

				commentMessage = responseMessage.getPayload(IbicoopDataConstants.TUBE_COMMENT_MESSAGE);
				
				return true;
			
			} catch(Exception e) {
				if (sender != null) sender.stop();
				if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
			}
			
			return false;
			
	}

	@Override
	protected void onPostExecute(Boolean result) {
		
		if (result) {
			if (callback != null) { 
				TubeStationCommentsCollection stationCommentsCollection = getTubeStationCommentsCollection(); 
				if (stationCommentsCollection != null) callback.tubeStationCommentsCollection(stationCommentsCollection);
				else callback.commentFailed();
			}
		} else {
			if (callback != null) callback.commentFailed();
		}
	}
	
	protected TubeStationCommentsCollection getTubeStationCommentsCollection() {
		TubeStationCommentsCollection stationCommentsCollection = null;
		
		if (commentMessage != null) {
			Gson gson = new Gson();
			stationCommentsCollection = gson.fromJson(commentMessage, TubeStationCommentsCollection.class);
		}
		
		return stationCommentsCollection;
	}

}