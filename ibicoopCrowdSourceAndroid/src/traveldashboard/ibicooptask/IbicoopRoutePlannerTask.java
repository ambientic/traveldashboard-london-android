package traveldashboard.ibicooptask;

import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationManager;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiSender;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;

import traveldashboard.routeplanner.ItdItinerary;
import traveldashboard.data.IbicoopDataConstants;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

public class IbicoopRoutePlannerTask extends AsyncTask<Void, Void, Boolean> {

	private Context context;
	private IbicoopRoutePlannerCallback callback;

	private String city;
	private String depStopName;
	private String arrStopName;
	private String depDate;
	private String depTime;
	private int[] motTypeIncluded;
	
	private String routePlannerMessage;
	
	private IbiSender sender;
	
	public IbicoopRoutePlannerTask(
			Context context,
			String city,
			String depStopName, 
			String arrStopName, 
			String depDate,
			String depTime,
			int[] motTypeIncluded,
			IbicoopRoutePlannerCallback callback) {

		this.context = context;
		this.city = city;
		this.depStopName = depStopName;
		this.arrStopName = arrStopName;
		this.depDate = depDate;
		this.depTime = depTime;
		this.motTypeIncluded = motTypeIncluded;
		this.callback = callback;
	}
	
	@Override
	protected Boolean doInBackground(Void... params) {
		
		if (isCancelled()) return false;
		
		try {
		
				if (!IbicoopInit.getInstance().isStarted()) {
					System.err.println("Ibicoop not started!");
					return false;
				}
			
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopRoutePlannerTask : Create sender!");
			
				IBIURL uriLocal = new IBIURL("ibiurl",
						IbicoopDataConstants.USER_ROUTE_PLANNER,
						IbicoopDataConstants.TERMINAL_ROUTE_PLANNER + String.valueOf(System.currentTimeMillis()),
						IbicoopDataConstants.APPLICATION, IbicoopDataConstants.ROUTE_PLANNER_SERVICE,
						IbicoopDataConstants.PATH_GET_ROUTE_PLANNER);
				
				IBIURL uriRemote = new IBIURL("ibiurl", IbicoopDataConstants.USER,
						IbicoopDataConstants.TERMINAL, IbicoopDataConstants.APPLICATION,
						IbicoopDataConstants.ROUTE_PLANNER_SERVICE,
						IbicoopDataConstants.PATH_GET_ROUTE_PLANNER);
		
				CommunicationOptions options = new CommunicationOptions();
				options.setCommunicationMode(new CommunicationMode(
						CommunicationConstants.MODE_PROXY));
		
				CommunicationManager comm = IbicoopInit.getInstance()
						.getCommunicationManager();
		
				sender = comm.createSender(uriLocal, uriRemote, options,
						null);
				
				NetworkMessage requestMessage = new NetworkMessageXml();
				requestMessage.setMsgOperation(IbicoopDataConstants.OPERATION_GET_ROUTE_PLANNER);
				requestMessage.setMsgType(NetworkMessage.MSG_TYPE_REQUEST);
				
				requestMessage.addPayload(IbicoopDataConstants.ROUTE_PLANNER_CITY, city);
				requestMessage.addPayload(IbicoopDataConstants.DEPART_STOP_NAME, depStopName);
				requestMessage.addPayload(IbicoopDataConstants.ARRIVAL_STOP_NAME, arrStopName);
				requestMessage.addPayload(IbicoopDataConstants.DEPART_DATE, depDate);
				requestMessage.addPayload(IbicoopDataConstants.DEPART_TIME, depTime);
				requestMessage.addPayload(IbicoopDataConstants.TOTAL_MOT_TYPE_INCLUDED, String.valueOf(motTypeIncluded.length));
				
				for (int j = 0; j < motTypeIncluded.length; j++) {
					requestMessage.addPayload(IbicoopDataConstants.MOT_TYPE_INCLUDED_ + j, String.valueOf(motTypeIncluded[j]));
				}
		
				ResponseMessage responseMessage = new ResponseMessage();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopRoutePlannerTask : Going to send request!");
					
				boolean ok = responseMessage.decode(sender
						.sendRequestResponse(requestMessage.encode()));
				sender.stop();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopRoutePlannerTask : Stop sender!");
				
				if (!ok) return false;

				routePlannerMessage = responseMessage.getPayload(IbicoopDataConstants.ROUTE_PLANNER_MESSAGE);
				
				return true;
			
			} catch(Exception e) {
				if (sender != null) sender.stop();
				if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
			}
			
			return false;
			
	}

	
	@Override
	protected void onPostExecute(Boolean result) {
		
		if (result) {
			if (callback != null) { 
				
				ItdItinerary itdItinerary = getItdItinerary();
				
				if (itdItinerary != null) callback.getItinerarySucess(itdItinerary);
				else callback.getItineraryFailed();
				
			}
		} else {
			if (callback != null) callback.getItineraryFailed();
		}
	}
	
	
	protected ItdItinerary getItdItinerary() {
		
		ItdItinerary itdItinerary = null;
		
		try {
			
			if (routePlannerMessage == null) return itdItinerary;
			
			Gson gson = new Gson();
			itdItinerary = gson.fromJson(routePlannerMessage, ItdItinerary.class);
			
		} catch (Exception e) {
			if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
		}
		
		return itdItinerary;
	}

}

