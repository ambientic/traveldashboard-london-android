package traveldashboard.ibicooptask;

import java.io.InputStream;

import traveldashboard.data.LondonBikeDbManager;
import traveldashboard.data.IbicoopDataConstants;
import android.os.AsyncTask;

public class IbicoopAddAllLondonBikeStopsInDbTask extends AsyncTask<Void, Void, Boolean>{

	//Variables
	private LondonBikeDbManager dbManager;
	private InputStream csvStationsInputStream;
	private AddAllLondonBikeStopsInDbTaskCallback callback;
	
	public interface AddAllLondonBikeStopsInDbTaskCallback {
		public void addAllStopsInDbOk();
		public void addAllStopsInDbFailed();	
	}
	
	public IbicoopAddAllLondonBikeStopsInDbTask(
			LondonBikeDbManager receivedDbManager, 
			InputStream receivedCsvStationInputStream,
			AddAllLondonBikeStopsInDbTaskCallback receivedCallback
			) {
		dbManager = receivedDbManager;
		csvStationsInputStream = receivedCsvStationInputStream;
		callback = receivedCallback;
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		
		if (isCancelled()) return false;
		
		boolean success = false;
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Add all london bike stops in db starting...");
		
		try {
			
			int tableCount = dbManager.getStopInfoDbCount();
			
			if (tableCount > 0) {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Stops are already in database! count = " + tableCount + "! return!");
				return true;
			}
						
			dbManager.insertStopIntoDbByInputStreams(csvStationsInputStream);

			
			try {
				csvStationsInputStream.close();
			} catch (Exception exception) {
				if (IbicoopDataConstants.DEBUG_MODE) System.err.println("Close csv input stream exception : " + exception.getMessage());
			}
			
			success = true;
			
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		
		return success;
	}
	
	@Override
	protected void onPostExecute(Boolean success) {
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Success = " + success);
		
		if (callback != null) {
			if (success) callback.addAllStopsInDbOk();
			else callback.addAllStopsInDbFailed();
		}
	}

}


