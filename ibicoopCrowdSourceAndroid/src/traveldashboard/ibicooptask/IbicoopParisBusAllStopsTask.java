package traveldashboard.ibicooptask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationManager;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiSender;
import org.ibicoop.exchange.ExchangeReceiver;
import org.ibicoop.exchange.ExchangeReceiverListener;
import org.ibicoop.exchange.ExchangeSession;
import org.ibicoop.filemanager.FileManager;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;
import org.ibicoop.utils.StringUtils;

import traveldashboard.data.ParisStopInfo;
import traveldashboard.data.IbicoopDataConstants;
import android.content.Context;
import android.os.AsyncTask;

public class IbicoopParisBusAllStopsTask extends AsyncTask<Void, Integer, ArrayList<ParisStopInfo>> {
	private IbicoopAllStopsReadyCallback callback;
	private Context context;
	private File stopsCache;
	private String city;
	private static ArrayList<ParisStopInfo> stopsStatic; // static list is cached
	
	private IbiSender sender;
	
	//Exchange receiver
	//Ibicoop file exchange receiver
	private ExchangeReceiver exchangeReceiver;
	private String exchangeReceiverIbiUrl;
	private boolean exchangeIsFinish;
	private byte[] busStopBytes;
	private ArrayList<ParisStopInfo> tmpLst;

	public IbicoopParisBusAllStopsTask(Context context, String city, IbicoopAllStopsReadyCallback callback) {
		this.callback = callback;
		this.context = context;
		this.city = city;
		File cacheDirectory = this.context.getCacheDir();
		stopsCache = new File(cacheDirectory,
		                IbicoopDataConstants.BUS_STOPS_CACHE_FILE_NAME);
	}
	
	@Override
	protected ArrayList<ParisStopInfo> doInBackground(Void... params) {
		
		if (stopsStatic != null) {
		        return stopsStatic;
		}
		
		if (isCancelled()) return null;
		
		
		byte[] responseData = null;
		
		try {
		        if (stopsCache.exists()) {
		                // file exists, so we do not need network
		                InputStream is = new FileInputStream(stopsCache);
		                ByteArrayOutputStream bos = new ByteArrayOutputStream();
		                byte[] b = new byte[1024];
		                int bytesRead;
		                while ((bytesRead = is.read(b)) != -1) {
		                        bos.write(b, 0, bytesRead);
		                }
		                byte[] bytes = bos.toByteArray();
		                is.close();
		                bos.close();
		                return parseMessage(bytes);
		
		        } else {
		        	
					if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Creating all paris bus stops file exchange directory");			
					//Create ibicoop file manager
					FileManager fileManager = IbicoopInit.getInstance().getFileManager();
					
					//Create file exchange directory and file
					fileManager.createDirectoryUserContent(IbicoopDataConstants.EXCHANGE_LOCATION);
					fileManager.createDirectoryUserContent(IbicoopDataConstants.EXCHANGE_COMMON_LOCATION);
					//fileManager.createFileUserContent(IbicoopDataConstants.EXCHANGE_COMMON_FILE_NAME);			
					
					if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopBusStopTask : Create receiver!");			
					
					//Create and start ExchangeReceiver
					//startExchangeReceiver();	
					
					//Create ibiurl
					IBIURL uri = new IBIURL("ibiurl", 
							IbicoopDataConstants.USER,
							IbicoopDataConstants.TERMINAL, 
							IbicoopDataConstants.APPLICATION, 
							IbicoopDataConstants.FILE_EXCHANGE_SERVICE, 
							(IbicoopDataConstants.PATH_GET_FILE_EXCHANGE  + String.valueOf(System.currentTimeMillis())));
					
					//Set the exchange receiver ibiurl to be sent by network sender
					exchangeReceiverIbiUrl = uri.toString();
					
					if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Exchange receiver url = " + exchangeReceiverIbiUrl);
					
					if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Delete exchange file: " + IbicoopDataConstants.EXCHANGE_COMMON_FILE_NAME2);
					//Create ibicoop file manager
					//FileManager fileManager = IbicoopInit.getInstance().getFileManager();
					
					if (fileManager.existsUserContent(IbicoopDataConstants.EXCHANGE_COMMON_FILE_NAME2)) {
						fileManager.deleteUserContent(IbicoopDataConstants.EXCHANGE_COMMON_FILE_NAME2);
					}
					
					//Use communication mode: socket, not proxy
					//Create exchange receiver		
					exchangeReceiver = IbicoopInit
							.getInstance()
							.getExchangeManager()
							.createExchangeReceiver(
									uri, 
									new CommunicationMode(CommunicationConstants.MODE_SOCKET), 
									IbicoopDataConstants.EXCHANGE_COMMON_LOCATION, 
									exchangeReceiverListener);
					
					//To be replaced by ExchangeReceiver: Create IbiReceiver
					//startReceiver();			
					if (exchangeReceiver == null) {
						if (IbicoopDataConstants.DEBUG_MODE) System.err.println("Create exchange receiver failed!");
					} else {
						if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Create exchange receiver ok!");
					}
			
	    			IBIURL uriLocal = new IBIURL("ibiurl",
	    					IbicoopDataConstants.USER_ALL_STOPS,
	    					IbicoopDataConstants.TERMINAL_ALL_STOPS + String.valueOf(System.currentTimeMillis()),
	    					IbicoopDataConstants.APPLICATION, IbicoopDataConstants.GET_ALL_STOPS_SERVICE,
	    					IbicoopDataConstants.PATH_GET_ALL_STOPS);
	    			IBIURL uriRemote = new IBIURL("ibiurl", IbicoopDataConstants.USER,
	    					IbicoopDataConstants.TERMINAL, IbicoopDataConstants.APPLICATION,
	    					IbicoopDataConstants.GET_ALL_STOPS_SERVICE,
	    					IbicoopDataConstants.PATH_GET_ALL_STOPS);
	    			
	    			CommunicationOptions options = new CommunicationOptions();
	    			options.setCommunicationMode(new CommunicationMode(
	    					CommunicationConstants.MODE_PROXY));
	    			
	    			options.setMaxCommTimeoutSeconds(60 * 15); //15 minutes
	    	
	    			CommunicationManager comm = IbicoopInit.getInstance()
	    					.getCommunicationManager();
	    	
	    			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopParisBusAllStopsTask: Create sender!");
	    			
	    			sender = comm.createSender(uriLocal, uriRemote, options, null);
	    			
	                NetworkMessage requestMessage = new NetworkMessageXml();
	                requestMessage.setMsgOperation(IbicoopDataConstants.OPERATION_GET_ALL_STOPS);
	                requestMessage.setMsgType(NetworkMessage.MSG_TYPE_REQUEST);
	                requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_CITY_TYPE, city);
	                //Route type 3 for bus
	                requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_ROUTE_TYPE, "3");
	                
					//Add exchange receiver ibiurl to the request network message
					requestMessage.addPayload(IbicoopDataConstants.FILE_EXCHANGE_IBIURL, exchangeReceiverIbiUrl);		
	                
		            responseData = sender.sendRequestResponse(requestMessage.encode());
		
	    			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopParisBusAllStopsTask: stop sender!");
	    			if (IbicoopDataConstants.DEBUG_MODE) System.out.println(new String(responseData));
		            
		            sender.stop();
		        }
		} catch (Exception exception) {
		        if (IbicoopDataConstants.DEBUG_MODE) System.err.println(exception.getMessage());
		}
		
		
		//Wait 15 minutes
		int timeout = 60 * 15;
		
		while ((!exchangeIsFinish) && (timeout > 0)) {
			
			try {
				Thread.sleep(1000); //Sleep 1 s
			} catch (Exception e) {
				if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
			}
			
			timeout--;
		}
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("After while loop of exchange waiting");
		
		//Terminate receiver must be done in the network thread
		exchangeReceiver.terminateReceiver();
		
        if (!exchangeIsFinish) {
        	return null;
        }
        
        if (tmpLst == null) {
            return null;
        }
        
        try {
            // store data to file for later use
            stopsCache.createNewFile();
            OutputStream os = new FileOutputStream(stopsCache);
            os.write(responseData);
            os.flush();
            os.close();
        } catch (Exception exception) {
        	if (IbicoopDataConstants.DEBUG_MODE) System.err.println(exception.getMessage());
        }

        return tmpLst;
	}
	
	@SuppressWarnings("unchecked")
	private ArrayList<ParisStopInfo> parseMessage(byte[] data) {
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Parse bus stops message");
		
		// Parsing the response
		NetworkMessage responseMessage = new NetworkMessageXml();
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Going to decode bus stops data");
		
		boolean result = responseMessage.decode(data);
	
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Decode bus stops data result = " + result);
		
		if (!result) {
			return null;
		}
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Going to get stop id vector");
		
		Vector<byte[]> stopIdVector = responseMessage.getKeyVector();
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Got stop id vector");
		
		HashMap<String, String> stopInfoMap = new HashMap<String, String>();
		List<String> stopIdList = new ArrayList<String>();
		int stopIdVectorSize = stopIdVector.size();
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Stop id vector size = " + stopIdVectorSize);
		
		for (int i = 0; i < stopIdVectorSize; i++) {
			
				if ((i % 500) == 0) if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Index of parsing = " + i);
			
		        try {
	                String stopIdKey = new String(stopIdVector.elementAt(i),
	                                StringUtils.UTF8);
	                String stopIdData = responseMessage.getPayload(i);
	                stopInfoMap.put(stopIdKey, stopIdData);
	
	                if (stopIdData.equals("ID")) {
	                        stopIdList.add(stopIdKey);
	                }
		        } catch (Exception e) {
		                if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
		        }
		}
		
		ArrayList<ParisStopInfo> stops = new ArrayList<ParisStopInfo>();
		// Create StopInfo object
		for (String stopId : stopIdList) {
		        String keyWordName = stopId + "_Name";
		        String keyWordLat = stopId + "_Lat";
		        String keyWordLon = stopId + "_Lon";
		        String keyWordRouteId = stopId + "_RouteId";
		        String keyWordRouteName = stopId + "_RouteName";
		        String keyWordDirection = stopId + "_Direction";
		
		        String name = stopInfoMap.get(keyWordName);
		        String routeId = stopInfoMap.get(keyWordRouteId);
		        String routeName = stopInfoMap.get(keyWordRouteName);
		        double latitude = Double.parseDouble(stopInfoMap.get(keyWordLat));
		        double longitude = Double.parseDouble(stopInfoMap.get(keyWordLon));
		        int direction = Integer.parseInt(stopInfoMap.get(keyWordDirection));
		        stops.add(new ParisStopInfo(stopId, routeId, routeName, name, latitude,
		                        longitude, direction));
		}
		
		// Now sorting the stops in alphabetical order
		Collections.sort(stops, new Comparator<ParisStopInfo>() {
		        @Override
		        public int compare(ParisStopInfo o1, ParisStopInfo o2) {
		                return o1.getStopName().compareTo(o2.getStopName());
		        }
		});
		
		return stops;
	}
	
	@Override
	protected void onPostExecute(ArrayList<ParisStopInfo> result) {
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopParisBusAllStopsTask: end!");
		
		if (isCancelled() || callback == null)
		        return;
		
		if (result != null) {
		        callback.allStopsReady(result);
		        stopsStatic = result;
		} else {
		        callback.allStopsFailed();
		}
	}
	
	//Exchange receiver listener to check file transfer status
		private ExchangeReceiverListener exchangeReceiverListener = new ExchangeReceiverListener() {
				
			@Override
			public void exchangeRcvPaused(ExchangeSession exchangeSession) {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("exchangeRcvPaused");
			}
			
			@Override
			public void exchangeRcvInterrupted(ExchangeSession exchangeSession) {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("exchangeRcvInterrupted");
				exchangeIsFinish = true;
			}
			
			@Override
			public void exchangeRcvCompleted(ExchangeSession exchangeSession) {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("exchangeRcvCompleted : " + exchangeSession.getLastBlock() + "/" + exchangeSession.getTotalNoOfBlocks());
				//printExchangeSession(exchangeSession);
				//Process the file receive
				//Create ibicoop file manager
				FileManager fileManager = IbicoopInit.getInstance().getFileManager();
				
				try {
					//read file
					if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Reading user content");
					busStopBytes = fileManager.readUserContent(IbicoopDataConstants.EXCHANGE_COMMON_FILE_NAME2);
			        if (busStopBytes != null) {
			        	tmpLst = parseMessage(busStopBytes);
			        }
				} catch (Exception exception) {
					exception.printStackTrace();
				}
				
				exchangeIsFinish = true;
			}
			
			@Override
			public void exchangeRcvAborted(ExchangeSession exchangeSession) {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("exchangeRcvAborted");
				exchangeIsFinish = true;
			}
			
			@Override
			public boolean exchangeDoAccept(ExchangeSession exchangeSession) {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("exchangeDoAccept");
				return true;
			}
			
			@Override
			public void exchangeBlockReceived(ExchangeSession exchangeSession) {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("exchangeBlockReceived: " + exchangeSession.getLastBlock() + "/" + exchangeSession.getTotalNoOfBlocks());
			}
		};
		
		//Stop exhange receiver
		public void stopExchangeReceiver() {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Stop exchange receiver");
			if (exchangeReceiver != null) exchangeReceiver.terminateReceiver();
		}	
}