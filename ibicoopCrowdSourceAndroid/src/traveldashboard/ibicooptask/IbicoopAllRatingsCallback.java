package traveldashboard.ibicooptask;

import traveldashboard.data.rating.TubeRatingsCollection;

public interface IbicoopAllRatingsCallback {
	public void tubeRatingFailed();
	public void tubeRatingsCollectionInfo(TubeRatingsCollection info);
}
