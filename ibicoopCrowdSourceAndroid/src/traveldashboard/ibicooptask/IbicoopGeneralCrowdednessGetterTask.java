package traveldashboard.ibicooptask;


import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationManager;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiSender;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;

import com.google.gson.Gson;

import traveldashboard.data.crowd.TubeStationCrowd;
import traveldashboard.data.crowd.TubeStationCrowdsCollection;
import traveldashboard.data.IbicoopDataConstants;

import android.content.Context;
import android.os.AsyncTask;

public class IbicoopGeneralCrowdednessGetterTask extends AsyncTask<Void, Void, Boolean>{

	private Context context;
	
	private IbicoopGeneralCrowdednessCallback callback;
	
	private long interval;
	private String city;
	private String stopId;
	private String routeId;
	
	private String crowdednessMessage;
	
	private IbiSender sender;


	public IbicoopGeneralCrowdednessGetterTask(Context context, String city, String stopId, String routeId, long interval, IbicoopGeneralCrowdednessCallback callback) {
		this.context = context;
		this.city = city;
		this.stopId = stopId;
		this.routeId = routeId;
		this.interval = interval;
		this.callback = callback;
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		
		if (isCancelled()) return false;
		
		try {
			IBIURL uriLocal = new IBIURL("ibiurl",
					IbicoopDataConstants.USER_GENERAL_CROWDEDNESS,
					IbicoopDataConstants.TERMINAL_GENERAL_CROWDEDNESS + String.valueOf(System.currentTimeMillis()),
					IbicoopDataConstants.APPLICATION, IbicoopDataConstants.GENERAL_CROWDEDNESS_SERVICE,
					IbicoopDataConstants.PATH_GET_GENERAL_CROWDEDNESS);
			
			IBIURL uriRemote = new IBIURL("ibiurl", IbicoopDataConstants.USER,
					IbicoopDataConstants.TERMINAL, IbicoopDataConstants.APPLICATION,
					IbicoopDataConstants.GENERAL_CROWDEDNESS_SERVICE,
					IbicoopDataConstants.PATH_GET_GENERAL_CROWDEDNESS);

			CommunicationOptions options = new CommunicationOptions();
			options.setCommunicationMode(new CommunicationMode(
					CommunicationConstants.MODE_PROXY));

			CommunicationManager comm = IbicoopInit.getInstance()
					.getCommunicationManager();

			sender = comm.createSender(uriLocal, uriRemote, options,
					null);

			NetworkMessage requestMessage = new NetworkMessageXml();
			requestMessage.setMsgOperation(IbicoopDataConstants.OPERATION_GET_METRO_GENERAL_CROWD);
			
			requestMessage.setMsgType(NetworkMessage.MSG_TYPE_REQUEST);
			
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_CITY_TYPE, city);				
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_STOP_ID, stopId);	
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_ROUTE_ID, routeId);
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_TIMESTAMP, String.valueOf(System.currentTimeMillis()));
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_INTERVAL, String.valueOf(interval));
			
			ResponseMessage responseMessage = new ResponseMessage();
			boolean ok = responseMessage.decode(sender
					.sendRequestResponse(requestMessage.encode()));
			sender.stop();

			if (ok) {
				crowdednessMessage = responseMessage.getPayload(IbicoopDataConstants.METRO_GENERAL_CROWD_MESSAGE);
			} else {
				return false;
			}

			return true;
		} catch(Exception e) {
			if (sender != null) sender.stop();
			if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
		}
		return false;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		
		if (result) {
			if (callback != null) { 
				TubeStationCrowdsCollection stationCrowdsCollection = getTubeStationCrowdsCollection();
				if (stationCrowdsCollection != null) callback.tubeStationCrowdInfo(stationCrowdsCollection);
				else callback.crowdednessFailed();
			}
		} else {
			if (callback != null) callback.crowdednessFailed();
		}
	}

	protected TubeStationCrowdsCollection getTubeStationCrowdsCollection() {
		
		TubeStationCrowdsCollection stationCrowdsCollection = null;
		
		if (crowdednessMessage != null) {
			Gson gson = new Gson();
			stationCrowdsCollection = gson.fromJson(crowdednessMessage, TubeStationCrowdsCollection.class);
		}
		
		return stationCrowdsCollection;
	}
}