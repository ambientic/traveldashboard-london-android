package traveldashboard.ibicooptask;


import traveldashboard.data.comment.TubeStationCommentsCollection;

public interface IbicoopTwitterCommentCallback {
	public void twitterCommentFailed();
	public void twitterCommentsCollection(TubeStationCommentsCollection info);
}
