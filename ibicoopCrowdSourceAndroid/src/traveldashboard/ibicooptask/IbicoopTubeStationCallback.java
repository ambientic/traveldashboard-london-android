package traveldashboard.ibicooptask;

import java.util.List;

import traveldashboard.data.TubeStation;
import traveldashboard.data.TubeStationsCollection;

public interface IbicoopTubeStationCallback {
	public void tubeStationFailed();
	public void tubeStationsCollectionInfo(TubeStationsCollection info);
}