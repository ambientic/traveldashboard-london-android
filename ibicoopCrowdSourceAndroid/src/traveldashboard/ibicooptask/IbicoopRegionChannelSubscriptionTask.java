package traveldashboard.ibicooptask;

import java.util.Locale;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import org.ibicoop.broker.common.BrokerConstants;
import org.ibicoop.broker.common.BrokerManager;
import org.ibicoop.broker.common.IbiTopic;
import org.ibicoop.broker.common.IbiTopicFilter;
import org.ibicoop.init.IbicoopInit;

import traveldashboard.compat.AsyncTaskCompat;
import traveldashboard.data.IbicoopDataConstants;

import android.app.IntentService;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

public class IbicoopRegionChannelSubscriptionTask extends AsyncTask<Void, Void, Boolean> {

	private static double LATITUDE_REGION_LENGTH = 0.02;
	private static double LONGITUDE_REGION_LENGTH = 0.04;
	
	private static AtomicReference<String> currentChannelName = new AtomicReference<String>();
	private static AtomicBoolean isSubscribed = new AtomicBoolean(false);
	
	private double mLatitude, mLongitude;
	private Context mContext;
	private IbicoopStartStopTask ibiStart;
	private IbicoopBrokerStartStopTask ibiBroker;
	private boolean mUnsubscribe;
	private IbiTopic mUnsubscribeTopic;
	private boolean mRegionFilter;
	
	private TSLContext tslApplicationContext;
	private Class<? extends IntentService> stationVoteIntentService;
	private Class<? extends IntentService> requestIntentService;
	
	public IbicoopRegionChannelSubscriptionTask(Context ctx, double latitude, double longitude, boolean isDebug,
			TSLContext tslApplicationContext, Class<? extends IntentService> stationVoteIntentService,
			Class<? extends IntentService> requestIntentService) {
		mLatitude = latitude;
		mLongitude = longitude;
		mContext = ctx;
		
		//FIXME: get region filter
		mRegionFilter = !isDebug;
		
		this.tslApplicationContext = tslApplicationContext;
		this.stationVoteIntentService = stationVoteIntentService;
		this.requestIntentService = requestIntentService;
	}
	
	public IbicoopRegionChannelSubscriptionTask(Context ctx, IbiTopic topic,
		boolean unSubscribe, boolean regionFilter) {
		mContext = ctx;
		mUnsubscribe = unSubscribe;
		mUnsubscribeTopic = topic;
		mRegionFilter = regionFilter;
	}
	
	@Override
	protected Boolean doInBackground(Void... params) {
	if (mUnsubscribe) {
		if (null == mUnsubscribeTopic)
			return unsubscribeAll();
		else
			return unsubscribe(mUnsubscribeTopic);
	} else {
		return subscribe();
	}
	}
	
	private boolean subscribe() {
		
		if(isCancelled()) return false;

		if (!IbicoopInit.getInstance().isStarted()) return false;	
		
		String newChannelName;
		if (mRegionFilter) {
			// we subscribe to the REGIONAL channel:
			newChannelName = convertLocationToChannelName(mLatitude, mLongitude);
		} else {
			// we subscribe to a GLOBAL channel:
			newChannelName = IbicoopDataConstants.GLOBAL_ON_DEMAND_CHANNEL;
		}
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("new channel name = " + newChannelName);
		
		
		// we are in the same region, nothing to do
		if (currentChannelName.get() != null
				&& currentChannelName.get().equals(newChannelName)
				&& (isSubscribed.get() == true)
				) {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Region did not change: "
					+ currentChannelName.get());
			return true;
		}
		
		// the region has changed, subscribe to a new channel
		currentChannelName.set(newChannelName);
		
		/*
		// wait for iBICOOP to start
		try {
			if (!waitForIbicoop()) {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("iBICOOP or broker did not start");
				return false;
			}
		} catch (ExecutionException e) {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("" + e.getMessage());
			return false;
		}*/
		
		// Unsubscribe from older topics
		unsubscribeAll();
		
		BrokerManager brokerManager = IbicoopInit.getInstance()
				.getBrokerManager(IbicoopDataConstants.BROKER_NAME);
		
		// Obtain the topic of this region
		IbiTopicFilter topicFilter = brokerManager.createIbiTopicFilter();
		topicFilter.setName(currentChannelName.get());
		
		IbiTopic[] currentTopics = brokerManager.getTopics(topicFilter);
		// There is no topic for this region, creating a new one
		if (currentTopics == null || currentTopics.length == 0) {
			currentTopics = new IbiTopic[1];
			currentTopics[0] = brokerManager.createIbiTopic(
					currentChannelName.get(), currentChannelName.get());
			if (currentTopics[0].registerTopic() != BrokerConstants.OK) {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Could not register topic "
						+ currentTopics[0].getName());			
				currentChannelName.set(null);
				return false;
			} else {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Register topic "
						+ currentTopics[0].getName() + " ok");			
			}
		}
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Region changed: " + currentChannelName.get());
		// Subscribe to the new topics
		for (int i = 0; i < currentTopics.length; i++) {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Subscribing to topic: "
					+ currentTopics[i].getName());
			if (currentTopics[i].subscribe() != BrokerConstants.OK){
				// there should be only one topic, and subscription shound never fail
				currentChannelName.set(null);
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Subscribing to topic: Could not subscribe to topic "
						+ currentTopics[i].getName());
			} else {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Subscribing to topic: can subscribe to topic "
						+ currentTopics[i].getName());		
			}
		}
	
		// sharing app-wide topic list
		for (IbiTopic ibiTopic : currentTopics) {
			tslApplicationContext.getCurrentChannelTopicList().add(ibiTopic);
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Topic = " + ibiTopic.getName());
		}
		
		isSubscribed.set(true);
		
		return true;
		}
		
		private boolean unsubscribeAll() {
			

			if (!IbicoopInit.getInstance().isStarted()) return false;
			
			boolean success = true;
				
			for (IbiTopic topic : tslApplicationContext.getCurrentChannelTopicList()) {
				boolean successTempo = unsubscribe(topic);
				success &= successTempo;
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Unsubscribe topic = " + topic.getName() + ", result = " + successTempo);
				}
			
			if (success) isSubscribed.set(false);
			
			return success;
	}
	
	private boolean unsubscribe(IbiTopic topic) {
		
		if (!IbicoopInit.getInstance().isStarted()) return false;
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Unsubscribing from topic: " + topic.getName());
		
		tslApplicationContext.getCurrentChannelTopicList().remove(topic);
		
		if (currentChannelName.get() != null
				&& currentChannelName.get().equals(topic.getName())) {
			currentChannelName.set(null);
		}
		
		boolean success = (topic.unSubscribe() == BrokerConstants.OK);
		
		if (success) isSubscribed.set(false);
		
		return success;
	}
	
	@Override
	protected void onPostExecute(Boolean result) {
	super.onPostExecute(result);
		if(!result) {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Un-/subscribe FAIL");
			if (IbicoopDataConstants.DEBUG_MODE) Toast.makeText(mContext, "Un-/subscribe FAIL", Toast.LENGTH_SHORT).show();
		} else {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Un-/subscribe success");
			if (IbicoopDataConstants.DEBUG_MODE) Toast.makeText(mContext, "Un-/subscribe success", Toast.LENGTH_SHORT).show();		
		}
	}
	/**
	* Utility method which returns the region channel name for a given location
	* 
	* @param lat
	* @param lon
	* @return
	*/
	public static String convertLocationToChannelName(double lat, double lon) {
	double coordLon = lon - (lon % LONGITUDE_REGION_LENGTH);
	double coordLat = lat - (lat % LATITUDE_REGION_LENGTH);
	return String.format(Locale.ENGLISH, "RegCh_%.4f_%.4f", coordLat,
			coordLon);
	}
	
	private boolean waitForIbicoop() throws ExecutionException {
	ibiStart = new IbicoopStartStopTask(mContext, false, null, tslApplicationContext, stationVoteIntentService, requestIntentService);
	AsyncTaskCompat.executeParallel(ibiStart);
	while (true) {
		try { // the wait might get interrupted, that's why we LOOP
			if (!ibiStart.get()) {
				// iBICOOP did not start
				return false;
			}
			ibiBroker = new IbicoopBrokerStartStopTask(mContext, false, tslApplicationContext, stationVoteIntentService, requestIntentService);
			AsyncTaskCompat.executeParallel(ibiBroker);
			break;
		} catch (InterruptedException e) {
			continue;
		} catch (CancellationException e) {
			// the task was canceled
			return false;
		}
	}
	
	while (true) {
		if (isCancelled())
			return false;
		try { // the wait might get interrupted, that's why we LOOP
			if (!ibiBroker.get()) {
				// broker did not start
				return false;
			}
			return true;
		} catch (InterruptedException e) {
			continue;
		} catch (CancellationException e) {
			// the task was canceled
			return false;
		}
	}
	}

}
