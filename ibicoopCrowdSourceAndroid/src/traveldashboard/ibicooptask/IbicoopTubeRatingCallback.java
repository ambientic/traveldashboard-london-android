package traveldashboard.ibicooptask;

import traveldashboard.data.rating.TubeRating;

public interface IbicoopTubeRatingCallback {
	public void tubeRatingFailed();
	public void tubeRatingInfo(TubeRating tubeRating);
}
