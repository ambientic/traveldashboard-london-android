package traveldashboard.ibicooptask;

import java.util.ArrayList;
import java.util.List;


import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationManager;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiSender;
import org.ibicoop.exchange.ExchangeReceiver;
import org.ibicoop.exchange.ExchangeReceiverListener;
import org.ibicoop.exchange.ExchangeSession;
import org.ibicoop.filemanager.FileManager;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;


import com.google.gson.Gson;

import traveldashboard.data.BusStation;
import traveldashboard.data.BusStationsCollection;
import traveldashboard.data.IbicoopDataConstants;
import android.content.Context;
import android.os.AsyncTask;

public class IbicoopBusStopTask extends AsyncTask<Void, Void, Boolean> {

	private Context context;
	
	private IbicoopBusStopCallback callback;
	
	//Ibicoop network message sender and receiver
	private IbiSender sender;

	//Ibicoop file exchange receiver
	private ExchangeReceiver exchangeReceiver;
	private String exchangeReceiverIbiUrl;
	private boolean exchangeIsFinish;
	
	private String latitude;
	private String longitude;
	private String radius;
	private String busStopMessage;
	private String busStopReplyMessage;
	private long initTime;
	private long finalTime;
			
	public IbicoopBusStopTask(Context context, String latitude, String longitude, String radius, IbicoopBusStopCallback callback) {
		this.context = context;
		this.latitude = latitude;
		this.longitude = longitude;
		this.radius = radius;
		this.callback = callback;
		initTime = System.currentTimeMillis();
		exchangeIsFinish = false;
	}
		
	@Override
	protected Boolean doInBackground(Void... params) {
	
		if (isCancelled()) return false;
		
		try {
			
				if (!IbicoopInit.getInstance().isStarted()) {
					if (IbicoopDataConstants.DEBUG_MODE) System.err.println("Ibicoop not started!");
					return false;
				}
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Creating bus stop file exchange directory");			
				//Create ibicoop file manager
				FileManager fileManager = IbicoopInit.getInstance().getFileManager();
				
				//Create file exchange directory and file
				fileManager.createDirectoryUserContent(IbicoopDataConstants.EXCHANGE_LOCATION);
				fileManager.createDirectoryUserContent(IbicoopDataConstants.EXCHANGE_COMMON_LOCATION);
				//fileManager.createFileUserContent(IbicoopDataConstants.EXCHANGE_COMMON_FILE_NAME);			
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopBusStopTask : Create receiver!");			
				
				//Create and start ExchangeReceiver
				//startExchangeReceiver();	
				
				//Create ibiurl
				IBIURL uri = new IBIURL("ibiurl", 
						IbicoopDataConstants.USER,
						IbicoopDataConstants.TERMINAL, 
						IbicoopDataConstants.APPLICATION, 
						IbicoopDataConstants.FILE_EXCHANGE_SERVICE, 
						(IbicoopDataConstants.PATH_GET_FILE_EXCHANGE  + String.valueOf(System.currentTimeMillis())));
				
				//Set the exchange receiver ibiurl to be sent by network sender
				exchangeReceiverIbiUrl = uri.toString();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Exchange receiver url = " + exchangeReceiverIbiUrl);
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Delete exchange file: " + IbicoopDataConstants.EXCHANGE_COMMON_FILE_NAME);
				//Create ibicoop file manager
				//FileManager fileManager = IbicoopInit.getInstance().getFileManager();
				
				if (fileManager.existsUserContent(IbicoopDataConstants.EXCHANGE_COMMON_FILE_NAME)) {
					fileManager.deleteUserContent(IbicoopDataConstants.EXCHANGE_COMMON_FILE_NAME);
				}
				
				//Use communication mode: socket, not proxy
				//Create exchange receiver		
				exchangeReceiver = IbicoopInit
						.getInstance()
						.getExchangeManager()
						.createExchangeReceiver(
								uri, 
								new CommunicationMode(CommunicationConstants.MODE_SOCKET), 
								IbicoopDataConstants.EXCHANGE_COMMON_LOCATION, 
								exchangeReceiverListener);
				
				//To be replaced by ExchangeReceiver: Create IbiReceiver
				//startReceiver();			
				if (exchangeReceiver == null) {
					if (IbicoopDataConstants.DEBUG_MODE) System.err.println("Create exchange receiver failed!");
				} else {
					if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Create exchange receiver ok!");
				}
			
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopBusStopTask : Create sender!");
		
				IBIURL uriLocal = new IBIURL("ibiurl",
						IbicoopDataConstants.USER_BUS_STOP,
						IbicoopDataConstants.TERMINAL_BUS_STOP + String.valueOf(System.currentTimeMillis()),
						IbicoopDataConstants.APPLICATION, IbicoopDataConstants.BUS_STOP_SERVICE,
						IbicoopDataConstants.PATH_GET_BUS_STOP);
				IBIURL uriRemote = new IBIURL("ibiurl", IbicoopDataConstants.USER,
						IbicoopDataConstants.TERMINAL, IbicoopDataConstants.APPLICATION,
						IbicoopDataConstants.BUS_STOP_SERVICE,
						IbicoopDataConstants.PATH_GET_BUS_STOP);
		
				CommunicationOptions options = new CommunicationOptions();
				options.setCommunicationMode(new CommunicationMode(
						CommunicationConstants.MODE_PROXY));
					
				CommunicationManager comm = IbicoopInit.getInstance()
						.getCommunicationManager();
		
				sender = comm.createSender(uriLocal, uriRemote, options, null);
				
				NetworkMessage requestMessage = new NetworkMessageXml();
				requestMessage.setMsgOperation(IbicoopDataConstants.OPERATION_GET_BUS_STOP);
				requestMessage.setMsgType(NetworkMessage.MSG_TYPE_REQUEST);
				
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_STOP_LAT, latitude);
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_STOP_LON, longitude);
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_RADIUS, radius);

				//Add exchange receiver ibiurl to the request network message
				requestMessage.addPayload(IbicoopDataConstants.FILE_EXCHANGE_IBIURL, exchangeReceiverIbiUrl);				
		
				ResponseMessage responseMessage = new ResponseMessage();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopBusStopTask : Going to send request!");

				boolean ok = responseMessage.decode(sender
						.sendRequestResponse(requestMessage.encode()));
					
				sender.stop();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopBusStopTask : Stop sender!");
				
				if (!ok) return false;

				busStopReplyMessage = responseMessage.getPayload(IbicoopDataConstants.BUS_STOP_MESSAGE);

				//Network thread should be called here
				//stopReceiver();
			
			} catch(Exception e) {
				if (sender != null) sender.stop();
				if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
			}
		
		
			//Wait 5 minutes
			int timeout = 60 * 5;
			
			while ((!exchangeIsFinish) && (timeout > 0)) {
				try {
					Thread.sleep(1000); //Sleep 1 s
				} catch (Exception exception) {
					exception.printStackTrace();
				}
				timeout--;
			}
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("After while loop of exchange waiting");
			
			//Terminate receiver must be done in the network thread
			exchangeReceiver.terminateReceiver();
			
			if (exchangeIsFinish) return true;
		
			return false;	
	}

	
	@Override
	protected void onPostExecute(Boolean result) {
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Bus stop on post execute get result : " + result);
		
		if (result) {
			
			if ((busStopReplyMessage != null) && busStopReplyMessage.equals("FAILED")) {
				if (callback != null) callback.busStopFailed();
			} else {
				//BusStationsCollection busStationsCollection = getNewBusStationsCollection(stationsCollectionList);
				BusStationsCollection busStationsCollection = getBusStationsCollection(busStopMessage);
				
				if (busStationsCollection != null) {
					if (callback != null) callback.busStationsCollection(busStationsCollection);
				} else {
					if (callback != null) callback.busStopFailed();
				}
			}
			
		} else {
			if (callback != null) callback.busStopFailed();
		}
		
		finalTime =System.currentTimeMillis();
		if (IbicoopDataConstants.DEBUG_MODE) System.err.println("#############Execution time############");
		long duration = finalTime - initTime;
		if (IbicoopDataConstants.DEBUG_MODE) System.err.println("duration of bus stop task = " + duration + " ms");
		if (IbicoopDataConstants.DEBUG_MODE) System.err.println("#############End Execution time############");
	}
	
	protected BusStationsCollection getBusStationsCollection(String message) {
		
		//if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Process message = " + message);
		BusStationsCollection busStationsCollection = null;
		
		try {
			//if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Get bus stations collection : busStopMessage = " + busStopMessage);
			Gson gson = new Gson();

			if (message == null) return busStationsCollection;
			busStationsCollection = gson.fromJson(message, BusStationsCollection.class);
			
		} catch (Exception e) {
			if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
		}
		
		return busStationsCollection;
	}
	
	protected BusStationsCollection getNewBusStationsCollection(List<BusStationsCollection> stationsList) {
		BusStationsCollection busStationsCollection = null;
		List<BusStation> stations = new ArrayList<BusStation>();
		
		for (int i = 0; i < stationsList.size(); i++) {
			BusStationsCollection stationCollection = stationsList.get(i);
			BusStation[] stationsArray = stationCollection.getBusStations();
			for (BusStation station : stationsArray) {
				stations.add(station);
			}
		}
		
		BusStation[] finalStationsArray = new BusStation[stations.size()];
		
		for (int i = 0; i < finalStationsArray.length; i++) {
			finalStationsArray[i] = stations.get(i);
		}
		
		busStationsCollection = new BusStationsCollection(finalStationsArray);
		
		return busStationsCollection;
	}

	//Exchange receiver listener to check file transfer status
	private ExchangeReceiverListener exchangeReceiverListener = new ExchangeReceiverListener() {
			
		@Override
		public void exchangeRcvPaused(ExchangeSession exchangeSession) {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("exchangeRcvPaused");
		}
		
		@Override
		public void exchangeRcvInterrupted(ExchangeSession exchangeSession) {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("exchangeRcvInterrupted");
			exchangeIsFinish = true;
		}
		
		@Override
		public void exchangeRcvCompleted(ExchangeSession exchangeSession) {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("exchangeRcvCompleted");
			//printExchangeSession(exchangeSession);
			//Process the file receive
			//Create ibicoop file manager
			FileManager fileManager = IbicoopInit.getInstance().getFileManager();
			
			try {
				//read file
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Reading user content");
				busStopMessage = new String(fileManager.readUserContent(IbicoopDataConstants.EXCHANGE_COMMON_FILE_NAME));
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("busStopMessage = " + busStopMessage);
			} catch (Exception exception) {
				exception.printStackTrace();
			}
			
			exchangeIsFinish = true;
		}
		
		@Override
		public void exchangeRcvAborted(ExchangeSession exchangeSession) {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("exchangeRcvAborted");
			exchangeIsFinish = true;
		}
		
		@Override
		public boolean exchangeDoAccept(ExchangeSession exchangeSession) {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("exchangeDoAccept");
			return true;
		}
		
		@Override
		public void exchangeBlockReceived(ExchangeSession exchangeSession) {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("exchangeBlockReceived");
		}
	};
	
	//Create and start exhange receiver
	public void startExchangeReceiver() {
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Start exchange receiver");
		try {
			
			//Create ibiurl
			IBIURL uri = new IBIURL("ibiurl", 
					IbicoopDataConstants.USER,
					IbicoopDataConstants.TERMINAL, 
					IbicoopDataConstants.APPLICATION, 
					IbicoopDataConstants.FILE_EXCHANGE_SERVICE, 
					(IbicoopDataConstants.PATH_GET_FILE_EXCHANGE  + String.valueOf(System.currentTimeMillis())));
			
			//Set the exchange receiver ibiurl to be sent by network sender
			exchangeReceiverIbiUrl = uri.toString();
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Exchange receiver url = " + exchangeReceiverIbiUrl);
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Delete exchange file: " + IbicoopDataConstants.EXCHANGE_COMMON_FILE_NAME);
			//Create ibicoop file manager
			FileManager fileManager = IbicoopInit.getInstance().getFileManager();
			
			if (fileManager.existsUserContent(IbicoopDataConstants.EXCHANGE_COMMON_FILE_NAME)) {
				fileManager.deleteUserContent(IbicoopDataConstants.EXCHANGE_COMMON_FILE_NAME);
			}
			
			//Use communication mode: socket, not proxy
			//Create exchange receiver		
			exchangeReceiver = IbicoopInit
					.getInstance()
					.getExchangeManager()
					.createExchangeReceiver(
							uri, 
							new CommunicationMode(CommunicationConstants.MODE_SOCKET), 
							IbicoopDataConstants.EXCHANGE_COMMON_LOCATION, 
							exchangeReceiverListener);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}
	
	//Stop exhange receiver
	public void stopExchangeReceiver() {
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Stop exchange receiver");
		if (exchangeReceiver != null) exchangeReceiver.terminateReceiver();
	}	
}