package traveldashboard.ibicooptask;

import traveldashboard.data.info.BusRealTimeInfosCollection;

public interface IbicoopBusCountdownCallback {
	public void busCountdownFailed(String routeId);
	public void busCountdownRealTimeInfo(String routeId, BusRealTimeInfosCollection infos);
}
