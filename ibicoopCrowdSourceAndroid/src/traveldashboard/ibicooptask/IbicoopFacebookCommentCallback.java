package traveldashboard.ibicooptask;

import traveldashboard.data.comment.TubeStationCommentsCollection;

public interface IbicoopFacebookCommentCallback {
	public void tubeStationFacebookCommentsCollection(TubeStationCommentsCollection info);
	public void tubeStationFacebookCommentsCollectionFailed();
}
