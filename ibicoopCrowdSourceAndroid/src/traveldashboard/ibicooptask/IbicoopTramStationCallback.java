package traveldashboard.ibicooptask;

import traveldashboard.data.TramStationsCollection;

/**
 * Tram stations callback
 * @author khoo
 *
 */
public interface IbicoopTramStationCallback {
	public void tramStationFailed();
	public void tramStationsCollectionInfo(TramStationsCollection info);
}
