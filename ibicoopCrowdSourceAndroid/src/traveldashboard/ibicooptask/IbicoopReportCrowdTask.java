package traveldashboard.ibicooptask;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.CancellationException;

import org.ibicoop.broker.common.BrokerConstants;
import org.ibicoop.broker.common.BrokerManager;
import org.ibicoop.broker.common.IbiTopic;
import org.ibicoop.broker.common.IbiTopicEvent;
import org.ibicoop.broker.common.IbiTopicFilter;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;

import traveldashboard.data.IbicoopDataConstants;

import android.content.Context;
import android.os.AsyncTask;

/**
 * @author khoo
 *
 */
public class IbicoopReportCrowdTask extends AsyncTask<Void, Void, Boolean>{
	
	//Android activity context
	private Context context;
	
	//
	//
	//Cached topic list
	private static IbiTopic[] topicList;
	//Crowd queue
	private static Queue<byte[]> persistentCrowdEventCache;
	
	private IbicoopStartStopTask ibiStart;
	private IbicoopBrokerStartStopTask ibiBroker;
	private IbicoopReportCrowdCallback callback;
	
	public interface IbicoopReportCrowdCallback {
		public void reportCrowdSuccess();
		public void reportCrowdFail();
	}

	/**
	 * Report crowd task constructor
	 * @param context
	 * @param city
	 * @param stopId
	 * @param routeId
	 * @param crowdLevel
	 */
	public IbicoopReportCrowdTask(Context context, String city, String stopId, String routeId, 
			int crowdLevel, IbicoopReportCrowdCallback reportCrowdCallback) {
		this.context = context;
		callback = reportCrowdCallback;
		
		if (persistentCrowdEventCache == null) {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportCrowdTask : Create persistentCrowdEventCache");
			//Create persistentCrowdEventCache
			readCrowdEventCached();
		}
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportCrowdTask : Create crowd data message: " +
				"stopId = " + stopId + ", routeId = " + routeId + ", crowd level = " + crowdLevel
				+ ", persistentCrowdEventCache size = " + persistentCrowdEventCache.size());
		NetworkMessage crowdDataMessage = new NetworkMessageXml();
		crowdDataMessage.addPayload(IbicoopDataConstants.PARAM_KEY_CITY_TYPE, city);
		crowdDataMessage.addPayload(IbicoopDataConstants.PARAM_KEY_STOP_ID, stopId);
		crowdDataMessage.addPayload(IbicoopDataConstants.PARAM_KEY_ROUTE_ID, routeId);
		crowdDataMessage.addPayload(IbicoopDataConstants.PARAM_KEY_LEVEL, String.valueOf(crowdLevel));	
		persistentCrowdEventCache.add(crowdDataMessage.encode());
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportCrowdTask : Insert crowd data into persistentCrowdEventCache, " +
				" new persistentCrowdEventCache size = " + persistentCrowdEventCache.size());	
	}
	
	/*
	 * 	@Override
	protected void onPreExecute() {
		super.onPreExecute();

		if(!isCancelled()){
			// we must assure iBICOOP is running
			ibiStart = new IbicoopStartStopTask(context, false, null);
			AsyncTaskCompat.executeParallel(ibiStart);
		}
	}
	
	 */

	@Override
	protected Boolean doInBackground(Void... params) {
		
		boolean publishSuccess = false;
		
		try {
		
			if(isCancelled()) return false;
			if (!IbicoopInit.getInstance().isStarted()) return false;
			
			/*
			while (true) {
				if(isCancelled())
					return false;
				
				try { // the wait might get interrupted, that's why we LOOP
					if (!ibiStart.get()) {
						// iBICOOP did not start
						return false;
					}
					ibiBroker = new IbicoopBrokerStartStopTask(context, false);
					AsyncTaskCompat.executeParallel(ibiBroker);
					break;
				} catch (InterruptedException e) {
					continue;
				} catch (CancellationException e){
					if (IbicoopDataConstants.DEBUG_MODE) System.err.println("report crowd cancelled");
					// the task was canceled
					return false;
				}
			}

			while (true) {
				if(isCancelled())
					return false;
				
				try { // the wait might get interrupted, that's why we LOOP
					if (!ibiBroker.get()) {
						// broker did not start
						return false;
					}
					break;
				} catch (InterruptedException e) {
					continue;
				} catch (CancellationException e){
					if (IbicoopDataConstants.DEBUG_MODE) System.err.println("report crowd cancelled");
					// the task was canceled
					return false;
				}
			}
			*/
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportCrowdTask : starting!");
			
			BrokerManager brokerManager = IbicoopInit.getInstance()
					.getBrokerManager(IbicoopDataConstants.BROKER_NAME);
			
			if (topicList == null) {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportCrowdTask : Create topic list!");
				IbiTopicFilter topicFilter = brokerManager.createIbiTopicFilter();
				topicFilter.setName(IbicoopDataConstants.TOPIC_NAME_CROWD);
				topicFilter.setType(IbicoopDataConstants.TOPIC_TYPE_CROWD);
				IbicoopReportCrowdTask.topicList = brokerManager.getTopics(topicFilter);
			}
			
			while (!persistentCrowdEventCache.isEmpty()) {
				//Publish all saved Crowd data
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportCrowdTask : Publish Crowd data!");
				byte[] CrowdData = persistentCrowdEventCache.peek();
				publishSuccess = false;
				
				for (IbiTopic topic : topicList) {
					IbiTopicEvent CrowdEvent = brokerManager.createIbiEvent(
							IbicoopDataConstants.EVENT_NAME_CROWD,
							IbicoopDataConstants.EVENT_DESC_CROWD,
							CrowdData);
					
					int result = topic.publishEvent(CrowdEvent);
					
					if (result == BrokerConstants.OK) {
						if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportCrowdTask : Publish event ok for topic = " + topic.getName() );
						publishSuccess = true;
					} else {
						if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IbicoopReportCrowdTask : Publish event not ok for topic = " + topic.getName());
					}
				}
				
				if (publishSuccess) {
					persistentCrowdEventCache.remove(CrowdData);
					if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportCrowdTask : Publish success! Remove Crowd data from persistentCrowdEventCache!" +
							" current size = " + persistentCrowdEventCache.size());
				} else {
					if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IbicoopReportCrowdTask : Publish failed!");
					break;
				}
			}
		} catch(Exception exception) {
			if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IbicoopReportCrowdTask : " + exception.getMessage());
			publishSuccess = false;
		}
		
		writeCrowdEventCached();
		
		try {
			//Waiting so that reporting crowd was arrived on the server side
			Thread.sleep(3000);
		} catch (Exception exception) {
			if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IbicoopReportCrowdTask : " + exception.getMessage());			
		}
		
		return publishSuccess;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportCrowdTask : ending!");
		
		if (callback != null) {
			if (result) {
				callback.reportCrowdSuccess();
			} else {
				callback.reportCrowdFail();
			}
		}
	}
	
	/**
	 * Read Crowd event cached
	 */
	@SuppressWarnings({ "unchecked"})
	private void readCrowdEventCached() {
		try {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportCrowdTask : read crowd event cached!");
			// Load the persistent cache if empty
			File cacheDirectory = context.getCacheDir();
			if (cacheDirectory == null) {
				throw new IOException("The local cache is null! Cannot store the events file!");
			}
			
			File cacheFile = new File(cacheDirectory, "persistentCrowdEventCache.bin");
			if (cacheFile.exists()) {
				persistentCrowdEventCache = (Queue<byte[]>) new ObjectInputStream(new FileInputStream(cacheFile)).readObject();
			}
			
		} catch (Exception exception) {
			if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IbicoopReportCrowdTask : Cannot create persistentCrowdEventCache: " 
					+ exception.getMessage());
		} finally {
			if (persistentCrowdEventCache == null) {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportCrowdTask : create new crowd event cached!");
				persistentCrowdEventCache = new LinkedList<byte[]>();
			} else {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportCrowdTask : got new crowd event cached!");
			}
		}
	}
	
	@SuppressWarnings("resource")
	private void writeCrowdEventCached() {
		try {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportCrowdTask : write crowd event cached!");
			File cacheDirectory = context.getCacheDir();
			if (cacheDirectory == null) {
				throw new IOException("The local cache is null! Cannot store the events file!");
			}
			
			File cacheFile = new File(cacheDirectory, "persistentCrowdEventCache.bin");
			
			if (cacheFile.exists()) {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportCrowdTask : delete previous Crowd event cached!");
				cacheFile.delete();
			}
			new ObjectOutputStream(new FileOutputStream(cacheFile)).writeObject(persistentCrowdEventCache);
		} catch (Exception exception) {
			if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IbicoopReportCrowdTask : Cannot write persistentCrowdEventCache: " 
					+ exception.getMessage());
		}
	}
}