package traveldashboard.ibicooptask;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

import org.ibicoop.broker.common.BrokerConstants;
import org.ibicoop.broker.common.BrokerManager;
import org.ibicoop.broker.common.EventNotificationListener;
import org.ibicoop.broker.common.IbiResource;
import org.ibicoop.broker.common.IbiResourceEvent;
import org.ibicoop.broker.common.IbiTopic;
import org.ibicoop.broker.common.IbiTopicEvent;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.platform.sysinfo.SystemInfoNative;

import traveldashboard.compat.AsyncTaskCompat;
import traveldashboard.data.IbicoopDataConstants;

import android.app.IntentService;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

/**
 * An AsyncTask to manage the start & stop of iBICOOP
 * 
 * @author andriesc
 * 
 */
public class IbicoopStartStopTask extends AsyncTask<Void, Void, Boolean> {

	//Debug mode
	private static final boolean DEBUG = true;	
	
	private static AtomicBoolean isStarted = new AtomicBoolean(false);
	
	private Context context;
	private boolean stopRequested;
	private IbicoopStartCallback callback;
	private IbicoopBrokerStartStopTask brokerStartStopTask;

	private TSLContext tslApplicationContext;
	private Class<? extends IntentService> stationVoteIntentService;
	private Class<? extends IntentService> requestIntentService;

	public IbicoopStartStopTask(Context ctx, boolean isStopping,
			IbicoopStartCallback callback, TSLContext tslApplicationContext,
			Class<? extends IntentService> stationVoteIntentService,
			Class<? extends IntentService> requestIntentService) {
		this.context = ctx;
		this.stopRequested = isStopping;
		this.callback = callback;

		this.tslApplicationContext = tslApplicationContext;
		this.stationVoteIntentService = stationVoteIntentService;
		this.requestIntentService = requestIntentService;
	}
	
	@Override
	protected Boolean doInBackground(Void... params) {
		
		if (isCancelled()) return false;
		
		// stopping iBICOOP
		try {
			if (stopRequested) {
				/* Terminate iBICOOP */
				IbicoopInit.getInstance().terminate();
				isStarted.set(false);
				return true;
			}
		} catch (Exception e) {
			if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
			return false;
		}
		
		// iBICOOP was already started before
		if(isStarted.get()){
			return true;
		}

		// starting iBICOOP
		try {
			String IBICOOP_USERNAME = "TravelDashboardLondon" + System.currentTimeMillis() + "Android@ibicoop.org";
			String IBICOOP_PASSWORD = "TravelDashboardLondon";
			
			SystemInfoNative sysInfo = new SystemInfoNative(
					context.getApplicationContext());
			Object[] initObjects = { context.getApplicationContext(),
					IBICOOP_USERNAME, IBICOOP_PASSWORD,
					sysInfo.getDeviceUniqueId() };
			IbicoopInit.getInstance().start(initObjects);
			
			brokerStartStopTask = new IbicoopBrokerStartStopTask(context, stopRequested, tslApplicationContext, stationVoteIntentService, requestIntentService);
			AsyncTaskCompat.executeParallel(brokerStartStopTask);

			/*
			BrokerManager brokerManager = IbicoopInit.getInstance()
					.getBrokerManager(IbicoopDataConstants.BROKER_NAME);
			brokerManager.startNotification(eventListener);

			// Wait until the broker is started
			if (brokerManager.getBrokerStatus() < 0)
				return false;

			waitForBrokerStatus(brokerManager,
					BrokerConstants.STATUS_BROKER_STARTED_WITH_NOTIFICATION);
			*/
			isStarted.set(true);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	protected void onPostExecute(Boolean ok) {
		if (stopRequested)
			// GUI is probably not displayed, will not show any message
			return;

		if (!ok) {
			if (DEBUG) if (IbicoopDataConstants.DEBUG_MODE) Toast.makeText(context, "Failed to start iBICOOP",
					Toast.LENGTH_LONG).show();
			
			if (callback != null) callback.ibicoopStartFailed();
		}
		else {
			if (DEBUG) if (IbicoopDataConstants.DEBUG_MODE) Toast.makeText(context, "Start iBICOOP ok",
				Toast.LENGTH_LONG).show();
			
			if (callback != null) callback.ibicoopStartOk();
		}
	}

	/*
	private EventNotificationListener eventListener = new EventNotificationListener() {

		@Override
		public boolean acceptNotificationFrom(String eventPublisher) {
			return true;
		}

		@Override
		public void brokerStatus(String brokerName, int statusCode) {
			log("brokerStatus(brokerName=%s, statusCode=%d);", brokerName,
					statusCode);
		}

		@Override
		public void receiveEvent(IbiTopic forTopic, IbiTopicEvent event) {
			log("receiveEvent(forTopic = %s, event = %s)", forTopic.getName(),
					event.getName());
			log("event.data = %s, timestamp = %s", new String(event.getData()),
					event.getPublicationDate().getTime());
		}

		@Override
		public void receiveEvent(IbiTopic forTopic,
				IbiResourceEvent resourceEvent) {
			log("receiveEvent(forTopic = %s, publisher = %s)",
					forTopic.getName(), resourceEvent.getPublisher());
		}

		@Override
		public void receiveEvent(IbiResource forResource,
				IbiResourceEvent resourceEvent) {
			log("receiveEvent(forResource = %s, publisher = %s)",
					forResource.getResName(), resourceEvent.getPublisher());
		}

	};

	private void waitForBrokerStatus(BrokerManager brokerManager, int code)
			throws IOException {
		long brokerStartTime = System.currentTimeMillis();
		while (brokerManager.getBrokerStatus() != code) {
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {

			}			
			if (System.currentTimeMillis() - brokerStartTime >= IbicoopDataConstants.BROKER_START_TIMEOUT_SECONDS * 1000) {
				if (IbicoopDataConstants.DEBUG_MODE) System.err.println("ibicoop cancelled");
				throw new IOException();
			}
		}
	}

	private static void log(String format, Object... args) {
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("[IbicoopStartStop] " + String.format(format, args));
	}
	*/
}
