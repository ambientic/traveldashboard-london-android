package traveldashboard.ibicooptask;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.CancellationException;

import org.ibicoop.broker.common.BrokerConstants;
import org.ibicoop.broker.common.BrokerManager;
import org.ibicoop.broker.common.IbiTopic;
import org.ibicoop.broker.common.IbiTopicEvent;
import org.ibicoop.broker.common.IbiTopicFilter;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;

import traveldashboard.data.IbicoopDataConstants;

import android.content.Context;
import android.os.AsyncTask;

/**
 * Report comment task
 * @author khoo
 *
 */
public class IbicoopReportCommentTask extends AsyncTask<Void, Void, Boolean>{
	
	//Android activity context
	private Context context;
	
	//
	//
	//Cached topic list
	private static IbiTopic[] topicList;
	//Comment queue
	private static Queue<byte[]> persistentCommentEventCache;
	
	private IbicoopStartStopTask ibiStart;
	private IbicoopBrokerStartStopTask ibiBroker;
	
	private IbicoopReportCommentCallback callback;
	
	public interface IbicoopReportCommentCallback {
		public void reportCommentSuccess();
		public void reportCommentFailed();
	}
	
	public IbicoopReportCommentTask(Context context, String city, String stopId, String routeId, 
			String userName, String comment, IbicoopReportCommentCallback reportCommentcallback) {
		this.context = context;
		callback = reportCommentcallback;
		
		if (persistentCommentEventCache == null) {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportCommentTask : Create persistentCommentEventCache");
			//Create persistentCommentEventCache
			readCommentEventCached();
		}
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportCommentTask : Createcomment data message: " +
				"stopId = " + stopId + ", userName = " + userName + ", comment " + comment
				+ ", persistentCommentEventCache size = " + persistentCommentEventCache.size());
		NetworkMessage CommentDataMessage = new NetworkMessageXml();
		CommentDataMessage.addPayload(IbicoopDataConstants.PARAM_KEY_CITY_TYPE, city);
		CommentDataMessage.addPayload(IbicoopDataConstants.PARAM_KEY_STOP_ID, stopId);
		CommentDataMessage.addPayload(IbicoopDataConstants.PARAM_KEY_ROUTE_ID, routeId);
		CommentDataMessage.addPayload(IbicoopDataConstants.PARAM_KEY_USER_NAME, userName);
		CommentDataMessage.addPayload(IbicoopDataConstants.PARAM_KEY_COMMENT, comment);	
		persistentCommentEventCache.add(CommentDataMessage.encode());
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportCommentTask : Insert Comment data into persistentCommentEventCache, " +
				" new persistentCommentEventCache size = " + persistentCommentEventCache.size());	
	}
	
	/*
	@Override
	protected void onPreExecute() {
		super.onPreExecute();

		if(!isCancelled()){
			// we must assure iBICOOP is running
			ibiStart = new IbicoopStartStopTask(context, false, null);
			AsyncTaskCompat.executeParallel(ibiStart);
		}
	}
	*/
	
	@Override
	protected Boolean doInBackground(Void... params) {
		
		
		boolean publishSuccess = false;
		
		try {
			if(isCancelled()) return false;
			if (!IbicoopInit.getInstance().isStarted()) return false;
			
			/*
			while (true) {
				if(isCancelled())
					return false;
				
				try { // the wait might get interrupted, that's why we LOOP
					if (!ibiStart.get()) {
						// iBICOOP did not start
						return false;
					}
					ibiBroker = new IbicoopBrokerStartStopTask(context, false);
					AsyncTaskCompat.executeParallel(ibiBroker);
					break;
				} catch (InterruptedException e) {
					continue;
				} catch (CancellationException e){
					if (IbicoopDataConstants.DEBUG_MODE) System.err.println("report comment cancelled");
					// the task was canceled
					return false;
				}
			}

			while (true) {
				if(isCancelled())
					return false;
				
				try { // the wait might get interrupted, that's why we LOOP
					if (!ibiBroker.get()) {
						// broker did not start
						return false;
					}
					break;
				} catch (InterruptedException e) {
					continue;
				} catch (CancellationException e){
					if (IbicoopDataConstants.DEBUG_MODE) System.err.println("report comment cancelled");
					// the task was canceled
					return false;
				}
			}
			*/
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportCommentTask : starting!");
			
			BrokerManager brokerManager = IbicoopInit.getInstance()
					.getBrokerManager(IbicoopDataConstants.BROKER_NAME);
			
			if (topicList == null) {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportCommentTask : Create topic list!");
				IbiTopicFilter topicFilter = brokerManager.createIbiTopicFilter();
				topicFilter.setName(IbicoopDataConstants.TOPIC_NAME_COMMENT);
				topicFilter.setType(IbicoopDataConstants.TOPIC_TYPE_COMMENT);
				IbicoopReportCommentTask.topicList = brokerManager.getTopics(topicFilter);
			}
			
			while (!persistentCommentEventCache.isEmpty()) {
				//Publish all saved Comment data
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportCommentTask : Publish Comment data!");
				byte[] CommentData = persistentCommentEventCache.peek();
				publishSuccess = false;
				
				for (IbiTopic topic : topicList) {
					IbiTopicEvent CommentEvent = brokerManager.createIbiEvent(
							IbicoopDataConstants.EVENT_NAME_COMMENT,
							IbicoopDataConstants.EVENT_DESC_COMMENT,
							CommentData);
					
					int result = topic.publishEvent(CommentEvent);
					
					if (result == BrokerConstants.OK) {
						if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportCommentTask : Publish event ok");
						publishSuccess = true;
					} else {
						if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IbicoopReportCommentTask : Publish event not ok");
					}
				}
				
				if (publishSuccess) {
					persistentCommentEventCache.remove(CommentData);
					if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportCommentTask : Publish success! Remove comment data from persistentCommentEventCache!" +
							" current size = " + persistentCommentEventCache.size());
				} else {
					if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IbicoopReportCommentTask : Publish failed!");
					break;
				}
			}
		} catch(Exception exception) {
			if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IbicoopReportCommentTask : " + exception.getMessage());
			publishSuccess = false;
		}
		
		writeCommentEventCached();
		
		try {
			//Waiting so that reporting comment was arrived on the server side
			Thread.sleep(3000);
		} catch (Exception exception) {
			if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IbicoopReportCommentTask : " + exception.getMessage());			
		}
		
		return publishSuccess;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportCommentTask : ending!");
		if (callback != null) {
			if (result) callback.reportCommentSuccess();
			else callback.reportCommentFailed();
		}
	}
	
	/**
	 * Read Comment event cached
	 */
	@SuppressWarnings({ "unchecked"})
	private void readCommentEventCached() {
		try {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportCommentTask : read comment event cached!");
			// Load the persistent cache if empty
			File cacheDirectory = context.getCacheDir();
			if (cacheDirectory == null) {
				throw new IOException("The local cache is null! Cannot store the events file!");
			}
			
			File cacheFile = new File(cacheDirectory, "persistentCommentEventCache.bin");
			if (cacheFile.exists()) {
				persistentCommentEventCache = (Queue<byte[]>) new ObjectInputStream(new FileInputStream(cacheFile)).readObject();
			}
			
		} catch (Exception exception) {
			if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IbicoopReportCommentTask : Cannot create persistentCommentEventCache: " 
					+ exception.getMessage());
		} finally {
			if (persistentCommentEventCache == null) {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportCommentTask : create new Comment event cached!");
				persistentCommentEventCache = new LinkedList<byte[]>();
			} else {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportCommentTask : got new Comment event cached!");
			}
		}
	}
	
	@SuppressWarnings("resource")
	private void writeCommentEventCached() {
		try {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportCommentTask : write comment event cached!");
			File cacheDirectory = context.getCacheDir();
			if (cacheDirectory == null) {
				throw new IOException("The local cache is null! Cannot store the events file!");
			}
			
			File cacheFile = new File(cacheDirectory, "persistentCommentEventCache.bin");
			
			if (cacheFile.exists()) {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportCommentTask : delete previous Comment event cached!");
				cacheFile.delete();
			}
			new ObjectOutputStream(new FileOutputStream(cacheFile)).writeObject(persistentCommentEventCache);
		} catch (Exception exception) {
			if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IbicoopReportCommentTask : Cannot write persistentCommentEventCache: " 
					+ exception.getMessage());
		}
	}
}
