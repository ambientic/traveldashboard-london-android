package traveldashboard.ibicooptask;

import java.util.ArrayList;

import traveldashboard.data.ParisStopInfo;

public interface IbicoopAllStopsReadyCallback {
	public void allStopsReady(ArrayList<ParisStopInfo> stops);
	public void allStopsFailed();
}
