package traveldashboard.ibicooptask;

import traveldashboard.data.crowd.TubeStationCrowdsCollection;

public interface IbicoopGeneralCrowdednessCallback {
	public void crowdednessFailed();
	public void tubeStationCrowdInfo(TubeStationCrowdsCollection crowdsCollection);
}
