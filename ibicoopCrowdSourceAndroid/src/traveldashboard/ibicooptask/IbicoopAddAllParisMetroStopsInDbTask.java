package traveldashboard.ibicooptask;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import traveldashboard.data.ParisGtfsNetworkMessageParser;
import traveldashboard.data.ParisMetroDbManager;
import traveldashboard.data.ParisStopInfo;
import traveldashboard.data.IbicoopDataConstants;
import android.os.AsyncTask;

/**
 * Add all stops in database
 * @author khoo
 *
 */
public class IbicoopAddAllParisMetroStopsInDbTask extends AsyncTask<Void, Void, Boolean>{

	//Variables
	private ParisMetroDbManager dbManager;
	private InputStream xmlFileInputStream;
	private ArrayList<ParisStopInfo> parisStopInfos;
	private AddAllParisStopsInDbTaskCallback callback;
	
	public interface AddAllParisStopsInDbTaskCallback {
		public void addAllStopsInDbOk();
		public void addAllStopsInDbFailed();	
	}
	
	public IbicoopAddAllParisMetroStopsInDbTask(
			ParisMetroDbManager receiveDbManager, 
			ArrayList<ParisStopInfo> receiveStopInfos,
			AddAllParisStopsInDbTaskCallback receiveCallback
			) {
		dbManager = receiveDbManager;
		parisStopInfos = new ArrayList<ParisStopInfo>();
		parisStopInfos.addAll(receiveStopInfos);
		callback = receiveCallback;
	}
	
	
	public IbicoopAddAllParisMetroStopsInDbTask(
			ParisMetroDbManager receivedDbManager, 
			InputStream receivedXmlInputStream,
			AddAllParisStopsInDbTaskCallback receivedCallback
			) {
		dbManager = receivedDbManager;
		xmlFileInputStream = receivedXmlInputStream;
		callback = receivedCallback;
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		
		if (isCancelled()) return false;
		
		boolean success = false;
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Add all stops in db starting...");
		
		try {
			
			int tableCount = dbManager.getStopInfoDbCount();
			
			if (tableCount > 0) {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Stops are already in database! count = " + tableCount + "! return!");
				return true;
			}
			
			//Parse xml file into StopInfo List
			if (parisStopInfos == null) {
				//Read xml file
				//Parse XML information into String
				BufferedReader br = new BufferedReader(new InputStreamReader(xmlFileInputStream));
				String line;
				StringBuilder sb = new StringBuilder();
	
				while((line=br.readLine())!= null){
				    sb.append(line.trim());
				}

				br.close();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Xml string = " + sb.toString());
				
				//Parse Networkmessage XML
				parisStopInfos = new ArrayList<ParisStopInfo>();
				parisStopInfos.addAll(ParisGtfsNetworkMessageParser.parseMessage(sb.toString().getBytes()));
			}
	
			for (ParisStopInfo parisStopInfo: parisStopInfos) {
				dbManager.insertStopIntoDb(parisStopInfo);
			}
			
			success = true;
			
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		
		return success;
	}
	
	@Override
	protected void onPostExecute(Boolean success) {
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Success = " + success);
		
		try {
			xmlFileInputStream.close();
		} catch (Exception exception) {
			if (IbicoopDataConstants.DEBUG_MODE) System.err.println("Close xml file input stream exception : " + exception.getMessage());
		}
		
		if (callback != null) {
			if (success) callback.addAllStopsInDbOk();
			else callback.addAllStopsInDbFailed();
		}
	}

}
