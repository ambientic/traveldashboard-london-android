package traveldashboard.ibicooptask;

import traveldashboard.routeplanner.ItdItinerary;

public interface IbicoopRoutePlannerCallback {
	public void getItinerarySucess(ItdItinerary itdItinerary);
	public void getItineraryFailed();
}
