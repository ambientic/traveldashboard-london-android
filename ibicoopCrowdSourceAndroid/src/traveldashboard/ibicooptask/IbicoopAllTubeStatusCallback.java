package traveldashboard.ibicooptask;

import traveldashboard.data.status.TubeStatusCollection;

public interface IbicoopAllTubeStatusCallback {
	public void tubeStatusFailed();
	public void tubeStatusCollectionInfo(TubeStatusCollection info);
}
