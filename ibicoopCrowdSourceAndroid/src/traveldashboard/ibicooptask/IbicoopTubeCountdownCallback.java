package traveldashboard.ibicooptask;

import traveldashboard.data.info.TubeRealTimeInfosCollection;

public interface IbicoopTubeCountdownCallback {
	public void tubeCountdownFailed(String routeId);
	public void tubeRealtimeInfo(String routeId, TubeRealTimeInfosCollection info);
}
