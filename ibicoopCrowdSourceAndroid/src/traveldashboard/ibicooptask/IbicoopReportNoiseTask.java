package traveldashboard.ibicooptask;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.CancellationException;

import org.ibicoop.broker.common.BrokerConstants;
import org.ibicoop.broker.common.BrokerManager;
import org.ibicoop.broker.common.IbiTopic;
import org.ibicoop.broker.common.IbiTopicEvent;
import org.ibicoop.broker.common.IbiTopicFilter;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;

import traveldashboard.data.IbicoopDataConstants;

import android.content.Context;
import android.os.AsyncTask;


/**
 * Report noise task
 * @author khoo
 *
 */
public class IbicoopReportNoiseTask extends AsyncTask<Void, Void, Boolean>{
	
	//Android activity context
	private Context context;
	
	//
	//
	//Cached topic list
	private static IbiTopic[] topicList;
	//Noise queue
	private static Queue<byte[]> persistentNoiseEventCache;
	
	private IbicoopStartStopTask ibiStart;
	private IbicoopBrokerStartStopTask ibiBroker;
	
	private IbicoopReportNoiseCallback callback;
	
	public interface IbicoopReportNoiseCallback {
		public void reportNoiseSuccess();
		public void reportNoiseFailed();
	}
	
	/**
	 * IbicoopReportNoiseTask constructor
	 * @param context Android activity context
	 * @param latitude Noise data latitude
	 * @param longitude Noise data longitude
	 * @param noiseLevel Noise level
	 */
	public IbicoopReportNoiseTask(Context context, double latitude, double longitude, 
			int noiseLevel, IbicoopReportNoiseCallback reportNoiseCallback) {
		
		this.context = context;
		callback = reportNoiseCallback;
		
		if (persistentNoiseEventCache == null) {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportNoiseTask : Create persistentNoiseEventCache");
			//Create persistentNoiseEventCache
			readNoiseEventCached();
		}
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportNoiseTask : Create noise data message: " +
				"latitude = " + latitude + ", longitude = " + longitude + ", noise level = " + noiseLevel
				+ ", persistentNoiseEventCache size = " + persistentNoiseEventCache.size());
		NetworkMessage noiseDataMessage = new NetworkMessageXml();
		noiseDataMessage.addPayload(IbicoopDataConstants.PARAM_KEY_STOP_LAT, String.valueOf(latitude));
		noiseDataMessage.addPayload(IbicoopDataConstants.PARAM_KEY_STOP_LON, String.valueOf(longitude));
		noiseDataMessage.addPayload(IbicoopDataConstants.PARAM_NOISE_LEVEL, String.valueOf(noiseLevel));
		persistentNoiseEventCache.add(noiseDataMessage.encode());
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportNoiseTask : Insert noise data into persistentNoiseEventCache, " +
				" new persistentNoiseEventCache size = " + persistentNoiseEventCache.size());	
	}
	
	/*
	@Override
	protected void onPreExecute() {
		super.onPreExecute();

		if(!isCancelled()){
			// we must assure iBICOOP is running
			ibiStart = new IbicoopStartStopTask(context, false, null);
			AsyncTaskCompat.executeParallel(ibiStart);
		}
	}*/
	
	
	@Override
	protected Boolean doInBackground(Void... params) {
		
		
		boolean publishSuccess = false;
		
		try {
			
			if(isCancelled()) return false;
			if (!IbicoopInit.getInstance().isStarted()) return false;
			
			/*
			while (true) {
				if(isCancelled())
					return false;
				
				try { // the wait might get interrupted, that's why we LOOP
					if (!ibiStart.get()) {
						// iBICOOP did not start
						return false;
					}
					ibiBroker = new IbicoopBrokerStartStopTask(context, false);
					AsyncTaskCompat.executeParallel(ibiBroker);
					break;
				} catch (InterruptedException e) {
					continue;
				} catch (CancellationException e){
					if (IbicoopDataConstants.DEBUG_MODE) System.err.println("report noise cancelled");
					// the task was canceled
					return false;
				}
			}

			while (true) {
				if(isCancelled())
					return false;
				
				try { // the wait might get interrupted, that's why we LOOP
					if (!ibiBroker.get()) {
						// broker did not start
						return false;
					}
					break;
				} catch (InterruptedException e) {
					continue;
				} catch (CancellationException e){
					if (IbicoopDataConstants.DEBUG_MODE) System.err.println("report noise cancelled");
					// the task was canceled
					return false;
				}
			}*/
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportNoiseTask : starting!");
			
			BrokerManager brokerManager = IbicoopInit.getInstance()
					.getBrokerManager(IbicoopDataConstants.BROKER_NAME);
			
			if (topicList == null) {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportNoiseTask : Create topic list!");
				IbiTopicFilter topicFilter = brokerManager.createIbiTopicFilter();
				topicFilter.setName(IbicoopDataConstants.TOPIC_NAME_NOISE);
				topicFilter.setType(IbicoopDataConstants.TOPIC_TYPE_NOISE);
				IbicoopReportNoiseTask.topicList = brokerManager.getTopics(topicFilter);
			}
			
			while (!persistentNoiseEventCache.isEmpty()) {
				//Publish all saved noise data
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportNoiseTask : Publish noise data!");
				byte[] noiseData = persistentNoiseEventCache.peek();
				publishSuccess = false;
				
				for (IbiTopic topic : topicList) {
					IbiTopicEvent noiseEvent = brokerManager.createIbiEvent(
							IbicoopDataConstants.EVENT_NAME_NOISE,
							IbicoopDataConstants.EVENT_DESC_NOISE,
							noiseData);
					
					int result = topic.publishEvent(noiseEvent);
					
					if (result == BrokerConstants.OK) {
						if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportNoiseTask : Publish event ok");
						publishSuccess = true;
					} else {
						if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IbicoopReportNoiseTask : Publish event not ok");
					}
				}
				
				if (publishSuccess) {
					persistentNoiseEventCache.remove(noiseData);
					if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportNoiseTask : Publish success! Remove noise data from persistentNoiseEventCache!" +
							" current size = " + persistentNoiseEventCache.size());
				} else {
					if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IbicoopReportNoiseTask : Publish failed!");
					break;
				}
			}
		} catch(Exception exception) {
			if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IbicoopReportNoiseTask : " + exception.getMessage());
			publishSuccess = false;
		}
		
		writeNoiseEventCached();
		
		return publishSuccess;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportNoiseTask : ending!");
		
		if (callback != null) {
			if (result) callback.reportNoiseSuccess();
			else callback.reportNoiseFailed();
		}
	}
	
	/**
	 * Read noise event cached
	 */
	@SuppressWarnings({ "unchecked"})
	private void readNoiseEventCached() {
		try {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportNoiseTask : read noise event cached!");
			// Load the persistent cache if empty
			File cacheDirectory = context.getCacheDir();
			if (cacheDirectory == null) {
				throw new IOException("The local cache is null! Cannot store the events file!");
			}
			
			File cacheFile = new File(cacheDirectory, "persistentNoiseEventCache.bin");
			if (cacheFile.exists()) {
				persistentNoiseEventCache = (Queue<byte[]>) new ObjectInputStream(new FileInputStream(cacheFile)).readObject();
			}
			
		} catch (Exception exception) {
			if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IbicoopReportNoiseTask : Cannot create persistentNoiseEventCache: " 
					+ exception.getMessage());
		} finally {
			if (persistentNoiseEventCache == null) {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportNoiseTask : create new noise event cached!");
				persistentNoiseEventCache = new LinkedList<byte[]>();
			} else {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportNoiseTask : got new noise event cached!");
			}
		}
	}
	
	@SuppressWarnings("resource")
	private void writeNoiseEventCached() {
		try {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportNoiseTask : write noise event cached!");
			File cacheDirectory = context.getCacheDir();
			if (cacheDirectory == null) {
				throw new IOException("The local cache is null! Cannot store the events file!");
			}
			
			File cacheFile = new File(cacheDirectory, "persistentNoiseEventCache.bin");
			
			if (cacheFile.exists()) {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportNoiseTask : delete previous noise event cached!");
				cacheFile.delete();
			}
			new ObjectOutputStream(new FileOutputStream(cacheFile)).writeObject(persistentNoiseEventCache);
		} catch (Exception exception) {
			if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IbicoopReportNoiseTask : Cannot write persistentNoiseEventCache: " 
					+ exception.getMessage());
		}
	}
}
