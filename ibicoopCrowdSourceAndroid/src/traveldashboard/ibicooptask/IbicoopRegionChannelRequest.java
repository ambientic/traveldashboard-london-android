package traveldashboard.ibicooptask;

import java.util.Random;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import org.ibicoop.broker.common.BrokerConstants;
import org.ibicoop.broker.common.BrokerManager;
import org.ibicoop.broker.common.IbiTopic;
import org.ibicoop.broker.common.IbiTopicEvent;
import org.ibicoop.broker.common.IbiTopicFilter;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;

import com.google.gson.Gson;

import traveldashboard.compat.AsyncTaskCompat;
import traveldashboard.utils.IntentConstants;
import traveldashboard.data.TubeStation;
import traveldashboard.data.IbicoopDataConstants;
import android.app.IntentService;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

public class IbicoopRegionChannelRequest extends AsyncTask<Void, Void, Boolean> {

	private IbicoopStartStopTask ibiStart;
	private IbicoopBrokerStartStopTask ibiBroker;
	private Context mContext;
	private TubeStation mStop;
	private TSLContext tslApplicationContext;
	private Class<? extends IntentService> stationVoteIntentService;
	private Class<? extends IntentService> requestIntentService;
	
	public IbicoopRegionChannelRequest(Context ctx, TubeStation stop, TSLContext tslApplicationContext,
			Class<? extends IntentService> stationVoteIntentService,
			Class<? extends IntentService> requestIntentService) {
		this.mContext = ctx;
		this.mStop = stop;
		this.tslApplicationContext = tslApplicationContext;
		this.stationVoteIntentService = stationVoteIntentService;
		this.requestIntentService = requestIntentService;
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		try {
			if(isCancelled()) return false;
			
			/*
			if (!waitForIbicoop()) {
				return false;
			}*/

			BrokerManager brokerManager = IbicoopInit.getInstance()
					.getBrokerManager(IbicoopDataConstants.BROKER_NAME);

			String topicName = IbicoopRegionChannelSubscriptionTask
					.convertLocationToChannelName(mStop.getLatitude(),
							mStop.getLongitude());
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Topic name = " + topicName);
			
			IbiTopicFilter topicFilter = brokerManager.createIbiTopicFilter();
			topicFilter.setName(topicName);
			IbiTopic[] topicList = brokerManager.getTopics(topicFilter);
	
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Topic list size = " + topicList.length);

			
			String topicNameGlobal = IbicoopDataConstants.GLOBAL_ON_DEMAND_CHANNEL;
			IbiTopicFilter topicFilterGlobal = brokerManager
					.createIbiTopicFilter();
			topicFilterGlobal.setName(topicNameGlobal);
			IbiTopic[] topicListGlobal = brokerManager
					.getTopics(topicFilterGlobal);
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Topic list global size = " + topicListGlobal.length);
					

			// we add a random request token
			String reqToken = "" + new Random(System.currentTimeMillis()).nextLong();
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("req token = " + reqToken);

			NetworkMessage eventMessage = new NetworkMessageXml();
			
			Gson gson = new Gson();
			String metroStopDesc = gson.toJson(mStop);
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopRegionChannelRequest: metroStopDesc = " + metroStopDesc);
			
			eventMessage.addPayload(IntentConstants.TUBE_STATION_GSON, metroStopDesc);
			
			IbiTopicEvent eventObj = brokerManager.createIbiEvent(
					IbicoopDataConstants.OD_VOTE_EVENT_NAME, reqToken,
					eventMessage.encode());
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Event description = " + eventObj.getDescription());
			
			boolean sendOk = false;

			// send information to the REGIONAL channel
			if (topicList != null && topicList.length > 0) {
				for (IbiTopic ibiTopic : topicList) {
					
					if (ibiTopic.publishEvent(eventObj) == BrokerConstants.OK) {
						sendOk = true;
						if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Topic name = " + ibiTopic.getName() + " publish ok");
						break;
					} else {
						if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Topic name = " + ibiTopic.getName() + " publish failed");
					}
				}
			} else {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println(
						"There is no topic registered for region " + topicName
								+ ". ignoring!");
			}
			
			// We save the token before sending
			tslApplicationContext.getPendingOnDemandRequests().add(reqToken);
			
			
			// send information to the GLOBAL channel
			if (topicListGlobal != null && topicListGlobal.length > 0) {
				for (IbiTopic ibiTopic : topicListGlobal) {
					if (ibiTopic.publishEvent(eventObj) == BrokerConstants.OK) {
						if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Publish topic list global for topic " + ibiTopic.getName() + " sucees");
						sendOk = true;
						break;
					} else {
						if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Publish topic list global for topic " + ibiTopic.getName() + " failed");
					}
				}
			} else {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println(
						"There is no GLOBAL topic registered: " + topicNameGlobal
								+ ". ignoring! (very strange)");
			}
			
			// we remove the token if send failed
			if(!sendOk){
				tslApplicationContext.getPendingOnDemandRequests().remove(reqToken);
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Remove token = " + reqToken);
			} else {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Going to subscribe to station channel");				
				AsyncTaskCompat.executeParallel(new IbicoopStationChannelSubscriptionTask(
						mContext, mStop, tslApplicationContext, stationVoteIntentService, requestIntentService));
			}
			
			return sendOk;
		} catch (Exception e) {
			return false;
		}
	}
	
	@Override
	protected void onPostExecute(Boolean sendOK) {
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Region channel request : " +
				"result = " + sendOK);
		
		if (IbicoopDataConstants.DEBUG_MODE) Toast.makeText(mContext, "Region channel request : " +
				"result = " + sendOK, Toast.LENGTH_SHORT).show();
	}

	private boolean waitForIbicoop() throws ExecutionException {
		ibiStart = new IbicoopStartStopTask(mContext, false, null, tslApplicationContext, stationVoteIntentService, requestIntentService);
		AsyncTaskCompat.executeParallel(ibiStart);
		while (true) {
			try { // the wait might get interrupted, that's why we LOOP
				if (!ibiStart.get()) {
					// iBICOOP did not start
					return false;
				}
				ibiBroker = new IbicoopBrokerStartStopTask(mContext, false, tslApplicationContext, stationVoteIntentService, requestIntentService);
				AsyncTaskCompat.executeParallel(ibiBroker);
				break;
			} catch (InterruptedException e) {
				continue;
			} catch (CancellationException e) {
				// the task was canceled
				return false;
			}
		}

		while (true) {
			if (isCancelled())
				return false;
			try { // the wait might get interrupted, that's why we LOOP
				if (!ibiBroker.get()) {
					// broker did not start
					return false;
				}
				return true;
			} catch (InterruptedException e) {
				continue;
			} catch (CancellationException e) {
				// the task was canceled
				return false;
			}
		}
	}
}
