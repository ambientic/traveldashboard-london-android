package traveldashboard.ibicooptask;

import traveldashboard.data.incentive.Incentive;

public interface IbicoopIncentiveCallback {
	public void getIncentiveSuccess(Incentive incentive);
	public void getIncentiveFailed();	
}
