package traveldashboard.ibicooptask;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationManager;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiSender;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;

import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.data.noise.NoiseData;
import traveldashboard.data.noise.NoiseDatasCollection;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

public class IbicoopClassifiedNoiseGetterTask  extends AsyncTask<Void, Void, Boolean> {

	private Context context;
	
	private IbicoopClassifiedNoiseCallback callback;

	private String city;
	private String stopId;
	private String routeId;
	
	private String noiseMessage;
	
	private IbiSender sender;

	public IbicoopClassifiedNoiseGetterTask(Context context, String city, String stopId, String routeId, 
			String queryTime, IbicoopClassifiedNoiseCallback noiseCallback) {
		this.context = context;
		this.city = city;
		this.stopId = stopId;
		this.routeId = routeId;
		this.callback = noiseCallback;
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		
		if (isCancelled()) return false;
		
		try {
			
			if (IbicoopDataConstants.DEBUG_MODE)  System.out.println("IbicoopClassifiedNoiseGetterTask: Creating sender");
			
			IBIURL uriLocal = new IBIURL("ibiurl",
					IbicoopDataConstants.USER_CLASSIFIED_NOISE,
					IbicoopDataConstants.TERMINAL_CLASSIFIED_NOISE + String.valueOf(System.currentTimeMillis()),
					IbicoopDataConstants.APPLICATION, IbicoopDataConstants.CLASSIFIED_NOISE_SERVICE,
					IbicoopDataConstants.PATH_GET_CLASSIFIED_NOISE);
			
			IBIURL uriRemote = new IBIURL("ibiurl", IbicoopDataConstants.USER,
					IbicoopDataConstants.TERMINAL, IbicoopDataConstants.APPLICATION,
					IbicoopDataConstants.CLASSIFIED_NOISE_SERVICE,
					IbicoopDataConstants.PATH_GET_CLASSIFIED_NOISE);

			CommunicationOptions options = new CommunicationOptions();
			options.setCommunicationMode(new CommunicationMode(
					CommunicationConstants.MODE_PROXY));

			CommunicationManager comm = IbicoopInit.getInstance()
					.getCommunicationManager();

			sender = comm.createSender(uriLocal, uriRemote, options,
					null);

			NetworkMessage requestMessage = new NetworkMessageXml();
			requestMessage.setMsgOperation(IbicoopDataConstants.OPERATION_GET_CLASSIFIED_NOISE);
			requestMessage.setMsgType(NetworkMessage.MSG_TYPE_REQUEST);
			
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_CITY_TYPE, city);
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_STOP_ID, stopId);
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_ROUTE_ID, routeId);
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_TIMESTAMP, String.valueOf(System.currentTimeMillis()));		

			ResponseMessage responseMessage = new ResponseMessage();
			
			if (IbicoopDataConstants.DEBUG_MODE)  System.out.println("IbicoopNoiseGetter: Going to send request");
			
			boolean ok = responseMessage.decode(sender
					.sendRequestResponse(requestMessage.encode()));
			sender.stop();

			if (ok) {
				noiseMessage = responseMessage.getPayload(IbicoopDataConstants.CLASSIFIED_NOISE_MESSAGE);
				if (IbicoopDataConstants.DEBUG_MODE)  System.out.println("IbicoopNoiseGetter: Classified noise message = " + noiseMessage);
			} else {
				return false;
			}

			return true;
		} catch(Exception e) {
			if (sender != null) sender.stop();
			if (IbicoopDataConstants.DEBUG_MODE)  e.printStackTrace();
		}
		return false;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		
		if (result) {
			if (callback != null) { 
				NoiseData noiseData = getNoiseData();
				if (noiseData != null) callback.getNoiseInfo(noiseData);
				else callback.getNoiseFailed();
			}
		} else {
			if (callback != null) callback.getNoiseFailed();
		}
	}

	protected NoiseData getNoiseData() {
		
		NoiseData noiseData = null;
		
		if (noiseMessage != null) {
			Gson gson = new Gson();
			noiseData = gson.fromJson(noiseMessage, NoiseData.class);
		}
		
		return noiseData;
	}
	
	/**
	 * Extract query time to current time format
	 * @param initialTime
	 * @return current time format
	 */
	public static String extractQueryTimeToCurrentTime(String initialTime) {
		
		String newTime = "";
			
		String hour = initialTime.substring(0, 2);
		String minute = initialTime.substring(2);

		String formatDate = "yyyy-MM-dd'T'";
		SimpleDateFormat sdfDate = new SimpleDateFormat(formatDate);
		String date = sdfDate.format(new Date());
		
		String formatSecond = ":ss.SSS'Z'";
		SimpleDateFormat sdfSecond = new SimpleDateFormat(formatSecond);
		String second = sdfSecond.format(new Date());
		
		newTime = date + hour + ":" + minute + second;
		
		if (IbicoopDataConstants.DEBUG_MODE)  System.out.println(">>>>New time = " + newTime);
		
		return newTime;
	}
	
	/**
	 * Convert current date string to timestamp
	 * @param dateString
	 * @return timestamp
	 */
	public static long convertDateStringToTimestamp(String city, String dateString) {
		
		long timestamp = -1l;
		
		try {
			if (IbicoopDataConstants.DEBUG_MODE)  System.out.println("Date string = " + dateString);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			Date date = sdf.parse(dateString);
			timestamp = date.getTime();
			
		} catch (Exception e) {
			if (IbicoopDataConstants.DEBUG_MODE)  e.printStackTrace();
		}
		
		return timestamp;
		
	}
}
