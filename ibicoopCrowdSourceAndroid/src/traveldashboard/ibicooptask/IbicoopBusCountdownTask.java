package traveldashboard.ibicooptask;


import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationManager;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiSender;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;

import com.google.gson.Gson;

import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.data.info.BusRealTimeInfosCollection;
import android.content.Context;
import android.os.AsyncTask;

public class IbicoopBusCountdownTask extends AsyncTask<Void, Void, Boolean>{

	private Context context;
	
	private IbicoopBusCountdownCallback callback;
	
	private String city;
	private String stopId;
	private String routeId;
	private int maxValues;
	
	private String busCountdownMessage;
	
	private IbiSender sender;

	public IbicoopBusCountdownTask(Context context, String city, String stopId, String routeId, int maxValues, IbicoopBusCountdownCallback callback) {
		this.context = context;
		this.city = city;
		this.stopId = stopId;
		this.routeId = routeId;
		this.maxValues = maxValues;
		this.callback = callback;
	}
	
	@Override
	protected Boolean doInBackground(Void... params) {
		
		if (isCancelled()) return false;
		
		try {
	
			IBIURL uriLocal = new IBIURL("ibiurl",
					IbicoopDataConstants.USER_BUS_COUNTDOWN,
					IbicoopDataConstants.TERMINAL_BUS_COUNTDOWN + String.valueOf(System.currentTimeMillis() + routeId),
					IbicoopDataConstants.APPLICATION, IbicoopDataConstants.BUS_COUNTDOWN_SERVICE,
					IbicoopDataConstants.PATH_GET_BUS_COUNTDOWN);
			
			IBIURL uriRemote = new IBIURL("ibiurl", IbicoopDataConstants.USER,
					IbicoopDataConstants.TERMINAL, IbicoopDataConstants.APPLICATION,
					IbicoopDataConstants.BUS_COUNTDOWN_SERVICE,
					IbicoopDataConstants.PATH_GET_BUS_COUNTDOWN);

			CommunicationOptions options = new CommunicationOptions();
			options.setCommunicationMode(new CommunicationMode(
					CommunicationConstants.MODE_PROXY));

			CommunicationManager comm = IbicoopInit.getInstance()
					.getCommunicationManager();

			sender = comm.createSender(uriLocal, uriRemote, options,
					null);

			NetworkMessage requestMessage = new NetworkMessageXml();
			requestMessage.setMsgOperation(IbicoopDataConstants.OPERATION_GET_BUS_COUNTDOWN);
			
			requestMessage.setMsgType(NetworkMessage.MSG_TYPE_REQUEST);
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_CITY_TYPE, city);
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_STOP_ID, stopId);
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_ROUTE_ID, routeId);
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_MAX_VALUES, String.valueOf(maxValues));

			ResponseMessage responseMessage = new ResponseMessage();
			boolean ok = responseMessage.decode(sender
					.sendRequestResponse(requestMessage.encode()));
			sender.stop();

			if (!ok)
				return false;

			busCountdownMessage = responseMessage.getPayload(IbicoopDataConstants.BUS_COUNTDOWN_MESSAGE);

			return true;
		} catch(Exception e) {
			if (sender != null) sender.stop();
			if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
		}
		return false;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		
		if (result) {
			if (callback != null) { 
				BusRealTimeInfosCollection busRealTimeInfosCollection = getBusRealTimeInfosCollection();
				if (busRealTimeInfosCollection != null) callback.busCountdownRealTimeInfo(routeId, busRealTimeInfosCollection);
				else callback.busCountdownFailed(routeId);
			}
		} else {
			if (callback != null) callback.busCountdownFailed(routeId);
		}
	}


	protected BusRealTimeInfosCollection getBusRealTimeInfosCollection() {
		BusRealTimeInfosCollection busRealTimeInfosCollection = null;
		
		try {
			
			if (busCountdownMessage == null) return busRealTimeInfosCollection;
			
			Gson gson = new Gson();
			busRealTimeInfosCollection = gson.fromJson(busCountdownMessage, BusRealTimeInfosCollection.class);
			
		} catch (Exception e) {
			if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
		}
		
		return busRealTimeInfosCollection;
	}
}
