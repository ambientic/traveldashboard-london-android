package traveldashboard.ibicooptask;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicReference;

import org.ibicoop.broker.common.BrokerConstants;
import org.ibicoop.broker.common.BrokerManager;
import org.ibicoop.broker.common.IbiTopic;
import org.ibicoop.broker.common.IbiTopicFilter;
import org.ibicoop.init.IbicoopInit;

import traveldashboard.compat.AsyncTaskCompat;
import traveldashboard.data.TubeStation;
import traveldashboard.data.IbicoopDataConstants;

import android.app.IntentService;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

public class IbicoopStationChannelSubscriptionTask extends AsyncTask<Void, Void, Boolean> {
	
	private static AtomicReference<String> currentChannelName = new AtomicReference<String>();
	private Context mContext;
	private IbicoopStartStopTask ibiStart;
	private IbicoopBrokerStartStopTask ibiBroker;
	private boolean mUnsubscribe;
	private IbiTopic mUnsubscribeTopic;
	private TubeStation mStopObj;

	private TSLContext tslApplicationContext;
	private Class<? extends IntentService> stationVoteIntentService;
	private Class<? extends IntentService> requestIntentService;
	
	public IbicoopStationChannelSubscriptionTask(Context ctx, TubeStation stopObj, TSLContext tslApplicationContext,
			Class<? extends IntentService> stationVoteIntentService, Class<? extends IntentService> requestIntentService) {
		mStopObj = stopObj;
		mContext = ctx;
		this.tslApplicationContext = tslApplicationContext;
		this.stationVoteIntentService = stationVoteIntentService;
		this.requestIntentService = requestIntentService;
	}
	
	public IbicoopStationChannelSubscriptionTask(Context ctx, IbiTopic topic,
		boolean unSubscribe) {
		mContext = ctx;
		mUnsubscribe = unSubscribe;
		mUnsubscribeTopic = topic;
	}
	
	@Override
	protected Boolean doInBackground(Void... params) {
	if (mUnsubscribe) {
		if (null == mUnsubscribeTopic)
			return unsubscribeAll();
		else
			return unsubscribe(mUnsubscribeTopic.getName());
	} else {
		return subscribe();
	}
	}
	
	private boolean subscribe() {
		
		if(isCancelled()) return false;
		
		if (!IbicoopInit.getInstance().isStarted()) return false;
		
		String newChannelName = String.format("StationCh_%s", mStopObj.getId());
		
		// we are in the same region, nothing to do
		if (currentChannelName.get() != null
				&& currentChannelName.get().equals(newChannelName)) {
			if (IbicoopDataConstants.DEBUG_MODE)  System.out.println("Station channel did not change: "
					+ currentChannelName.get());
			return true;
		} else {
			if (IbicoopDataConstants.DEBUG_MODE)  System.out.println("Station channel change:" + newChannelName);
		}
		
		// the region has changed, subscribe to a new channel
		currentChannelName.set(newChannelName);
		
		/*
		// wait for iBICOOP to start
		try {
			if (!waitForIbicoop()) {
				if (IbicoopDataConstants.DEBUG_MODE)  System.err.println("iBICOOP or broker did not start");
				return false;
			}
		} catch (ExecutionException e) {
			if (IbicoopDataConstants.DEBUG_MODE)  System.err.println("" + e.getMessage());
			return false;
		}*/
		
		// Unsubscribe from older topics
		unsubscribeAll();
		
		BrokerManager brokerManager = IbicoopInit.getInstance()
				.getBrokerManager(IbicoopDataConstants.BROKER_NAME);
		
		// Obtain the topic of this region
		IbiTopicFilter topicFilter = brokerManager.createIbiTopicFilter();
		topicFilter.setName(currentChannelName.get());
		
		IbiTopic[] currentTopics = brokerManager.getTopics(topicFilter);
		// There is no topic for this region, creating a new one
		if (currentTopics == null || currentTopics.length == 0) {
			currentTopics = new IbiTopic[1];
			currentTopics[0] = brokerManager.createIbiTopic(
					currentChannelName.get(), currentChannelName.get());
			if (currentTopics[0].registerTopic() != BrokerConstants.OK) {
				if (IbicoopDataConstants.DEBUG_MODE)  System.out.println("Could not register topic "
						+ currentTopics[0].getName());
				currentChannelName.set(null);
				return false;
			}
		}
		
		if (IbicoopDataConstants.DEBUG_MODE)  System.out.println("Station channel changed: " + currentChannelName.get());
		// Subscribe to the new topics
		for (int i = 0; i < currentTopics.length; i++) {
			if (IbicoopDataConstants.DEBUG_MODE)  System.out.println("Subscribing to topic: "
					+ currentTopics[i].getName());
			if (currentTopics[i].subscribe() != BrokerConstants.OK){
				// there should be only one topic, and subscription should never fail
				currentChannelName.set(null);
				 if (IbicoopDataConstants.DEBUG_MODE)  System.out.println("Could not subscribe to topic "
						+ currentTopics[i].getName());
			}
		}
		
	
		// sharing app-wide topic list
		for (IbiTopic ibiTopic : currentTopics) {
			tslApplicationContext.getCurrentMetroStationTopicList().put(ibiTopic.getName(), mStopObj);
		}
		
		return true;
	}
	
	private boolean unsubscribeAll() {

		if (!IbicoopInit.getInstance().isStarted()) return false;
		
		boolean success = true;

		if (tslApplicationContext == null) return false;
		
		for (String topic : tslApplicationContext.getCurrentMetroStationTopicList().keySet()) {
			boolean successTempo = unsubscribe(topic);
			
			if (IbicoopDataConstants.DEBUG_MODE)  System.out.println("Unsubscribe all : " + "topic = " + topic + ", result = " + successTempo);
			
			success &= successTempo;
		}
		return success;
	}
	
	private boolean unsubscribe(String topicName) {

		if (!IbicoopInit.getInstance().isStarted()) return false;
		
		if (IbicoopDataConstants.DEBUG_MODE)  System.out.println("Unsubscribing from topic: " + topicName);
		
		BrokerManager brokerManager = IbicoopInit.getInstance()
				.getBrokerManager(IbicoopDataConstants.BROKER_NAME);
		// Obtain the topic of this region
		IbiTopicFilter topicFilter = brokerManager.createIbiTopicFilter();
				topicFilter.setName(topicName);
		IbiTopic[] currentTopics = brokerManager.getTopics(topicFilter);
		
		if (IbicoopDataConstants.DEBUG_MODE)  System.out.println("Current topics size = " + currentTopics.length);
		
		boolean success = false;
		
		for (int i = 0; i < currentTopics.length; i++) {
			success |= (BrokerConstants.OK == currentTopics[i].unSubscribe());
			if (IbicoopDataConstants.DEBUG_MODE)  System.out.println("Unsubscribe from current topic = " +  currentTopics[i].getName() + ", result = " + success);
		}
		
		tslApplicationContext.getCurrentMetroStationTopicList().remove(topicName);
		
		if (currentChannelName.get() != null
				&& currentChannelName.get().equals(topicName)) {
			currentChannelName.set(null);
			
			if (IbicoopDataConstants.DEBUG_MODE)  System.out.println("Set channel name as null");
		}
		else {
			if (IbicoopDataConstants.DEBUG_MODE)  System.out.println("Current channel name not equal to " + topicName);
		}
		
		return success;
	}
	
	@Override
	protected void onPostExecute(Boolean result) {
		if (IbicoopDataConstants.DEBUG_MODE) Toast.makeText(mContext, "StationChannelSubscription : " +
				"subscribe = " + !mUnsubscribe + ", " +
				"result = " + result, Toast.LENGTH_SHORT).show();
		
		if (IbicoopDataConstants.DEBUG_MODE)  System.out.println("StationChannelSubscription : " +
				"subscribe = " + !mUnsubscribe + ", " +
				"result = " + result);
		
		super.onPostExecute(result);
		if(!result)
			if (IbicoopDataConstants.DEBUG_MODE)  System.out.println("Un-/subscribe FAIL");
	}
	
	private boolean waitForIbicoop() throws ExecutionException {
		
		ibiStart = new IbicoopStartStopTask(mContext, false, null, tslApplicationContext, stationVoteIntentService, requestIntentService);
		
		AsyncTaskCompat.executeParallel(ibiStart);
		
		while (true) {
			try { // the wait might get interrupted, that's why we LOOP
				if (!ibiStart.get()) {
					// iBICOOP did not start
					return false;
				}
				ibiBroker = new IbicoopBrokerStartStopTask(mContext, false, tslApplicationContext, stationVoteIntentService, requestIntentService);
				AsyncTaskCompat.executeParallel(ibiBroker);
				break;
			} catch (InterruptedException e) {
				continue;
			} catch (CancellationException e) {
				// the task was canceled
				return false;
			}
		}
	
	while (true) {
		if (isCancelled())
			return false;
		try { // the wait might get interrupted, that's why we LOOP
			if (!ibiBroker.get()) {
				// broker did not start
				return false;
			}
			return true;
		} catch (InterruptedException e) {
			continue;
		} catch (CancellationException e) {
			// the task was canceled
			return false;
		}
	}
	}

}
