package traveldashboard.ibicooptask;

import traveldashboard.data.comment.TubeStationCommentsCollection;

public interface IbicoopAllCommentsCallback {
	public void allCommentsFailed();
	public void tubeStationUserCommentsCollection(TubeStationCommentsCollection info);
	public void tubeStationTwitterCommentsCollection(TubeStationCommentsCollection info);
	public void tubeStationFacebookCommentsCollection(TubeStationCommentsCollection info);
	public void tubeStationUserCommentsCollectionFailed();
	public void tubeStationTwitterCommentsCollectionFailed();
	public void tubeStationFacebookCommentsCollectionFailed();
}
