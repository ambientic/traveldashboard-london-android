package traveldashboard.ibicooptask;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationManager;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiSender;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;

import traveldashboard.data.crowd.TubeStationCrowd;
import traveldashboard.data.IbicoopDataConstants;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

public class IbicoopClassifiedCrowdednessGetterTask extends AsyncTask<Void, Void, Boolean>{

	private Context context;
	
	private IbicoopClassifiedCrowdednessCallback callback;
	
	private long timestamp;
	
	private String city;
	private String stopId;
	private String routeId;
	
	private String crowdednessMessage;
	
	private IbiSender sender;

	public IbicoopClassifiedCrowdednessGetterTask(Context context, String city, String stopId, String routeId, String queryTime, IbicoopClassifiedCrowdednessCallback callback) {
		this.context = context;
		this.city = city;
		this.stopId = stopId;
		this.routeId = routeId;
		timestamp = convertDateStringToTimestamp(city, extractQueryTimeToCurrentTime(queryTime));		
		this.callback = callback;
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		
		if (isCancelled()) return false;
		
		
		try {
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopClassifiedCrowdednessGetter: Creating sender");
			
			IBIURL uriLocal = new IBIURL("ibiurl",
					IbicoopDataConstants.USER_CLASSIFIED_CROWDEDNESS,
					IbicoopDataConstants.TERMINAL_CLASSIFIED_CROWDEDNESS + String.valueOf(System.currentTimeMillis()),
					IbicoopDataConstants.APPLICATION, IbicoopDataConstants.CLASSIFIED_CROWDEDNESS_SERVICE,
					IbicoopDataConstants.PATH_GET_CLASSIFIED_CROWDEDNESS);
			
			IBIURL uriRemote = new IBIURL("ibiurl", IbicoopDataConstants.USER,
					IbicoopDataConstants.TERMINAL, IbicoopDataConstants.APPLICATION,
					IbicoopDataConstants.CLASSIFIED_CROWDEDNESS_SERVICE,
					IbicoopDataConstants.PATH_GET_CLASSIFIED_CROWDEDNESS);

			CommunicationOptions options = new CommunicationOptions();
			options.setCommunicationMode(new CommunicationMode(
					CommunicationConstants.MODE_PROXY));

			CommunicationManager comm = IbicoopInit.getInstance()
					.getCommunicationManager();

			sender = comm.createSender(uriLocal, uriRemote, options,
					null);

			NetworkMessage requestMessage = new NetworkMessageXml();
			requestMessage.setMsgOperation(IbicoopDataConstants.OPERATION_GET_METRO_CLASSIFIED_CROWD);
			requestMessage.setMsgType(NetworkMessage.MSG_TYPE_REQUEST);
			
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_CITY_TYPE, city);			
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_ROUTE_ID, routeId);	
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_STOP_ID, stopId);	
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_TIMESTAMP, String.valueOf(timestamp));
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Timestamp = " + timestamp);

			ResponseMessage responseMessage = new ResponseMessage();
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopClassifiedCrowdednessGetter: Going to send request");
			
			boolean ok = responseMessage.decode(sender
					.sendRequestResponse(requestMessage.encode()));
			sender.stop();

			if (ok) {
				crowdednessMessage = responseMessage.getPayload(IbicoopDataConstants.METRO_CLASSIFIED_CROWD_MESSAGE);
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopClassifiedCrowdednessGetter: Crowdedness message = " + crowdednessMessage);
				//processCrowdednessMessage();
			} else {
				return false;
			}

			return true;
		} catch(Exception e) {
			if (sender != null) sender.stop();
			if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
		}
		return false;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		
		if (result) {
			if (callback != null) { 
				//callback.crowdednessInfo(level);
				TubeStationCrowd stationCrowd = getTubeStationCrowd();
				if (stationCrowd != null) callback.tubeStationCrowdInfo(stationCrowd);
				else callback.crowdednessFailed();
			}
		} else {
			if (callback != null) callback.crowdednessFailed();
		}
	}

	protected TubeStationCrowd getTubeStationCrowd() {
		TubeStationCrowd stationCrowd = null;
		
		if (crowdednessMessage != null) {
			Gson gson = new Gson();
			stationCrowd = gson.fromJson(crowdednessMessage, TubeStationCrowd.class);
		}
		
		return stationCrowd;
	}
	
	
	/**
	 * Extract query time to current time format
	 * @param initialTime
	 * @return current time format
	 */
	public static String extractQueryTimeToCurrentTime(String initialTime) {
		
		String newTime = "";
			
		String hour = initialTime.substring(0, 2);
		String minute = initialTime.substring(2);

		String formatDate = "yyyy-MM-dd'T'";
		SimpleDateFormat sdfDate = new SimpleDateFormat(formatDate);
		String date = sdfDate.format(new Date());
		
		String formatSecond = ":ss.SSS'Z'";
		SimpleDateFormat sdfSecond = new SimpleDateFormat(formatSecond);
		String second = sdfSecond.format(new Date());
		
		newTime = date + hour + ":" + minute + second;
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println(">>>>New time = " + newTime);
		
		return newTime;
	}
	
	/**
	 * Convert current date string to timestamp
	 * @param dateString
	 * @return timestamp
	 */
	public static long convertDateStringToTimestamp(String city, String dateString) {
		
		long timestamp = -1l;
		
		try {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Date string = " + dateString);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			Date date = sdf.parse(dateString);
			timestamp = date.getTime();
			
		} catch (Exception e) {
			if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
		}
		
		return timestamp;
		
	}
}
