package traveldashboard.ibicooptask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationManager;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiSender;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;

import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.data.info.TramRealTimeInfosCollection;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

/**
 * To get the tram real time countdown
 * @author khoo
 *
 */
public class IbicoopTramCountdownTask extends AsyncTask<Void, Void, Boolean> {

	private Context context;
	
	private IbicoopTramCountdownCallback callback;
	
	private String city;
	private String stopId;
	private String routeId;
	
	private String tramCountdownMessage;
	private int maxValues;

	private IbiSender sender;
	
	public IbicoopTramCountdownTask(Context context,  String city, String stopId, String routeId, int maxValues, IbicoopTramCountdownCallback callback) {
		this.context = context;
		this.city = city;
		this.routeId = routeId;
		this.stopId = stopId;
		this.maxValues = maxValues;
		this.callback = callback;
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		
		if (isCancelled()) return false;
		
		
		try {
			
			IBIURL uriLocal = new IBIURL("ibiurl",
					IbicoopDataConstants.USER_TRAM_COUNTDOWN,
					IbicoopDataConstants.TERMINAL_TRAM_COUNTDOWN  + String.valueOf(System.currentTimeMillis()),
					IbicoopDataConstants.APPLICATION, IbicoopDataConstants.TRAM_COUNTDOWN_SERVICE,
					IbicoopDataConstants.PATH_GET_TRAM_COUNTDOWN);
			
			IBIURL uriRemote = new IBIURL("ibiurl", IbicoopDataConstants.USER,
					IbicoopDataConstants.TERMINAL, IbicoopDataConstants.APPLICATION,
					IbicoopDataConstants.TRAM_COUNTDOWN_SERVICE,
					IbicoopDataConstants.PATH_GET_TRAM_COUNTDOWN);

			CommunicationOptions options = new CommunicationOptions();
			options.setCommunicationMode(new CommunicationMode(
					CommunicationConstants.MODE_PROXY));

			CommunicationManager comm = IbicoopInit.getInstance()
					.getCommunicationManager();

			sender = comm.createSender(uriLocal, uriRemote, options,
					null);

			NetworkMessage requestMessage = new NetworkMessageXml();
			requestMessage.setMsgOperation(IbicoopDataConstants.OPERATION_GET_TRAM_COUNTDOWN);
			requestMessage.setMsgType(NetworkMessage.MSG_TYPE_REQUEST);
			
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_CITY_TYPE, city);
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_STOP_ID, stopId);
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_ROUTE_ID, routeId);
			requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_MAX_VALUES, String.valueOf(maxValues));

			ResponseMessage responseMessage = new ResponseMessage();
			boolean ok = responseMessage.decode(sender
					.sendRequestResponse(requestMessage.encode()));
			sender.stop();

			if (!ok)
				return false;

			tramCountdownMessage = responseMessage.getPayload(IbicoopDataConstants.TRAM_COUNTDOWN_MESSAGE);

			return true;
		} catch(Exception e) {
			if (sender != null) sender.stop();
			if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
		}
		return false;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		
		if (result) {
			if (callback != null) { 
				TramRealTimeInfosCollection tramRealTimeInfosCollection = getTramRealTimeInfosCollection(); 
				if (tramRealTimeInfosCollection != null) callback.tramRealtimeInfo(tramRealTimeInfosCollection);
				else callback.tramCountdownFailed();
			}
		} else {
			if (callback != null) callback.tramCountdownFailed();
		}
	}

	protected TramRealTimeInfosCollection getTramRealTimeInfosCollection() {
		TramRealTimeInfosCollection tramRealTimeInfosCollection = null;
		
		if (tramCountdownMessage != null) {
			Gson gson = new Gson();
			tramRealTimeInfosCollection = gson.fromJson(tramCountdownMessage, TramRealTimeInfosCollection.class);
		}
		
		return tramRealTimeInfosCollection;
	}
}
