package traveldashboard.ibicooptask;

public interface IbicoopStartCallback {
	public void ibicoopStartOk();
	public void ibicoopStartFailed();
}
