package traveldashboard.ibicooptask;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.Queue;

import org.ibicoop.broker.common.BrokerConstants;
import org.ibicoop.broker.common.BrokerManager;
import org.ibicoop.broker.common.IbiTopic;
import org.ibicoop.broker.common.IbiTopicEvent;
import org.ibicoop.broker.common.IbiTopicFilter;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;

import traveldashboard.data.IbicoopDataConstants;

import android.content.Context;
import android.os.AsyncTask;

/**
 * Report rating task
 * @author khoo
 *
 */
public class IbicoopReportRatingTask extends AsyncTask<Void, Void, Boolean>{
	
	//Android activity context
	private Context context;
	
	//
	//
	//Cached topic list
	private static IbiTopic[] topicList;
	//Rating queue
	private static Queue<byte[]> persistentRatingEventCache;
	
	public IbicoopReportRatingTask(Context context, String city, String routeId, String userName, int rating) {
		this.context = context;
		
		if (persistentRatingEventCache == null) {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportRatingTask : Create persistentRatingEventCache");
			//Create persistentRatingEventCache
			readRatingEventCached();
		}
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportRatingTask : CreateRating data message: " +
				"routeId = " + routeId + ", userName = " + userName + ", rating " + rating
				+ ", persistentRatingEventCache size = " + persistentRatingEventCache.size());
		NetworkMessage RatingDataMessage = new NetworkMessageXml();
		RatingDataMessage.addPayload(IbicoopDataConstants.PARAM_KEY_CITY_TYPE, city);
		RatingDataMessage.addPayload(IbicoopDataConstants.PARAM_KEY_ROUTE_ID, routeId);
		RatingDataMessage.addPayload(IbicoopDataConstants.PARAM_KEY_USER_NAME, userName);
		RatingDataMessage.addPayload(IbicoopDataConstants.PARAM_KEY_RATING, String.valueOf(rating));	
		persistentRatingEventCache.add(RatingDataMessage.encode());
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportRatingTask : Insert Rating data into persistentRatingEventCache, " +
				" new persistentRatingEventCache size = " + persistentRatingEventCache.size());	
	}
	
	@Override
	protected Boolean doInBackground(Void... params) {
		
		
		boolean publishSuccess = false;
		
		try {
			
			if (isCancelled()) return false;
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportRatingTask : starting!");
			
			BrokerManager brokerManager = IbicoopInit.getInstance()
					.getBrokerManager(IbicoopDataConstants.BROKER_NAME);
			
			if (topicList == null) {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportRatingTask : Create topic list!");
				IbiTopicFilter topicFilter = brokerManager.createIbiTopicFilter();
				topicFilter.setName(IbicoopDataConstants.TOPIC_NAME_RATING);
				topicFilter.setType(IbicoopDataConstants.TOPIC_TYPE_RATING);
				IbicoopReportRatingTask.topicList = brokerManager.getTopics(topicFilter);
			}
			
			while (!persistentRatingEventCache.isEmpty()) {
				//Publish all saved Rating data
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportRatingTask : Publish Rating data!");
				byte[] RatingData = persistentRatingEventCache.peek();
				publishSuccess = false;
				
				for (IbiTopic topic : topicList) {
					IbiTopicEvent RatingEvent = brokerManager.createIbiEvent(
							IbicoopDataConstants.EVENT_NAME_RATING,
							IbicoopDataConstants.EVENT_DESC_RATING,
							RatingData);
					
					int result = topic.publishEvent(RatingEvent);
					
					if (result == BrokerConstants.OK) {
						if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportRatingTask : Publish event ok");
						publishSuccess = true;
					} else {
						if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IbicoopReportRatingTask : Publish event not ok");
					}
				}
				
				if (publishSuccess) {
					persistentRatingEventCache.remove(RatingData);
					if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportRatingTask : Publish success! Remove rating data from persistentRatingEventCache!" +
							" current size = " + persistentRatingEventCache.size());
				} else {
					if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IbicoopReportRatingTask : Publish failed!");
					break;
				}
			}
		} catch(Exception exception) {
			if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IbicoopReportRatingTask : " + exception.getMessage());
			publishSuccess = false;
		}
		
		writeRatingEventCached();
		
		return publishSuccess;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportRatingTask : ending!");
		super.onPostExecute(result);
	}
	
	/**
	 * Read Rating event cached
	 */
	@SuppressWarnings({ "unchecked"})
	private void readRatingEventCached() {
		try {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportRatingTask : read rating event cached!");
			// Load the persistent cache if empty
			File cacheDirectory = context.getCacheDir();
			if (cacheDirectory == null) {
				throw new IOException("The local cache is null! Cannot store the events file!");
			}
			
			File cacheFile = new File(cacheDirectory, "persistentRatingEventCache.bin");
			if (cacheFile.exists()) {
				persistentRatingEventCache = (Queue<byte[]>) new ObjectInputStream(new FileInputStream(cacheFile)).readObject();
			}
			
		} catch (Exception exception) {
			if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IbicoopReportRatingTask : Cannot create persistentRatingEventCache: " 
					+ exception.getMessage());
		} finally {
			if (persistentRatingEventCache == null) {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportRatingTask : create new Rating event cached!");
				persistentRatingEventCache = new LinkedList<byte[]>();
			} else {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportRatingTask : got new Rating event cached!");
			}
		}
	}
	
	@SuppressWarnings("resource")
	private void writeRatingEventCached() {
		try {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportRatingTask : write rating event cached!");
			File cacheDirectory = context.getCacheDir();
			if (cacheDirectory == null) {
				throw new IOException("The local cache is null! Cannot store the events file!");
			}
			
			File cacheFile = new File(cacheDirectory, "persistentRatingEventCache.bin");
			
			if (cacheFile.exists()) {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopReportRatingTask : delete previous rating event cached!");
				cacheFile.delete();
			}
			new ObjectOutputStream(new FileOutputStream(cacheFile)).writeObject(persistentRatingEventCache);
		} catch (Exception exception) {
			if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IbicoopReportRatingTask : Cannot write persistentRatingEventCache: " 
					+ exception.getMessage());
		}
	}
}
