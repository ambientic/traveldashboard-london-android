package traveldashboard.ibicooptask;

import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationManager;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiSender;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;

import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.data.status.TubeStatusCollection;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

public class IbicoopAllTubeStatusTask extends AsyncTask<Void, Void, Boolean> {

	private Context context;
	
	private IbicoopAllTubeStatusCallback callback;
	
	private String city;
	
	private String statusMessage;
	
	private IbiSender sender;
		
	public IbicoopAllTubeStatusTask(Context context, String city, IbicoopAllTubeStatusCallback callback) {
		this.context = context;
		this.city = city;
		this.callback = callback;
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		
		if (isCancelled()) return false;
		

		try {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopAllTubeStatusTask : Create all tube status sender!");
			
				IBIURL uriLocal = new IBIURL("ibiurl",
						IbicoopDataConstants.USER_ALL_TUBE_STATUS,
						IbicoopDataConstants.TERMINAL_ALL_TUBE_STATUS  + String.valueOf(System.currentTimeMillis()),
						IbicoopDataConstants.APPLICATION, IbicoopDataConstants.ALL_TUBE_STATUS_SERVICE,
						IbicoopDataConstants.PATH_GET_TUBE_STATUS);
				
				IBIURL uriRemote = new IBIURL("ibiurl", IbicoopDataConstants.USER,
						IbicoopDataConstants.TERMINAL, IbicoopDataConstants.APPLICATION,
						IbicoopDataConstants.ALL_TUBE_STATUS_SERVICE,
						IbicoopDataConstants.PATH_GET_ALL_TUBE_STATUS);
		
				CommunicationOptions options = new CommunicationOptions();
				options.setCommunicationMode(new CommunicationMode(
						CommunicationConstants.MODE_PROXY));
		
				CommunicationManager comm = IbicoopInit.getInstance()
						.getCommunicationManager();
		
				sender = comm.createSender(uriLocal, uriRemote, options,
						null);
				
				NetworkMessage requestMessage = new NetworkMessageXml();
				requestMessage.setMsgOperation(IbicoopDataConstants.OPERATION_GET_ALL_TUBE_STATUS);
				requestMessage.setMsgType(NetworkMessage.MSG_TYPE_REQUEST);
				
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_CITY_TYPE, city);

				ResponseMessage responseMessage = new ResponseMessage();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopTubeStatusTask : Going to send all tube status request!");
				
				boolean ok = false;
				
				try {
					ok = responseMessage.decode(sender
							.sendRequestResponse(requestMessage.encode()));
					
					if (ok) statusMessage = responseMessage.getPayload(IbicoopDataConstants.ALL_TUBE_STATUS_MESSAGE);
					
				} catch (Exception e) {
					if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
				}

				sender.stop();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopAllTubeStatusTask : Stop tube status sender!");
		
				return ok;
			
			} catch(Exception e) {
				if (sender != null) sender.stop();
				if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
			}
			
			return false;
			
	}

	
	@Override
	protected void onPostExecute(Boolean result) {
		
		if (result) {
			if (callback != null) {
				//Return tube status collection object
				TubeStatusCollection statusCollection = processTubeStatus();
				if (statusCollection != null) callback.tubeStatusCollectionInfo(statusCollection);
				else callback.tubeStatusFailed();
			}
		} else {
			if (callback != null) callback.tubeStatusFailed();
		}
	}
	
	protected TubeStatusCollection processTubeStatus() {
		//Process tube status
		TubeStatusCollection tubeStatusCollection = null;
		
		if ((statusMessage == null) || (statusMessage.equals("null"))) return tubeStatusCollection; 
			
			Gson gson = new Gson();
			tubeStatusCollection = gson.fromJson(statusMessage, TubeStatusCollection.class);
		
		return tubeStatusCollection;
	}
}
	

