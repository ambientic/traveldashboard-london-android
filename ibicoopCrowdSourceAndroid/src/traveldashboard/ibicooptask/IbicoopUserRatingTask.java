package traveldashboard.ibicooptask;

import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationManager;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiSender;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;

import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.data.rating.TubeRating;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

public class IbicoopUserRatingTask extends AsyncTask<Void, Void, Boolean> {

	private Context context;
	
	private IbicoopTubeRatingCallback callback;

	private String routeId;
	
	private String userRatingMessage;
	
	private TSLContext tslApplicationContext;
	
	private IbiSender sender;
	
	public IbicoopUserRatingTask(Context context, TSLContext tslApplicationContext, 
			String routeId, 
			IbicoopTubeRatingCallback callback) {
		this.context = context;
		this.tslApplicationContext = tslApplicationContext;
		this.routeId = routeId;
		this.callback = callback;
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		
		if (isCancelled()) return false;
		if (!tslApplicationContext.checkIfUserIsLoggedIn()) return false;
		
		try {
				boolean ok = false;
					
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopUserRatingTask : Create user tube rating sender!");
				
				IBIURL uriLocal = new IBIURL("ibiurl",
						IbicoopDataConstants.USER_USER_RATING,
						IbicoopDataConstants.TERMINAL_USER_RATING + String.valueOf(System.currentTimeMillis()),
						IbicoopDataConstants.APPLICATION, IbicoopDataConstants.USER_RATING_SERVICE,
						IbicoopDataConstants.PATH_GET_USER_RATING);
				
				IBIURL uriRemote = new IBIURL("ibiurl", IbicoopDataConstants.USER,
						IbicoopDataConstants.TERMINAL, IbicoopDataConstants.APPLICATION,
						IbicoopDataConstants.USER_RATING_SERVICE,
						IbicoopDataConstants.PATH_GET_USER_RATING);
		
				CommunicationOptions options = new CommunicationOptions();
				options.setCommunicationMode(new CommunicationMode(
						CommunicationConstants.MODE_PROXY));
		
				CommunicationManager comm = IbicoopInit.getInstance()
						.getCommunicationManager();
		
				sender = comm.createSender(uriLocal, uriRemote, options,
						null);
				
				NetworkMessage requestMessage = new NetworkMessageXml();
				
				requestMessage.setMsgOperation(IbicoopDataConstants.OPERATION_GET_USER_RATING);
				
				requestMessage.setMsgType(NetworkMessage.MSG_TYPE_REQUEST);
				
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_USER_NAME, tslApplicationContext.currentSHA1EmailAddress());
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_ROUTE_ID, routeId);
				
				ResponseMessage responseMessage = new ResponseMessage();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopUserRatingTask : Going to send user rating request!");
				
				try {
					ok = responseMessage.decode(sender
							.sendRequestResponse(requestMessage.encode()));
					
					if (ok) userRatingMessage = responseMessage.getPayload(IbicoopDataConstants.USER_RATING_MESSAGE);
					
				} catch (Exception e) {
					if (IbicoopDataConstants.DEBUG_MODE)  e.printStackTrace();
				}
				
				sender.stop();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopUserRatingTask : Stop user rating sender!");
			
				return ok;
			} catch(Exception e) {
				if (sender != null) sender.stop();
				if (IbicoopDataConstants.DEBUG_MODE)  e.printStackTrace();
			}
			
			return false;
	}

	
	@Override
	protected void onPostExecute(Boolean result) {
		
		if (result) {
			if (callback != null) {				
				//Return tube rating object
				TubeRating rating =  processUserTubeRatings();
				if (rating != null) callback.tubeRatingInfo(rating);
				else callback.tubeRatingFailed();
			}
		} else {
			if (callback != null) callback.tubeRatingFailed();
		}
	}
		
	protected TubeRating processUserTubeRatings() {
		
		TubeRating tubeRating = null;
		
		if ((userRatingMessage == null) || (userRatingMessage == "null")) return tubeRating;
	
		Gson gson = new Gson();
		tubeRating = gson.fromJson(userRatingMessage, TubeRating.class);
		
		//tslApplicationContext.submitRatingForLine(tubeRating.getTube().getId(), (float) tubeRating.getRating().getRatingLevel().getRating());			
	
		return tubeRating;
	}
}
