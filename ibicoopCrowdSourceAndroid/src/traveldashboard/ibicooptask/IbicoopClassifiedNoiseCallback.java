package traveldashboard.ibicooptask;

import traveldashboard.data.noise.NoiseData;

public interface IbicoopClassifiedNoiseCallback {
	public void getNoiseFailed();
	public void getNoiseInfo(NoiseData noiseData);
}
