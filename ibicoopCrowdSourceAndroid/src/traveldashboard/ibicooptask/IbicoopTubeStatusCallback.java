package traveldashboard.ibicooptask;

import traveldashboard.data.status.TubeStatus;

public interface IbicoopTubeStatusCallback {
	public void tubeStatusFailed();
	public void tubeStatusInfo(TubeStatus tubeStatus);
}
