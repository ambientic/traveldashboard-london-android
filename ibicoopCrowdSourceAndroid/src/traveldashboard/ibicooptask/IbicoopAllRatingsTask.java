package traveldashboard.ibicooptask;

import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationManager;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiSender;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;

import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.data.rating.TubeRatingsCollection;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

public class IbicoopAllRatingsTask extends AsyncTask<Void, Void, Boolean> {

	private Context context;
	
	private IbicoopAllRatingsCallback callback;
	
	private String allRatingsMessage;

	private String city;
	
	private IbiSender sender;
	
	public IbicoopAllRatingsTask(Context context, String city, IbicoopAllRatingsCallback callback) {
		this.context = context;
		this.callback = callback;
		this.city = city;
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		
		if (isCancelled()) return false;
		
		try {
				boolean ok1 = false;
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopTask : Create all ratings sender!");
			
				IBIURL 	uriLocal = new IBIURL("ibiurl",
						IbicoopDataConstants.USER_ALL_TUBE_RATING,
						IbicoopDataConstants.TERMINAL_ALL_TUBE_RATING  + String.valueOf(System.currentTimeMillis()),
						IbicoopDataConstants.APPLICATION, IbicoopDataConstants.ALL_TUBE_RATING_SERVICE,
						IbicoopDataConstants.PATH_GET_ALL_TUBE_RATING); 
				
				
				IBIURL 	uriRemote = new IBIURL("ibiurl", IbicoopDataConstants.USER,
						IbicoopDataConstants.TERMINAL, IbicoopDataConstants.APPLICATION,
						IbicoopDataConstants.ALL_TUBE_RATING_SERVICE,
						IbicoopDataConstants.PATH_GET_ALL_TUBE_RATING);
		
				CommunicationOptions options = new CommunicationOptions();
				options.setCommunicationMode(new CommunicationMode(
						CommunicationConstants.MODE_PROXY));
		
				CommunicationManager comm = IbicoopInit.getInstance()
						.getCommunicationManager();
		
				sender = comm.createSender(uriLocal, uriRemote, options,
						null);
				
				NetworkMessage requestMessage = new NetworkMessageXml();
				requestMessage.setMsgOperation(IbicoopDataConstants.OPERATION_GET_ALL_TUBE_RATING);
				
				requestMessage.setMsgType(NetworkMessage.MSG_TYPE_REQUEST);
				
				requestMessage.addPayload(IbicoopDataConstants.PARAM_KEY_CITY_TYPE, city);

				ResponseMessage responseMessage = new ResponseMessage();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("IbicoopRatingsTask : Going to send tube status request!");
				
				try {
					ok1 = responseMessage.decode(sender
							.sendRequestResponse(requestMessage.encode()));
					
					if (ok1) allRatingsMessage = responseMessage.getPayload(IbicoopDataConstants.ALL_TUBE_RATING_MESSAGE);
					
				} catch (Exception e) {
					if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
				}
				
				sender.stop();
				
				return (ok1);
			
			} catch(Exception e) {
				if (sender != null) sender.stop();
				if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
			}
			
			return false;
			
	}

	
	@Override
	protected void onPostExecute(Boolean result) {
		
		if (result) {
			if (callback != null) {
				TubeRatingsCollection ratingsCollection =  processTubeRatings();
				if (ratingsCollection != null) callback.tubeRatingsCollectionInfo(ratingsCollection);
				else callback.tubeRatingFailed();
			}
		} else {
			if (callback != null) callback.tubeRatingFailed();
		}
	}
	
	
	protected TubeRatingsCollection processTubeRatings() {
		//Process tube ratings
		TubeRatingsCollection tubeRatingsCollection = null;
		
		if ((allRatingsMessage == null) || (allRatingsMessage.equals("null"))) return tubeRatingsCollection;
		
			Gson gson = new Gson();
			tubeRatingsCollection = gson.fromJson(allRatingsMessage, TubeRatingsCollection.class);
		
		return tubeRatingsCollection;
	}


}