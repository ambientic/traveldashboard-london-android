package traveldashboard.ibicooptask;


import traveldashboard.data.weather.Weather;

public interface IbicoopWeatherCallback {
	public void weatherFailed();
	public void weatherObject(Weather weather);
}
