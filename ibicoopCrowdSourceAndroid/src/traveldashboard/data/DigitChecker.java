package traveldashboard.data;

public class DigitChecker {

	public static boolean containsDigit(String word) {
		
		return (
				word.contains("1") || 
				word.contains("2") ||
				word.contains("3") ||
				word.contains("4") ||
				word.contains("5") ||
				word.contains("6") ||
				word.contains("7") ||
				word.contains("8") ||
				word.contains("9") ||
				word.contains("9")
				);
	}
}
