package traveldashboard.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.io.InputStream;
import java.io.InputStreamReader;

import traveldashboard.data.IbicoopDataConstants;


import au.com.bytecode.opencsv.CSVReader;

public class LondonCsvMessageParser {
	
	public static synchronized ArrayList<LondonMetroStopInfo> parseMessage(InputStream[] stationsIs) {
		
		ArrayList<LondonMetroStopInfo> londonMetroStopInfos = new ArrayList<LondonMetroStopInfo>();
		
		try {
			
			for (int index = 0; index < 8; index ++)  {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("LondonCsvMessageParser: index = " + index);
				
				CSVReader reader = new CSVReader(new InputStreamReader(stationsIs[index]));
				List<String[]> stations = reader.readAll();
				
				for (int i = 1; i < stations.size(); i++) {
					//Ignore first row
					String[] station = stations.get(i);
					
					if (IbicoopDataConstants.DEBUG_MODE) System.out.println("LondonCsvMessageParser: station = " + station.toString());
					
					if (!station[1].trim().equalsIgnoreCase("*") && !station[2].trim().equalsIgnoreCase("*")) {
						
						String stopName = station[0];
						String stopNlc = station[1].trim();
						String stopId = station[2].trim();
						
						HashMap<String, String> routeIdsWithNamesMap = new HashMap<String, String>();
						HashMap<String, String> routeIdsWithDirectionsMap = new HashMap<String, String>();			
						List<String> routeIds = new ArrayList<String>();
						
						for (int j = 3; j <= 7; j++) {
							String routeId = station[j];
							String routeName = IbicoopDataConstants.LONDON_ROUTEID_ROUTENAME_MAP.get(routeId);
							String routeDirection = IbicoopDataConstants.LONDON_TUBE_DESTINATIONS.get(routeName);
							
							routeIdsWithNamesMap.put(routeId, routeName);
							routeIdsWithDirectionsMap.put(routeId, routeDirection);
							routeIds.add(routeId);
						}
						
						double lat = Double.parseDouble(station[8].trim());
						double lon = Double.parseDouble(station[9].trim());
						
						LondonMetroStopInfo londonMetroStopInfo = new LondonMetroStopInfo(stopId, stopName, stopNlc, routeIdsWithNamesMap, routeIdsWithDirectionsMap, lat, lon);
						
						if (IbicoopDataConstants.DEBUG_MODE) System.out.println("LondonCsvMessageParser: " + londonMetroStopInfo.toString());
						
						londonMetroStopInfos.add(londonMetroStopInfo);
					}
				}
				
				reader.close();
			}
			
		} catch (Exception exception) {
			if (IbicoopDataConstants.DEBUG_MODE) System.err.println("LondonCsvMessageParser : " + exception.getMessage());
		}
		
		return londonMetroStopInfos;
	}
}
