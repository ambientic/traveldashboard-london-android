package traveldashboard.data;

public class BoundingArea {

	private double minLat = 0.0;
	private double maxLat = 0.0;
	private double minLon = 0.0;
	private double maxLon = 0.0;
	
	public BoundingArea(double latitude, double longitude, double radius) {
		updateBoundingArea(latitude, longitude, radius);
	}
	
	public void updateBoundingArea(double latitude, double longitude, double radius) {
        maxLat = latitude + (radius/111000);
        minLat = latitude - (radius/111000);
        maxLon = longitude + (radius/72000);
        minLon = longitude - (radius/72000);
	}
	
	public boolean outOfTheArea(double latitude, double longitude) {
		boolean out = false;
		
		if ((latitude > maxLat) || (latitude < minLat) || (longitude > maxLon) || (longitude < minLon)) {
			out = true;
		}
		
		return out;
	}
	
	public double getMinLat() {
		return minLat;
	}
	
	public double getMaxLat() {
		return maxLat;
	}
	
	public double getMinLon() {
		return minLon;
	}
	
	public double getMaxLon() {
		return maxLon;
	}
	
	@Override
	public String toString() {
		return "minLat = " + minLat 
				+ ", maxLat = " + maxLat
				+ ", minLon = " + minLon
				+ ", maxLon = " + maxLon;
	}
	
}
