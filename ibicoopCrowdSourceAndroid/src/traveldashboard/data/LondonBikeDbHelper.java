package traveldashboard.data;

import traveldashboard.data.IbicoopDataConstants;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public class LondonBikeDbHelper extends SQLiteOpenHelper {
	// If we change the database schema, we must increment the database version.
	public static final int DATABASE_VERSION = 1;
	public static final String DATABASE_NAME_PARIS = "TravelDashboardPariBikes.db";
	public static final String DATABASE_NAME_LONDON = "TravelDashboardLondonBike.db";
	
	/**
	 * Get database name according to city
	 * @param city
	 * @return
	 */
	public static String getDatabaseName(String city) {
		
		if (city.equals(IbicoopDataConstants.PARIS)) {
			return DATABASE_NAME_PARIS;
		} else if (city.equals(IbicoopDataConstants.LONDON)) {
			return DATABASE_NAME_LONDON;
		}
		
		return "";
	}
	
	//BaseColums inner class
	public static abstract class BikeDbEntry implements BaseColumns {
		//Table name
		public static final String TABLE_NAME = "bikeStops";
		//Table column names
	    public static final String PARAM_KEY_STOP_ID = "id";
	    public static final String PARAM_KEY_STOP_NAME = "name";
	    public static final String PARAM_KEY_STOP_LAT = "lat";
	    public static final String PARAM_KEY_STOP_LON = "long";

	    public static final String PARAM_KEY_NB_BIKES = "nbBikes";
	    public static final String PARAM_KEY_NB_EMPTY_DOCKS = "nbEmptyDocks";
	    public static final String PARAM_KEY_NB_DOCKS = "nbDocks";
	    public static final String PARAM_KEY_LOCKED = "locked";
	    
	    public static final String COLUMN_NAME_NULLABLE = "Nullable";
	}
    
    //Data type
    private static final String TEXT_TYPE = " TEXT";
    private static final String REAL_TYPE = " REAL";
    private static final String COMMA_SEP = ",";
	
	private static final String SQL_CREATE_ENTRIES =
			"CREATE TABLE IF NOT EXISTS " + BikeDbEntry.TABLE_NAME + " (" +
			BikeDbEntry._ID + " INTEGER PRIMARY KEY," + 
			BikeDbEntry.PARAM_KEY_STOP_NAME + TEXT_TYPE + COMMA_SEP +
			BikeDbEntry.PARAM_KEY_STOP_ID + TEXT_TYPE + COMMA_SEP +			
			BikeDbEntry.PARAM_KEY_STOP_LON + REAL_TYPE + COMMA_SEP +
			BikeDbEntry.PARAM_KEY_STOP_LAT + REAL_TYPE + COMMA_SEP +
			
			BikeDbEntry.PARAM_KEY_NB_BIKES + TEXT_TYPE + COMMA_SEP +
			BikeDbEntry.PARAM_KEY_NB_EMPTY_DOCKS + TEXT_TYPE + COMMA_SEP +
			BikeDbEntry.PARAM_KEY_NB_DOCKS + TEXT_TYPE + COMMA_SEP +
			BikeDbEntry.PARAM_KEY_LOCKED + TEXT_TYPE + ")";
	
	private static final String SQL_DELETE_ENTRIES =
			"DROP TABLE IF EXISTS " + BikeDbEntry.TABLE_NAME;
	
	public LondonBikeDbHelper(Context context) {
		super(context, getDatabaseName(IbicoopDataConstants.LONDON), null, DATABASE_VERSION);
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Created London BikeHelper");
		//Get readable and writable so that the database is created really on mobile
		SQLiteDatabase readableDb =  getReadableDatabase();
		SQLiteDatabase writableDb = getWritableDatabase();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("db.execSQL(" + SQL_CREATE_ENTRIES + ")");
		db.execSQL(SQL_CREATE_ENTRIES);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("db.execSQL(" + SQL_DELETE_ENTRIES + ")");
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
	}
	
	@Override
	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		onUpgrade(db, oldVersion, newVersion);
	}
}
