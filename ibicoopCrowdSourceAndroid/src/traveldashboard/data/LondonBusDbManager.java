package traveldashboard.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import traveldashboard.data.IbicoopDataConstants;


import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class LondonBusDbManager {
	private static final String DB_NAME = "londonBus.db";
	
	private static final String TABLE_NAME = "londonBusTable";
	private static final String PARAM_KEY_STOP_LAT = "stopLat";
	private static final String PARAM_KEY_STOP_LON = "stopLon";
	private static final String PARAM_KEY_STOP_ID = "stopId";
	private static final String PARAM_KEY_STOP_NAME = "stopName";	
	
	private Context context;
	private LondonBusDbHelper busDbHelper;
	
	public LondonBusDbManager(Context context) {
		this.context = context;
		busDbHelper = new LondonBusDbHelper(context);
		
        try {
        	busDbHelper.createDataBase(context);
 
 	} catch (IOException ioe) {
 
 		throw new Error("Unable to create database");
 
 	}
 
 	try {
 
 		busDbHelper.openDataBase();
 
 	}catch(SQLException sqle){
 
 		throw sqle;
 
 	}
	}
	
	public int getStopInfoDbCount() {
		int count = -1;
		Cursor cursor = busDbHelper.getReadableDatabase().rawQuery("select * from " + TABLE_NAME, null);
		if (cursor != null) {
			count = cursor.getCount();
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Db table count = " + count);
			cursor.close();
		}
		return count;
	}
	
	public void testDb(String stopId) {
		
		SQLiteDatabase readableDb =  busDbHelper.getReadableDatabase();
		
	    Cursor ti = readableDb.rawQuery("PRAGMA table_info(" + TABLE_NAME + ")", null);
	    if ( ti.moveToFirst() ) {
	        do {
	            System.out.println("col: " + ti.getString(1));
	        } while (ti.moveToNext());
	    }

		String query =  "select * " 
				+ " from " + TABLE_NAME + 	
				" where " + PARAM_KEY_STOP_ID + "='" + stopId+ "'";

		Cursor cursor = readableDb.rawQuery(query, null);

		System.out.println("cursor = " + cursor.toString() + ", count = " + cursor.getCount());
	}	
	
	public ArrayList<LondonBusStopInfo> readFromDbByCoord(double latitude, double longitude, double radius) {
		
		ArrayList<LondonBusStopInfo> londonBusStopInfos = new ArrayList<LondonBusStopInfo >();
		
        BoundingArea boundingArea = new BoundingArea(latitude, longitude, radius);
        double maxLat = boundingArea.getMaxLat();
        double minLat = boundingArea.getMinLat();
        double maxLon = boundingArea.getMaxLon();
        double minLon = boundingArea.getMinLon();
		
        if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Read from db by coord:");
        if (IbicoopDataConstants.DEBUG_MODE) System.out.println("lat = " + latitude + ", lon = " + longitude + ", radius = " + radius
        		+ ", maxLat = " + maxLat + ", minLat = " + minLat 
        		+ ", maxLon = " + maxLon + ", minLon = " + minLon);
		
		String query =  "select * from " + TABLE_NAME + 	" where " + 
				"(" + PARAM_KEY_STOP_LAT + " >= " +  minLat + ") and " +
				"(" + PARAM_KEY_STOP_LAT + " <= " +  maxLat + ") and " +					
				"(" + PARAM_KEY_STOP_LON + " >= " +  minLon + ") and " +
				"(" + PARAM_KEY_STOP_LON + " <= " +  maxLon + ")	";
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Query = " + query);
		
		Cursor cursor = busDbHelper.getReadableDatabase().rawQuery(query, null);
		
		if (cursor != null) {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("cursor count = " + cursor.getCount());
			
			cursor.moveToFirst();
			
			for (int i = 0;  i < cursor.getCount(); i++) {
				
				String id = cursor.getString(cursor.getColumnIndexOrThrow(PARAM_KEY_STOP_ID));				
				String stopName = cursor.getString(cursor.getColumnIndexOrThrow(PARAM_KEY_STOP_NAME));
				double lat = cursor.getDouble(cursor.getColumnIndexOrThrow(PARAM_KEY_STOP_LAT));
				double lon = cursor.getDouble(cursor.getColumnIndexOrThrow(PARAM_KEY_STOP_LON));
				
				HashMap<String, String> routeIdsWithNamesMap = new HashMap<String, String>();
				HashMap<String, String> routeIdsWithDirectionsMap = new HashMap<String, String>();			
				List<String> routeIds = new ArrayList<String>();

				for (int j= 1; j <= 20; j++) {
					String routeId = cursor.getString(cursor.getColumnIndexOrThrow("busId" + j));
					String routeName = cursor.getString(cursor.getColumnIndexOrThrow("busName" + j));
					String routeDirection = cursor.getString(cursor.getColumnIndexOrThrow("busDest" + j));
					routeIdsWithNamesMap.put(routeId, routeName);
					routeIdsWithDirectionsMap.put(routeId, routeDirection);
					routeIds.add(routeId);
				}

				LondonBusStopInfo londonBusStopInfo = new LondonBusStopInfo(id, stopName, routeIdsWithNamesMap, routeIdsWithDirectionsMap, lat, lon);
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("readFromDb : " + londonBusStopInfo.toString());
				
				londonBusStopInfos.add(londonBusStopInfo);
				
				//Next row
				cursor.moveToNext();
			}
			
			cursor.close();
		}

		
		return londonBusStopInfos;
	}
	
}
