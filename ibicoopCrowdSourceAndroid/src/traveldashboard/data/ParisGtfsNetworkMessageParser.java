package traveldashboard.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.utils.StringUtils;

import traveldashboard.data.IbicoopDataConstants;



public class ParisGtfsNetworkMessageParser {
	
	public static synchronized ArrayList<ParisStopInfo> parseMessage(byte[] data) {
		
		
		// Parsing the response
		NetworkMessage responseMessage = new NetworkMessageXml();
		boolean result = responseMessage.decode(data);

		if (IbicoopDataConstants.DEBUG_MODE) System.out.println( "decode result = " + result);
		
		if (!result) {
			return null;
		}
		
		Vector<byte[]> stopIdVector = responseMessage.getKeyVector();
		HashMap<String, String> stopInfoMap = new HashMap<String, String>();
		List<String> stopIdList = new ArrayList<String>();
		int stopIdVectorSize = stopIdVector.size();
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println( "stop id vector size = " + stopIdVectorSize);
		 
		for (int i = 0; i < stopIdVectorSize; i++) {
		        try {
		        	if ((i % 500) == 0) if (IbicoopDataConstants.DEBUG_MODE) System.out.println( "stop id vector size = " + i);
		        	
	                String stopIdKey = new String(stopIdVector.elementAt(i),
	                                StringUtils.UTF8);
	                String stopIdData = responseMessage.getPayload(i);
	                stopInfoMap.put(stopIdKey, stopIdData);
	
	                if (stopIdData.equals("ID")) {
	                        stopIdList.add(stopIdKey);
	                }
		        } catch (Exception e) {
		                if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
		        }
		}
		
		ArrayList<ParisStopInfo> stops = new ArrayList<ParisStopInfo>();
		
		int index = 0;
		// Create StopInfo object
		for (String stopId : stopIdList) {
			
			index++;
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println( "Paris bus metro info index = " + index);
			
		        String keyWordName = stopId + "_Name";
		        String keyWordLat = stopId + "_Lat";
		        String keyWordLon = stopId + "_Lon";
		        String keyWordRouteId = stopId + "_RouteId";
		        String keyWordRouteName = stopId + "_RouteName";
		        String keyWordDirection = stopId + "_Direction";
		
		        String name = stopInfoMap.get(keyWordName);
		        String routeId = stopInfoMap.get(keyWordRouteId);
		        String routeName = stopInfoMap.get(keyWordRouteName);
		        double latitude = Double.parseDouble(stopInfoMap.get(keyWordLat));
		        double longitude = Double.parseDouble(stopInfoMap.get(keyWordLon));
		        int direction = Integer.parseInt(stopInfoMap.get(keyWordDirection));
		        stops.add(new ParisStopInfo(stopId, routeId, routeName, name, latitude,
		                        longitude, direction));
		}
		
		// Now sorting the stops in alphabetical order
		Collections.sort(stops, new Comparator<ParisStopInfo>() {
		        @Override
		        public int compare(ParisStopInfo o1, ParisStopInfo o2) {
		                return o1.getStopName().compareTo(o2.getStopName());
		        }
		});
		
		return stops;
	}
}
