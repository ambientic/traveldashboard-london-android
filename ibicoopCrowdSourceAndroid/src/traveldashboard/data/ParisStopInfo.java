package traveldashboard.data;

import java.io.Serializable;

public class ParisStopInfo implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String stopId;
    private String stopName;
    private double latitude;
    private double longitude;
    private String routeId;
    private String routeName;
    private float currentDistanceMeters = -1.0f;
    private int direction;

    public ParisStopInfo(String stopId, String routeId, String routeName, String name,
                    double latitude, double longitude, int direction) {
            this.stopId = stopId;
            this.stopName = name;
            this.routeId = routeId;
            this.routeName = routeName;
            this.latitude = latitude;
            this.longitude = longitude;
            this.direction = direction;
    }

    public String getRouteId() {
    	return routeId;
    }
    
    public String getRouteName() {
            return routeName;
    }

    public void setRouteName(String routeName) {
            this.routeName = routeName;
    }

    public String getStopId() {
            return stopId;
    }

    public String getStopName() {
            return stopName;
    }

    public double getLatitude() {
            return latitude;
    }

    public double getLongitude() {
            return longitude;
    }

    public float getCurrentDistanceMeters() {
            return currentDistanceMeters;
    }

    public void setCurrentDistanceMeters(float currentDistanceMeters) {
            this.currentDistanceMeters = currentDistanceMeters;
    }

    @Override
    public String toString() {
            return String.format("%s - %s", getRouteName(), getStopName());
    }

    public int getDirection() {
            return direction;
    }

    public void setDirection(int direction) {
            this.direction = direction;
    }	

}
