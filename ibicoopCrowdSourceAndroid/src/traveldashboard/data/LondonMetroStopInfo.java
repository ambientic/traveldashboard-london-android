package traveldashboard.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class LondonMetroStopInfo implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String stopId; //Equivalent to STC in the initial csv file
    private String stopName; //Equivalent to id in the initial csv file
    private String stopNlc;
    private double latitude;
    private double longitude;
    
    //Key = routeId (line), Value = routeName (lineName)
    private HashMap<String, String> routeIdsWithNamesMap;
    private HashMap<String, String> routeIdsWithDirectionsMap;
    
    private List<String> routeIds;
    
    private float currentDistanceMeters = -1.0f;

    public LondonMetroStopInfo(String stopId, String stopName, String stopNlc,
    		HashMap<String, String> routeIdsWithNameMap, 
    		HashMap<String, String> routeIdsWithDirectionMap,
            double latitude, double longitude)
    {
            this.stopId = stopId;
            this.stopName = stopName;
            this.stopNlc = stopNlc;
            this.latitude = latitude;
            this.longitude = longitude;
            
            this.routeIdsWithNamesMap = new HashMap<String, String>();
            this.routeIdsWithNamesMap.putAll(routeIdsWithNameMap);
            
            this.routeIds = new ArrayList<String>();
            Set<String> routeIdsSet = routeIdsWithNameMap.keySet();
            this.routeIds.addAll(routeIdsSet);
            
            this.routeIdsWithDirectionsMap = new HashMap<String, String>();
            this.routeIdsWithDirectionsMap.putAll(routeIdsWithDirectionMap);
    }

    public HashMap<String, String> getRouteIdsWithNamesMap() {
    	return routeIdsWithNamesMap;
    }
    
    public HashMap<String, String> getRouteIdsWithDirectionsMap() {
    	return routeIdsWithDirectionsMap;
    }

    public List<String> getRouteIds() {
    	return routeIds;
    }

    public String getStopId() {
            return stopId;
    }

    public String getStopName() {
            return stopName;
    }
    
    public String getStopNlc() {
    		return stopNlc;
    }

    public double getLatitude() {
            return latitude;
    }

    public double getLongitude() {
            return longitude;
    }

    public float getCurrentDistanceMeters() {
            return currentDistanceMeters;
    }

    public void setCurrentDistanceMeters(float currentDistanceMeters) {
            this.currentDistanceMeters = currentDistanceMeters;
    }

    @Override
    public String toString() {
    		StringBuilder sb = new StringBuilder();
    		
    		sb.append("Stop name = " + getStopName() + ": ");
    		
    		for (String routeId : routeIds) {
    			sb.append(routeId + ", ");
    		}
            return sb.toString();
    }

}
