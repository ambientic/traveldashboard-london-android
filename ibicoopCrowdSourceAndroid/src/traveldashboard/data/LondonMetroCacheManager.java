package traveldashboard.data;

import java.util.ArrayList;
import java.util.List;

public class LondonMetroCacheManager {

	private static final Object _synObj = new Object();

	private static LondonMetroCacheManager instance;

	private boolean loading = false;

	private List<LondonMetroStopInfo> stopInfos;

	public static LondonMetroCacheManager getInstance() {
		if (instance == null) { // double-checked, Java memory model
			synchronized (_synObj) {
				if (instance == null) instance = new LondonMetroCacheManager();
			}
		}
		return instance;
	}

	public void setLoading(boolean loading) {
		synchronized (_synObj) {
		    this.loading = loading;
		}
	}

	public boolean isLoading() {
		return loading;
	}

	public void addStopInfo(LondonMetroStopInfo stopInfo) {
		stopInfos.add(stopInfo);
	}

	public List<LondonMetroStopInfo> getAllStopInfos() {
		return stopInfos;
	}

	public void clear() {
		stopInfos.clear();
	}

	public LondonMetroStopInfo getStopInfo(String stopId) {
		for (LondonMetroStopInfo tmp : stopInfos) {
			if (tmp.getStopId().equals(stopId)) return tmp;
		}
		return null;
	}

	public ArrayList<LondonMetroStopInfo> getStopInfos(BoundingArea boundingArea) {
		ArrayList<LondonMetroStopInfo> rets = new ArrayList<LondonMetroStopInfo>();
		for (LondonMetroStopInfo tmp : stopInfos) {
			if (checkBetween(boundingArea.getMinLat(), boundingArea.getMaxLat(), tmp.getLatitude())
					&& checkBetween(boundingArea.getMinLon(), boundingArea.getMaxLon(), tmp.getLongitude())) {
				rets.add(tmp);
			}
		}
		return rets;
	}


	public List<String> getAllLondonMetroStopNames() {
		List<String> rets = new ArrayList<String>();
		for (LondonMetroStopInfo tmp : stopInfos) {
			rets.add(tmp.getStopName());
		}
		return rets;
	}

	private LondonMetroCacheManager() {
		stopInfos = new ArrayList<LondonMetroStopInfo>();
	}

	private boolean checkBetween(double min, double max, double value) {
		if (value >= min && value <= max) return true;
		return false;
	}

}
