package traveldashboard.data;

import traveldashboard.data.IbicoopDataConstants;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;


public class ParisMetroDbHelper extends SQLiteOpenHelper {
	// If we change the database schema, we must increment the database version.
	public static final int DATABASE_VERSION = 1;
	public static final String DATABASE_NAME_PARIS = "TravelDashboardParis.db";
	public static final String DATABASE_NAME_LONDON = "TravelDashboardLondon.db";
	
	/**
	 * Get database name according to city
	 * @param city
	 * @return
	 */
	public static String getDatabaseName(String city) {
		
		if (city.equals(IbicoopDataConstants.PARIS)) {
			return DATABASE_NAME_PARIS;
		} else if (city.equals(IbicoopDataConstants.LONDON)) {
			return DATABASE_NAME_LONDON;
		}
		
		return "";
	}
	
	//BaseColums inner class
	public static abstract class MetroDbEntry implements BaseColumns {
		//Table name
		public static final String TABLE_NAME = "metroStops";
		//Table column names
	    public static final String PARAM_VALUE_STOP_ID = "ID";
	    public static final String PARAM_KEY_STOP_NAME = "Name";
	    public static final String PARAM_KEY_STOP_LON = "Lon";
	    public static final String PARAM_KEY_STOP_LAT = "Lat";
	    public static final String PARAM_KEY_ROUTE_ID = "RouteId";
	    public static final String PARAM_KEY_ROUTE_NAME = "RouteName";
	    public static final String PARAM_KEY_DIRECTION_ID = "Direction";
	    //Alarm set: 0 if not set, 1 if set
	    public static final String PARAM_KEY_ALARM_SET = "AlarmSet";
	    //Alarm time: HH:mm
	    public static final String PARAM_KEY_ALARM_TIME = "AlarmTime";
	    public static final String COLUMN_NAME_NULLABLE = "Nullable";
	}
    
    //Data type
    private static final String TEXT_TYPE = " TEXT";
    private static final String REAL_TYPE = " REAL";
    private static final String COMMA_SEP = ",";
	
	private static final String SQL_CREATE_ENTRIES =
			"CREATE TABLE IF NOT EXISTS " + MetroDbEntry.TABLE_NAME + " (" +
			MetroDbEntry._ID + " INTEGER PRIMARY KEY," + 
			MetroDbEntry.PARAM_VALUE_STOP_ID + TEXT_TYPE + COMMA_SEP +
			MetroDbEntry.PARAM_KEY_STOP_NAME + TEXT_TYPE + COMMA_SEP +		
			MetroDbEntry.PARAM_KEY_STOP_LON + REAL_TYPE + COMMA_SEP +
			MetroDbEntry.PARAM_KEY_STOP_LAT + REAL_TYPE + COMMA_SEP +
			MetroDbEntry.PARAM_KEY_ROUTE_ID + TEXT_TYPE + COMMA_SEP +
			MetroDbEntry.PARAM_KEY_ROUTE_NAME + TEXT_TYPE + COMMA_SEP +
			MetroDbEntry.PARAM_KEY_DIRECTION_ID + TEXT_TYPE + COMMA_SEP +
			MetroDbEntry.PARAM_KEY_ALARM_SET + REAL_TYPE + COMMA_SEP +
			MetroDbEntry.PARAM_KEY_ALARM_TIME + TEXT_TYPE + ")"
			;
	
	private static final String SQL_DELETE_ENTRIES =
			"DROP TABLE IF EXISTS " + MetroDbEntry.TABLE_NAME;
	
	public ParisMetroDbHelper(Context context) {
		super(context, getDatabaseName(IbicoopDataConstants.PARIS), null, DATABASE_VERSION);
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Created MetroHelper");
		//Get readable and writable so that the database is created really on mobile
		SQLiteDatabase readableDb =  getReadableDatabase();
		SQLiteDatabase writableDb = getWritableDatabase();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("db.execSQL(" + SQL_CREATE_ENTRIES + ")");
		db.execSQL(SQL_CREATE_ENTRIES);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("db.execSQL(" + SQL_DELETE_ENTRIES + ")");
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
	}
	
	@Override
	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		onUpgrade(db, oldVersion, newVersion);
	}
}
