package traveldashboard.data;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import traveldashboard.data.IbicoopDataConstants;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import au.com.bytecode.opencsv.CSVReader;

public class LondonMetroDbManager {
	
	private String city = "";
	
	private final String[] projection = {
			LondonMetroDbHelper.MetroDbEntry._ID,
			LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_NAME,
			LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_ID,
			LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_NLC,
			LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LON,
			LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LAT,
			
			LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ROUTE_ID1,
			LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ROUTE_NAME1,
			LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ROUTE_DIRECTION1,
			
			LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ROUTE_ID2,
			LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ROUTE_NAME2,
			LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ROUTE_DIRECTION2,
			
			LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ROUTE_ID3,
			LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ROUTE_NAME3,
			LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ROUTE_DIRECTION3,
			
			LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ROUTE_ID4,
			LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ROUTE_NAME4,
			LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ROUTE_DIRECTION4,
			
			LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ROUTE_ID5,
			LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ROUTE_NAME5,
			LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ROUTE_DIRECTION5,
			
			LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_SET,
			LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_TIME,
	};
	
	private Context context;
	private LondonMetroDbHelper dbHelper;

	public LondonMetroDbManager(Context ctx) {
		context = ctx;
		city = IbicoopDataConstants.LONDON;
		dbHelper = new LondonMetroDbHelper(context);
		 if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Created metro db manager for " + city);
	}
	
	public void deleteDb() {
		 if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Delete database for " + city);
		SQLiteDatabase writableDb = dbHelper.getWritableDatabase();
		writableDb.delete(LondonMetroDbHelper.MetroDbEntry.TABLE_NAME, null, null);
	}
	
	public int getStopInfoDbCount() {
		if (LondonMetroCacheManager.getInstance().isLoading()) {
			return LondonMetroCacheManager.getInstance().getAllStopInfos().size();
		}
		int count = -1;
		SQLiteDatabase readableDb =  dbHelper.getReadableDatabase();
		Cursor cursor = readableDb.rawQuery("select * from " + LondonMetroDbHelper.MetroDbEntry.TABLE_NAME, null);
		if (cursor != null) {
			count = cursor.getCount();
			 if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Db table count = " + count);
			cursor.close();
		}
		return count;
	}
	
	public void updateAlarmSet(String stopId, int alarmSet) {
		
		 if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Update alarm set for stopId = " + stopId + ", alarmSet = " + alarmSet);

		ContentValues values = new ContentValues();
		values.put(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_SET, alarmSet);
		
		SQLiteDatabase writableDb = dbHelper.getWritableDatabase();
		
		writableDb.update(LondonMetroDbHelper.MetroDbEntry.TABLE_NAME, values, LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_ID + "='" + stopId+ "'", null);
	}
	
	public void testDb(String stopId) {
		

			
		String query =  "select * " 
				+ " from " + LondonMetroDbHelper.MetroDbEntry.TABLE_NAME + 	
				" where " + LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_ID + "='" + stopId+ "'";
				
		SQLiteDatabase readableDb =  dbHelper.getReadableDatabase();
	
		
	    Cursor ti = readableDb.rawQuery("PRAGMA table_info(" + LondonMetroDbHelper.MetroDbEntry.TABLE_NAME + ")", null);
	    if ( ti.moveToFirst() ) {
	        do {
	            if (IbicoopDataConstants.DEBUG_MODE) System.out.println("col: " + ti.getString(1));
	        } while (ti.moveToNext());
	    }
		
		
		Cursor cursor = readableDb.rawQuery(query, null);

		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("cursor = " + cursor.toString() + ", count = " + cursor.getCount());
	}
	
	public int readAlarmSet(String stopId) {
		
		int alarmSet = -1;
		
		 if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Read alarm set for stopId = " + stopId);
		
		String query =  "select " + LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_SET 
				+ " from " + LondonMetroDbHelper.MetroDbEntry.TABLE_NAME + 	
				" where " + LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_ID + "='" + stopId+ "'";
				
		SQLiteDatabase readableDb =  dbHelper.getReadableDatabase();
		
		Cursor cursor = readableDb.rawQuery(query, null);
		
		 if (IbicoopDataConstants.DEBUG_MODE) System.out.println("cursor = " + cursor.toString() + ", count = " + cursor.getCount());
		
		if (cursor != null) {
			cursor.moveToFirst();
			alarmSet = cursor.getInt(cursor.getColumnIndex(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_SET));
			cursor.close();
		}

		return alarmSet;
	}
	
	public void updateAlarmTime(String stopId, int hour, int minute) {

		 if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Update alarm set for stopId = " + stopId + ", hour = " + hour + ", minute = " + minute);

		ContentValues values = new ContentValues();
		
		String hourString = String.valueOf(hour);
		String minuteString = String.valueOf(minute);
		
		if (hour < 10) hourString = "0" + hourString;
		if (minute < 10) minuteString = "0" + minuteString;
		
		String time = hourString + minuteString;
		
		//Example: 1200
		 if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Set alarm for time = " + time);
		
		values.put(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_TIME, time);
		
		SQLiteDatabase writableDb = dbHelper.getWritableDatabase();
		
		writableDb.update(LondonMetroDbHelper.MetroDbEntry.TABLE_NAME, values, LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_ID + "='" + stopId+ "'", null);
		
	}
	
	public String readAlarmTime(String stopId) {
		
		String alarmTime = "null";
		
		 if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Read alarm set for stopId = " + stopId);
		
		SQLiteDatabase readableDb =  dbHelper.getReadableDatabase();
		
		String query =  "select " + LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_TIME +
				" from " + LondonMetroDbHelper.MetroDbEntry.TABLE_NAME +
				" where " + LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_ID + "='" + stopId+ "'";

		Cursor cursor = readableDb.rawQuery(query, null);
		
		if (cursor != null) {
			cursor.moveToFirst();
			alarmTime = cursor.getString(cursor.getColumnIndexOrThrow(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_TIME));
			cursor.close();
		}

		return alarmTime;
	}
	
	
	public void insertStopIntoDbByInputStreams(InputStream[] stationsIs) {
		
		try {
			LondonMetroCacheManager.getInstance().setLoading(true);
			long time1 = System.currentTimeMillis();
			final SQLiteDatabase writableDb = dbHelper.getWritableDatabase();
			final List<ContentValues> listContentValues = new ArrayList<ContentValues>();
			for (int index = 0; index < 8; index ++)  {
				 if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Insert stop into db by input streams: index = " + index);
				 long time3 = System.currentTimeMillis();
				CSVReader reader = new CSVReader(new InputStreamReader(stationsIs[index]));
				List<String[]> stations = reader.readAll();
				long time4 = System.currentTimeMillis();
				System.out.println("time to parse a CVS: " + (time4 - time3));
				
				for (int i = 1; i < stations.size(); i++) {
					//Ignore first row
					String[] station = stations.get(i);
					
					//if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Insert stop into db by input streams: station = " + station.toString());
					
					if (!station[1].trim().equalsIgnoreCase("*") && !station[2].trim().equalsIgnoreCase("*")) {
						
						String stopName = station[0];
						String stopNlc = station[1].trim();
						String stopId = station[2].trim();
						
						HashMap<String, String> routeIdsWithNamesMap = new HashMap<String, String>();
						HashMap<String, String> routeIdsWithDirectionsMap = new HashMap<String, String>();			
						List<String> routeIds = new ArrayList<String>();
						
						for (int j = 3; j <= 7; j++) {
							String routeId = station[j].trim();
							String routeName = IbicoopDataConstants.LONDON_ROUTEID_ROUTENAME_MAP.get(routeId);
							String routeDirection = IbicoopDataConstants.LONDON_TUBE_DESTINATIONS.get(routeName);
							
							routeIdsWithNamesMap.put(routeId, routeName);
							routeIdsWithDirectionsMap.put(routeId, routeDirection);
							routeIds.add(routeId);
						}
						
						double lat = Double.parseDouble(station[8].trim());
						double lon = Double.parseDouble(station[9].trim());

						ContentValues values = new ContentValues();
						values.put(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_NAME, stopName);
						values.put(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_ID, stopId);
						values.put(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_NLC, stopNlc);	
						values.put(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LAT, lat);
						values.put(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LON, lon);

						 if (IbicoopDataConstants.DEBUG_MODE) System.out.println(
								"stopName = " + stopName + ", " +
								"stopNlc = " + stopNlc + ", " +
								"stopId = " + stopId
								);
						
						int a = 1;
						
						for (String routeId : routeIds) {
							values.put("line" + a, routeId);
							values.put("lineName" + a, routeIdsWithNamesMap.get(routeId));
							values.put("lineDirection" + a, routeIdsWithDirectionsMap.get(routeId));
							
							 if (IbicoopDataConstants.DEBUG_MODE) System.out.println("routeId = " + routeId);
							 if (IbicoopDataConstants.DEBUG_MODE) System.out.println("routeName = " + routeIdsWithNamesMap.get(routeId));
							 if (IbicoopDataConstants.DEBUG_MODE) System.out.println("routeDirection = " + routeIdsWithDirectionsMap.get(routeId));
							
							a++;
						}

						//Set alarm as inactive(0)
						values.put(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_SET, 0);
						//Set alarm time as 1200
						values.put(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_TIME, "1200");

						LondonMetroCacheManager.getInstance().addStopInfo(convertContentValuesToStopInfo(values));
						
						listContentValues.add(values);
						
//						long newRowId;
//						newRowId = writableDb.insert(
//								LondonMetroDbHelper.MetroDbEntry.TABLE_NAME, 
//								LondonMetroDbHelper.MetroDbEntry.COLUMN_NAME_NULLABLE, 
//								values);
//						
//						 if (IbicoopDataConstants.DEBUG_MODE) System.out.println("newRowId = " + newRowId);
						 

					}
				}
				
				reader.close();
			}
			
			long time2 = System.currentTimeMillis();
			System.out.println("time to parse CVSs: " + (time2 - time1));
			
			Runnable runnable = new Runnable() {
				
				@Override
				public void run() {
					System.out.println("start inserting values to sqlite");
					for (ContentValues values : listContentValues) {
						writableDb.insert(
								LondonMetroDbHelper.MetroDbEntry.TABLE_NAME, 
								LondonMetroDbHelper.MetroDbEntry.COLUMN_NAME_NULLABLE, 
								values);
					}
					System.out.println("finish inserting values to sqlite");
					LondonMetroCacheManager.getInstance().setLoading(false);
					LondonMetroCacheManager.getInstance().clear();
				}
			};
//			Thread thd = new Thread(runnable);
//			thd.start();
			Executors.newSingleThreadExecutor().execute(runnable);
		} catch (Exception exception) {
			if (IbicoopDataConstants.DEBUG_MODE) System.err.println("LondonCsvMessageParser : " + exception.getMessage());
		}
	}
	
	
	private LondonMetroStopInfo convertContentValuesToStopInfo(
			ContentValues values) {
		String id = (String) values.get(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_ID);
		String stopName = (String) values.get(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_NAME);
		String stopNlc = (String) values.get(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_NLC);
		double lat = (Double) values.get(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LAT);
		double lon = (Double) values.get(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LON);
		
		
		HashMap<String, String> routeIdsWithNamesMap = new HashMap<String, String>();
		HashMap<String, String> routeIdsWithDirectionsMap = new HashMap<String, String>();			
		List<String> routeIds = new ArrayList<String>();
		
		for (int j= 1; j <= 5; j++) {
			String routeId = (String) values.get("line" + j);
			String routeName = (String) values.get("lineName" + j);
			String routeDirection = (String) values.get("lineDirection" + j);
			
			routeIdsWithNamesMap.put(routeId, routeName);
			routeIdsWithDirectionsMap.put(routeId, routeDirection);
			routeIds.add(routeId);
		}
					
		LondonMetroStopInfo londonMetroStopInfo = new LondonMetroStopInfo(id, stopName, stopNlc, routeIdsWithNamesMap, routeIdsWithDirectionsMap, lat, lon);
		return londonMetroStopInfo;
	}

	public void insertStopIntoDb(LondonMetroStopInfo stop) {
		 if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Insert stop into db: " + stop.toString());
		ContentValues values = new ContentValues();
		values.put(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_NAME, stop.getStopName());
		values.put(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_ID, stop.getStopId());
		values.put(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_NLC, stop.getStopNlc());	
		values.put(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LAT, stop.getLatitude());
		values.put(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LON, stop.getLongitude());
		
		HashMap<String, String> routeIdsWithNamesMap = new HashMap<String, String>();
		routeIdsWithNamesMap.putAll(stop.getRouteIdsWithNamesMap());
		
		HashMap<String, String> routeIdsWithDirectionsMap = new HashMap<String, String>();
		routeIdsWithDirectionsMap.putAll(stop.getRouteIdsWithDirectionsMap());
		
		List<String> routeIds = new ArrayList<String>();
		routeIds.addAll(stop.getRouteIds());
		
		int index = 1;
		
		for (String routeId : routeIds) {
			values.put("line" + index, routeId);
			values.put("lineName" + index, routeIdsWithNamesMap.get(routeId));
			values.put("lineDirection" + index, routeIdsWithDirectionsMap.get(routeId));
			index++;
		}

		//Set alarm as inactive(0)
		values.put(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_SET, 0);
		//Set alarm time as 1200
		values.put(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_TIME, "1200");
		
		SQLiteDatabase writableDb = dbHelper.getWritableDatabase();
		long newRowId;
		newRowId = writableDb.insert(
				LondonMetroDbHelper.MetroDbEntry.TABLE_NAME, 
				LondonMetroDbHelper.MetroDbEntry.COLUMN_NAME_NULLABLE, 
				values);
		 if (IbicoopDataConstants.DEBUG_MODE) System.out.println("newRowId = " + newRowId);
		 if (IbicoopDataConstants.DEBUG_MODE) System.out.println("stop = " + stop.toString());
	}
	
	public LondonMetroStopInfo readFromDb(String stopId) {
		if (LondonMetroCacheManager.getInstance().isLoading()) {
			LondonMetroStopInfo tmp = LondonMetroCacheManager.getInstance().getStopInfo(stopId);
			if (tmp != null) return tmp;
		}
		LondonMetroStopInfo londonMetroStopInfo = null;
		
		//Sorted by ascending or descending order
		String sortOrder = LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_ID + " ASC";
		
		SQLiteDatabase readableDb =  dbHelper.getReadableDatabase();
		
		String selection = LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_ID + "='" + stopId+ "'";
		String[] selectionArgs = {};
		
		Cursor cursor = readableDb.query(
						LondonMetroDbHelper.MetroDbEntry.TABLE_NAME, 
						projection, 
						selection, 
						selectionArgs, 
						null, 
						null, 
						sortOrder);
		
		if (cursor != null) {
			
			cursor.moveToFirst();
			
			String id = cursor.getString(cursor.getColumnIndexOrThrow(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_ID));
			String stopName = cursor.getString(cursor.getColumnIndexOrThrow(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_NAME));
			String stopNlc = cursor.getString(cursor.getColumnIndexOrThrow(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_NLC));
			double lat = cursor.getDouble(cursor.getColumnIndexOrThrow(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LAT));
			double lon = cursor.getDouble(cursor.getColumnIndexOrThrow(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LON));
			
			
			HashMap<String, String> routeIdsWithNamesMap = new HashMap<String, String>();
			HashMap<String, String> routeIdsWithDirectionsMap = new HashMap<String, String>();			
			List<String> routeIds = new ArrayList<String>();
			
			for (int i = 1; i <= 5; i++) {
				String routeId = cursor.getString(cursor.getColumnIndexOrThrow("line" + i));
				String routeName = cursor.getString(cursor.getColumnIndexOrThrow("lineName" + i));
				String routeDirection = cursor.getString(cursor.getColumnIndexOrThrow("lineDirection" + i));
				
				routeIdsWithNamesMap.put(routeId, routeName);
				routeIdsWithDirectionsMap.put(routeId, routeDirection);
				routeIds.add(routeId);
			}
			
			int alarmSet = cursor.getInt(cursor.getColumnIndexOrThrow(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_SET));
			String alarmTime = cursor.getString(cursor.getColumnIndexOrThrow(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_TIME));
						
			londonMetroStopInfo = new LondonMetroStopInfo(id, stopName, stopNlc, routeIdsWithNamesMap, routeIdsWithDirectionsMap, lat, lon);
			
			 if (IbicoopDataConstants.DEBUG_MODE) System.out.println("readFromDb : " + londonMetroStopInfo.toString() + ", alarmSet = " + alarmSet + ", alarmTime = " + alarmTime);
			
			cursor.close();
		}

		return londonMetroStopInfo;
	}
	
	public ArrayList<LondonMetroStopInfo> readFromDbByCoord(double latitude, double longitude, double radius) {

		BoundingArea boundingArea = new BoundingArea(latitude, longitude, radius);

		if (LondonMetroCacheManager.getInstance().isLoading()) {
			return LondonMetroCacheManager.getInstance().getStopInfos(boundingArea);
		}

		ArrayList<LondonMetroStopInfo> londonMetroStopInfos = new ArrayList<LondonMetroStopInfo>();
		
        double maxLat = boundingArea.getMaxLat();
        double minLat = boundingArea.getMinLat();
        double maxLon = boundingArea.getMaxLon();
        double minLon = boundingArea.getMinLon();
		
         if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Read from db by coord:");
         if (IbicoopDataConstants.DEBUG_MODE) System.out.println("lat = " + latitude + ", lon = " + longitude + ", radius = " + radius
        		+ ", maxLat = " + maxLat + ", minLat = " + minLat 
        		+ ", maxLon = " + maxLon + ", minLon = " + minLon);        
        
		SQLiteDatabase readableDb =  dbHelper.getReadableDatabase();
		
		String query =  "select * from " + LondonMetroDbHelper.MetroDbEntry.TABLE_NAME + 	" where " + 
				"(" + LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LAT + " >= " +  minLat + ") and " +
				"(" + LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LAT + " <= " +  maxLat + ") and " +					
				"(" + LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LON + " >= " +  minLon + ") and " +
				"(" + LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LON + " <= " +  maxLon + ")	";
		
		 if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Query = " + query);
		
		Cursor cursor = readableDb.rawQuery(query, null);
		
		if (cursor != null) {
			 if (IbicoopDataConstants.DEBUG_MODE) System.out.println("cursor count = " + cursor.getCount());
			
			cursor.moveToFirst();
			
			for (int i = 0;  i < cursor.getCount(); i++) {

				String id = cursor.getString(cursor.getColumnIndexOrThrow(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_ID));
				String stopName = cursor.getString(cursor.getColumnIndexOrThrow(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_NAME));
				String stopNlc = cursor.getString(cursor.getColumnIndexOrThrow(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_NLC));
				double lat = cursor.getDouble(cursor.getColumnIndexOrThrow(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LAT));
				double lon = cursor.getDouble(cursor.getColumnIndexOrThrow(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LON));
				
				
				HashMap<String, String> routeIdsWithNamesMap = new HashMap<String, String>();
				HashMap<String, String> routeIdsWithDirectionsMap = new HashMap<String, String>();			
				List<String> routeIds = new ArrayList<String>();
				
				for (int j= 1; j <= 5; j++) {
					String routeId = cursor.getString(cursor.getColumnIndexOrThrow("line" + j));
					String routeName = cursor.getString(cursor.getColumnIndexOrThrow("lineName" + j));
					String routeDirection = cursor.getString(cursor.getColumnIndexOrThrow("lineDirection" + j));
					
					routeIdsWithNamesMap.put(routeId, routeName);
					routeIdsWithDirectionsMap.put(routeId, routeDirection);
					routeIds.add(routeId);
				}
				
				int alarmSet = cursor.getInt(cursor.getColumnIndexOrThrow(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_SET));
				String alarmTime = cursor.getString(cursor.getColumnIndexOrThrow(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_TIME));
							
				LondonMetroStopInfo londonMetroStopInfo = new LondonMetroStopInfo(id, stopName, stopNlc, routeIdsWithNamesMap, routeIdsWithDirectionsMap, lat, lon);
				
				 if (IbicoopDataConstants.DEBUG_MODE) System.out.println("readFromDb : " + londonMetroStopInfo.toString() + ", alarmSet = " + alarmSet + ", alarmTime = " + alarmTime);
				
				londonMetroStopInfos.add(londonMetroStopInfo);
				
				//Next row
				cursor.moveToNext();
			}
			
			cursor.close();
		}

		
		return londonMetroStopInfos;
	}
	
	public List<String> getAllLondonMetroStopNames() {

		if (LondonMetroCacheManager.getInstance().isLoading()) {
			return LondonMetroCacheManager.getInstance().getAllLondonMetroStopNames();
		}
		
		List<String> londonMetroStopNames = new ArrayList<String>();
		
        
		SQLiteDatabase readableDb =  dbHelper.getReadableDatabase();
		
		String query =  "select " + LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_NAME + " from " + LondonMetroDbHelper.MetroDbEntry.TABLE_NAME;
		
		 if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Query = " + query);
		
		Cursor cursor = readableDb.rawQuery(query, null);
		
		if (cursor != null) {
			 if (IbicoopDataConstants.DEBUG_MODE) System.out.println("cursor count = " + cursor.getCount());
			
			cursor.moveToFirst();
			
			for (int i = 0;  i < cursor.getCount(); i++) {
				String stopName = cursor.getString(cursor.getColumnIndexOrThrow(LondonMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_NAME));
				londonMetroStopNames.add(stopName);
				//Next row
				cursor.moveToNext();
			}
			
			cursor.close();
		}
		
		return londonMetroStopNames;
	}
}