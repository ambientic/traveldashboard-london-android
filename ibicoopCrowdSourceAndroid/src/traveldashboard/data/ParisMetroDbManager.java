package traveldashboard.data;

import java.util.ArrayList;
import java.util.List;

import traveldashboard.data.Tube;
import traveldashboard.data.TubeStation;
import traveldashboard.data.TubeStationsCollection;
import traveldashboard.data.IbicoopDataConstants;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class ParisMetroDbManager {
	
	private boolean debug = true;
	
	private String city = "";
	
	private final String[] projection = {
			ParisMetroDbHelper.MetroDbEntry._ID,
			ParisMetroDbHelper.MetroDbEntry.PARAM_VALUE_STOP_ID,
			ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_NAME,
			ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LAT,
			ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LON,
			ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_ROUTE_ID,
			ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_ROUTE_NAME,
			ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_DIRECTION_ID,
			ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_SET,
			ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_TIME,
	};
	
	private Context context;
	private ParisMetroDbHelper dbHelper;

	public ParisMetroDbManager(Context ctx) {
		context = ctx;
		city = IbicoopDataConstants.PARIS;
		dbHelper = new ParisMetroDbHelper(context);
		if (debug) if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Created metro db manager for " + city);
	}
	
	public void deleteDb() {
		if (debug) if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Delete database for " + city);
		SQLiteDatabase writableDb = dbHelper.getWritableDatabase();
		writableDb.delete(ParisMetroDbHelper.MetroDbEntry.TABLE_NAME, null, null);
	}
	
	public int getStopInfoDbCount() {
		int count = -1;
		SQLiteDatabase readableDb =  dbHelper.getReadableDatabase();
		Cursor cursor = readableDb.rawQuery("select * from " + ParisMetroDbHelper.MetroDbEntry.TABLE_NAME, null);
		if (cursor != null) {
			count = cursor.getCount();
			if (debug) if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Db table count = " + count);
			cursor.close();
		}
		return count;
	}
	
	public void updateAlarmSet(String stopId, int alarmSet) {
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Update alarm set for stopId = " + stopId + ", alarmSet = " + alarmSet);

		ContentValues values = new ContentValues();
		values.put(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_SET, alarmSet);
		
		SQLiteDatabase writableDb = dbHelper.getWritableDatabase();
		
		writableDb.update(ParisMetroDbHelper.MetroDbEntry.TABLE_NAME, values, ParisMetroDbHelper.MetroDbEntry.PARAM_VALUE_STOP_ID + "=" + stopId, null);
		
	}
	
	public int readAlarmSet(String stopId) {
		
		int alarmSet = -1;
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Read alarm set for stopId = " + stopId);
		
		String query =  "select " + ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_SET 
				+ " from " + ParisMetroDbHelper.MetroDbEntry.TABLE_NAME + 	
				" where " + ParisMetroDbHelper.MetroDbEntry.PARAM_VALUE_STOP_ID + "=" + stopId;
				
		SQLiteDatabase readableDb =  dbHelper.getReadableDatabase();
		
		Cursor cursor = readableDb.rawQuery(query, null);
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("cursor = " + cursor.toString() + ", count = " + cursor.getCount());
		
		if (cursor != null) {
			cursor.moveToFirst();
			alarmSet = cursor.getInt(cursor.getColumnIndex(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_SET));
			cursor.close();
		}

		return alarmSet;
	}
	
	public void updateAlarmTime(String stopId, int hour, int minute) {
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Update alarm set for stopId = " + stopId + ", hour = " + hour + ", minute = " + minute);

		ContentValues values = new ContentValues();
		
		String hourString = String.valueOf(hour);
		String minuteString = String.valueOf(minute);
		
		if (hour < 10) hourString = "0" + hourString;
		if (minute < 10) minuteString = "0" + minuteString;
		
		String time = hourString + minuteString;
		
		//Example: 1200
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Set alarm for time = " + time);
		
		values.put(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_TIME, time);
		
		SQLiteDatabase writableDb = dbHelper.getWritableDatabase();
		
		writableDb.update(ParisMetroDbHelper.MetroDbEntry.TABLE_NAME, values, ParisMetroDbHelper.MetroDbEntry.PARAM_VALUE_STOP_ID + "=" + stopId, null);
		
	}
	
	public String readAlarmTime(String stopId) {
		
		String alarmTime = "null";
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Read alarm set for stopId = " + stopId);
		
		SQLiteDatabase readableDb =  dbHelper.getReadableDatabase();
		
		String query =  "select " + ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_TIME +
				" from " + ParisMetroDbHelper.MetroDbEntry.TABLE_NAME +
				" where " + ParisMetroDbHelper.MetroDbEntry.PARAM_VALUE_STOP_ID + "=" + stopId;

		Cursor cursor = readableDb.rawQuery(query, null);
		
		if (cursor != null) {
			cursor.moveToFirst();
			alarmTime = cursor.getString(cursor.getColumnIndexOrThrow(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_TIME));
			cursor.close();
		}

		return alarmTime;
	}
	
	public void insertStopIntoDb(ParisStopInfo stop) {
		if (debug) if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Insert stop into db: " + stop.toString());
		ContentValues values = new ContentValues();
		values.put(ParisMetroDbHelper.MetroDbEntry.PARAM_VALUE_STOP_ID, stop.getStopId());
		values.put(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_NAME, stop.getStopName());
		values.put(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LAT, stop.getLatitude());
		values.put(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LON, stop.getLongitude());
		values.put(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_ROUTE_ID, stop.getRouteId());
		values.put(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_ROUTE_NAME, stop.getRouteName());
		values.put(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_DIRECTION_ID, String.valueOf(stop.getDirection()));
		//Set alarm as inactive(0)
		values.put(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_SET, 0);
		//Set alarm time as 1200
		values.put(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_TIME, "1200");
		
		SQLiteDatabase writableDb = dbHelper.getWritableDatabase();
		long newRowId;
		newRowId = writableDb.insert(
				ParisMetroDbHelper.MetroDbEntry.TABLE_NAME, 
				ParisMetroDbHelper.MetroDbEntry.COLUMN_NAME_NULLABLE, 
				values);
		if (debug) if (IbicoopDataConstants.DEBUG_MODE) System.out.println("newRowId = " + newRowId);
	}
	
	public ParisStopInfo readFromDb(String stopId) {
		ParisStopInfo parisStopInfo = null;
		
		//Sorted by ascending or descending order
		String sortOrder = ParisMetroDbHelper.MetroDbEntry.PARAM_VALUE_STOP_ID + " ASC";
		
		SQLiteDatabase readableDb =  dbHelper.getReadableDatabase();
		
		String selection = ParisMetroDbHelper.MetroDbEntry.PARAM_VALUE_STOP_ID + "=" + stopId;
		String[] selectionArgs = {};
		
		Cursor cursor = readableDb.query(
						ParisMetroDbHelper.MetroDbEntry.TABLE_NAME, 
						projection, 
						selection, 
						selectionArgs, 
						null, 
						null, 
						sortOrder);
		
		if (cursor != null) {
			cursor.moveToFirst();
			String id = cursor.getString(cursor.getColumnIndexOrThrow(ParisMetroDbHelper.MetroDbEntry.PARAM_VALUE_STOP_ID));
			String stopName = cursor.getString(cursor.getColumnIndexOrThrow(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_NAME));
			double lat = cursor.getDouble(cursor.getColumnIndexOrThrow(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LAT));
			double lon = cursor.getDouble(cursor.getColumnIndexOrThrow(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LON));
			String routeId = cursor.getString(cursor.getColumnIndexOrThrow(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_ROUTE_ID));
			String routeName = cursor.getString(cursor.getColumnIndexOrThrow(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_ROUTE_NAME));
			String direction = cursor.getString(cursor.getColumnIndexOrThrow(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_DIRECTION_ID));
			int alarmSet = cursor.getInt(cursor.getColumnIndexOrThrow(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_SET));
			String alarmTime = cursor.getString(cursor.getColumnIndexOrThrow(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_TIME));
			
			String message = "receiveStopId = " + stopId + ", " +
								"getStopId = " + id + ", " + 
								"stopName = " + stopName + ", " +
								"lat = " + lat + ", " +
								"lon = " + lon + ", " +
								"routeId = " + routeId + ", " +
								"routeName = " + routeName + ", " + 
								"direction = " + direction + ", " +
								"alarm set = " + alarmSet + ", " + 
								"alarm time = " + alarmTime
								;
			if (debug) if (IbicoopDataConstants.DEBUG_MODE) System.out.println("readFromDb : " + message);
			
			parisStopInfo = new ParisStopInfo(id, routeId, routeName, stopName, lat, lon, Integer.parseInt(direction));
			
			cursor.close();
		}

		return parisStopInfo;
	}
	
	public ArrayList<ParisStopInfo> readFromDbByCoord(double latitude, double longitude, double radius) {
		
		ArrayList<ParisStopInfo> parisStopInfos = new ArrayList<ParisStopInfo>();
		
        BoundingArea boundingArea = new BoundingArea(latitude, longitude, radius);
        double maxLat = boundingArea.getMaxLat();
        double minLat = boundingArea.getMinLat();
        double maxLon = boundingArea.getMaxLon();
        double minLon = boundingArea.getMinLon();
		
        if (debug) if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Read from db by coord:");
        if (debug) if (IbicoopDataConstants.DEBUG_MODE) System.out.println("lat = " + latitude + ", lon = " + longitude + ", radius = " + radius
        		+ ", maxLat = " + maxLat + ", minLat = " + minLat 
        		+ ", maxLon = " + maxLon + ", minLon = " + minLon);        
        
		SQLiteDatabase readableDb =  dbHelper.getReadableDatabase();
		
		String query =  "select * from " + ParisMetroDbHelper.MetroDbEntry.TABLE_NAME + 	" where " + 
				"(" + ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LAT + " >= " +  minLat + ") and " +
				"(" + ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LAT + " <= " +  maxLat + ") and " +					
				"(" + ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LON + " >= " +  minLon + ") and " +
				"(" + ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LON + " <= " +  maxLon + ")	";
		
		if (debug) if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Query = " + query);
		
		Cursor cursor = readableDb.rawQuery(query, null);
		
		if (cursor != null) {
			if (debug) if (IbicoopDataConstants.DEBUG_MODE) System.out.println("cursor count = " + cursor.getCount());
			
			cursor.moveToFirst();
			
			for (int i = 0;  i < cursor.getCount(); i++) {
				String id = cursor.getString(cursor.getColumnIndexOrThrow(ParisMetroDbHelper.MetroDbEntry.PARAM_VALUE_STOP_ID));
				String stopName = cursor.getString(cursor.getColumnIndexOrThrow(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_NAME));
				double lat = cursor.getDouble(cursor.getColumnIndexOrThrow(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LAT));
				double lon = cursor.getDouble(cursor.getColumnIndexOrThrow(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LON));
				String routeId = cursor.getString(cursor.getColumnIndexOrThrow(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_ROUTE_ID));
				String routeName = cursor.getString(cursor.getColumnIndexOrThrow(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_ROUTE_NAME));
				String direction = cursor.getString(cursor.getColumnIndexOrThrow(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_DIRECTION_ID));
				int alarmSet = cursor.getInt(cursor.getColumnIndexOrThrow(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_SET));
				String alarmTime = cursor.getString(cursor.getColumnIndexOrThrow(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_ALARM_TIME));	
				
				String message = 
									"getStopId = " + id + ", " + 
									"stopName = " + stopName + ", " +
									"lat = " + lat + ", " +
									"lon = " + lon + ", " +
									"routeId = " + routeId + ", " +
									"routeName = " + routeName + ", " + 
									"direction = " + direction + ", " +
									"alarm set = " + alarmSet + ", " + 
									"alarm time = " + alarmTime
											;
				if (debug) if (IbicoopDataConstants.DEBUG_MODE) System.out.println("readFromDb : " + message);
				
				ParisStopInfo parisStopInfo = new ParisStopInfo(id, routeId, routeName, stopName, lat, lon, Integer.parseInt(direction));				
				
				parisStopInfos.add(parisStopInfo);
				
				//Next row
				cursor.moveToNext();
			}
			
			cursor.close();
		}

		return parisStopInfos;
	}
	
	/**
	 * Get metro stops from database
	 * @param latitude
	 * @param longitude
	 * @param radius
	 * @return Metro stops collection
	 */
	public TubeStationsCollection getMetroStopsFromdb(double latitude, double longitude, double radius) {
		
		if (debug) if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Get  metro stops from database");
		
		TubeStationsCollection metroStopsCollection = null;
		
		List<TubeStation> metroStopsList = new ArrayList<TubeStation>();
		
        BoundingArea boundingArea = new BoundingArea(latitude, longitude, radius);
        double maxLat = boundingArea.getMaxLat();
        double minLat = boundingArea.getMinLat();
        double maxLon = boundingArea.getMaxLon();
        double minLon = boundingArea.getMinLon();
		
        if (debug) if (IbicoopDataConstants.DEBUG_MODE) System.out.println("getMetroStopsFromdb:");
        if (debug) if (IbicoopDataConstants.DEBUG_MODE) System.out.println("lat = " + latitude + ", lon = " + longitude + ", radius = " + radius
        		+ ", maxLat = " + maxLat + ", minLat = " + minLat 
        		+ ", maxLon = " + maxLon + ", minLon = " + minLon);        
        
		SQLiteDatabase readableDb =  dbHelper.getReadableDatabase();
		
		String query =  "select * from " + ParisMetroDbHelper.MetroDbEntry.TABLE_NAME + 	" where " + 
				"(" + ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LAT + " >= " +  minLat + ") and " +
				"(" + ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LAT + " <= " +  maxLat + ") and " +					
				"(" + ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LON + " >= " +  minLon + ") and " +
				"(" + ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LON + " <= " +  maxLon + ")	";
		
		if (debug) if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Query = " + query);
		
		Cursor cursor = readableDb.rawQuery(query, null);
		
		if (cursor != null) {
			if (debug) if (IbicoopDataConstants.DEBUG_MODE) System.out.println("cursor count = " + cursor.getCount());
			
			cursor.moveToFirst();
			
			for (int i = 0;  i < cursor.getCount(); i++) {
				String stopId = cursor.getString(cursor.getColumnIndexOrThrow(ParisMetroDbHelper.MetroDbEntry.PARAM_VALUE_STOP_ID));
				String stopName = cursor.getString(cursor.getColumnIndexOrThrow(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_NAME));
				double lat = cursor.getDouble(cursor.getColumnIndexOrThrow(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LAT));
				double lon = cursor.getDouble(cursor.getColumnIndexOrThrow(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_STOP_LON));
				String routeId = cursor.getString(cursor.getColumnIndexOrThrow(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_ROUTE_ID));
				String routeName = cursor.getString(cursor.getColumnIndexOrThrow(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_ROUTE_NAME));
				String direction = cursor.getString(cursor.getColumnIndexOrThrow(ParisMetroDbHelper.MetroDbEntry.PARAM_KEY_DIRECTION_ID));
				
				direction = IbicoopDataConstants.PARIS_METRO_DESTINATIONS.get(routeName).getDirection(direction);
				
				String message = 
									"getStopId = " + stopId + ", " + 
									"stopName = " + stopName + ", " +
									"lat = " + lat + ", " +
									"lon = " + lon + ", " +
									"routeId = " + routeId + ", " +
									"routeName = " + routeName + ", " + 
									"direction = " + direction;
				
				if (debug) if (IbicoopDataConstants.DEBUG_MODE) System.out.println("readFromDb : " + message);
				
				
				Tube tube = new Tube(routeId, routeName, direction);
				Tube[] tubes = {tube};
				
				TubeStation metroStop = new TubeStation(stopId, stopName, stopId, lat, lon, tubes);
				metroStopsList.add(metroStop);
				
				//Next row
				cursor.moveToNext();
			}
			
			cursor.close();
		}
		
		TubeStation[] stops = new TubeStation[metroStopsList.size()];
		
		for (int i = 0; i < metroStopsList.size(); i++) {
			stops[i] = metroStopsList.get(i);
		}
		
		metroStopsCollection = new TubeStationsCollection(stops);
		
		return metroStopsCollection;
	}
}
