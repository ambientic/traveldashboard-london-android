package traveldashboard.data;

import java.io.Serializable;

public class LondonBikeStopInfo implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String stopId; 
    private String stopName;

    private double latitude;
    private double longitude;

    public String nbBikes;
    public String nbEmptyDocks;
    public String nbDocks;
    public String locked;   
    
    private float currentDistanceMeters = -1.0f;

    public LondonBikeStopInfo(String stopId, String stopName,
            double latitude, double longitude,
            String nbBikes, String nbEmptyDocks,
            String nbDocks, String locked
    		)
    {
            this.stopId = stopId;
            this.stopName = stopName;
            this.latitude = latitude;
            this.longitude = longitude;
            this.nbBikes = nbBikes;
            this.nbEmptyDocks = nbEmptyDocks;
            this.nbDocks = nbDocks;
            this.locked = locked;
    }

    public String getStopId() {
            return stopId;
    }

    public String getStopName() {
            return stopName;
    }
    
    public String getNbBikes() {
    	return nbBikes;
    }
    
    public String getNbEmptyDocks() {
    	return nbEmptyDocks;
    }
    
    public String getNbDocks() {
    	return nbDocks;
    }
    
    public String getLocked() {
    	return locked;
    }
    
    public double getLatitude() {
            return latitude;
    }

    public double getLongitude() {
            return longitude;
    }

    public float getCurrentDistanceMeters() {
            return currentDistanceMeters;
    }

    public void setCurrentDistanceMeters(float currentDistanceMeters) {
            this.currentDistanceMeters = currentDistanceMeters;
    }

    @Override
    public String toString() {
    		StringBuilder sb = new StringBuilder();    		
    		sb.append("Stop name = " + getStopName() + ": ");
    		sb.append("id = " + stopId + ", ");
    		sb.append("latitude = " + latitude + ", ");
    		sb.append("longitude = " + longitude + ", ");
       		sb.append("nbBikes = " + nbBikes + ", ");   
       		sb.append("nbEmptyDocks = " + nbEmptyDocks + ", "); 
       		sb.append("nbDocks = " + nbDocks + ", ");
       		sb.append("locked = " + locked);
            return sb.toString();
    }

}
