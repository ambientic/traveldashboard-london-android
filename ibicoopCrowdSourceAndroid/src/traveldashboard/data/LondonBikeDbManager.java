package traveldashboard.data;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

import traveldashboard.data.IbicoopDataConstants;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class LondonBikeDbManager {
		
	private String city = "";
	
	private Context context;
	private LondonBikeDbHelper dbHelper;
	private LondonBikeDbUpdateCallback callback;

	public interface LondonBikeDbUpdateCallback {
		public void londonBikeDbUpdate();
	}
	
	public LondonBikeDbManager(Context ctx, LondonBikeDbUpdateCallback updateCallback) {
		context = ctx;
		city = IbicoopDataConstants.LONDON;
		dbHelper = new LondonBikeDbHelper(context);
		callback = updateCallback;
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Created bike db manager for " + city);
	}
	
	public void deleteDb() {
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Delete database for " + city);
		SQLiteDatabase writableDb = dbHelper.getWritableDatabase();
		writableDb.delete(LondonBikeDbHelper.BikeDbEntry.TABLE_NAME, null, null);
	}
	
	public int getStopInfoDbCount() {
		int count = -1;
		SQLiteDatabase readableDb =  dbHelper.getReadableDatabase();
		Cursor cursor = readableDb.rawQuery("select * from " + LondonBikeDbHelper.BikeDbEntry.TABLE_NAME, null);
		if (cursor != null) {
			count = cursor.getCount();
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Db table count = " + count);
			cursor.close();
		}
		return count;
	}
	

	public void testDb(String stopId) {
		String query =  "select * " 
				+ " from " + LondonBikeDbHelper.BikeDbEntry.TABLE_NAME + 	
				" where " + LondonBikeDbHelper.BikeDbEntry.PARAM_KEY_STOP_ID + "='" + stopId+ "'";
				
		SQLiteDatabase readableDb =  dbHelper.getReadableDatabase();
	
		
	    Cursor ti = readableDb.rawQuery("PRAGMA table_info(" + LondonBikeDbHelper.BikeDbEntry.TABLE_NAME + ")", null);
	    if ( ti.moveToFirst() ) {
	        do {
	        	if (IbicoopDataConstants.DEBUG_MODE) System.out.println("col: " + ti.getString(1));
	        } while (ti.moveToNext());
	    }
		
		
		Cursor cursor = readableDb.rawQuery(query, null);

		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("cursor = " + cursor.toString() + ", count = " + cursor.getCount());
	}
	

	public void insertStopIntoDbByInputStreams(InputStream is) {
		
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			
			StringBuilder sb = new StringBuilder();
			
			String line;
			
			while((line=br.readLine())!= null){
			    sb.append(line.trim());
			}
			
			br.close();
					
		    // Convert XML to JSON Object
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println(sb.toString());
            JSONObject xmlJSONObj = XML.toJSONObject(sb.toString());
			
			JSONObject stationsObj = xmlJSONObj.getJSONObject("stations");
			
			JSONArray stationArray = stationsObj.getJSONArray("station");

			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Station array length = " + stationArray.length());
			
			SQLiteDatabase writableDb = dbHelper.getWritableDatabase();
			
			for (int i = 0; i  < stationArray.length(); i++) {	
				JSONObject jsonCurrentObject = stationArray.getJSONObject(i);
				String id = jsonCurrentObject.getString("id");
				String name = jsonCurrentObject.getString("name");
				double latBike = jsonCurrentObject.getDouble("lat");
				double lonBike = jsonCurrentObject.getDouble("long");
				int nbBikes = jsonCurrentObject.getInt("nbBikes");
				int nbEmptyDocks = jsonCurrentObject.getInt("nbEmptyDocks");
				int nbTotalDocks = jsonCurrentObject.getInt("nbDocks");
				boolean locked = jsonCurrentObject.getBoolean("locked");
				
				ContentValues values = new ContentValues();
				values.put(LondonBikeDbHelper.BikeDbEntry.PARAM_KEY_STOP_ID, id);
				values.put(LondonBikeDbHelper.BikeDbEntry.PARAM_KEY_STOP_NAME, name);	
				values.put(LondonBikeDbHelper.BikeDbEntry.PARAM_KEY_STOP_LAT, latBike);
				values.put(LondonBikeDbHelper.BikeDbEntry.PARAM_KEY_STOP_LON, lonBike);
				values.put(LondonBikeDbHelper.BikeDbEntry.PARAM_KEY_NB_BIKES, nbBikes);
				values.put(LondonBikeDbHelper.BikeDbEntry.PARAM_KEY_NB_EMPTY_DOCKS, nbEmptyDocks);
				values.put(LondonBikeDbHelper.BikeDbEntry.PARAM_KEY_NB_DOCKS, nbTotalDocks);
				values.put(LondonBikeDbHelper.BikeDbEntry.PARAM_KEY_LOCKED, locked);
				
				long newRowId = writableDb.insert(
						LondonBikeDbHelper.BikeDbEntry.TABLE_NAME, 
						LondonBikeDbHelper.BikeDbEntry.COLUMN_NAME_NULLABLE, 
						values);
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("newRowId = " + newRowId + ": id = " + id + ", name = " + name);
			}
			
		} catch (Exception exception) {
			if (IbicoopDataConstants.DEBUG_MODE) System.err.println(exception.getMessage());
		}
	}
	
	public void	updateBikeInformation(String bikeXmlJsonString) {
		
		try {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Update bike information");
			
            JSONObject xmlJSONObj = XML.toJSONObject(bikeXmlJsonString);
			
			JSONObject stationsObj = xmlJSONObj.getJSONObject("stations");
			
			JSONArray stationArray = stationsObj.getJSONArray("station");

			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Station array length = " + stationArray.length());
			
			SQLiteDatabase writableDb = dbHelper.getWritableDatabase();
			
			for (int i = 0; i  < stationArray.length(); i++) {	
				JSONObject jsonCurrentObject = stationArray.getJSONObject(i);
				String id = jsonCurrentObject.getString("id");
				String name = jsonCurrentObject.getString("name");
				double latBike = jsonCurrentObject.getDouble("lat");
				double lonBike = jsonCurrentObject.getDouble("long");
				int nbBikes = jsonCurrentObject.getInt("nbBikes");
				int nbEmptyDocks = jsonCurrentObject.getInt("nbEmptyDocks");
				int nbTotalDocks = jsonCurrentObject.getInt("nbDocks");
				boolean locked = jsonCurrentObject.getBoolean("locked");
				
				ContentValues values = new ContentValues();
				values.put(LondonBikeDbHelper.BikeDbEntry.PARAM_KEY_NB_BIKES, nbBikes);
				values.put(LondonBikeDbHelper.BikeDbEntry.PARAM_KEY_NB_EMPTY_DOCKS, nbEmptyDocks);
				values.put(LondonBikeDbHelper.BikeDbEntry.PARAM_KEY_NB_DOCKS, nbTotalDocks);
				values.put(LondonBikeDbHelper.BikeDbEntry.PARAM_KEY_LOCKED, locked);
				
				long newRowId = writableDb.update(LondonBikeDbHelper.BikeDbEntry.TABLE_NAME, values, LondonBikeDbHelper.BikeDbEntry.PARAM_KEY_STOP_ID + "='" + id + "'" , null);
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("newRowId = " + newRowId + ": id = " + id + ", name = " + name);
			}
			
			if (callback != null) callback.londonBikeDbUpdate();
			
		} catch (Exception exception) {
			if (IbicoopDataConstants.DEBUG_MODE) System.err.println(exception.getMessage());
		}
	}
	
	
	public ArrayList<LondonBikeStopInfo> readFromDbByCoord(double latitude, double longitude, double radius) {
		
		ArrayList<LondonBikeStopInfo> londonBikeStopInfos = new ArrayList<LondonBikeStopInfo>();
		
        BoundingArea boundingArea = new BoundingArea(latitude, longitude, radius);
        double maxLat = boundingArea.getMaxLat();
        double minLat = boundingArea.getMinLat();
        double maxLon = boundingArea.getMaxLon();
        double minLon = boundingArea.getMinLon();
		
        if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Read from db by coord:");
        if (IbicoopDataConstants.DEBUG_MODE) System.out.println("lat = " + latitude + ", lon = " + longitude + ", radius = " + radius
        		+ ", maxLat = " + maxLat + ", minLat = " + minLat 
        		+ ", maxLon = " + maxLon + ", minLon = " + minLon);        
        
		SQLiteDatabase readableDb =  dbHelper.getReadableDatabase();
		
		String query =  "select * from " + LondonBikeDbHelper.BikeDbEntry.TABLE_NAME + 	" where " + 
				"(" + LondonBikeDbHelper.BikeDbEntry.PARAM_KEY_STOP_LAT + " >= " +  minLat + ") and " +
				"(" + LondonBikeDbHelper.BikeDbEntry.PARAM_KEY_STOP_LAT + " <= " +  maxLat + ") and " +					
				"(" + LondonBikeDbHelper.BikeDbEntry.PARAM_KEY_STOP_LON + " >= " +  minLon + ") and " +
				"(" + LondonBikeDbHelper.BikeDbEntry.PARAM_KEY_STOP_LON + " <= " +  maxLon + ")	";
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Query = " + query);
		
		Cursor cursor = readableDb.rawQuery(query, null);
		
		if (cursor != null) {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("cursor count = " + cursor.getCount());
			
			cursor.moveToFirst();
			
			for (int i = 0;  i < cursor.getCount(); i++) {

				String stopId = cursor.getString(cursor.getColumnIndexOrThrow(LondonBikeDbHelper.BikeDbEntry.PARAM_KEY_STOP_ID));
				String stopName = cursor.getString(cursor.getColumnIndexOrThrow(LondonBikeDbHelper.BikeDbEntry.PARAM_KEY_STOP_NAME));
				double lat = cursor.getDouble(cursor.getColumnIndexOrThrow(LondonBikeDbHelper.BikeDbEntry.PARAM_KEY_STOP_LAT));
				double lon = cursor.getDouble(cursor.getColumnIndexOrThrow(LondonBikeDbHelper.BikeDbEntry.PARAM_KEY_STOP_LON));		
				
				String nbBikes = cursor.getString(cursor.getColumnIndexOrThrow(LondonBikeDbHelper.BikeDbEntry.PARAM_KEY_NB_BIKES));
				String nbEmptyDocks = cursor.getString(cursor.getColumnIndexOrThrow(LondonBikeDbHelper.BikeDbEntry.PARAM_KEY_NB_EMPTY_DOCKS));
				String nbDocks = cursor.getString(cursor.getColumnIndexOrThrow(LondonBikeDbHelper.BikeDbEntry.PARAM_KEY_NB_DOCKS));
				String locked = cursor.getString(cursor.getColumnIndexOrThrow(LondonBikeDbHelper.BikeDbEntry.PARAM_KEY_LOCKED));
				
				
				LondonBikeStopInfo info = new LondonBikeStopInfo(
						stopId, 
						stopName, 
						lat, 
						lon, 
						nbBikes, 
						nbEmptyDocks, 
						nbDocks, 
						locked);
				
				londonBikeStopInfos.add(info);

				//Next row
				cursor.moveToNext();
			}
			
			cursor.close();
		}

		
		return londonBikeStopInfos;
	}
}
