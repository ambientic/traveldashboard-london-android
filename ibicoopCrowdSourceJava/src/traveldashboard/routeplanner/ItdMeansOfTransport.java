package traveldashboard.routeplanner;

public class ItdMeansOfTransport {

	private String name;
	private String destination;
	private MotType motType;
	
	public ItdMeansOfTransport(String name, String destination, MotType motType) {
		this.name = name;
		this.destination = destination;
		this.motType = motType;
	}
	
	public String getName() {
		return name;
	}
	
	public String getDestination() {
		return destination;
	}
	
	public MotType getMode() {
		return motType;
	}
	
	@Override 
	public String toString() {
		return "Name = " + name + ", destination = " + destination + ", mode = " + motType.getDesc();
	}
}
