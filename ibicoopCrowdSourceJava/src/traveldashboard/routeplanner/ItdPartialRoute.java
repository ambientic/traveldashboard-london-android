package traveldashboard.routeplanner;

import java.util.List;

public class ItdPartialRoute {

	private int timeMinute;
	private ItdPoint depPoint;
	private ItdPoint arrPoint;
	private List<ItdMeansOfTransport> itdMeansOfTransportList;
	
	public ItdPartialRoute(int timeMinute, ItdPoint depPoint, ItdPoint arrPoint, List<ItdMeansOfTransport> itdMeansOfTransportList) {
		this.timeMinute = timeMinute;
		this.depPoint = depPoint;
		this.arrPoint = arrPoint;
		this.itdMeansOfTransportList = itdMeansOfTransportList;
	}
	
	public int getTimeMinute() {
		return timeMinute;
	}
	
	public ItdPoint getDeparturePoint() {
		return depPoint;
	}
	
	public ItdPoint getArrivalPoint() {
		return arrPoint;
	}
	
	public List<ItdMeansOfTransport> getItdMeansOfTransportList() {
		return itdMeansOfTransportList;
	}
	
	@Override
	public String toString() {
		
		String info = "Departure point = " + depPoint.toString() 
				+ ", arrival point = " + arrPoint.toString() + ", "
				+ "time minute = " + timeMinute + ", ";
		
		for (int i = 0; i < itdMeansOfTransportList.size(); i++) {
			info = info + "\nitdMeansOfTransport " + (i + 1) + " = " + itdMeansOfTransportList.get(i).toString() + "; ";
		}
		
		return info;
	}
}
