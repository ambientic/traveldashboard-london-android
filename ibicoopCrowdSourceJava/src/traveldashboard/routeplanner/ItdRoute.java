package traveldashboard.routeplanner;

import java.util.List;

public class ItdRoute {

	private int routeTripIndex;
	private int vehicleTime;
	private String publicDuration;
	private List<ItdPartialRoute> itdPartialRouteList;
	
	public ItdRoute(int routeTripIndex, int vehicleTime, String publicDuration, List<ItdPartialRoute> itdPartialRouteList) {
		this.routeTripIndex = routeTripIndex;
		this.vehicleTime = vehicleTime;
		this.publicDuration = publicDuration;
		this.itdPartialRouteList = itdPartialRouteList;
	}
	
	public int getRouteTripIndex() {
		return routeTripIndex;
	}
	
	public int getVehicleTime() {
		return vehicleTime;
	}
	
	public String getPublicDuration() {
		return publicDuration;
	}
	
	public List<ItdPartialRoute> getItdPartialRouteList() {
		return itdPartialRouteList;
	}
	
	@Override
	public String toString() {
		String info = "";
		
		info = "Route trip index = " + routeTripIndex + ", vehicle time = " + vehicleTime 
				+ ", public duration = " + publicDuration + ", ";
		
		for (int i = 0; i < itdPartialRouteList.size(); i++) {
			info = info + "\nitdPartialRoute " + (i + 1) + " = " +  itdPartialRouteList.get(i).toString() + "; ";
		}
		
		return info;
	}
	
}
