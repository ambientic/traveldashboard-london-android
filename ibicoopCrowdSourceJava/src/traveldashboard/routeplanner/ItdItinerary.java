package traveldashboard.routeplanner;

import java.util.List;

public class ItdItinerary {

	private List<ItdRoute> itdRouteList;
	
	public ItdItinerary(List<ItdRoute> itdRouteList) {
		this.itdRouteList = itdRouteList;	
	}
	
	public List<ItdRoute> getItdRouteList() {
		return itdRouteList;
	}
	
	@Override
	public String toString() {
		
		String info = "";
		
		for (int i = 0; i < itdRouteList.size(); i++) {
			info = info + "\n\nitdRoute " + (i + 1) + " = " + itdRouteList.get(i).toString() + ";";
		}
		
		return info;
	}
}
