package traveldashboard.routeplanner;

import java.util.HashMap;

public class ItdDateTime {

	private int year;
	private int month;
	private int day;
	private int weekday;
	private int hour;
	private int minute;
	
	public static final HashMap<Integer, String> WEEKDAYMAPS;
	
	static {
		WEEKDAYMAPS = new HashMap<Integer, String>();
		WEEKDAYMAPS.put(1, "Sunday");
		WEEKDAYMAPS.put(2, "Monday");
		WEEKDAYMAPS.put(3, "Tuesday");
		WEEKDAYMAPS.put(4, "Wednesday");
		WEEKDAYMAPS.put(5, "Thursday");
		WEEKDAYMAPS.put(6, "Friday");
		WEEKDAYMAPS.put(7, "Saturday");
	}
	
	
	public ItdDateTime(int year, int month, int day, int weekday, int hour, int minute) {
		this.year = year;
		this.month = month;
		this.day = day;
		this.weekday = weekday;
		this.hour = hour;
		this.minute = minute;
	}
	
	public int getYear() {
		return year;
	}
	
	public int getMonth() {
		return month;
	}
	
	public int getDay() {
		return day;
	}
	
	public int getWeekday() {
		return weekday;
	}
	
	public int getHour() {
		return hour;
	}
	
	public int getMinute() {
		return minute;
	}
	
	@Override
	public String toString() {
		String info = "";
		
		info = day + "/" + month + "/" + year + " " +  WEEKDAYMAPS.get(weekday) + " " + hour + ":" + minute;
		
		return info;
	}
	
	
}
