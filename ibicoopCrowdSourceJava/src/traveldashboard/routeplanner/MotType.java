package traveldashboard.routeplanner;

public enum MotType {

	TRAIN(0, "Train"),
	COMMUTER_RAILWAY(1, "Commuter railway"),
	UNDERGROUND(2, "Underground"),
	CITY_RAIL(3, "City rail"),
	TRAM(4, "Tram"),
	CITY_BUS(5, "City bus"),
	REGIONAL_BUS(6, "Regional bus"),
	COACH(7, "Coach"),
	CABLE_CAR(8, "Cable car"),
	BOAT(9, "Boat"),
	TRANSIT(10, "Transit on demand"),
	OTHER(11, "Other");
	
	private final int id;
	private final String motType;
	
	private MotType(int id, String motType) {
		this.id = id;
		this.motType = motType;
	}
	
	public int getId() {
		return id;
	}
	
	public String getDesc() {
		return motType;
	}

	@Override
	public String toString() {
		return "id = " + id + ", " + motType;
	}
}
