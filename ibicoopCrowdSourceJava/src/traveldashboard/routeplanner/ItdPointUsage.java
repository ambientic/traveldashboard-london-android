package traveldashboard.routeplanner;

public enum ItdPointUsage {
	DEPARTUTE("departure"), ARRIVAL("arrival");
	
	private final String usage;
	
	private ItdPointUsage(String usage) {
		this.usage = usage;
	}
	
	@Override
	public String toString() {
		return usage;
	}
	
}
