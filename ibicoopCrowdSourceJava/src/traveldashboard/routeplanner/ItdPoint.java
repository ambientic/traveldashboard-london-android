package traveldashboard.routeplanner;


public class ItdPoint {

	private ItdPointUsage usage;//departure or arrival
	private String stationName;
	private String platformName;
	private ItdDateTime itdDateTime;
	private int crowdLevel;
	private int noiseLevel;
	
	public ItdPoint(String stationName, String platformName, ItdPointUsage usage, ItdDateTime itdDateTime
			, int crowdLevel, int noiseLevel) {
		this.stationName = stationName;
		this.platformName = platformName;
		this.usage = usage;
		this.itdDateTime = itdDateTime;
		this.crowdLevel = crowdLevel;
		this.noiseLevel = noiseLevel;
	}
	
	public String getStationName() {
		return stationName;
	}
	
	public String getPlatformName() {
		return platformName;
	}
	
	public ItdPointUsage getItdPointUsage() {
		return usage;
	}
	
	public ItdDateTime getItdDateTime() {
		return itdDateTime;
	}
	
	public int getCrowdLevel() {
		return crowdLevel;
	}
	
	public int getNoiseLevel() {
		return noiseLevel;
	}
	
	@Override
	public String toString() {
		return "Station name = " + stationName + ", " + "platformName = " + platformName 
				+ ", usage = " + usage.toString() + " time = " + itdDateTime.toString()
				+ ", crowd level = " + crowdLevel + ", noise level = " + noiseLevel;
	}
}