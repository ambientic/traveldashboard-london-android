package traveldashboard.data;

/**
 * Collection of tube stations
 * @author khoo
 *
 */
public class TubeStationsCollection {
	
	protected TubeStation[] stations;
	
	protected TubeStationsCollection() {}
	
	/**
	 * Constructor of tube stations collection
	 * @param tubeStations
	 */
	public TubeStationsCollection(TubeStation[] tubeStations) {
		this.stations = tubeStations;
	}
	
	/**
	 * Get tube stations
	 * @return tube stations
	 */
	public TubeStation[] getTubeStations() {
		return stations;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < stations.length; i++) {
			sb.append(stations[i].toString() + "\n");
		}
		
		return sb.toString();
	}
	
}