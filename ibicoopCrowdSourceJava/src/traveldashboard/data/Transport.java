package traveldashboard.data;

/**
 * Transport
 * @author khoo
 *
 */
public class Transport {
	
	protected String id;
	protected String name;
	protected TransportType type;
	protected String destination;
	
	protected Transport() {}
	
	/**
	 * Transport constructor
	 * @param id Transport id
	 * @param name Transport name
	 * @param type Transport type
	 * @param destination Destination
	 */
	public Transport(String id, String name, TransportType type, String destination) {
		this.id = id;
		this.name = name;
		this.type = type;
		this.destination = destination;
	}
	
	/**
	 * Get transport id
	 * @return transport id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Get transport name
	 * @return transport name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Get transport type
	 * @return transport type
	 */
	public TransportType getType() {
		return type;
	}

	/**
	 * Get destination
	 * @return destination
	 */
	public String getDestination() {
		return destination;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Id: " + id + "\n");
		sb.append("Name: " + name + "\n");
		sb.append("Type: " + type.toString() + "\n");
		sb.append("Destination: " + destination + "\n");
		return sb.toString();
	}
	
}
