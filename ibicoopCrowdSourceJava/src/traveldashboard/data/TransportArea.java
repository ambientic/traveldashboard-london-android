package traveldashboard.data;

public final class TransportArea {

	//London area
	public static final String LONDON = "london";
	//London area max lat reference city: Luton
	public static final double LONDON_MAX_LAT = 52.0;
	//London area min lat reference city: Crawley
	public static final double LONDON_MIN_LAT = 51.0;
	//London area max lon reference city: Chelmsford
	public static final double LONDON_MAX_LON = 0.5;
	//London area min lon reference city: Reading
	public static final double LONDON_MIN_LON = -1.0;
	
	//Paris area
	public static final String PARIS = "paris";
	//Paris area max lat reference city: Orry-la-Ville-Coye
	public static final double PARIS_MAX_LAT = 49.5;
	//Paris area min lat reference city: Malesherbes
	public static final double PARIS_MIN_LAT = 48.0;
	//Paris area max lon reference city: Marne la Vall�e Chessy
	public static final double PARIS_MAX_LON = 3.0;
	//Paris area min lon reference city: Dourdan-la-For�t
	public static final double PARIS_MIN_LON = 1.5;
	
	//Other area
	public static final String OTHER = "Other";
	
	
	public static String getArea(double latitude, double longitude) {
		if ((latitude <= LONDON_MAX_LAT) && (latitude >= LONDON_MAX_LON) && (longitude <= LONDON_MAX_LON) && (longitude >= LONDON_MIN_LON)) {
			return LONDON;
		}
		else if ((latitude <= PARIS_MAX_LAT) && (latitude >= PARIS_MAX_LON) && (longitude <= PARIS_MAX_LON) && (longitude >= PARIS_MIN_LON)) {
			return PARIS;
		}
		
		return OTHER;
	}
	
}
