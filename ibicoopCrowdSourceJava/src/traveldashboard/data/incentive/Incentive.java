package traveldashboard.data.incentive;

/**
 * Incentive class, can be of type fact or of type joke
 * @author khoo
 *
 */
public class Incentive {

	private String stopId;
	private String stopName;
	private IncentiveType incentiveType;
	private String stopIncentiveContent;
	//Add type
	
	public Incentive(String stopId, String stopName, IncentiveType incentiveType, String stopIncentiveContent) {
		this.stopId = stopId;
		this.stopName = stopName;
		this.incentiveType = incentiveType;
		this.stopIncentiveContent = stopIncentiveContent;
	}
	
	public String getStopId() {
		return stopId;
	}
	
	public String getStopName() {
		return stopName;
	}
	
	public IncentiveType getIncentiveType() {
		return incentiveType;
	}
	
	public String getStopIncentiveContent() {
		return stopIncentiveContent;
	}
	
	@Override
	public String toString() {
		return "stop id = " + stopId + ", stop name = " + stopName 
				+ ", incentive type = " + incentiveType.toString()
				+ ", incentive = " + stopIncentiveContent;
	}
}
