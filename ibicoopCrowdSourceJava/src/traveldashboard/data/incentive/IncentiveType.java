package traveldashboard.data.incentive;

public enum IncentiveType {
	FACT("Fact"), JOKE("Joke");
	
	private String desc = "";
	
	private IncentiveType(String desc) {
		this.desc = desc;
	}
	
	@Override
	public String toString() {
		return desc;
	}
}
