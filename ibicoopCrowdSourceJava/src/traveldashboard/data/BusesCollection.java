package traveldashboard.data;

/**
 * Buses collection
 * @author khoo
 *
 */
public class BusesCollection {
	
	protected Bus[] buses;
	
	protected BusesCollection() {}
	
	/**
	 * Constructor of buses collection
	 * @param buses
	 */
	public BusesCollection(Bus[] buses) {
		this.buses = buses;
	}
	
	/**
	 * Get buses
	 * @return buses
	 */
	public Bus[] getBuses() {
		return buses;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < buses.length; i++) {
			sb.append(buses[i].toString() + "\n");
		}
		return sb.toString();
	}
	
}