package traveldashboard.data;

public class Tram extends Transport {

	protected Tram() {}
	
	/**
	 * Tram constructor
	 * @param id Tram id
	 * @param name Tram name
	 * @param destination Destination
	 */
	public Tram(String id, String name, String destination) {
		super(id, name, TransportType.TRAM, destination);

	}
}
