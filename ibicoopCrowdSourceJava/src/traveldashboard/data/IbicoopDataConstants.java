package traveldashboard.data;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import traveldashboard.data.crowd.CrowdLevel;
import traveldashboard.data.rating.RatingLevel;

public class IbicoopDataConstants {
	
	//Debug mode
	public static final boolean DEBUG_MODE = false;
	
	//City type
	public static final String PARIS = "paris";
	public static final String LONDON = "london";
	
	//Simulation mode
	public static final boolean SIMULATION_MODE = false;
	public static final boolean USE_JOKE = true;
	
	//Noise value
	public static final int NOISE_MAX_VALUE = 3000;
	
	
	//In region monitoring
	public static final String MOCK_LOCATION_PROVIDER = "MockLocationProvider";
	//Radius in meter
	public static final float MONITORING_RADIUS = 100; //100m
	//Notification
	public static final String PROXIMITY_NOTIFICATION_TITLE = "TravelDashboard proximity alert";
	public static final String PROXIMITY_NOTIFICATION_CONTENT = "You are near the station ";	
	
	
	/**
	 * Location related constants minimum accuracy to accept a location obtained
	 */
	public static final int MIN_ACCURACY_RADIUS_METERS = 11500;
	/**
	 * maximum time duration since the GPS fix was obtained
	 */
	public static final int MAX_FIX_DELAY_MILLIS = 7 * 60 * 1000;
	/**
	 * NOT SHOWING AS NEARBY all stations which are further than:
	 */
	public static final int MAX_DISTANCE_METERS = 15000; // showing stations	
	
	
	//
	//
	//Time constants
	//For connection
	public static final int TIMEOUT_CONNECTION = 5000;
	public static final int TIMEOUT_SOCKET = 5000;
	public static final int TIMEOUT_SEND_FILE_S = 30;
	
	
	
	//For time interval in ms
	public static final String TIME_INTERVAL = "TimeInterval";
	public static final long ONE_SECOND = 1000;
	public static final long ONE_MINUTE = ONE_SECOND*60;
	public static final long ONE_HOUR = ONE_MINUTE*60;
	public static final long HALF_DAY = ONE_HOUR*12;
	public static final long ONE_DAY = ONE_HOUR*24;
	public static final long ONE_WEEK = ONE_DAY*7;
	public static final long ONE_MONTH = ONE_DAY*31;
	
	
	
	
	//
	//
	//
	//For Ibicoop file exchange sender and receiver configuration
	public static final String EXCHANGE_LOCATION = "TravelDashboardFileExchange/";

	public static final String EXCHANGE_RECEIVER_LOCATION = EXCHANGE_LOCATION + "ExchangeReceiver/";
	public static final String EXCHANGE_RECEIVER_FILE_NAME = EXCHANGE_RECEIVER_LOCATION + "exchangeReceiver.txt";
	public static final String EXCHANGE_RECEIVER_FILE_NAME2 = EXCHANGE_RECEIVER_LOCATION + "exchangeReceiver2.txt";

	public static final String EXCHANGE_SENDER_LOCATION = EXCHANGE_LOCATION + "ExchangeSender/";
	public static final String EXCHANGE_SENDER_FILE_NAME = EXCHANGE_SENDER_LOCATION + "exchangeSender.txt";
	public static final String EXCHANGE_SENDER_FILE_NAME2 = EXCHANGE_SENDER_LOCATION + "exchangeSender2.txt";

	public static final String EXCHANGE_COMMON_LOCATION = EXCHANGE_LOCATION + "ExchangeLocation/";
	public static final String EXCHANGE_COMMON_FILE_NAME = EXCHANGE_COMMON_LOCATION + "exchange.txt";
	public static final String EXCHANGE_COMMON_FILE_NAME2 = EXCHANGE_COMMON_LOCATION + "exchange2.txt";

	public static final String FILE_EXCHANGE_SERVICE = "fileExchangeService";
	public static final String PATH_GET_FILE_EXCHANGE = "getFileExchangePath";	
	
	public static final String FILE_EXCHANGE_IBIURL = "fileExchangeIbiurl";	
	
	
	
	
 			
    
	
	//
	//
	//
	//General receiver IBIURL
	public static final String APPLICATION = "APPLICATION_TraveldashboardLondonParisFinal";
	public static final String USER = "USER_TraveldashboardLondonParisFinal@ibicoop.org";
	public static final String USER_HEAD = "USER_TraveldashboardLondonParisFinal";
	public static final String USER_END = "@ibicoop.org";
	public static final String TERMINAL = "TERMINAL_TraveldashboardLondonParisFinal";
	//For IbiEvent
	//Broker name
	public static final String BROKER_NAME = "TraveldashboardApplicationBrokerLondonParisFinal";





	//Total waiting time in second for activation of broker
	public static final int BROKER_START_TIMEOUT_SECONDS = 30;
	//Loop waiting time for activiation of broker
	public static final int BROKER_LOOP_WAIT_MS = 250;	
	
	
	//
	//
	//For all stops
	//Network message parameters
	public static final String PARAM_VALUE_STOP_ID = "ID";
	public static final String PARAM_KEY_STOP_ID = "StopId";
	public static final String PARAM_KEY_STOP_NAME = "Name";
	public static final String PARAM_KEY_STOP_LON = "Lon";
	public static final String PARAM_KEY_STOP_LAT = "Lat";
	public static final String PARAM_KEY_ROUTE_ID = "RouteId";
	public static final String PARAM_KEY_ROUTE_NAME = "RouteName";
	public static final String PARAM_KEY_DIRECTION_ID = "Direction";
	public static final String PARAM_KEY_ROUTE_TYPE = "RouteType";
	public static final String PARAM_KEY_CITY_TYPE = "CityType";
	public static final String PARAM_KEY_RADIUS = "Radius";
	public static final String PARAM_KEY_MAX_VALUES = "MaxValues";
	
	
	
	//Receiver IBIURL
	public static final String USER_ALL_STOPS = "TraveldashboardAllStops@ibicoop.org";
	public static final String TERMINAL_ALL_STOPS = "TraveldashboardAllStops";
	public static final String GET_ALL_STOPS_SERVICE = "stop";
	public static final String PATH_GET_ALL_STOPS = "getAll";
	public static final String OPERATION_GET_ALL_STOPS = "GetAllStops";

	
	//
	//
	//
	//For route planner
	//Network message parameters
	public static final String OPERATION_GET_ROUTE_PLANNER = "GetRoutePlanner";
	public static final String ROUTE_PLANNER_MESSAGE = "RoutePlannerMessage";
	
	//Receiver UBIURL
	public static final String ROUTE_PLANNER_SERVICE = "RoutePlanner";
	public static final String PATH_GET_ROUTE_PLANNER = "getRoutePlanner";
	public static final String USER_ROUTE_PLANNER = "TraveldashboardUserRoutePlanner@ibicoop.org";
	public static final String TERMINAL_ROUTE_PLANNER = "TraveldashboardRoutePlanner";
	
	public static final String ROUTE_PLANNER_CITY ="RoutePlannerCity";
	public static final String DEPART_STOP_NAME ="DepartStopName";
	public static final String ARRIVAL_STOP_NAME ="ArrivalStopName";
	public static final String DEPART_DATE ="DepartDate";
	public static final String DEPART_TIME ="DepartTime";
	public static final String TOTAL_MOT_TYPE_INCLUDED ="TotalMotTypeIncluded";
	public static final String MOT_TYPE_INCLUDED_ ="MotTypeIncluded_";

	
	
	//
	//
	//
	//For bike countdown
	//Network message parameters
	public static final String OPERATION_GET_BIKE = "GetBike";
	public static final String BIKE_MESSAGE = "BikeMessage";
	
	//Receiver UBIURL
	public static final String BIKE_SERVICE = "bike";
	public static final String PATH_GET_BIKE = "getBike";
	public static final String USER_BIKE = "TraveldashboardUserBike@ibicoop.org";
	public static final String TERMINAL_BIKE = "TraveldashboardBike";	
	
	
	
	
	//
	//
	//
	//For bus stop
	//Network message parameters
	//For bus stop
	public static final String OPERATION_GET_BUS_STOP = "GetBusStop";
	public static final String BUS_STOP_MESSAGE = "BusStopMessage";

	
	//Receiver IBIURL
	//Create receiver on server and sender on mobile
	public static final String BUS_STOP_SERVICE = "busStop";
	public static final String PATH_GET_BUS_STOP = "getBusStop";
	
	
	//For mobile
	public static final String USER_BUS_STOP = "TraveldashboardUserBusStop@ibicoop.org";
	public static final String TERMINAL_BUS_STOP = "TraveldashboardBusStop";
	
	
	
	//
	//
	//
	//For bus countdown
	//Network message parameters

	
	public static final String OPERATION_GET_BUS_COUNTDOWN = "GetBusCountdown";
	public static final String BUS_COUNTDOWN_MESSAGE = "BusCountdownMessage";
	
	//Receiver UBIURL
	public static final String BUS_COUNTDOWN_SERVICE = "busCountdown";
	public static final String PATH_GET_BUS_COUNTDOWN = "getBusCountdown";
	public static final String USER_BUS_COUNTDOWN = "TraveldashboardUserBusCountdown@ibicoop.org";
	public static final String TERMINAL_BUS_COUNTDOWN = "TraveldashboardBusCountdown";	
	
	//For bus countdown
	public static final String ROUTE = "Route";
	public static final String TIME = "Time";	
	public static final String DESTINATION = "Destination";
	
	
	
	//
	//
	//For tube stations
	
	//Network message parameters
	public static final String OPERATION_GET_TUBE_STATION = "GetTubeStation";
	public static final String TUBE_STATION_MESSAGE = "TubeStationMessage";
	
	//Receiver IBIURL
	public static final String TUBE_STATION_SERVICE = "tubeStationService";
	public static final String PATH_GET_TUBE_STATION = "getTubeStationPath";
	public static final String USER_TUBE_STATION = "TraveldashboardTubeStation@ibicoop.org";
	public static final String TERMINAL_TUBE_STATION = "TraveldashboardTubeStation";
		
	
	
	
	
	
	
	
	
	//
	//
	//For tube countdown
	
	//Network messgae paramters
	public static final String OPERATION_GET_TUBE_COUNTDOWN = "GetTubeCountdown";
	public static final String TUBE_COUNTDOWN_MESSAGE = "TubeCountdownMessage";
	
	//Receiver IBIURL
	public static final String TUBE_COUNTDOWN_SERVICE = "tubeCountdownService";
	public static final String PATH_GET_TUBE_COUNTDOWN = "getTubeCountdownPath";
	public static final String USER_TUBE_COUNTDOWN = "TraveldashboardTubeCountdown@ibicoop.org";
	public static final String TERMINAL_TUBE_COUNTDOWN = "TraveldashboardTubeCountdown";
	
	
	//For tube count down
	public static final String PLATFORM = "Route";
	
	
	//
	//
	//
	//For tram stations
	
	//Network message parameters
	public static final String OPERATION_GET_TRAM_STATION = "GetTramStation";
	public static final String TRAM_STATION_MESSAGE = "TramStationMessage";
	
	//Receiver IBIURL
	public static final String TRAM_STATION_SERVICE = "tramStationService";
	public static final String PATH_GET_TRAM_STATION = "getTramStationPath";
	public static final String USER_TRAM_STATION = "TraveldashboardTramStation@ibicoop.org";
	public static final String TERMINAL_TRAM_STATION = "TraveldashboardTramStation";

	

	
	//
	//
	//
	//For tram countdown
	//Network message parameters
	public static final String OPERATION_GET_TRAM_COUNTDOWN = "GetTramCountdown";
	public static final String TRAM_COUNTDOWN_MESSAGE = "TramCountdownMessage";	
	
	
	//Receiver IBIURL
	public static final String TRAM_COUNTDOWN_SERVICE = "tramCountdownService";
	public static final String PATH_GET_TRAM_COUNTDOWN = "getTramCountdownPath";
	public static final String USER_TRAM_COUNTDOWN = "TraveldashboardTramCountdown@ibicoop.org";
	public static final String TERMINAL_TRAM_COUNTDOWN = "TraveldashboardTramCountdown";	
	
	
	

	
	//
	//
	//
	//Crowd
	//Network message parameters
	
	//Receiver IBIURL
	public static final String GENERAL_CROWDEDNESS_SERVICE = "generalCrowdedness";
	public static final String PATH_GET_GENERAL_CROWDEDNESS = "getGeneralCrowdedness";
	public static final String USER_GENERAL_CROWDEDNESS = "TraveldashboardGeneralCrowdedness@ibicoop.org";
	public static final String TERMINAL_GENERAL_CROWDEDNESS = "TraveldashboardGeneralCrowdedness";
	
	//For getting fixed time crowdedness
	public static final String CLASSIFIED_CROWDEDNESS_SERVICE = "classifiedCrowdedness";
	public static final String PATH_GET_CLASSIFIED_CROWDEDNESS = "getClassifiedCrowdedness";
	public static final String USER_CLASSIFIED_CROWDEDNESS = "TraveldashboardClassifiedCrowdedness@ibicoop.org";
	public static final String TERMINAL_CLASSIFIED_CROWDEDNESS = "TraveldashboardClassifiedCrowdedness";

	
	// on-demand vote parameters
	public static final String GLOBAL_ON_DEMAND_CHANNEL = "RegCh_ParisLondon_All";
	public static final String OD_VOTE_EVENT_NAME = "vote_event";
	public static final String OD_KEY_STATION_CITY = "station_city";
	public static final String OD_KEY_STATION_ID = "station_id";
	public static final String OD_KEY_STATION_NAME = "station_name";
	public static final String OD_KEY_LATITUDE = "latitude";
	public static final String OD_KEY_LONGITUDE = "longitude";
	public static final String OD_KEY_ROUTE_ID = "route_id";
	public static final String OD_KEY_DIRECTION = "direction";
	
	// on-demand response
	public static final String OD_RESPONSE_EVENT_NAME = "vote_response_event";
	public static final String OD_KEY_LEVEL = "vote_response_event";	
	
	
	//
	//
	//
	//For tube comment
	//Network message parameters
	public static final String OPERATION_GET_TUBE_COMMENT = "GetTubeComment";
	public static final String TUBE_COMMENT_MESSAGE = "TubeCommentMessage";
	
	//Receiver IBIURL
	public static final String TUBE_COMMENT_SERVICE = "tubeComment";
	public static final String PATH_GET_TUBE_COMMENT = "getTubeComment";
	public static final String USER_TUBE_COMMENT = "TraveldashboardTubeComment@ibicoop.org";
	public static final String TERMINAL_TUBE_COMMENT = "TraveldashboardTubeComment";
	
	
	
	
	
	//
	//
	//
	//For user comment
	//Nerwork message parameters
	public static final String OPERATION_GET_USER_COMMENT = "GetUserComment";
	public static final String USER_COMMENT_MESSAGE = "UserCommentMessage";
	
	
	//Receiver IBIURL
	public static final String USER_COMMENT_SERVICE = "userComment";
	public static final String PATH_GET_USER_COMMENT = "getUserComment";
	public static final String USER_USER_COMMENT = "TraveldashboardUserComment@ibicoop.org";
	public static final String TERMINAL_USER_COMMENT = "TraveldashboardUserComment";
	
	
	

	
	//
	//
	//
	//For twitter comment
	//Network message parameters
	public static final String STATION_NAME = "StationName";
	public static final String OPERATION_GET_TWITTER_COMMENT = "GetTwitterComment";
	public static final String TWITTER_COMMENT_MESSAGE = "TwitterCommentMessage";
	
	//Receiver ibiurl
	public static final String TWITTER_COMMENT_SERVICE = "twitterComment";
	public static final String PATH_GET_TWITTER_COMMENT = "getTwitterComment";
	public static final String USER_TWITTER_COMMENT = "TraveldashboardTwitterComment@ibicoop.org";
	public static final String TERMINAL_TWITTER_COMMENT = "TraveldashboardTwitterComment";
	
	
	
	
	//
	//
	//
	//For facebook comment
	//Network message
	public static final String OPERATION_GET_FACEBOOK_COMMENT = "GetFacebookComment";
	public static final String FACEBOOK_COMMENT_MESSAGE = "FacebookCommentMessage";
	
	
	//Receiver IBIURL
	public static final String FACEBOOK_COMMENT_SERVICE = "facebookComment";
	public static final String PATH_GET_FACEBOOK_COMMENT = "getFacebookComment";
	public static final String USER_FACEBOOK_COMMENT = "TraveldashboardFacebookComment@ibicoop.org";
	public static final String TERMINAL_FACEBOOK_COMMENT = "TraveldashboardFacebookComment";
	

	public static final String TYPE_COMMENT = "Comment";
	public static final String TYPE_TWITTER = "Twitter";
	public static final String TYPE_FACEBOOK = "Facebook";
	public static final String TEXT = "text";
	

	
	//
	//
	//For tube status
	//Network message parameters
	public static final String OPERATION_GET_ALL_TUBE_STATUS = "GetAllTubeStatus";
	public static final String ALL_TUBE_STATUS_MESSAGE = "AllTubeStatusMessage";
	
	public static final String OPERATION_GET_TUBE_STATUS = "GetTubeStatus";
	public static final String TUBE_STATUS_MESSAGE = "TubeStatusMessage";
	
	
	//Receiver IBIURL
	public static final String ALL_TUBE_STATUS_SERVICE = "allTubeStatus";
	public static final String PATH_GET_ALL_TUBE_STATUS = "getAllTubeStatus";
	public static final String USER_ALL_TUBE_STATUS = "TraveldashboardAllTubeStatus@ibicoop.org";
	public static final String TERMINAL_ALL_TUBE_STATUS = "TraveldashboardAllTubeStatus";
	
	public static final String TUBE_STATUS_SERVICE = "tubeStatus";
	public static final String PATH_GET_TUBE_STATUS = "getTubeStatus";
	public static final String USER_TUBE_STATUS = "TraveldashboardTubeStatus@ibicoop.org";
	public static final String TERMINAL_TUBE_STATUS = "TraveldashboardTubeStatus";
	
	public static final String STATUS = "Status";	
	
	//
	//
	//For tube rating
	//Network message parameters
	public static final String OPERATION_GET_ALL_TUBE_RATING = "GetAllTubeRating";
	public static final String ALL_TUBE_RATING_MESSAGE = "AllTubeRatingMessage";
	
	public static final String OPERATION_GET_TUBE_RATING = "GetTubeRating";
	public static final String TUBE_RATING_MESSAGE = "TubeRatingMessage";
	
	//Receiver IBIURL
	public static final String ALL_TUBE_RATING_SERVICE = "allTubeRating";
	public static final String PATH_GET_ALL_TUBE_RATING = "getAllTubeRating";
	public static final String USER_ALL_TUBE_RATING = "TraveldashboardAllTubeRating@ibicoop.org";
	public static final String TERMINAL_ALL_TUBE_RATING = "TraveldashboardAllTubeRating";
	
	public static final String TUBE_RATING_SERVICE = "tubeRating";
	public static final String PATH_GET_TUBE_RATING = "getTubeRating";
	public static final String USER_TUBE_RATING = "TraveldashboardTubeRating@ibicoop.org";
	public static final String TERMINAL_TUBE_RATING = "TraveldashboardTubeRating";
	
	
	
	
	//
	//
	//
	//For user rating
	//Network message parameters
	public static final String OPERATION_GET_ALL_USER_RATING = "GetAllUserRating";
	public static final String ALL_USER_RATING_MESSAGE = "AllUserRatingMessage";
	
	public static final String OPERATION_GET_USER_RATING = "GetUserRating";
	public static final String USER_RATING_MESSAGE = "UserRatingMessage";	
	
	
	//Receiver IBIURL
	public static final String ALL_USER_RATING_SERVICE = "allUserRating";
	public static final String PATH_GET_ALL_USER_RATING = "getAllUserRating";
	public static final String USER_ALL_USER_RATING = "TraveldashboardAllUserRating@ibicoop.org";
	public static final String TERMINAL_ALL_USER_RATING = "TraveldashboardAllUserRating";
	
	
	public static final String USER_RATING_SERVICE = "userRating";
	public static final String PATH_GET_USER_RATING = "getUserRating";
	public static final String USER_USER_RATING = "TraveldashboardUserRating@ibicoop.org";
	public static final String TERMINAL_USER_RATING = "TraveldashboardUserRating";
	


	
	//
	//
	//
	//For weather
	
	//Network message parameters
	public static final String OPERATION_GET_WEATHER = "GetWeather";
	public static final String WEATHER_MESSAGE = "WeatherMessage";
	
	
	//Receiver IBIURL
	public static final String WEATHER_SERVICE = "weather";
	public static final String PATH_GET_WEATHER = "getWeather";
	public static final String USER_WEATHER = "TraveldashboardUserWeather@ibicoop.org";
	public static final String TERMINAL_WEATHER = "TraveldashboardWeather";
	

	
	//
	//
	//Incentive
	//Incentive NetworkMessage parameters
	public static final String PARAM_INCENTIVE_TYPE = "IncentiveType";//Fact or Joke

	//For getting incentive message
	public static final String OPERATION_GET_INCENTIVE = "GetIncentive";
	public static final String INCENTIVE_MESSAGE = "IncentiveMessage";
	
	public static final String INCENTIVE_SERVICE = "incentive";
	public static final String PATH_GET_INCENTIVE = "getIncentive";
	public static final String USER_INCENTIVE = "TraveldashboardUserIncentive@ibicoop.org";
	public static final String TERMINAL_INCENTIVE = "TraveldashboardIncentive";	


	
	
	//
	//
	//Noise
	//Noise IbiTopic
	public static final String TOPIC_NAME_NOISE = "TraveldashboardNoiseFinal";
	public static final String TOPIC_TYPE_NOISE = "Noise";
	//Noise IbiTopicEvent
	public static final String EVENT_NAME_NOISE = "Noise";
	public static final String EVENT_TYPE_NOISE = "Noise";
	public static final String EVENT_DESC_NOISE = "Noise";
	//Noise NetworkMessage parameters
	public static final String PARAM_NOISE_LEVEL = "NoiseLevel";
	
	//For getting noise message
	public static final String OPERATION_GET_NOISE = "GetNoise";
	public static final String NOISE_MESSAGE = "NoiseMessage";
	
	public static final String NOISE_SERVICE = "noise";
	public static final String PATH_GET_NOISE = "getNoise";
	public static final String USER_NOISE = "TraveldashboardUserNoise@ibicoop.org";
	public static final String TERMINAL_NOISE = "TraveldashboardNoise";
	
	
	//For getting classified noise message
	public static final String OPERATION_GET_CLASSIFIED_NOISE = "GetClassifiedNoise";
	public static final String CLASSIFIED_NOISE_MESSAGE = "ClassifiedNoiseMessage";
	
	public static final String CLASSIFIED_NOISE_SERVICE = "classifiedNoise";
	public static final String PATH_GET_CLASSIFIED_NOISE = "getClassifiedNoise";
	public static final String USER_CLASSIFIED_NOISE = "TraveldashboardUserClassifiedNoise@ibicoop.org";
	public static final String TERMINAL_CLASSIFIED_NOISE = "TraveldashboardClassifiedNoise";
	
	
	//
	//
	//Crowd
	//For crowdedness protocol
	public static final String PARAM_KEY_TIMESTAMP = "Timestamp";
	public static final String PARAM_KEY_INTERVAL = "Interval";
	public static final String PARAM_KEY_LEVEL = "Level";
	
	//For getting metro general crowd message
	public static final String OPERATION_GET_METRO_GENERAL_CROWD = "GetMetroGeneralCrowd";
	public static final String METRO_GENERAL_CROWD_MESSAGE = "MetroGeneralCrowdMessage";
	
	//For getting metro classified crowd message
	public static final String OPERATION_GET_METRO_CLASSIFIED_CROWD = "GetMetroClassifiedCrowd";
	public static final String METRO_CLASSIFIED_CROWD_MESSAGE = "MetroClassifiedCrowdMessage";

	//Crowd IbiTopic
	public static final String TOPIC_NAME_CROWD = "TraveldashboardCrowdFinal";
	public static final String TOPIC_TYPE_CROWD = "Crowd";
	//Crowd IbiTopicEvent
	public static final String EVENT_NAME_CROWD = "Crowd";
	public static final String EVENT_TYPE_CROWD = "Crowd";
	public static final String EVENT_DESC_CROWD = "Crowd";
	
	

	//
	//
	//Comment
	//Comment NetworkMessage parameters
	public static final String PARAM_KEY_USER_NAME = "UserName";
	public static final String PARAM_KEY_COMMENT = "Comment";
	
	//Comment IbiTopic
	public static final String TOPIC_NAME_COMMENT = "TraveldashboardCommentFinal";
	public static final String TOPIC_TYPE_COMMENT = "Comment";
	//Comment IbiTopicEvent
	public static final String EVENT_NAME_COMMENT = "Comment";
	public static final String EVENT_TYPE_COMMENT = "Comment";
	public static final String EVENT_DESC_COMMENT = "Comment";


	
	

	//
	//
	//Rating
	//Rating NetworkMessage parameters
	public static final String PARAM_KEY_RATING = "Rating";
	
	//Rating IbiTopic
	public static final String TOPIC_NAME_RATING = "TraveldashboardRatingFinal";
	public static final String TOPIC_TYPE_RATING = "Rating";
	//Rating IbiTopicEvent
	public static final String EVENT_NAME_RATING = "Rating";
	public static final String EVENT_TYPE_RATING = "Rating";
	public static final String EVENT_DESC_RATING = "Rating";
	
		

	
	
	
	
	
	//
	//
	//
	//Cache
	//File name of the stops cache
	public static File LOCAL_CACHE_FOLDER = null;
	public static final String TRAM_STOPS_CACHE_FILE_NAME = "tramStopsMessageCache.xml";
	public static final String METRO_STOPS_CACHE_FILE_NAME = "metroStopsMessageCache.xml";
	public static final String RAIL_STOPS_CACHE_FILE_NAME = "railStopsMessageCache.xml";
	public static final String BUS_STOPS_CACHE_FILE_NAME = "busStopsMessageCache.xml";
	
	
	

	
	
	
	
	
	
	//
	//
	//London
	//Constants
	public static final String PICCADILLY_LINE = "Piccadilly";
	public static final String DISTRICT_LINE = "District";
	public static final String VICTORIA_LINE = "Victoria";
	public static final String CIRCLE_LINE = "Circle";
	public static final String HAMMERSMITH_CITY_LINE = "Hammersmith and City";
	public static final String BAKERLOO_LINE = "Bakerloo";
	public static final String WATERLOO_CITY_LINE = "Waterloo and City";
	public static final String CENTRAL_LINE = "Central";
	public static final String JUBILEE_LINE = "Jubilee";
	public static final String METROPOLITAN_LINE = "Metropolitan";
	public static final String NORTHERN_LINE = "Northern";
	public static final String HAMMERSMITH_CIRCLE = "Hammersmith or Circle";
	public static final String DLR_LINE = "DLR";
	public static final String OVERGROUND_LINE = "Overground";
	
	//List and map
	public static final List<String> LONDON_METRO_ROUTE_NAMES;
	public static final ArrayList<String> LONDON_METRO_ROUTE_ID;
	public static final HashMap<String, String> LONDON_TUBE_DESTINATIONS;
	public static final Map<String, String> LONDON_ROUTENAME_ROUTEID_MAP; 
	public static final Map<String, String> LONDON_ROUTEID_ROUTENAME_MAP; 
	
	static {
		//LONDON
		//Route id
		LONDON_METRO_ROUTE_ID = new ArrayList<String>();
		LONDON_METRO_ROUTE_ID.add("B");
		LONDON_METRO_ROUTE_ID.add("C");
		LONDON_METRO_ROUTE_ID.add("D");
		LONDON_METRO_ROUTE_ID.add("H");
		LONDON_METRO_ROUTE_ID.add("J");	
		LONDON_METRO_ROUTE_ID.add("M");			
		LONDON_METRO_ROUTE_ID.add("N");	
		LONDON_METRO_ROUTE_ID.add("P");	
		LONDON_METRO_ROUTE_ID.add("V");	
		LONDON_METRO_ROUTE_ID.add("W");
		LONDON_METRO_ROUTE_ID.add("N/A");
		LONDON_METRO_ROUTE_ID.add("*");
		
		
		//Route name
		LONDON_METRO_ROUTE_NAMES = new ArrayList<String>();
		LONDON_METRO_ROUTE_NAMES.add(BAKERLOO_LINE);
		LONDON_METRO_ROUTE_NAMES.add(CENTRAL_LINE);
		LONDON_METRO_ROUTE_NAMES.add(CIRCLE_LINE);
		LONDON_METRO_ROUTE_NAMES.add(DISTRICT_LINE);
		LONDON_METRO_ROUTE_NAMES.add(DLR_LINE);
		LONDON_METRO_ROUTE_NAMES.add(JUBILEE_LINE);
		LONDON_METRO_ROUTE_NAMES.add(HAMMERSMITH_CITY_LINE);
		LONDON_METRO_ROUTE_NAMES.add(METROPOLITAN_LINE);
		LONDON_METRO_ROUTE_NAMES.add(NORTHERN_LINE);
		LONDON_METRO_ROUTE_NAMES.add(OVERGROUND_LINE);
		LONDON_METRO_ROUTE_NAMES.add(PICCADILLY_LINE);
		LONDON_METRO_ROUTE_NAMES.add(VICTORIA_LINE);
		LONDON_METRO_ROUTE_NAMES.add(WATERLOO_CITY_LINE);	
		LONDON_METRO_ROUTE_NAMES.add("*");
		LONDON_METRO_ROUTE_NAMES.add("N/A");		
		
		//London code line map
		LONDON_ROUTENAME_ROUTEID_MAP = new ConcurrentHashMap<String, String>();
		LONDON_ROUTENAME_ROUTEID_MAP.put(BAKERLOO_LINE, "B");
		LONDON_ROUTENAME_ROUTEID_MAP.put(CENTRAL_LINE, "C");
		LONDON_ROUTENAME_ROUTEID_MAP.put(DISTRICT_LINE, "D");
		LONDON_ROUTENAME_ROUTEID_MAP.put(HAMMERSMITH_CIRCLE, "H");
		LONDON_ROUTENAME_ROUTEID_MAP.put(HAMMERSMITH_CITY_LINE, "H");	
		LONDON_ROUTENAME_ROUTEID_MAP.put(JUBILEE_LINE, "J");
		LONDON_ROUTENAME_ROUTEID_MAP.put(METROPOLITAN_LINE, "M");
		LONDON_ROUTENAME_ROUTEID_MAP.put(NORTHERN_LINE, "N");
		LONDON_ROUTENAME_ROUTEID_MAP.put(PICCADILLY_LINE, "P");
		LONDON_ROUTENAME_ROUTEID_MAP.put(VICTORIA_LINE, "V");
		LONDON_ROUTENAME_ROUTEID_MAP.put(WATERLOO_CITY_LINE, "W");
		LONDON_ROUTENAME_ROUTEID_MAP.put(CIRCLE_LINE, "H");
		LONDON_ROUTENAME_ROUTEID_MAP.put(DLR_LINE, "N/A");
		LONDON_ROUTENAME_ROUTEID_MAP.put(OVERGROUND_LINE, "N/A");

		
		//London code line map
		LONDON_ROUTEID_ROUTENAME_MAP = new ConcurrentHashMap<String, String>();
		LONDON_ROUTEID_ROUTENAME_MAP.put("B", BAKERLOO_LINE);
		LONDON_ROUTEID_ROUTENAME_MAP.put("C", CENTRAL_LINE);
		LONDON_ROUTEID_ROUTENAME_MAP.put("D", DISTRICT_LINE);
		LONDON_ROUTEID_ROUTENAME_MAP.put("H", HAMMERSMITH_CIRCLE);
		LONDON_ROUTEID_ROUTENAME_MAP.put("J", JUBILEE_LINE);
		LONDON_ROUTEID_ROUTENAME_MAP.put("M", METROPOLITAN_LINE);
		LONDON_ROUTEID_ROUTENAME_MAP.put("N", NORTHERN_LINE);
		LONDON_ROUTEID_ROUTENAME_MAP.put("P", PICCADILLY_LINE);
		LONDON_ROUTEID_ROUTENAME_MAP.put("V", VICTORIA_LINE);
		LONDON_ROUTEID_ROUTENAME_MAP.put("W", WATERLOO_CITY_LINE);
		LONDON_ROUTEID_ROUTENAME_MAP.put("N/A", "N/A");
		LONDON_ROUTEID_ROUTENAME_MAP.put("*", "*");

		// London
		LONDON_TUBE_DESTINATIONS = new HashMap<String, String>();
		LONDON_TUBE_DESTINATIONS.put(BAKERLOO_LINE, "Harrow and Wealdstone/Elephant and Castle");
		LONDON_TUBE_DESTINATIONS.put(CENTRAL_LINE, "Ealing Broadway/West Ruislip/Epping");
		LONDON_TUBE_DESTINATIONS.put(CIRCLE_LINE, "Hammersmith/Edgware Road");
		LONDON_TUBE_DESTINATIONS.put(DISTRICT_LINE, "Ealing Broadway/Richmond/Wimbledon/Edgware Road/Upminster");
		LONDON_TUBE_DESTINATIONS.put(HAMMERSMITH_CIRCLE, "Hammersmith/Barking/Edgware Road");
		LONDON_TUBE_DESTINATIONS.put(HAMMERSMITH_CITY_LINE, "Hammersmith/Barking");
		LONDON_TUBE_DESTINATIONS.put(JUBILEE_LINE, "Stanmore/Stratford");
		LONDON_TUBE_DESTINATIONS.put(METROPOLITAN_LINE, "Amersham/Chesham/Watford/Uxbridge/Aldgate");
		LONDON_TUBE_DESTINATIONS.put(NORTHERN_LINE, "Morden/Edgware/Mill Hill East/High Barnet");
		LONDON_TUBE_DESTINATIONS.put(PICCADILLY_LINE, "Heathrow Terminal 5/Uxbridge/Cockfosters");
		LONDON_TUBE_DESTINATIONS.put(VICTORIA_LINE, "Brixton/Walthamstow Central");
		LONDON_TUBE_DESTINATIONS.put(WATERLOO_CITY_LINE, "Waterloo/Bank");
		LONDON_TUBE_DESTINATIONS.put(OVERGROUND_LINE, "Richmond/Watford Junction/Crystal Palace/West Croydon/New Cross/Barking/Stratford");
		LONDON_TUBE_DESTINATIONS.put(DLR_LINE, "Bank/Monument/Tower Gateway/Lewisham/Woolwich Arsenal/Beckton/Stratford International");
		LONDON_TUBE_DESTINATIONS.put("*", "*");
		LONDON_TUBE_DESTINATIONS.put("N/A", "N/A");
		
	}
	
	
	
	
	
	
	
	
	
	//
	//
	//
	//Paris
	
	//
	//
	//Constants
	//Metro
	public static final String METRO1 = "Metro 1";
	public static final String METRO2 = "Metro 2";
	public static final String METRO3 = "Metro 3";
	public static final String METRO3BIS = "Metro 3bis";
	public static final String METRO4 = "Metro 4";
	public static final String METRO5 = "Metro 5";
	public static final String METRO6 = "Metro 6";
	public static final String METRO7 = "Metro 7";
	public static final String METRO7BIS = "Metro 7bis";
	public static final String METRO8 = "Metro 8";
	public static final String METRO9 = "Metro 9";	
	public static final String METRO10 = "Metro 10";
	public static final String METRO11 = "Metro 11";
	public static final String METRO12 = "Metro 12";
	public static final String METRO13 = "Metro 13";
	public static final String METRO14 = "Metro 14";
	
	//Rail
	public static final String RERA = "RER A";
	public static final String RERB = "RER B";
	
	//Tram
	public static final String TRAM1 = "Tram 1";
	public static final String TRAM2 = "Tram 2";
	public static final String TRAM3A = "Tram 3a";
	public static final String TRAM3B = "Tram 3b";
	public static final String TRAM5 = "Tram 5";	
	
	//For twitter comments
	public static final String LIGNE1 = "Ligne1 RATP trafic";
	public static final String LIGNE2 = "Ligne2 RATP trafic";
	public static final String LIGNE3 = "Ligne3 RATP trafic";
	public static final String LIGNE3BIS = "Ligne3bis RATP trafic";
	public static final String LIGNE4 = "Ligne4 RATP trafic";
	public static final String LIGNE5 = "Ligne5 RATP trafic";
	public static final String LIGNE6 = "Ligne6 RATP trafic";
	public static final String LIGNE7 = "Ligne7 RATP trafic";
	public static final String LIGNE7BIS = "Ligne7bis RATP trafic";
	public static final String LIGNE8 = "Ligne8 RATP trafic";
	public static final String LIGNE9 = "Ligne9 RATP trafic";	
	public static final String LIGNE10 = "Ligne10 RATP trafic";
	public static final String LIGNE11 = "Ligne11 RATP trafic";
	public static final String LIGNE12 = "Ligne12 RATP trafic";
	public static final String LIGNE13 = "Ligne13 RATP trafic";
	public static final String LIGNE14 = "Ligne14 RATP trafic";
	public static final String LIGNE_RERA = "RERA RATP trafic";
	public static final String LIGNE_RERB = "RERB RATP trafic";
	
	public static final String RATP_LIGNE1 = "Ligne 1 RATP";
	public static final String RATP_LIGNE2 = "Ligne 2 RATP";
	public static final String RATP_LIGNE3 = "Ligne 3 RATP";
	public static final String RATP_LIGNE3BIS = "Ligne 3bis RATP";
	public static final String RATP_LIGNE4 = "Ligne 4 RATP";
	public static final String RATP_LIGNE5 = "Ligne 5 RATP";
	public static final String RATP_LIGNE6 = "Ligne 6 RATP";
	public static final String RATP_LIGNE7 = "Ligne 7 RATP";
	public static final String RATP_LIGNE7BIS = "Ligne 7bis RATP";
	public static final String RATP_LIGNE8 = "Ligne 8 RATP";
	public static final String RATP_LIGNE9 = "Ligne 9 RATP";	
	public static final String RATP_LIGNE10 = "Ligne 10 RATP";
	public static final String RATP_LIGNE11 = "Ligne 11 RATP";
	public static final String RATP_LIGNE12 = "Ligne 12 RATP";
	public static final String RATP_LIGNE13 = "Ligne 13 RATP";
	public static final String RATP_LIGNE14 = "Ligne 14 RATP";
	public static final String RATP_LIGNE_RERA = "RER A RATP";
	public static final String RATP_LIGNE_RERB = "RER B RATP";
	

	//
	//
	//List and map
	//Metro
	public static final ArrayList<String> PARIS_METRO_ROUTE_NAMES;
	public static final ArrayList<String> PARIS_METRO_ROUTE_ID;
	public static final HashMap<String, Direction> PARIS_METRO_DESTINATIONS;
	
	
	//Rail
	public static final ArrayList<String> PARIS_RAIL_ROUTE_NAMES;
	public static final ArrayList<String> PARIS_RAIL_ROUTE_ID;	
	public static final HashMap<String, Direction> PARIS_RAIL_DESTINATIONS;	
	
	//Tram
	public static final ArrayList<String> PARIS_TRAM_ROUTE_NAME;
	public static final HashMap<String, Direction> PARIS_TRAM_DESTINATIONS;	
	
	//Twitter
	public static final HashMap<String, String> PARIS_METRO_TWITTER_SEARCH_TERM;
	public static final ArrayList<String> PARIS_METRO_TWITTER_SEARCH_USERS;
	public static final HashMap<String, String> PARIS_RAIL_TWITTER_SEARCH_TERM;
	public static final ArrayList<String> PARIS_RAIL_TWITTER_SEARCH_USERS;
	
	
	public static class Direction {
		String aller; //0
		String retour; //1
		
		public Direction(String aller, String retour) {
			this.aller = aller;
			this.retour = retour;
		}
		
		public String getDirection(String direction) {
			if (direction.equals("0")) return aller;
			else return retour;
		}
	}
	
	static {
		//PARIS
		//Route name
		//Metro
		PARIS_METRO_ROUTE_NAMES = new ArrayList<String>();
		PARIS_METRO_ROUTE_NAMES.add("1");
		PARIS_METRO_ROUTE_NAMES.add("2");
		PARIS_METRO_ROUTE_NAMES.add("3");
		PARIS_METRO_ROUTE_NAMES.add("3B");
		PARIS_METRO_ROUTE_NAMES.add("4");
		PARIS_METRO_ROUTE_NAMES.add("5");
		PARIS_METRO_ROUTE_NAMES.add("6");
		PARIS_METRO_ROUTE_NAMES.add("7");
		PARIS_METRO_ROUTE_NAMES.add("7B");
		PARIS_METRO_ROUTE_NAMES.add("8");
		PARIS_METRO_ROUTE_NAMES.add("9");
		PARIS_METRO_ROUTE_NAMES.add("10");
		PARIS_METRO_ROUTE_NAMES.add("11");
		PARIS_METRO_ROUTE_NAMES.add("12");
		PARIS_METRO_ROUTE_NAMES.add("13");
		PARIS_METRO_ROUTE_NAMES.add("14");
		
		
		PARIS_METRO_ROUTE_ID = new ArrayList<String>();
		PARIS_METRO_ROUTE_ID.add("918044"); //1 direction 0
		PARIS_METRO_ROUTE_ID.add("918045"); //1 direction 1
		
		PARIS_METRO_ROUTE_ID.add("860877"); //2 direction 0
		PARIS_METRO_ROUTE_ID.add("860878"); //2 direction 1
		
		PARIS_METRO_ROUTE_ID.add("708886"); //3 direction 0
		PARIS_METRO_ROUTE_ID.add("708887"); //3 direction 1
				
		PARIS_METRO_ROUTE_ID.add("708898"); //3B direction 0
		PARIS_METRO_ROUTE_ID.add("708899"); //3B direction 1
		
		PARIS_METRO_ROUTE_ID.add("943843"); //4 direction 0
		PARIS_METRO_ROUTE_ID.add("943844"); //4 direction 1
		
		PARIS_METRO_ROUTE_ID.add("708924"); //5 direction 0
		PARIS_METRO_ROUTE_ID.add("708925"); //5 direction 1
		
		PARIS_METRO_ROUTE_ID.add("805338"); //6 direction 0
		PARIS_METRO_ROUTE_ID.add("805339"); //6 direction 1
		
		PARIS_METRO_ROUTE_ID.add("656041"); //7 direction 0
		PARIS_METRO_ROUTE_ID.add("656042"); //7 direction 1
		PARIS_METRO_ROUTE_ID.add("656043"); //7 direction 1
		
		PARIS_METRO_ROUTE_ID.add("656103"); //7B direction 0
		PARIS_METRO_ROUTE_ID.add("656104"); //7B direction 1

		PARIS_METRO_ROUTE_ID.add("795705"); //8 direction 0
		PARIS_METRO_ROUTE_ID.add("795706"); //8 direction 1
		
		PARIS_METRO_ROUTE_ID.add("711644"); //9 direction 0
		PARIS_METRO_ROUTE_ID.add("711645"); //9 direction 1

		PARIS_METRO_ROUTE_ID.add("858489"); //10 direction 0	
		PARIS_METRO_ROUTE_ID.add("858490"); //10 direction 0
		PARIS_METRO_ROUTE_ID.add("858491"); //10 direction 1
		
		PARIS_METRO_ROUTE_ID.add("858494"); //11 direction 0
		PARIS_METRO_ROUTE_ID.add("858495"); //11 direction 1
		
		PARIS_METRO_ROUTE_ID.add("943841"); //12 direction 0
		PARIS_METRO_ROUTE_ID.add("943842"); //12 direction 1
		
		PARIS_METRO_ROUTE_ID.add("655989"); //13 direction 0	
		PARIS_METRO_ROUTE_ID.add("655991"); //13 direction 1
		
		PARIS_METRO_ROUTE_ID.add("831554"); //14 direction 0	
		PARIS_METRO_ROUTE_ID.add("831555"); //14 direction 1	
		
		//Rail
		PARIS_RAIL_ROUTE_NAMES = new ArrayList<String>();
		PARIS_RAIL_ROUTE_NAMES.add("A");
		PARIS_RAIL_ROUTE_NAMES.add("B");
		
		PARIS_RAIL_ROUTE_ID = new ArrayList<String>();
		
		PARIS_RAIL_ROUTE_ID.add("742330"); //A
		PARIS_RAIL_ROUTE_ID.add("742331"); //A
		PARIS_RAIL_ROUTE_ID.add("742332"); //A
		PARIS_RAIL_ROUTE_ID.add("742333"); //A
		PARIS_RAIL_ROUTE_ID.add("742334"); //A
		PARIS_RAIL_ROUTE_ID.add("742335"); //A
		PARIS_RAIL_ROUTE_ID.add("742336"); //A
		
		PARIS_RAIL_ROUTE_ID.add("959286"); //B
		PARIS_RAIL_ROUTE_ID.add("959287"); //B
		PARIS_RAIL_ROUTE_ID.add("959288"); //B
		PARIS_RAIL_ROUTE_ID.add("959289"); //B
		PARIS_RAIL_ROUTE_ID.add("959290"); //B
		PARIS_RAIL_ROUTE_ID.add("959291"); //B
		
		
		//Tram
		PARIS_TRAM_ROUTE_NAME = new ArrayList<String>();
		PARIS_TRAM_ROUTE_NAME.add("T1");
		PARIS_TRAM_ROUTE_NAME.add("T2");
		PARIS_TRAM_ROUTE_NAME.add("T3");
		PARIS_TRAM_ROUTE_NAME.add("T3b");		
		
		
		
		//
		//
		//Destination
		//Metro
		PARIS_METRO_DESTINATIONS = new HashMap<String, Direction>();
		PARIS_METRO_DESTINATIONS.put("1", new Direction("CHATEAU DE VINCENNES -> LA DEFENSE", "CHATEAU DE VINCENNES <- LA DEFENSE"));
		PARIS_METRO_DESTINATIONS.put("2", new Direction("NATION -> PORTE DAUPHINE", "NATION <- PORTE DAUPHINE"));
		PARIS_METRO_DESTINATIONS.put("3", new Direction("GALLIENI -> PONT DE LEVALLOIS - BECON", "GALLIENI <-> PONT DE LEVALLOIS - BECON"));
		PARIS_METRO_DESTINATIONS.put("3B", new Direction("PORTE DES LILAS -> GAMBETTA", "PORTE DES LILAS <- GAMBETTA"));
		PARIS_METRO_DESTINATIONS.put("4", new Direction("PORTE DE CLIGNANCOURT -> MAIRIE DE MONTROUGE", "PORTE DE CLIGNANCOURT <- MAIRIE DE MONTROUGE"));
		PARIS_METRO_DESTINATIONS.put("5", new Direction("BOBIGNY - PABLO PICASSO -> PLACE D'ITALIE", "BOBIGNY - PABLO PICASSO <- PLACE D'ITALIE"));
		PARIS_METRO_DESTINATIONS.put("6", new Direction("NATION -> CHARLES DE GAULLE - ETOILE", "NATION <- CHARLES DE GAULLE - ETOILE"));
		PARIS_METRO_DESTINATIONS.put("7", new Direction("LA COURNEUVE - 8 MAI 1945 -> VILLEJUIF-L. ARAGON / MAIRIE D'IVRY", "LA COURNEUVE - 8 MAI 1945 <- VILLEJUIF-L. ARAGON / MAIRIE D'IVRY"));
		PARIS_METRO_DESTINATIONS.put("7B", new Direction("PORTE DES LILAS -> GAMBETTA", "PORTE DES LILAS <- GAMBETTA"));
		PARIS_METRO_DESTINATIONS.put("8", new Direction("BALARD -> POINTE DU LAC", "BALARD <- POINTE DU LAC"));
		PARIS_METRO_DESTINATIONS.put("9", new Direction("PONT DE SEVRES -> MAIRIE DE MONTREUIL", "PONT DE SEVRES <- MAIRIE DE MONTREUIL"));
		PARIS_METRO_DESTINATIONS.put("10", new Direction("BOULOGNE - PONT DE SAINT CLOUD -> GARE D'AUSTERLITZ", "BOULOGNE - PONT DE SAINT CLOUD <- GARE D'AUSTERLITZ"));
		PARIS_METRO_DESTINATIONS.put("11", new Direction("MAIRIE DES LILAS -> CHATELET", "MAIRIE DES LILAS <- CHATELET"));	
		PARIS_METRO_DESTINATIONS.put("12", new Direction("MAIRIE D'ISSY -> FRONT POPULAIRE", "MAIRIE D'ISSY <- FRONT POPULAIRE"));
		PARIS_METRO_DESTINATIONS.put("13",  new Direction("CHATILLON - MONTROUGE -> ST-DENIS-UNIVERSITE/LES COURTILLES", "CHATILLON - MONTROUGE <- ST-DENIS-UNIVERSITE/LES COURTILLES"));
		PARIS_METRO_DESTINATIONS.put("14", new Direction("SAINT-LAZARE -> OLYMPIADES", "SAINT-LAZARE <- OLYMPIADES"));
		
		//Rail
		PARIS_RAIL_DESTINATIONS = new HashMap<String, Direction>();
		PARIS_RAIL_DESTINATIONS.put("A", new Direction("CERGY-POISSY-SAINT GERMAIN EN LAYE -> MARNE LA VALLEE-BOISSY SAINT LEGER", "CERGY-POISSY-SAINT GERMAIN EN LAYE <- MARNE LA VALLEE-BOISSY SAINT LEGER"));
		PARIS_RAIL_DESTINATIONS.put("B", new Direction("AEROPORT CH.DE GAULLE 2-MITRY CLAYE -> ROBINSON-SAINT REMY LES CHEVREUSE", "AEROPORT CH.DE GAULLE 2-MITRY CLAYE <-> ROBINSON-SAINT REMY LES CHEVREUSE"));
		
		//Tram
		PARIS_TRAM_DESTINATIONS = new HashMap<String, Direction>();
		PARIS_TRAM_DESTINATIONS.put("T1", new Direction("NOISY-LE-SEC RER -> LES COURTILLES", "NOISY-LE-SEC RER <- LES COURTILLES"));
		PARIS_TRAM_DESTINATIONS.put("T2", new Direction("PONT DE BEZONS -> PORTE DE VERSAILLES", "PONT DE BEZONS <- PORTE DE VERSAILLES"));
		PARIS_TRAM_DESTINATIONS.put("T3", new Direction("PONT GARIGLIANO - HOP G.POMPIDOU -> PORTE DE VINCENNES", "PONT GARIGLIANO - HOP G.POMPIDOU <- PORTE DE VINCENNES"));
		PARIS_TRAM_DESTINATIONS.put("T3b", new Direction("PORTE DE VINCENNES -> PORTE DE LA CHAPELLE", "PORTE DE VINCENNES <- PORTE DE LA CHAPELLE"));
		
		//
		//
		//Twitter
		//Metro search user name
		PARIS_METRO_TWITTER_SEARCH_USERS = new ArrayList<String>();
		PARIS_METRO_TWITTER_SEARCH_USERS.add(RATP_LIGNE1);
		PARIS_METRO_TWITTER_SEARCH_USERS.add(RATP_LIGNE2);
		PARIS_METRO_TWITTER_SEARCH_USERS.add(RATP_LIGNE3);
		PARIS_METRO_TWITTER_SEARCH_USERS.add(RATP_LIGNE3BIS);
		PARIS_METRO_TWITTER_SEARCH_USERS.add(RATP_LIGNE4);
		PARIS_METRO_TWITTER_SEARCH_USERS.add(RATP_LIGNE5);
		PARIS_METRO_TWITTER_SEARCH_USERS.add(RATP_LIGNE6);
		PARIS_METRO_TWITTER_SEARCH_USERS.add(RATP_LIGNE7);
		PARIS_METRO_TWITTER_SEARCH_USERS.add(RATP_LIGNE7BIS);
		PARIS_METRO_TWITTER_SEARCH_USERS.add(RATP_LIGNE8);
		PARIS_METRO_TWITTER_SEARCH_USERS.add(RATP_LIGNE9);
		PARIS_METRO_TWITTER_SEARCH_USERS.add(RATP_LIGNE10);
		PARIS_METRO_TWITTER_SEARCH_USERS.add(RATP_LIGNE11);
		PARIS_METRO_TWITTER_SEARCH_USERS.add(RATP_LIGNE12);
		PARIS_METRO_TWITTER_SEARCH_USERS.add(RATP_LIGNE13);
		PARIS_METRO_TWITTER_SEARCH_USERS.add(RATP_LIGNE14);
		
		//Rail search user name
		PARIS_RAIL_TWITTER_SEARCH_USERS = new ArrayList<String>();
		PARIS_RAIL_TWITTER_SEARCH_USERS.add(RATP_LIGNE_RERA);
		PARIS_RAIL_TWITTER_SEARCH_USERS.add(RATP_LIGNE_RERB);	

		
		//Metro search term
		PARIS_METRO_TWITTER_SEARCH_TERM = new HashMap<String, String>();
		PARIS_METRO_TWITTER_SEARCH_TERM.put("1", LIGNE1);
		PARIS_METRO_TWITTER_SEARCH_TERM.put("2", LIGNE2);
		PARIS_METRO_TWITTER_SEARCH_TERM.put("3", LIGNE3);
		PARIS_METRO_TWITTER_SEARCH_TERM.put("3B", LIGNE3BIS);
		PARIS_METRO_TWITTER_SEARCH_TERM.put("4", LIGNE4);
		PARIS_METRO_TWITTER_SEARCH_TERM.put("5", LIGNE5);
		PARIS_METRO_TWITTER_SEARCH_TERM.put("6", LIGNE6);
		PARIS_METRO_TWITTER_SEARCH_TERM.put("7", LIGNE7);
		PARIS_METRO_TWITTER_SEARCH_TERM.put("7B", LIGNE7BIS);
		PARIS_METRO_TWITTER_SEARCH_TERM.put("8", LIGNE8);
		PARIS_METRO_TWITTER_SEARCH_TERM.put("9", LIGNE9);
		PARIS_METRO_TWITTER_SEARCH_TERM.put("10", LIGNE10);
		PARIS_METRO_TWITTER_SEARCH_TERM.put("11", LIGNE11);
		PARIS_METRO_TWITTER_SEARCH_TERM.put("12", LIGNE12);
		PARIS_METRO_TWITTER_SEARCH_TERM.put("13", LIGNE13);
		PARIS_METRO_TWITTER_SEARCH_TERM.put("14", LIGNE14);
		
		//Rail search term
		PARIS_RAIL_TWITTER_SEARCH_TERM = new HashMap<String, String>();
		PARIS_RAIL_TWITTER_SEARCH_TERM.put("A", LIGNE_RERA);
		PARIS_RAIL_TWITTER_SEARCH_TERM.put("B", LIGNE_RERB);
	}
	
	
	
	
	
	
	
//	//
//	//
//	//Background resource
//	public static final HashMap<String, Integer> BACKGROUND_RESOURCE;
//	
//	
//	
//	static {
//		//London
//		BACKGROUND_RESOURCE = new HashMap<String, Integer>();
//		BACKGROUND_RESOURCE.put(BAKERLOO_LINE, traveldashboardlondon.app.R.drawable.gradient_bakerloo);
//		BACKGROUND_RESOURCE.put(CENTRAL_LINE, traveldashboardlondon.app.R.drawable.gradient_central);
//		BACKGROUND_RESOURCE.put(CIRCLE_LINE, traveldashboardlondon.app.R.drawable.gradient_circle);
//		BACKGROUND_RESOURCE.put(DISTRICT_LINE,traveldashboardlondon.app.R.drawable.gradient_district);
//		BACKGROUND_RESOURCE.put(DLR_LINE,traveldashboardlondon.app.R.drawable.gradient_dlr);		
//		BACKGROUND_RESOURCE.put(HAMMERSMITH_CIRCLE,traveldashboardlondon.app.R.drawable.gradient_hammersmith_circle);
//		BACKGROUND_RESOURCE.put(HAMMERSMITH_CITY_LINE,traveldashboardlondon.app.R.drawable.gradient_hammersmith);	
//		BACKGROUND_RESOURCE.put(JUBILEE_LINE,traveldashboardlondon.app.R.drawable.gradient_jubilee);		
//		BACKGROUND_RESOURCE.put(METROPOLITAN_LINE,traveldashboardlondon.app.R.drawable.gradient_metropolitan);	
//		BACKGROUND_RESOURCE.put(NORTHERN_LINE,traveldashboardlondon.app.R.drawable.gradient_northern);
//		BACKGROUND_RESOURCE.put(OVERGROUND_LINE,traveldashboardlondon.app.R.drawable.gradient_overground);
//		BACKGROUND_RESOURCE.put(PICCADILLY_LINE,traveldashboardlondon.app.R.drawable.gradient_piccadilly);	
//		BACKGROUND_RESOURCE.put(VICTORIA_LINE,traveldashboardlondon.app.R.drawable.gradient_victoria);
//		BACKGROUND_RESOURCE.put(WATERLOO_CITY_LINE,traveldashboardlondon.app.R.drawable.gradient_waterloo);
//		//Paris
//		BACKGROUND_RESOURCE.put("1",traveldashboardlondon.app.R.drawable.m_1);
//		BACKGROUND_RESOURCE.put("2",traveldashboardlondon.app.R.drawable.m_2);
//		BACKGROUND_RESOURCE.put("3",traveldashboardlondon.app.R.drawable.m_3);
//		BACKGROUND_RESOURCE.put("3B",traveldashboardlondon.app.R.drawable.m_3bis);
//		BACKGROUND_RESOURCE.put("4",traveldashboardlondon.app.R.drawable.m_4);
//		BACKGROUND_RESOURCE.put("5",traveldashboardlondon.app.R.drawable.m_5);
//		BACKGROUND_RESOURCE.put("6",traveldashboardlondon.app.R.drawable.m_6);
//		BACKGROUND_RESOURCE.put("7",traveldashboardlondon.app.R.drawable.m_7);
//		BACKGROUND_RESOURCE.put("7B",traveldashboardlondon.app.R.drawable.m_7bis);
//		BACKGROUND_RESOURCE.put("8",traveldashboardlondon.app.R.drawable.m_8);
//		BACKGROUND_RESOURCE.put("9",traveldashboardlondon.app.R.drawable.m_9);
//		BACKGROUND_RESOURCE.put("10",traveldashboardlondon.app.R.drawable.m_10);
//		BACKGROUND_RESOURCE.put("11",traveldashboardlondon.app.R.drawable.m_11);
//		BACKGROUND_RESOURCE.put("12",traveldashboardlondon.app.R.drawable.m_12);
//		BACKGROUND_RESOURCE.put("13",traveldashboardlondon.app.R.drawable.m_13);
//		BACKGROUND_RESOURCE.put("14",traveldashboardlondon.app.R.drawable.m_14);
//		BACKGROUND_RESOURCE.put("A",traveldashboardlondon.app.R.drawable.rer_a);
//		BACKGROUND_RESOURCE.put("B",traveldashboardlondon.app.R.drawable.rer_b);
//	}
	
	
	
	
	
	
	//
	//
	//Crowd and ratings
	//Constants
	public static final String NA = "N/A";
	public static final String EMPTY = "EMPTY";
	public static final String ALMOST_EMPTY = "ALMOST EMPTY";
	public static final String NORMAL = "NORMAL";
	public static final String CROWDED = "CROWDED";
	public static final String VERY_CROWDED = "VERY CROWDED";
	public static final String EXTREMELY_CROWDED = "EXTREMELY CROWDED";
	
	//Map
	public static final HashMap<String, Integer> CROWD_LEVEL_MAP;
	public static final HashMap<String, CrowdLevel> CROWD_LEVEL_OBJECT_MAP;
	public static final HashMap<Integer, String> CROWD_LEVEL_DESCRIPTION_MAP;
	public static final HashMap<Integer, CrowdLevel> CROWD_LEVEL_INT_OBJECT_MAP;
	public static final Map<Integer, RatingLevel> RATING_MAP;
	
	static {
		CROWD_LEVEL_MAP = new HashMap<String, Integer>();
		CROWD_LEVEL_MAP.put(NA, 0);		
		CROWD_LEVEL_MAP.put(EMPTY, 0);
		CROWD_LEVEL_MAP.put(ALMOST_EMPTY, 1);
		CROWD_LEVEL_MAP.put(NORMAL, 2);
		CROWD_LEVEL_MAP.put(CROWDED, 3);
		CROWD_LEVEL_MAP.put(VERY_CROWDED, 4);
		CROWD_LEVEL_MAP.put(EXTREMELY_CROWDED, 5);
		
		CROWD_LEVEL_OBJECT_MAP = new HashMap<String, CrowdLevel>();
		CROWD_LEVEL_OBJECT_MAP.put(NA, CrowdLevel.ZERO);
		CROWD_LEVEL_OBJECT_MAP.put(EMPTY, CrowdLevel.ZERO);
		CROWD_LEVEL_OBJECT_MAP.put(ALMOST_EMPTY, CrowdLevel.ONE);
		CROWD_LEVEL_OBJECT_MAP.put(NORMAL, CrowdLevel.TWO);
		CROWD_LEVEL_OBJECT_MAP.put(CROWDED, CrowdLevel.THREE);
		CROWD_LEVEL_OBJECT_MAP.put(VERY_CROWDED, CrowdLevel.FOUR);
		CROWD_LEVEL_OBJECT_MAP.put(EXTREMELY_CROWDED, CrowdLevel.FIVE);
		
		CROWD_LEVEL_INT_OBJECT_MAP = new HashMap<Integer, CrowdLevel>();
		CROWD_LEVEL_INT_OBJECT_MAP.put(0, CrowdLevel.ZERO);
		CROWD_LEVEL_INT_OBJECT_MAP.put(1, CrowdLevel.ONE);
		CROWD_LEVEL_INT_OBJECT_MAP.put(2, CrowdLevel.TWO);
		CROWD_LEVEL_INT_OBJECT_MAP.put(3, CrowdLevel.THREE);
		CROWD_LEVEL_INT_OBJECT_MAP.put(4, CrowdLevel.FOUR);
		CROWD_LEVEL_INT_OBJECT_MAP.put(5, CrowdLevel.FIVE);
				
		CROWD_LEVEL_DESCRIPTION_MAP = new HashMap<Integer, String>();
		CROWD_LEVEL_DESCRIPTION_MAP.put(0, EMPTY);
		CROWD_LEVEL_DESCRIPTION_MAP.put(1, ALMOST_EMPTY);
		CROWD_LEVEL_DESCRIPTION_MAP.put(2, NORMAL);
		CROWD_LEVEL_DESCRIPTION_MAP.put(3, CROWDED);
		CROWD_LEVEL_DESCRIPTION_MAP.put(4, VERY_CROWDED);
		CROWD_LEVEL_DESCRIPTION_MAP.put(5, EXTREMELY_CROWDED);	
		
		RATING_MAP = new HashMap<Integer, RatingLevel>();
		RATING_MAP.put(-1, RatingLevel.UNKNOWN);
		RATING_MAP.put(0, RatingLevel.ZERO);
		RATING_MAP.put(1, RatingLevel.ONE);
		RATING_MAP.put(2, RatingLevel.TWO);
		RATING_MAP.put(3, RatingLevel.THREE);
		RATING_MAP.put(4, RatingLevel.FOUR);
		RATING_MAP.put(5, RatingLevel.FIVE);
	}
	
}
