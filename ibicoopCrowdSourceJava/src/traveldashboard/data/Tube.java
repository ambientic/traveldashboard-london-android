package traveldashboard.data;

/**
 * Tube
 * 
 * @author khoo
 * 
 */
public class Tube extends Transport {

	private boolean isRER;

	protected Tube() {
	}

	/**
	 * Tube constructor
	 * 
	 * @param id
	 *            Tube id
	 * @param name
	 *            Tube name
	 * @param destination
	 *            Destination
	 */
	public Tube(String id, String name, String destination) {
		super(id, name, TransportType.TUBE, destination);
		isRER = (name.equals("A") || name.equals("B"));
	}

	/**
	 * returns whether this line is an has any RERs passing through it
	 * 
	 * @return true if this line is an RER, false otherwise.
	 */
	public boolean isRER() {
		return isRER;
	}
}
