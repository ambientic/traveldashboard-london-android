package traveldashboard.data.noise;

public class NoiseData {
	
	private long timestamp;
	private double latitude;
	private double longitude;
	private int noiseLevel;
	private int nbVotes;
	
	public NoiseData(long timestamp, double latitude, double longitude, int noiseLevel, int nbVotes) {
		this.timestamp = timestamp;
		this.latitude = latitude;
		this.longitude = longitude;
		this.noiseLevel = noiseLevel;
		this.nbVotes = nbVotes;
	}
	
	public long getTimestamp() {
		return timestamp;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public int getNoiseLevel() {
		return noiseLevel;
	}
	
	public int getNbVotes() {
		return nbVotes;
	}
	
	@Override
	public String toString() {
		return "timestamp = " + timestamp
				+ ", latitude = " + latitude
				+ ", longitude = " + longitude
				+ ", noise level = " + noiseLevel
				+ ", votes = " + nbVotes
				;
	}
	
}
