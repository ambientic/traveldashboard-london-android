package traveldashboard.data.noise;

public class NoiseDatasCollection {

	private NoiseData[] noiseDatas;
	
	public NoiseDatasCollection(NoiseData[] noiseDatas) {
		this.noiseDatas = noiseDatas;
	}
	
	public NoiseData[] getNoiseDatas() {
		return noiseDatas;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < noiseDatas.length; i++) {
			sb.append(noiseDatas[i].toString() + "\n");
		}
		
		return sb.toString();
	}
}
