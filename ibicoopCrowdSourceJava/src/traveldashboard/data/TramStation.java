package traveldashboard.data;

public class TramStation extends Station {

	protected Tram[] trams;
	
	protected TramStation() {}

	/**
	 * Tram station constructor
	 * @param id Tram station id
	 * @param name Tram station name
	 * @param latitude Latitude of the station
	 * @param longitude Longitude of the station
	 * @param trams All trams
	 */
	public TramStation(String id, String name, double latitude, double longitude, Tram[] trams) {
		super(id, name, latitude, longitude);
		this.trams = trams;
	}
	
	/**
	 * Get trams
	 * @return trams
	 */
	public Tram[] getTrams() {
		return trams;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		
		
		for (int j = 0; j < trams.length; j++) {
			sb.append("Tram: " + trams[j].toString() + "\n");
		}
		
		return sb.toString();
	}
}
