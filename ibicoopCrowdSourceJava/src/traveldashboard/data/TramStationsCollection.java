package traveldashboard.data;

/**
 * Collection of tram stations
 * @author khoo
 *
 */
public class TramStationsCollection {
	
	protected TramStation[] stations;
	
	protected TramStationsCollection() {}
	
	/**
	 * Constructor of tram stations collection
	 * @param tramStations
	 */
	public TramStationsCollection(TramStation[] tramStations) {
		this.stations = tramStations;
	}
	
	/**
	 * Get tram stations
	 * @return tram stations
	 */
	public TramStation[] getTramStations() {
		return stations;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < stations.length; i++) {
			sb.append(stations[i].toString() + "\n");
		}
		
		return sb.toString();
	}
}