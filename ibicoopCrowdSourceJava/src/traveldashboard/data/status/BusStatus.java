package traveldashboard.data.status;

import traveldashboard.data.Bus;

/**
 * Bus status
 * @author khoo
 *
 */
public class BusStatus extends GeneralStatus {
	
	protected Bus bus;
	
	protected BusStatus() {}
	
	/**
	 * Bus status constructor
	 * @param bus
	 * @param status
	 * @param description
	 */
	public BusStatus(Bus bus, String status, String description) {
		super(status, description);
		this.bus = bus;
	}
	
	/**
	 * Get bus
	 * @return bus
	 */
	public Bus getBus() {
		return bus;
	}
}