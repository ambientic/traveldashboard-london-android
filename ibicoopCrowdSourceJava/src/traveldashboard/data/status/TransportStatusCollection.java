package traveldashboard.data.status;

/**
 * Transport status collection
 * @author khoo
 *
 */
public class TransportStatusCollection {
	
	protected TransportStatus[] status;
	
	protected TransportStatusCollection() {}
	
	/**
	 * Transport status collection constructor
	 * @param status
	 */
	public TransportStatusCollection(TransportStatus[] status) {
		this.status = status;
	}
	
	/**
	 * Get status collection
	 * @return status collection
	 */
	public TransportStatus[] getTransportStatusCollection() {
		return status;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < status.length; i++) {
			sb.append(status[i].toString() + "\n");
		}
		return sb.toString();
	}

}
