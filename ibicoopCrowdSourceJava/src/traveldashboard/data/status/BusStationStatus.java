package traveldashboard.data.status;

import traveldashboard.data.BusStation;


/**
 * Bus station status
 * @author khoo
 *
 */
public class BusStationStatus extends StationStatus {
	
	protected BusStationStatus() {}
	
	/**
	 * Bus station status constructor
	 * @param station
	 * @param status
	 * @param description
	 */
	public BusStationStatus(BusStation station, String status, String description) {
		super(station, status, description);
	}
	
	/**
	 * Get bus station
	 * @return bus station
	 */
	public BusStation getBusStation() {
		return (BusStation) station;
	}
}
