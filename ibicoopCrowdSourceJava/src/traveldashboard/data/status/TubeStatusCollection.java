package traveldashboard.data.status;

/**
 * Tube status collection
 * @author khoo
 *
 */
public class TubeStatusCollection {
	
	protected TubeStatus[] status;
	
	protected TubeStatusCollection() {};
	
	/**
	 * Tube status collection constructor
	 * @param status
	 */
	public TubeStatusCollection(TubeStatus[] status) {
		this.status = status;
	}
	
	/**
	 * Get tube status collection
	 * @return tube status collection
	 */
	public TubeStatus[] getTubeStatusCollection() {
		return status;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < status.length; i++) {
			sb.append(status[i].toString() + "\n");
		}
		
		return sb.toString();
	}
}
