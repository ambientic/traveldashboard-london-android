package traveldashboard.data.status;

/**
 * Tube station status collection
 * @author khoo
 *
 */
public class TubeStationStatusCollection {
	
	protected TubeStationStatus[] status;
	
	protected TubeStationStatusCollection() {}
	
	/**
	 * Tube station status collection constructor
	 * @param status
	 */
	public TubeStationStatusCollection(TubeStationStatus[] status) {
		this.status = status;
	}
	
	/**
	 * Get tube station status collection
	 * @return
	 */
	public TubeStationStatus[] getTubeStationStatusCollection() {
		return status;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < status.length; i++) {
			sb.append(status[i].toString() + "\n");
		}
		return sb.toString();
	}

}
