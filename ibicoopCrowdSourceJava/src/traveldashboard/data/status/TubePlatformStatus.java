package traveldashboard.data.status;

import traveldashboard.data.TubePlatform;

/**
 * Tube platform status
 * @author khoo
 *
 */
public class TubePlatformStatus extends GeneralStatus {
	
	protected TubePlatform platform;
	
	protected TubePlatformStatus() {}
	
	/**
	* TubePlatform status constructor
	* @param platform
	* @param status
	* @param description
	*/
	public TubePlatformStatus(TubePlatform platform, String status, String description) {
		super(status, description);
		this.platform = platform;
	}
	
	
	/**
	* Get platform
	* @return platform
	*/
	public TubePlatform getStation() {
		return platform;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("TubePlatform: " + platform.getName() + "\n");
		sb.append(super.toString());
		return sb.toString();
	}

}
