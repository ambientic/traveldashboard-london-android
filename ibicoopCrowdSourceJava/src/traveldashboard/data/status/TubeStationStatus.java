package traveldashboard.data.status;

import traveldashboard.data.TubeStation;

/**
 * Tube station status
 * @author khoo
 *
 */
public class TubeStationStatus extends StationStatus {
	
	protected TubeStationStatus() {}

	/**
	 * Tube station status constructor
	 * @param station
	 * @param status
	 * @param description
	 */
	public TubeStationStatus(TubeStation station, String status, String description) {
		super(station, status, description);
	}
	
	/**
	 * Get tube station
	 * @return
	 */
	public TubeStation getTubeStation() {
		return (TubeStation) station;
	}
	
}
