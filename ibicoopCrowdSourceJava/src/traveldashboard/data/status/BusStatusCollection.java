package traveldashboard.data.status;

/**
 * Bus status collection
 * @author khoo
 *
 */
public class BusStatusCollection {
	
	protected BusStatus[] status;
	
	protected BusStatusCollection() {}
	
	/**
	 * Bus status collection constructor
	 * @param status
	 */
	public BusStatusCollection(BusStatus[] status) {
		this.status = status;
	}
	
	/**
	 * Get bus status collection
	 * @return
	 */
	public BusStatus[] getBusStatusCollection() {
		return status;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < status.length; i++) {
			sb.append(status[i].toString() + "\n");
		}
		return sb.toString();
	}
}
