package traveldashboard.data.status;

/**
 * Station status collection 
 * @author khoo
 *
 */
public class StationStatusCollection extends GeneralStatusCollection {
	
	protected StationStatus[] status;
	
	protected StationStatusCollection() {}
	
	/**
	 * Station station collection constructor
	 * @param status
	 */
	public StationStatusCollection(StationStatus[] status) {
		this.status = status;
	}
	
	/**
	 * Get station status collection
	 * @return station status collection
	 */
	public StationStatus[] getStationStatusCollection() {
		return status;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < status.length; i++) {
			sb.append(status[i].toString() + "\n");
		}
		return sb.toString();
	}
}
