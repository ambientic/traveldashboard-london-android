package traveldashboard.data.status;

import traveldashboard.data.Station;

public class StationStatus extends GeneralStatus {
			
	protected Station station;

	protected StationStatus() {}
	
	/**
	 * Station status constructor
	 * @param station
	 * @param status
	 * @param description
	 */
	public StationStatus(Station station, String status, String description) {
		super(status, description);
		this.station = station;
	}
	

	/**
	 * Get station
	 * @return station
	 */
	public Station getStation() {
		return station;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Station: " + station.getName() + "\n");
		sb.append(super.toString());
		return sb.toString();
	}

}
