package traveldashboard.data.status;

import traveldashboard.data.BikeStation;

/**
 * Bike station status
 * @author khoo
 *
 */
public class BikeStationStatus extends StationStatus {
	
	protected BikeStationStatus() {}
	
	/**
	 * Bike station status constructor
	 * @param station
	 * @param status
	 * @param description
	 */
	public BikeStationStatus(BikeStation station, String status, String description) {
		super(station, status, description);
	}
	
	/**
	 * Get bike station
	 * @return bike station
	 */
	public BikeStation getBikeStation() {
		return (BikeStation) station;
	}
}
