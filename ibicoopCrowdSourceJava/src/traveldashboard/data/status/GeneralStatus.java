package traveldashboard.data.status;

/**
 * General status
 * @author khoo
 *
 */
public class GeneralStatus {
	
	protected String status;
	protected String description;
	
	protected GeneralStatus() {}
	
	/**
	 * General status constructor
	 * @param status
	 * @param description
	 */
	public GeneralStatus(String status, String description) {
		this.status = status;
		this.description = description;
	}
	
	/**
	 * Get status
	 * @return status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Get detail description
	 * @return
	 */
	public String getDescription() {
		return description;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Status: " + status + " - " + description + "\n");
		return sb.toString();
	}
}
