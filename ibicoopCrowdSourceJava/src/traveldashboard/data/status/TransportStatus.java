package traveldashboard.data.status;

import traveldashboard.data.Transport;


/**
 * Transport status
 * @author khoo
 *
 */
public class TransportStatus extends GeneralStatus {
	
	protected Transport transport;

	protected TransportStatus() {}
	
	/**
	 * Transport status constructor
	 * @param transport
	 * @param status
	 * @param description
	 */
	public TransportStatus(Transport transport, String status, String description) {
		super(status, description);
		this.transport = transport;
	}
	

	/**
	 * Get transport
	 * @return transport
	 */
	public Transport getTransport() {
		return transport;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Transport: " + transport.getName() + "\n");
		sb.append(super.toString());
		return sb.toString();
	}
}
