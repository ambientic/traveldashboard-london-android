package traveldashboard.data.status;

public class GeneralStatusCollection {
	
	protected GeneralStatus[] status;
	
	protected GeneralStatusCollection() {}
	
	/**
	 * General status collection
	 * @param status
	 */
	public GeneralStatusCollection(GeneralStatus[] status) {
		this.status = status;
	}
	
	/**
	 * get general status collection
	 * @return gene
	 */
	public GeneralStatus[] getGeneralStatusCollection() {
		return status;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < status.length; i++) {
			sb.append(status[i].toString() + "\n");
		}
		return sb.toString();
	}
}
