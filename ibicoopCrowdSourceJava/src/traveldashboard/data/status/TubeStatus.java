package traveldashboard.data.status;

import traveldashboard.data.Tube;

/**
 * Tube status
 * @author khoo
 *
 */
public class TubeStatus extends GeneralStatus {
	
	protected Tube tube;
	
	protected TubeStatus() {}
	
	/**
	 * Tube status constructor
	 * @param tube
	 * @param status
	 * @param description
	 */
	public TubeStatus(Tube tube, String status, String description) {
		super(status, description);
		this.tube = tube;
	}
	
	/**
	 * Get tube
	 * @return tube
	 */
	public Tube getTube() {
		return tube;
	}
}
