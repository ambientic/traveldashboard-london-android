package traveldashboard.data.status;

/**
 * Bus station status collection
 * @author khoo
 *
 */
public class BusStationStatusCollection {
	
	protected BusStationStatus[] status;
	
	protected BusStationStatusCollection() {}

	/**
	 * Bus station status collection constructor
	 * @param status
	 */
	public BusStationStatusCollection(BusStationStatus[] status) {
		this.status = status;
	}
	
	/**
	 * Get bus station status collection
	 * @return bus station status collection
	 */
	public BusStationStatus[] getBusStationStatusCollection() {
		return status;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < status.length; i++) {
			sb.append(status[i].toString() + "\n");
		}
		return sb.toString();
	}
}
