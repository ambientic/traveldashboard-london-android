package traveldashboard.data.status;

/**
 * Bike station status collection
 * @author khoo
 *
 */
public class BikeStationStatusCollection {
	
	protected BikeStationStatus[] status;
	
	protected BikeStationStatusCollection() {}
	
	/**
	 * Bike station status collection constructor
	 * @param status
	 */
	public BikeStationStatusCollection(BikeStationStatus[] status) {
		this.status = status;
	}
	
	/**
	 * Get bike station status collection
	 * @return bike station status collection
	 */
	public BikeStationStatus[] getBikeStationStatusCollection() {
		return status;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < status.length; i++) {
			sb.append(status[i].toString() + "\n");
		}
		return sb.toString();
	}
}
