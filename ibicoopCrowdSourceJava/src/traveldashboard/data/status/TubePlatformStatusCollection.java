package traveldashboard.data.status;

/**
 * Tube platform status collection
 * @author khoo
 *
 */
public class TubePlatformStatusCollection extends GeneralStatusCollection {
	
	protected TubePlatformStatus[] status;
	
	protected TubePlatformStatusCollection() {}
	
	/**
	 * Tube platform status collection
	 * @param status
	 */
	public TubePlatformStatusCollection(TubePlatformStatus[] status) {
		this.status = status;
	}
	
	/**
	 * Get tube platform status collection
	 * @return tube platform status collection
	 */
	public TubePlatformStatus[] getTubePlatformStatusCollection() {
		return status;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < status.length; i++) {
			sb.append(status[i].toString() + "\n");
		}
		return sb.toString();
	}
}
