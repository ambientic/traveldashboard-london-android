package traveldashboard.data;

/**
 * Tube platforms collection
 * @author khoo
 *
 */
public class TubePlatformsCollection {
	
	protected TubePlatform[] tubePlatforms;
	
	protected TubePlatformsCollection() {}
	
	/**
	 * Tube platforms collection
	 * @param tubeplatforms
	 */
	public TubePlatformsCollection(TubePlatform[] tubePlatforms) {
		this.tubePlatforms = tubePlatforms;
	}
	
	/**
	 * Get tube platforms
	 * @return tube platforms
	 */
	public TubePlatform[] getTubePlatforms() {
		return tubePlatforms;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < tubePlatforms.length; i++) {
			sb.append(tubePlatforms[i].getName());
		}
		return sb.toString();
	}
}
