package traveldashboard.data;

/**
 * Collection of transports
 * @author khoo
 *
 */
public class TransportsCollection {
	
	protected Transport[] transports;
	
	protected TransportsCollection() {}
	
	/**
	 * Transports collection
	 * @param transports
	 */
	public TransportsCollection(Transport[] transports) {
		this.transports = transports;
	}
	
	
	/**
	 * Get transports
	 * @return transports
	 */
	public Transport[] getTransports() {
		return transports;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < transports.length; i++) {
			sb.append(transports[i].getName());
		}
		return sb.toString();
	}
}
