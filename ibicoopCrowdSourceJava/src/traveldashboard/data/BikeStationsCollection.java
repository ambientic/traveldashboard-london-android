package traveldashboard.data;

/**
 * Bike stations collection
 * @author khoo
 *
 */
public class BikeStationsCollection {
	
	protected BikeStation[] stations;
	
	protected BikeStationsCollection() {}
	
	/**
	 * Constructor of bike stations collection
	 * @param bikeStations
	 */
	public BikeStationsCollection(BikeStation[] bikeStations) {
		stations = bikeStations;
	}
	
	/**
	 * Get bike stations
	 * @return bike stations
	 */
	public BikeStation[] getBikeStations() {
		return stations;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < stations.length; i++) {
			sb.append(stations[i].toString() + "\n");
		}
		return sb.toString();
	}
	
}
