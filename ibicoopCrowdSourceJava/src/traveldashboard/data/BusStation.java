package traveldashboard.data;

/**
 * Bus station
 * @author khoo
 *
 */
public class BusStation extends Station {

	protected Bus[] buses;
			
	protected BusStation() {}
	
	/**
	 * Bus station constructor
	 * @param id Bus station id
	 * @param name Bus station name
	 * @param latitude Bus station latitude
	 * @param longitude Bus station longitude
	 * @param buses Buses which pass by this station
	 */
	public BusStation(String id, String name, double latitude, double longitude, Bus[] buses) {
		super(id, name, latitude, longitude);
		this.buses = buses;
	}
	
	/**
	 * Get buses
	 * @return Buses
	 */
	public Bus[] getBuses() {
		return buses;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		
		for (int i = 0; i < buses.length; i++) {
			sb.append("Bus: " + buses[i].toString() + "\n");
		}
		
		return sb.toString();
	}
	
}
