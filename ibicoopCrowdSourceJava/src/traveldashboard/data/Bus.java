package traveldashboard.data;

/**
 * Bus
 * @author khoo
 *
 */
public class Bus extends Transport {

	protected Bus() {}
	
	/**
	 * Bus constructor
	 * @param id Bus id
	 * @param name Bus name
	 * @param destination Destination
	 */
	public Bus(String id, String name, String destination) {
		super(id, name, TransportType.BUS, destination);
	}

}
