package traveldashboard.data;

public class TubeStation extends Station {

	protected Tube[] tubes;

	protected String locationCode;

	private Boolean hasRER = null;

	protected TubeStation() {
	}


	/**
	 * Tube station constructor
	 * 
	 * @param id
	 *            Tube station id
	 * @param name
	 *            Tube station name
	 * @param locationCode
	 *            Tube location code
	 * @param latitude
	 *            Latitude of the station
	 * @param longitude
	 *            Longitude of the station
	 * @param tubes
	 *            All tubes
	 */
	public TubeStation(String id, String name, String locationCode,
			double latitude, double longitude, Tube[] tubes) {
		super(id, name, latitude, longitude);
		this.tubes = tubes;
		this.locationCode = locationCode;
	}

	/**
	 * Get tubes
	 * 
	 * @return tubes
	 */
	public Tube[] getTubes() {
		return tubes;
	}

	/**
	 * Get tube station location code
	 * 
	 * @return location code
	 */
	public String getLocationCode() {
		return locationCode;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());

		sb.append("Location code: " + locationCode + "\n");

		for (int j = 0; j < tubes.length; j++) {
			sb.append("Tube: " + tubes[j].toString() + "\n");
		}

		return sb.toString();
	}


	/**
	 * computes exactly once per object if it has an RER line going through it.
	 * @return true if the station has an RER line going through it, false if not.
	 */
	public boolean hasRER() {
		
		//hasRER computed only once
		if (hasRER == null) {
			hasRER = false;

			Tube[] tubes = getTubes();
			for (Tube tube : tubes) {
				if (tube.isRER()) {
					hasRER = true;
					break;
				}
			}
		}
		return hasRER;
	}
}
