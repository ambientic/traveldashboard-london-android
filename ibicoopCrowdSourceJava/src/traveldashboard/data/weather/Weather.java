package traveldashboard.data.weather;

/**
 * Weather
 * @author khoo
 *
 */
public class Weather {

	protected double latitude;
	protected double longitude;
	protected String temperature;
	protected String description;
	protected String iconUrl;
	
	public Weather(double latitude, double longitude, String temperature, String description, String iconUrl) {
		this.latitude = latitude;
		this.longitude = longitude;
		this.temperature = temperature;
		this.description = description;
		this.iconUrl = iconUrl;
	}
	
	public double getLatitude() {
		return latitude;
	}
	
	public double getLongitude() {
		return longitude;
	}
	
	public String getTemperature() {
		return temperature;
	}
	
	public String getDescription() {
		return description;
	}
	
	public String getIconUrl() {
		return iconUrl;
	}
	
	public String toString() {
		return "Today's weather forecast: " + "\n" +  temperature + "\u00B0" + "C, "  + description;
	}
}
