package traveldashboard.data;

/**
 * Tubes collection
 * @author khoo
 *
 */
public class TubesCollection extends TransportsCollection {
	
	protected TubesCollection() {}
	
	/**
	 * Constructor of tubes collection
	 * @param tubes
	 */
	public TubesCollection(Tube[] tubes) {
		super(tubes);
	}
	
	/**
	 * Get tubes
	 * @return tubes
	 */
	public Tube[] getTubes() {
		return (Tube[]) transports;
	}
	
}