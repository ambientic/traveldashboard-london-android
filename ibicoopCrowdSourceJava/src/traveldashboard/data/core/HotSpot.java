package traveldashboard.data.core;

/**
 * This interface represents a subject which will be used later to describe any information
 * about transport system, for example station, bus, tram, etc.
 *
 * @author Cong-Kinh NGUYEN
 *
 */
public interface HotSpot {

}
