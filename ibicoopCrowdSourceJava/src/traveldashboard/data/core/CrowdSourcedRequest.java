package traveldashboard.data.core;

/**
 * In order to execute a pull from a server, this interface is used to represent
 * request information which will be sent to the server.
 * 
 * @author Cong-Kinh NGUYEN
 *
 */
public interface CrowdSourcedRequest {
}
