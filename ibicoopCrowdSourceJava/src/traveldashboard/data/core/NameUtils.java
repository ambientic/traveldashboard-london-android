package traveldashboard.data.core;

/**
 * This class defines pattern names used to create the name of services, methods and operations
 * in order to create channels to communicate between a telephone and a server.
 * 
 * @author Cong-Kinh NGUYEN
 *
 */
public class NameUtils {

	private static final String SERVICE = "Service";
	private static final String PATH_GET = "get";
	private static final String PREFIX_OP_NAME = "Get";
	private static final String SERVICE_FOR_ALL = "ServiceForAll";
	private static final String PATH_GET_FOR_ALL = "getAll";
	private static final String PREFIX_OP_NAME_FOR_ALL = "GetAll";

	private static final String POSTFIX_TOPIC_NAME = "TopicName";
	private static final String POSTFIX_TOPIC_TYPE = "TopicType";
	private static final String POSTFIX_EVENT_NAME = "EventName";
	private static final String POSTFIX_BROKER_NAME = "BrokerManager";

	public static String getService(String clazzName) {
		if (clazzName == null) return null;
		return clazzName + SERVICE;
	}

	public static String getPath(String clazzName) {
		if (clazzName == null) return null;
		return PATH_GET + clazzName;
	}
	
	public static String getOperationName(String clazzName) {
		if (clazzName == null) return null;
		return PREFIX_OP_NAME + clazzName;
	}
	
	private NameUtils() {
	}

	public static String getService4All(String clazzName) {
		if (clazzName == null) return null;
		return clazzName + SERVICE_FOR_ALL;
	}

	public static String getPath4All(String clazzName) {
		if (clazzName == null) return null;
		return PATH_GET_FOR_ALL + clazzName;
	}

	public static String getOperationName4All(String clazzName) {
		if (clazzName == null) return null;
		return PREFIX_OP_NAME_FOR_ALL + clazzName;
	}

	public static String getTopicName(String clazzName) {
		return clazzName + POSTFIX_TOPIC_NAME;
	}

	public static String getTopicType(String clazzName) {
		return clazzName + POSTFIX_TOPIC_TYPE;
	}

	public static String getEventName(String clazzName) {
		return clazzName + POSTFIX_EVENT_NAME;
	}

	/**
	 * Returns an identical name. The name is to create channel to communicate between
	 * a telephone and server.
	 *
	 * @param clazz
	 * @return
	 */
	public static <C extends CrowdSourcedData<H, V>, H extends HotSpot, V extends CrowdSourcedValue> String getClazzName(
			Class<C> clazz) {
		return clazz.getName();
	}

	public static String getBrokerName(String clazzName) {
		return clazzName + POSTFIX_BROKER_NAME;
	}
}
