package traveldashboard.data.core;

/**
 * This interface defines super class of crowdsourced data value.
 * 
 * @author Cong-Kinh NGUYEN
 *
 */
public interface CrowdSourcedValue {

}
