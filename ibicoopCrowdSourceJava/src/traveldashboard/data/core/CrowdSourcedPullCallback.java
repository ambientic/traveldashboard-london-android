package traveldashboard.data.core;

import java.util.List;

/**
 * This interface is to update user interface when pulling crowdsourced data from server.
 * 
 * @author Cong-Kinh NGUYEN
 *
 * @param <C>
 * @param <H>
 * @param <V>
 */
public interface CrowdSourcedPullCallback<C extends CrowdSourcedData<H, V>, H extends HotSpot, V extends CrowdSourcedValue> {

	void failed();
	void update(List<C> crowdSourceds);
	void update(C crowdSourced);
}
