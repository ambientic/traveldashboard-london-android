package traveldashboard.data.core;

/**
 * This interface is used to update user interface when executing a push of crowdsourced data
 * to a server.
 *
 * @author Cong-Kinh NGUYEN
 *
 */
public interface CrowdSourcedPushCallback {

	void failed();
	void success();
}
