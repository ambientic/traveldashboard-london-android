package traveldashboard.data.core;

import java.util.Date;

public abstract class CrowdSourcedData<H extends HotSpot, V extends CrowdSourcedValue> {

	private H subject;
	
	private long timeStamp;
	
	private V crowdSourcedValue;

	public V getCrowdSourcedValue() {
		return crowdSourcedValue;
	}

	public void setCrowdSourcedValue(V crowdSourcedValue) {
		this.crowdSourcedValue = crowdSourcedValue;
	}

	public H getHotSpot() {
		return subject;
	}

	public void setHotSpot(H subject) {
		this.subject = subject;
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	public Date getDate() {
		return new Date(timeStamp);
	}
}
