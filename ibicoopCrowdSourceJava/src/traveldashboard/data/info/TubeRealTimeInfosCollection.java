package traveldashboard.data.info;

/**
 * Tube real time infos collection
 * @author khoo
 *
 */
public class TubeRealTimeInfosCollection {
	
	protected TubeRealTimeInfo[] tubeRealTimeInfos;
	
	protected TubeRealTimeInfosCollection() {}
	
	/**
	 * tube real time info collection
	 * @param tubeRealTimeInfos
	 */
	public TubeRealTimeInfosCollection(TubeRealTimeInfo[] tubeRealTimeInfos) {
		this.tubeRealTimeInfos = tubeRealTimeInfos;
	}
	
	/**
	 * Get tube real time info collection
	 * @return tube real time info collection
	 */
	public TubeRealTimeInfo[] getTubeRealTimeInfos() {
		return tubeRealTimeInfos;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < tubeRealTimeInfos.length; i++) {
			sb.append(tubeRealTimeInfos[i].toString() + "\n");
		}
		
		return sb.toString();
	}

}