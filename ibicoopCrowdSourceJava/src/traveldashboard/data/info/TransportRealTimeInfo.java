package traveldashboard.data.info;

import traveldashboard.data.Station;
import traveldashboard.data.Transport;

/**
 * Transport real time information
 * @author khoo
 *
 */
public class TransportRealTimeInfo {
	
	protected Transport transport;
	protected Station station;
	protected String destination;
	protected String time;
	
	protected TransportRealTimeInfo() {}
	
	/**
	 * Transport real time information constructor
	 * @param transport
	 * @param destination
	 * @param time
	 */
	public TransportRealTimeInfo(Transport transport, Station station, String time) {
		this.transport = transport;
		this.station = station;
		this.destination = transport.getDestination();
		this.time = time;
	}
	
	/**
	 * Get transport
	 * @return transport
	 */
	public Transport getTransport() {
		return transport;
	}

	/**
	 * Get station
	 * @return station
	 */
	public Station getStation() {
		return station;
	}
	
	/**
	 * Get destination
	 * @return destination
	 */
	public String getDestination() {
		return destination;
	}

	/**
	 * Get time
	 * @return time
	 */
	public String getTime() {
		return time;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Transport: " + transport.getName() + "\n");
		sb.append("Station: " + station.getName() + "\n");
		sb.append("Destination: " + destination + "\n");
		sb.append("Time: " + time + "\n");
		return sb.toString();
	}
	
}
