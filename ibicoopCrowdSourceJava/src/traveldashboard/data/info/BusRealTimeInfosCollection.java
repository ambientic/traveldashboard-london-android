package traveldashboard.data.info;

/**
 * Bus real time info collection
 * @author khoo
 *
 */
public class BusRealTimeInfosCollection {
	
	protected BusRealTimeInfo[] busRealTimeInfos;
	
	protected BusRealTimeInfosCollection() {}
	
	/**
	 * bus real time info collection
	 * @param busRealTimeInfos
	 */
	public BusRealTimeInfosCollection(BusRealTimeInfo[] busRealTimeInfos) {
		this.busRealTimeInfos = busRealTimeInfos;
	}
	
	/**
	 * Get bus real time info collection
	 * @return bus real time info collection
	 */
	public BusRealTimeInfo[] getBusRealTimeInfos() {
		return busRealTimeInfos;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < busRealTimeInfos.length; i++) {
			sb.append(busRealTimeInfos[i].toString() + "\n");
		}
		return sb.toString();
	}

}
