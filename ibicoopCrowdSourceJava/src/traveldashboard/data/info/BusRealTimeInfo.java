package traveldashboard.data.info;

import traveldashboard.data.Bus;
import traveldashboard.data.BusStation;

/**
 * Bus real time info
 * @author khoo
 *
 */
public class BusRealTimeInfo extends TransportRealTimeInfo {

	protected BusRealTimeInfo() {}
	
	/**
	 * Bus real time info constructor
	 * @param bus
	 * @param destination
	 * @param time
	 */
	public BusRealTimeInfo(Bus bus, BusStation station, String time) {
		super(bus, station, time);
	}
}
