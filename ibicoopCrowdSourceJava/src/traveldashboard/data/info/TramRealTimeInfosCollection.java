package traveldashboard.data.info;

public class TramRealTimeInfosCollection {
	
	protected TramRealTimeInfo[] tramRealTimeInfos;
	
	protected TramRealTimeInfosCollection() {}
	
	/**
	 * tram real time info collection
	 * @param tramRealTimeInfos
	 */
	public TramRealTimeInfosCollection(TramRealTimeInfo[] tramRealTimeInfos) {
		this.tramRealTimeInfos = tramRealTimeInfos;
	}
	
	/**
	 * Get tram real time info collection
	 * @return tram real time info collection
	 */
	public TramRealTimeInfo[] getTramRealTimeInfos() {
		return tramRealTimeInfos;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < tramRealTimeInfos.length; i++) {
			sb.append(tramRealTimeInfos[i].toString() + "\n");
		}
		return sb.toString();
	}

}
