package traveldashboard.data.info;

import traveldashboard.data.Tram;
import traveldashboard.data.TramStation;

public class TramRealTimeInfo extends TransportRealTimeInfo {

	protected TramRealTimeInfo() {}
	
	/**
	 * Tram real time info constructor
	 * @param tram
	 * @param destination
	 * @param time
	 */
	public TramRealTimeInfo(Tram tram, TramStation station, String time) {
		super(tram, station, time);
	}
}

