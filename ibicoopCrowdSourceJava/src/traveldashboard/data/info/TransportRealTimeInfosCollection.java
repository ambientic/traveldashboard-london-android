package traveldashboard.data.info;

public class TransportRealTimeInfosCollection {

	protected TransportRealTimeInfo[] transportRealTimeInfos;
	
	protected TransportRealTimeInfosCollection() {}
	
	/**
	 * Transport real time info collection
	 * @param transportRealTimeInfos
	 */
	protected TransportRealTimeInfosCollection(TransportRealTimeInfo[] transportRealTimeInfos) {
		this.transportRealTimeInfos = transportRealTimeInfos;
	}
	
	/**
	 * Get transport real time info collection
	 * @return transport real time info collection
	 */
	public TransportRealTimeInfo[] getTransportRealTimeInfos() {
		return transportRealTimeInfos;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < transportRealTimeInfos.length; i++) {
			sb.append(transportRealTimeInfos[i].toString());
		}
		return sb.toString();
	}
}
