package traveldashboard.data.info;

import traveldashboard.data.Tube;
import traveldashboard.data.TubePlatform;
import traveldashboard.data.TubeStation;


/**
 * Tube real time info
 * @author khoo
 *
 */
public class TubeRealTimeInfo extends TransportRealTimeInfo {
	
	protected TubePlatform platform;
	
	protected TubeRealTimeInfo() {}
	
	/**
	 * Tube real time info constructor
	 * @param tube
	 * @param station
	 * @param platform
	 * @param destination
	 * @param time
	 */
	public TubeRealTimeInfo(Tube tube, TubeStation station, TubePlatform platform, String time) {
		super(tube, station, time);
		this.platform = platform;
	}
	
	/**
	 * Get tube platform
	 * @return tube platform
	 */
	public TubePlatform getTubePlatform() {
		return platform;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("Platform: " + platform.getName() + "\n");
		return sb.toString();
	}
	
}
