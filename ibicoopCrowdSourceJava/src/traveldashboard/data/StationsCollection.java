package traveldashboard.data;

/**
 * Collection of stations
 * @author khoo
 *
 */
public class StationsCollection {

	protected Station[] stations;
	
	protected StationsCollection() {}
	
	/**
	 * Stations collection
	 * @param stations
	 */
	public StationsCollection(Station[] stations) {
		this.stations = stations;
	}
	
	/**
	 * Get stations
	 * @return stations
	 */
	public Station[] getStations() {
		return stations;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < stations.length; i++) {
			sb.append(stations[i].toString());
		}
		return sb.toString();
	}
}
