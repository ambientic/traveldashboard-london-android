package traveldashboard.data;

/**
 * Bike station
 * @author khoo
 *
 */
public class BikeStation extends Station {

	protected int nbBikes;
	protected int nbEmptyDocks;
	protected int nbTotalDocks;
	protected boolean locked;
	
	protected BikeStation() {}
	
	/**
	 * Bike station constructor
	 * @param id Bike station id
	 * @param name Bike station name
	 * @param latitude Bike station latitude
	 * @param longitude Bike station longitude
	 * @param nbBikes Number of bikes available
	 * @param nbEmptyDocks Number of empty bike docks
	 * @param nbTotalDocks Number of total bike docks
	 * @param locked Bike station is locked
	 */
	public BikeStation(String id, String name, double latitude, double longitude, 
			int nbBikes, int nbEmptyDocks, int nbTotalDocks, boolean locked) {
		super(id, name, latitude, longitude);
		this.nbBikes= nbBikes;
		this.nbEmptyDocks = nbEmptyDocks;
		this.nbTotalDocks = nbTotalDocks;
		this.locked = locked;
	}
	
	/**
	 * Get number of bikes available
	 * @return number of bikes available
	 */
	public int getNbBikes() {
		return nbBikes;
	}

	/**
	 * Get number of empty bike docks
	 * @return Number of empty bike docks
	 */
	public int getNbEmptyDocks() {
		return nbEmptyDocks;
	}

	/**
	 * Get number of total bike docks
	 * @return Number of total bike docks
	 */
	public int getNbTotalDocks() {
		return nbTotalDocks;
	}

	/**
	 * Check if bike station is locked
	 * @return true if bike station is locked, false otherwise
	 */
	public boolean isLocked() {
		return locked;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("Bikes available: " + String.valueOf(nbBikes) + "\n");
		sb.append("Empty docks: " + String.valueOf(nbEmptyDocks) + "\n");
		sb.append("Total docks: " + String.valueOf(nbTotalDocks) + "\n");
		sb.append("Locked: " + String.valueOf(locked) + "\n");
		return sb.toString();
	}
}
