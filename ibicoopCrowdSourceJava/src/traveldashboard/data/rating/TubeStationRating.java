package traveldashboard.data.rating;

import traveldashboard.data.TubeStation;

/**
 * Tube station rating
 * @author khoo
 *
 */
public class TubeStationRating extends StationRating {

	protected TubeStationRating() {}
	
	/**
	 * tube station rating constructor 
	 * @param tubeStation
	 * @param rating
	 */
	public TubeStationRating(TubeStation tubeStation, Rating rating) {
		super(tubeStation, rating);
	}
	
	/**
	 * Get tube station
	 * @return tube station
	 */
	public TubeStation getTubeStation() {
		return (TubeStation) station;
	}
}