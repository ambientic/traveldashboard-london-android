package traveldashboard.data.rating;

import traveldashboard.data.Station;

/**
 * Station rating
 * @author khoo
 *
 */
public class StationRating extends GeneralRating {

	protected Station station;
	
	protected StationRating() {}
	
	/**
	 * Constructor of station rating
	 * @param station
	 * @param rating
	 */
	public StationRating(Station station, Rating rating) {
		super(rating);
		this.station = station;
	}
	
	/**
	 * Get station
	 * @return station
	 */
	public Station getStation() {
		return station;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Station: " + station.getName() + "\n");
		sb.append(super.toString());
		return sb.toString();
	}
}