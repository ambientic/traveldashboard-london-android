package traveldashboard.data.rating;

import traveldashboard.data.Transport;

/**
 * Transport rating
 * @author khoo
 *
 */
public class TransportRating extends GeneralRating {

	protected Transport transport;
	
	protected TransportRating() {}
	
	/**
	 * Constructor of transport rating
	 * @param transport
	 * @param rating
	 */
	public TransportRating(Transport transport, Rating rating) {
		super(rating);
		this.transport = transport;
	}
	
	/**
	 * Get transport
	 * @return transport
	 */
	public Transport getTransport() {
		return transport;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Transport: " + transport.getName() + "\n");
		sb.append(super.toString());
		return sb.toString();
	}
}
