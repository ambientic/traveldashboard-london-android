package traveldashboard.data.rating;

/*
 * Bike station ratings collection
 */
public class BikeStationRatingsCollection {

	protected BikeStationRating[] ratings;
	
	protected BikeStationRatingsCollection() {}
	
	/**
	 * Bike station ratings collection
	 * @param ratings
	 */
	public BikeStationRatingsCollection(BikeStationRating[] ratings) {
		this.ratings = ratings;
	}
	
	/**
	 * get bike station ratings
	 * @return bike station ratings
	 */
	public BikeStationRating[] getBikeStationRatings() {
		return ratings;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < ratings.length; i++) {
			sb.append(ratings[i].toString() + "\n");
		}
		return sb.toString();
	}
}