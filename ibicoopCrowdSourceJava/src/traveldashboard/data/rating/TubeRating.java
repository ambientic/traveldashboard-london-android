package traveldashboard.data.rating;

import traveldashboard.data.Tube;

/**
 * Tube rating
 * @author khoo
 *
 */
public class TubeRating extends GeneralRating {

	protected Tube tube;
	
	protected TubeRating() {}
	
	/**
	 * tube rating constructor 
	 * @param tube
	 * @param rating
	 */
	public TubeRating(Tube tube, Rating rating) {
		super(rating);
		this.tube = tube;
	}
	
	/**
	 * Get tube
	 * @return tube
	 */
	public Tube getTube() {
		return tube;
	}
}