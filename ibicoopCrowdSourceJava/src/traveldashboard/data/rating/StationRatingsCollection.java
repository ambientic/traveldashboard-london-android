package traveldashboard.data.rating;

/**
 * Station ratings collection
 * @author khoo
 *
 */
public class StationRatingsCollection {
	
	protected StationRating[] ratings;
	
	protected StationRatingsCollection() {}

	/**
	 * Station ratings collection
	 * @param ratings
	 */
	public StationRatingsCollection(StationRating[] ratings) {
		this.ratings = ratings;
	}
	
	/**
	 * get station ratings
	 * @return station ratings
	 */
	public StationRating[] getStationRatings() {
		return ratings;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < ratings.length; i++) {
			sb.append(ratings[i].toString() + "\n");
		}
		return sb.toString();
	}
}
