package traveldashboard.data.rating;

/**
 * General ratings collection
 * @author khoo
 *
 */
public class GeneralRatingsCollection {

	protected GeneralRating[] generalRatings;
	
	protected GeneralRatingsCollection() {}
	
	/**
	 * general ratings collection constructor
	 * @param generalRatings
	 */
	protected GeneralRatingsCollection(GeneralRating[] generalRatings) {
		this.generalRatings = generalRatings;
	}
	
	/**
	 * get general ratings
	 * @return general ratings
	 */
	public GeneralRating[] getGeneralRatings() {
		return generalRatings;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < generalRatings.length; i++) {
			sb.append(generalRatings[i].toString());
		}
		return sb.toString();
	}
}
