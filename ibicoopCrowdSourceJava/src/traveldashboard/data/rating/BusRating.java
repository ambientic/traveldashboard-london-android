package traveldashboard.data.rating;

import traveldashboard.data.Bus;

public class BusRating extends GeneralRating {

	protected Bus bus;
	
	protected BusRating() {}
	
	/**
	 * bus rating constructor 
	 * @param bus
	 * @param rating
	 */
	public BusRating(Bus bus, Rating rating) {
		super(rating);
		this.bus = bus;
	}
	
	/**
	 * Get bus station
	 * @return bus station
	 */
	public Bus getBus() {
		return bus;
	}
}