package traveldashboard.data.rating;

/**
 * Transport ratings collection
 * @author khoo
 *
 */
public class TransportRatingsCollection{

	protected TransportRating[] ratings;
	
	protected TransportRatingsCollection() {}
	
	/**
	 * Transport ratings collection
	 * @param ratings
	 */
	public TransportRatingsCollection(TransportRating[] ratings) {
		this.ratings = ratings;
	}
	
	/**
	 * Get transport ratings
	 * @return ratings
	 */
	public TransportRating[] getTransportRatings() {
		return ratings;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < ratings.length; i++) {
			sb.append(ratings[i].toString() + "\n");
		}
		return sb.toString();
	}
}
