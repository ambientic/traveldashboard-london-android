package traveldashboard.data.rating;

import traveldashboard.data.BikeStation;

/**
 * Bike station rating
 * @author khoo
 *
 */
public class BikeStationRating extends StationRating {

	protected BikeStationRating() {}
	
	/**
	 * bike station rating constructor 
	 * @param bikeStation
	 * @param rating
	 */
	public BikeStationRating(BikeStation bikeStation, Rating rating) {
		super(bikeStation, rating);
	}
	
	/**
	 * Get bike station
	 * @return bike station
	 */
	public BikeStation getBikeStation() {
		return (BikeStation) station;
	}
}
