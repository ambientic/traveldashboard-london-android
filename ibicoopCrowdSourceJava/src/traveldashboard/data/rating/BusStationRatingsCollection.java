package traveldashboard.data.rating;

/**
 * Collection of bus station ratings collection
 * @author khoo
 *
 */
public class BusStationRatingsCollection {

	protected BusStationRating[] ratings;
	
	protected BusStationRatingsCollection() {}

	/**
	 * Bus station ratings collection
	 * @param ratings
	 */
	public BusStationRatingsCollection(BusStationRating[] ratings) {
		this.ratings = ratings;
	}
	
	/**
	 * get bus station ratings
	 * @return bus station ratings
	 */
	public BusStationRating[] getBusStationRatings() {
		return ratings;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < ratings.length; i++) {
			sb.append(ratings[i].toString() + "\n");
		}
		return sb.toString();
	}
}