package traveldashboard.data.rating;

import traveldashboard.data.TubePlatform;

public class TubePlatformRating extends GeneralRating {

	protected TubePlatform platform;
	
	protected TubePlatformRating() {}
	
	/**
	 * Constructor of tube platform rating
	 * @param platform
	 * @param rating
	 */
	public TubePlatformRating(TubePlatform platform, Rating rating) {
		super(rating);
		this.platform = platform;
	}
	
	/**
	 * Get tube platform
	 * @return tube platform
	 */
	public TubePlatform getTubePlatform() {
		return platform;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("TubePlatform: " + platform.getName() + "\n");
		sb.append(super.toString());
		return sb.toString();
	}
}
