package traveldashboard.data.rating;

public class TubeStationRatingsCollection {

	protected TubeStationRating[] ratings;
	
	protected TubeStationRatingsCollection() {}

	/**
	 * Tube station ratings collection
	 * @param ratings
	 */
	public TubeStationRatingsCollection(TubeStationRating[] ratings) {
		this.ratings = ratings;
	}
	
	/**
	 * get tube station ratings
	 * @return tube station ratings
	 */
	public TubeStationRating[] getTubeStationRatings() {
		return ratings;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < ratings.length; i++) {
			sb.append(ratings[i].toString() + "\n");
		}
		return sb.toString();
	}
}
