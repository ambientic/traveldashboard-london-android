package traveldashboard.data.rating;

/**
 * User tube rating
 * @author khoo
 *
 */
public class UserTubeRating {

	protected String userName;
	protected TubeRating tubeRating;
	
	/**
	 * User tube rating constructor
	 * @param userName
	 * @param tubeRating
	 */
	public UserTubeRating(String userName, TubeRating tubeRating) {
		this.userName = userName;
		this.tubeRating = tubeRating;
	}
	
	/**
	 * Get user name
	 * @return user name
	 */
	public String getUserName() {
		return userName;
	}
	
	/**
	 * Get tube rating
	 * @return tube rating
	 */
	public TubeRating getTubeRating() {
		return tubeRating;
	}
}
