package traveldashboard.data.rating;

import traveldashboard.data.BikeStation;

public class BusStationRating extends StationRating {

	protected BusStationRating() {}
	
	/**
	 * bike station rating constructor 
	 * @param bikeStation
	 * @param rating
	 */
	public BusStationRating(BikeStation bikeStation, Rating rating) {
		super(bikeStation, rating);
	}
	
	/**
	 * Get bike station
	 * @return bike station
	 */
	public BikeStation getBikeStation() {
		return (BikeStation) station;
	}
}