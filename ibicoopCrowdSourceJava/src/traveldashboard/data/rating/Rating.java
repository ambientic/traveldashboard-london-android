package traveldashboard.data.rating;

/**
 * Rating
 * @author khoo
 *
 */
public class Rating {
	
	protected RatingLevel ratingLevel;
	protected int nbVotes;
	
	protected Rating() {}
	
	/**
	 * Rating constructor
	 * @param ratingLevel Rating level
	 * @param nbVoters Number of voters
	 */
	public Rating(RatingLevel ratingLevel, int nbVoters) {
		this.ratingLevel = ratingLevel;
		this.nbVotes = nbVoters;
	}
	
	/**
	 * Get rating level
	 * @return rating level
	 */
	public RatingLevel getRatingLevel() {
		return ratingLevel;
	}

	/**
	 * Get number of rating votes
	 * @return
	 */
	public int getNbVotes() {
		return nbVotes;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Rating level: " + ratingLevel.toString() + "\n");
		sb.append("Number of votes: " + String.valueOf(nbVotes) + "\n");	
		return sb.toString();
	}
}
