package traveldashboard.data.rating;

/**
 * Tube ratings collection
 * @author khoo
 *
 */
public class TubeRatingsCollection {

	protected TubeRating[] ratings;
	
	protected TubeRatingsCollection() {}
	
	/**
	 * Tube ratings collection
	 * @param ratings
	 */
	public TubeRatingsCollection(TubeRating[] ratings) {
		this.ratings = ratings;
	}
	
	/**
	 * Get tube ratings
	 * @return ratings
	 */
	public TubeRating[] getTubeRatings() {
		return ratings;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < ratings.length; i++) {
			sb.append(ratings[i].getTube().getName() + "\n" + ratings[i].toString());
		}
		
		return sb.toString();
	}
}
