package traveldashboard.data.rating;

/**
 * Rating
 * @author khoo
 *
 */
public enum RatingLevel {
	UNKNOWN(-1),
	ZERO(0),
	ONE(1), 
	TWO(2), 
	THREE(3), 
	FOUR(4), 
	FIVE(5);
	
	int rating;
	
	private RatingLevel() {}
	
	private RatingLevel(int rating) {
		this.rating = rating;
	}
	
	@Override
	public String toString() {
		return String.valueOf(rating);
	}
	
	public int getRating() {
		return rating;
	}
}
