package traveldashboard.data.rating;


/**
 * General rating
 * @author khoo
 *
 */
public class GeneralRating {

	protected Rating rating;
	
	protected GeneralRating() {}
	
	/**
	 * General rating constructor
	 * @param rating
	 */
	public GeneralRating(Rating rating) {
		this.rating = rating;
	}
	
	/**
	 * Get rating
	 * @return rating
	 */
	public Rating getRating() {
		return rating;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Rating: " + rating.toString() + "\n");
		return sb.toString();
	}
}
