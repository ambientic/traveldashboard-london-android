package traveldashboard.data.rating;

/**
 * Tube platform ratings collection
 * @author khoo
 *
 */
public class TubePlatformRatingsCollection {
	
	protected TubePlatformRating[] ratings;
	
	protected TubePlatformRatingsCollection() {}

	/**
	 * Tube platform ratings collection constructor
	 * @param ratings
	 */
	public TubePlatformRatingsCollection(TubePlatformRating[] ratings) {
		this.ratings = ratings;
	}
	
	/**
	 * Get tube platform ratings
	 * @return ratings
	 */
	public TubePlatformRating[] getTubePlatformRatings() {
		return ratings;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < ratings.length; i++) {
			sb.append(ratings[i].toString() + "\n");
		}
		return sb.toString();
	}
}
