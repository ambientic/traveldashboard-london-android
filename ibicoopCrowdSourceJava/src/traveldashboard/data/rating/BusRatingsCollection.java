package traveldashboard.data.rating;

/**
 * Bus ratings collection
 * @author khoo
 *
 */
public class BusRatingsCollection {

	protected BusRating[] ratings;
	
	protected BusRatingsCollection() {}
	
	/**
	 * Bus ratings collection constructor
	 * @param ratings
	 */
	public BusRatingsCollection(BusRating[] ratings) {
		this.ratings = ratings;
	}
	
	/**
	 * Get bus ratings
	 * @return
	 */
	public BusRating[] getBusRatings() {
		return ratings;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < ratings.length; i++) {
			sb.append(ratings[i].toString() + "\n");
		}
		return sb.toString();
	}
}
