package traveldashboard.data.crowd;

import traveldashboard.data.Transport;

public class TransportCrowd extends GeneralCrowd {
	
	protected Transport transport;
	
	protected TransportCrowd() {}
	
	/**
	 * Transport crowd constructor
	 * @param transport
	 * @param crowd
	 */
	public TransportCrowd(Transport transport, Crowd crowd) {
		super(crowd);
		this.transport = transport;
	}
	
	/**
	 * Get transport
	 * @return transport
	 */
	public Transport getTransport() {
		return transport;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Transport: " + transport.getName() + "\n");
		sb.append(super.toString());
		return sb.toString();
	}
}