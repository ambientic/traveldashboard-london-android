package traveldashboard.data.crowd;

/**
 * Tube station crowds collection
 * @author khoo
 *
 */
public class TubeStationCrowdsCollection {

	protected TubeStationCrowd[] tubeStationCrowds;
	
	protected TubeStationCrowdsCollection() {}
	
	/**
	 * tube station crowds collection
	 * @param tubeStationCrowds
	 */
	public TubeStationCrowdsCollection(TubeStationCrowd[] tubeStationCrowds) {
		this.tubeStationCrowds = tubeStationCrowds;
	}
	
	/**
	 * Get tube station crowds
	 * @return tube station crowds
	 */
	public TubeStationCrowd[] getTubeStationCrowds() {
		return tubeStationCrowds;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < tubeStationCrowds.length; i++) {
			sb.append(tubeStationCrowds[i].toString() + "\n");
		}
		return sb.toString();
	}
}
