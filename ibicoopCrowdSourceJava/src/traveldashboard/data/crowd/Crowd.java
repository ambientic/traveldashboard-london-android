package traveldashboard.data.crowd;

public class Crowd {

	protected CrowdLevel crowdLevel;
	protected int nbVotes;
	
	protected Crowd() {}
	
	/**
	 * Crowd constructor
	 * @param crowdLevel Crowd level
	 * @param nbVotes Number of voters
	 */
	public Crowd(CrowdLevel crowdLevel, int nbVotes) {
		this.crowdLevel = crowdLevel;
		this.nbVotes = nbVotes;
	}
	
	/**
	 * Get crowd level
	 * @return crowd level
	 */
	public CrowdLevel getCrowdLevel() {
		return crowdLevel;
	}

	/**
	 * Get number of crowd voters
	 * @return
	 */
	public int getNbVotes() {
		return nbVotes;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Crowd level: " + crowdLevel.toString() + "\n");
		sb.append("Number of votes: " + String.valueOf(nbVotes) + "\n");	
		return sb.toString();
	}

}
