package traveldashboard.data.crowd;

/**
 * Bus station crowds collection
 * @author khoo
 *
 */
public class BusStationCrowdsCollection {

	protected BusStationCrowd[] busStationCrowds;
	
	protected BusStationCrowdsCollection() {}
	
	/**
	 * bus station crowds collection
	 * @param busStationCrowds
	 */
	public BusStationCrowdsCollection(BusStationCrowd[] busStationCrowds) {
		this.busStationCrowds = busStationCrowds;
	}
	
	/**
	 * Get bus station crowds
	 * @return bus station crowds
	 */
	public BusStationCrowd[] getBusStationCrowds() {
		return busStationCrowds;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < busStationCrowds.length; i++) {
			sb.append(busStationCrowds[i].toString() + "\n");
		}
		return sb.toString();
	}
}
