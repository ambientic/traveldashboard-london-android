package traveldashboard.data.crowd;

/**
 * Tube crowds
 * @author khoo
 *
 */
public class TubeCrowdsCollection {

	protected TubeCrowd[] tubeCrowd;
	
	protected TubeCrowdsCollection() {}
	
	/**
	 * Tube crowds collection
	 * @param tubeCrowd
	 */
	public TubeCrowdsCollection(TubeCrowd[] tubeCrowd) {
		this.tubeCrowd = tubeCrowd;
	}
	
	/**
	 * Get tube crowds
	 * @return tube crowds
	 */
	public TubeCrowd[] getBusCrowds() {
		return tubeCrowd;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < tubeCrowd.length; i++) {
			sb.append(tubeCrowd[i].toString() + "\n");
		}
		return sb.toString();
	}
}
