package traveldashboard.data.crowd;

import traveldashboard.data.TubePlatform;

/**
 * Crowdedness on platform
 * @author khoo
 *
 */
public class TubePlatformCrowd  extends GeneralCrowd {
	
	protected TubePlatform platform;
	
	protected TubePlatformCrowd() {}
	
	/**
	 * Tube platform crowd constructor
	 * @param platform
	 * @param crowd
	 */
	public TubePlatformCrowd(TubePlatform platform, Crowd crowd) {
		super(crowd);
		this.platform = platform;
	}
	
	/**
	 * Get platform
	 * @return platform
	 */
	public TubePlatform getTubePlatform() {
		return platform;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Tube platform: " + platform.getName() + "\n");
		sb.append(super.toString());
		return sb.toString();
	}
}