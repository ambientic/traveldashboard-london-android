package traveldashboard.data.crowd;

import traveldashboard.data.BikeStation;

public class BikeStationCrowd extends StationCrowd {
	
	protected BikeStationCrowd() {}
	
	/**
	 * Bike station crowd constructor
	 * @param bike station
	 * @param crowd
	 */
	public BikeStationCrowd(BikeStation bikeStation, Crowd crowd) {
		super(bikeStation, crowd);
	}
	
	/**
	 * Get bike station
	 * @return bike station
	 */
	public BikeStation getBikeStation() {
		return (BikeStation) (station);
	}
}