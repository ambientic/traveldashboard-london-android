package traveldashboard.data.crowd;

import traveldashboard.data.Tube;

/**
 * Crowd in the tube
 * @author khoo
 *
 */
public class TubeCrowd extends TransportCrowd {
	
	protected TubeCrowd() {}
	
	/**
	 * tube crowd constructor
	 * @param tube
	 * @param crowd
	 */
	public TubeCrowd(Tube tube, Crowd crowd) {
		super(tube, crowd);
	}
	
	/**
	 * Get tube
	 * @return tube
	 */
	public Tube getTube() {
		return (Tube) (transport);
	}
}
