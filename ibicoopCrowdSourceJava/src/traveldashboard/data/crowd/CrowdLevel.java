package traveldashboard.data.crowd;

/**
 * Crowd level
 * @author khoo
 *
 */
public enum CrowdLevel {
	UNKNOWN(-1),
	ZERO(0),
	ONE(1), 
	TWO(2), 
	THREE(3), 
	FOUR(4), 
	FIVE(5);
	
	int level;
	
	private CrowdLevel() {}
	
	private CrowdLevel(int level) {
		this.level = level;
	}
	
	@Override
	public String toString() {
		return String.valueOf(level);
	}
	
	public int getLevel() {
		return level;
	}
}
