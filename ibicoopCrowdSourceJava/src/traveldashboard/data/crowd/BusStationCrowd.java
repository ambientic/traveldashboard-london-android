package traveldashboard.data.crowd;

import traveldashboard.data.BusStation;

/**
 * Bus station crowd
 * @author khoo
 *
 */
public class BusStationCrowd extends StationCrowd {
	
	protected BusStationCrowd() {}
	
	/**
	 * Bus station crowd constructor
	 * @param bus station
	 * @param crowd
	 */
	public BusStationCrowd(BusStation busStation, Crowd crowd) {
		super(busStation, crowd);
	}
	
	/**
	 * Get bus station
	 * @return bus station
	 */
	public BusStation getStation() {
		return (BusStation) (station);
	}
}
