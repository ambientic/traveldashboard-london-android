package traveldashboard.data.crowd;

/**
 * Station crowd collection
 * @author khoo
 *
 */
public class StationCrowdsCollection {

	protected StationCrowd[] stationCrowds;
	
	protected StationCrowdsCollection() {}
	
	/**
	 * station crowds collection
	 * @param stationCrowds
	 */
	public StationCrowdsCollection(StationCrowd[] stationCrowds) {
		this.stationCrowds = stationCrowds;
	}
	
	/**
	 * Get station crowds
	 * @return station crowds
	 */
	public StationCrowd[] getStationCrowds() {
		return stationCrowds;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < stationCrowds.length; i++) {
			sb.append(stationCrowds[i].toString() + "\n");
		}
		return sb.toString();
	}
}
