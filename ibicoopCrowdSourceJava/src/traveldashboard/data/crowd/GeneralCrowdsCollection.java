package traveldashboard.data.crowd;

/**
 * General crowds collection
 * @author khoo
 *
 */
public class GeneralCrowdsCollection {

	protected GeneralCrowd[] generalCrowds;
	
	protected GeneralCrowdsCollection() {}
	
	/**
	 * General crowds collection
	 * @param generalCrowds
	 */
	public GeneralCrowdsCollection(GeneralCrowd[] generalCrowds) {
		this.generalCrowds = generalCrowds;
	}
	
	/**
	 * Get general crowds
	 * @return general crowds
	 */
	public GeneralCrowd[] getGeneralCrowd() {
		return generalCrowds;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < generalCrowds.length; i++) {
			sb.append(generalCrowds[i].toString());
		}
		return sb.toString();
	}
}
