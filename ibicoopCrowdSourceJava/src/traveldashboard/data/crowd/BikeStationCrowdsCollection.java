package traveldashboard.data.crowd;

public class BikeStationCrowdsCollection {

	protected BikeStationCrowd[] crowds;
	
	protected BikeStationCrowdsCollection() {}
	
	/**
	 * bike station crowds collection
	 * @param bikeStationCrowds
	 */
	public BikeStationCrowdsCollection(BikeStationCrowd[] bikeStationCrowds) {
		this.crowds = bikeStationCrowds;
	}
	
	/**
	 * Get bike station crowds
	 * @return bike station crowds
	 */
	public BikeStationCrowd[] getBikeStationCrowds() {
		return crowds;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < crowds.length; i++) {
			sb.append(crowds[i].toString() + "\n");
		}
		return sb.toString();
	}
}
