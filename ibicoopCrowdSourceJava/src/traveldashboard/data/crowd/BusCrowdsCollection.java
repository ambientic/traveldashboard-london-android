package traveldashboard.data.crowd;


/**
 * Bus crowds collection
 * @author khoo
 *
 */
public class BusCrowdsCollection {

	protected BusCrowd[] busCrowd;
	
	protected BusCrowdsCollection() {}
	
	/**
	 * Bus crowds collection
	 * @param busCrowd
	 */
	public BusCrowdsCollection(BusCrowd[] busCrowd) {
		this.busCrowd = busCrowd;
	}
	
	/**
	 * Get transport crowds
	 * @return transport crowds
	 */
	public BusCrowd[] getBusCrowds() {
		return busCrowd;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < busCrowd.length; i++) {
			sb.append(busCrowd[i].toString() + "\n");
		}
		return sb.toString();
	}
}
