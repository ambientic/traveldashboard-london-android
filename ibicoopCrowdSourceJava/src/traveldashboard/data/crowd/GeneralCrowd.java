package traveldashboard.data.crowd;

public class GeneralCrowd {
	protected Crowd crowd;
	
	
	protected GeneralCrowd() {}
	
	/**
	 * General crowdedness constructor
	 * @param crowd
	 */
	public GeneralCrowd(Crowd crowd) {
		this.crowd = crowd;
	}
	
	/**
	 * Get crowdedness
	 * @return crowdedness
	 */
	public Crowd getCrowd() {
		return crowd;
	}
	
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Crowd: " + crowd.toString() + "\n");
		return sb.toString();
	}
}
