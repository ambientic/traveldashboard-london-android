package traveldashboard.data.crowd;

import traveldashboard.data.Bus;

/**
 * Crowd in the bus
 * @author khoo
 *
 */
public class BusCrowd extends TransportCrowd {
	
	protected BusCrowd() {}
	
	/**
	 * bus crowd constructor
	 * @param bus
	 * @param crowd
	 */
	public BusCrowd(Bus bus, Crowd crowd) {
		super(bus, crowd);
	}
	
	/**
	 * Get bus
	 * @return bus
	 */
	public Bus getBus() {
		return (Bus) (transport);
	}
}
