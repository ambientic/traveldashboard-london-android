package traveldashboard.data.crowd;

import traveldashboard.data.Station;

public class StationCrowd extends GeneralCrowd {
	
	protected Station station;
	
	protected StationCrowd() {}
	
	/**
	 * Station crowd constructor
	 * @param station
	 * @param crowd
	 */
	public StationCrowd(Station station, Crowd crowd) {
		super(crowd);
		this.station = station;

	}
	
	/**
	 * Get station
	 * @return station
	 */
	public Station getStation() {
		return station;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Station: " + station.getName() + "\n");
		sb.append(super.toString());
		return sb.toString();
	}
}
