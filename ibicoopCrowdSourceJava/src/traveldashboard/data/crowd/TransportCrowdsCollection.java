package traveldashboard.data.crowd;

public class TransportCrowdsCollection {

	protected TransportCrowd[] transportCrowds;
	
	protected TransportCrowdsCollection() {}
	
	/**
	 * General crowds collection
	 * @param transportCrowds
	 */
	public TransportCrowdsCollection(TransportCrowd[] transportCrowds) {
		this.transportCrowds = transportCrowds;
	}
	
	/**
	 * Get transport crowds
	 * @return transport crowds
	 */
	public TransportCrowd[] getTransportCrowds() {
		return transportCrowds;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < transportCrowds.length; i++) {
			sb.append(transportCrowds[i].toString() + "\n");
		}
		return sb.toString();
	}
}
