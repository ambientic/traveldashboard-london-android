package traveldashboard.data.crowd;

import traveldashboard.data.TubeStation;

/**
 * Tube station crowd
 * @author khoo
 *
 */
public class TubeStationCrowd extends GeneralCrowd {
	
	protected TubeStation tubeStation;
	
	protected TubeStationCrowd() {}
	
	/**
	 * Tube station crowd constructor
	 * @param tube station
	 * @param crowd
	 */
	public TubeStationCrowd(TubeStation tubeStation, Crowd crowd) {
		super(crowd);
		this.tubeStation = tubeStation;
	}
	
	/**
	 * Get tube station
	 * @return tube station
	 */
	public TubeStation getTubeStation() {
		return tubeStation;
	}
}
