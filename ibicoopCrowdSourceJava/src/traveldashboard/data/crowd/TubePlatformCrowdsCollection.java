package traveldashboard.data.crowd;

/**
 * Tube platform crowds collection
 * @author khoo
 *
 */
public class TubePlatformCrowdsCollection {

	protected TubePlatformCrowd[] tubePlatformCrowds;
	
	protected TubePlatformCrowdsCollection() {}
	
	/**
	 * Tube platform crowds collection
	 * @param tubePlatformCrowds
	 */
	public TubePlatformCrowdsCollection(TubePlatformCrowd[] tubePlatformCrowds) {
		this.tubePlatformCrowds = tubePlatformCrowds;
	}
	
	/**
	 * Get tube platform crowds
	 * @return tube platform crowds
	 */
	public TubePlatformCrowd[] getTubePlatformCrowds() {
		return tubePlatformCrowds;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < tubePlatformCrowds.length; i++) {
			sb.append(tubePlatformCrowds[i].toString() + "\n");
		}
		return sb.toString();
	}
}