package traveldashboard.data;

/**
 * Tube platform
 * @author khoo
 *
 */
public class TubePlatform {
	
	protected String id;
	protected String name;
	
	protected TubePlatform() {}
	
	/**
	 * Tube platform constructor
	 * @param id Platform id
	 * @param name Platform name
	 */
	public TubePlatform(String id, String name) {
		this.id = id;
		this.name = name;
	}
	
	/**
	 * Get tube platform id
	 * @return tube platform id
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * Get tube platform name
	 * @return tube platform name
	 */
	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Id: " + id + "\n");
		sb.append("Name: " + name + "\n");
		return sb.toString();
	}

}
