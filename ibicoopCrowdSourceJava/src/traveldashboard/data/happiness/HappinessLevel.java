package traveldashboard.data.happiness;

import traveldashboard.data.core.CrowdSourcedValue;

/**
 * This class defines a level of happiness which is represented by an integer value and description.
 *
 * @author Cong-Kinh NGUYEN
 *
 */
public class HappinessLevel implements CrowdSourcedValue {

	public static final HappinessLevel HAPPY = new HappinessLevel(5, "Happy");
	public static final HappinessLevel GLAD = new HappinessLevel(4, "Glad");
	public static final HappinessLevel OK = new HappinessLevel(3, "Ok");
	public static final HappinessLevel SAD = new HappinessLevel(2, "Sad");
	public static final HappinessLevel CRIED = new HappinessLevel(1, "Cried");

	private int level;
	
	private String description;

	public HappinessLevel() {
	}

	protected HappinessLevel(int level, String description) {
		this.level = level;
		this.description = description;
	}
	
	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
