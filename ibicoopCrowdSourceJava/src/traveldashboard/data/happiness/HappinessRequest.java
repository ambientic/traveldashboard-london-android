package traveldashboard.data.happiness;

import traveldashboard.data.Station;
import traveldashboard.data.core.CrowdSourcedRequest;


/**
 * This class represents a request sent to server when asking for happiness at a station.
 *
 * @author Cong-Kinh NGUYEN
 *
 */
public class HappinessRequest implements CrowdSourcedRequest {

	/**Interval for timestamp*/
	public static final long INTERVAL = 15 * 60 * 1000;

	/**Attribute which indicates timestamp we need to ask for happiness*/
	private long timestamp;
	
	private Station subject;

	public Station getStation() {
		return subject;
	}

	public void setStation(Station station) {
		this.subject = station;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimstamp(long timestamp) {
		this.timestamp = timestamp;
	}
}
