package traveldashboard.data.happiness;

import traveldashboard.data.Station;
import traveldashboard.data.core.CrowdSourcedData;

/**
 * This class defines happiness at a station. 
 * @author Cong-Kinh NGUYEN
 *
 */
public class Happiness extends CrowdSourcedData<Station, HappinessLevel> {
}
