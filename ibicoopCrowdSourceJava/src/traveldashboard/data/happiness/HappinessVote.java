package traveldashboard.data.happiness;

/**
 * This class defines a response when asking for happiness at a station. The response contains
 * information about happiness at a station and statistic data for voting happiness.
 *
 * @author Cong-Kinh NGUYEN
 *
 */
public class HappinessVote extends Happiness {
	
	/**Number of votes for the value of happiness*/
	private int numberOfVotes;

	/**Total number of votes for a station in the interval set by HappinessRequest*/
	private int totalVotes;

	public int getNumberOfVotes() {
		return numberOfVotes;
	}
	public void setNumberOfVotes(int numberOfVotes) {
		this.numberOfVotes = numberOfVotes;
	}
	public int getTotalVotes() {
		return totalVotes;
	}
	public void setTotalVotes(int totalVotes) {
		this.totalVotes = totalVotes;
	}

	
}
