package traveldashboard.data.comment;

import traveldashboard.data.Transport;


/**
 * Transport comment
 * @author khoo
 *
 */
public class TransportComment extends GeneralComment {
	
	protected Transport transport;
	
	protected TransportComment() {}
	
	/**
	 * Constructor of transport comment
	 * @param user
	 * @param comment
	 * @param timestamp
	 * @param transport
	 */
	public TransportComment(String user, String comment, long timestamp, Transport transport) {
		super(user, comment, timestamp);
		this.transport = transport;
	}
	
	/**
	 * Get transport
	 * @return transport
	 */
	public Transport getStation() {
		return transport;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Transport: " + transport.getName() + "\n");
		sb.append(super.toString());
		return sb.toString();
	}

}