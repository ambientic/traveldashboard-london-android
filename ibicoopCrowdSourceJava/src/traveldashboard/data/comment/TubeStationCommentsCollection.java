package traveldashboard.data.comment;

/**
 * Tube station comments
 * @author khoo
 *
 */
public class TubeStationCommentsCollection {

	protected TubeStationComment[] comments;
	
	protected TubeStationCommentsCollection() {}
	
	/**
	 * Constructor of tube station comments collection
	 * @param comments
	 */
	public TubeStationCommentsCollection(TubeStationComment[] comments) {
		this.comments = comments;
	}
	
	/**
	 * Get tube station comments
	 * @return tube station comments
	 */
	public TubeStationComment[] getTubeStationComments() {
		return comments;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < comments.length; i++) {
			sb.append(comments[i].toString() + "\n");
		}
		
		return sb.toString();
	}
}
