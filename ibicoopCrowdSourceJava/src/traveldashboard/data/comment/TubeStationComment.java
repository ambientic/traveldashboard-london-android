package traveldashboard.data.comment;

import traveldashboard.data.TubeStation;

public class TubeStationComment extends GeneralComment {

	protected TubeStation station;
	
	protected TubeStationComment() {}
	
	/**
	 * Constructor of tubeStation comment
	 * @param user
	 * @param comment
	 * @param timestamp
	 * @param tubeStation
	 */
	public TubeStationComment(String user, String comment, long timestamp, TubeStation tubeStation) {
		super(user, comment, timestamp);
		station = tubeStation;
	}
	
	/**
	 * Get tube station
	 * @return tube station
	 */
	public TubeStation getTubeStation() {
		return station;
	}	
}