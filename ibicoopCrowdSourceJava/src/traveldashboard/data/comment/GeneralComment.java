package traveldashboard.data.comment;

/**
 * General comment
 * @author khoo
 *
 */
public class GeneralComment {
	
	protected String user;
	protected String comment;
	protected long timestamp;

	protected GeneralComment() {}
	
	/**
	 * Constructor of general comment
	 * @param user
	 * @param comment
	 * @param timestamp
	 */
	public GeneralComment(String user, String comment, long timestamp) {
		this.user = user;
		this.comment = comment;
		this.timestamp = timestamp;
	}
	
	/**
	 * Get user
	 * @return user
	 */
	public String getUser() {
		return user;
	}

	/**
	 * Get comment
	 * @return comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Get timestamp
	 * @return timestamp
	 */
	public long getTimestamp() {
		return timestamp;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("User: " + user + "\n");
		sb.append("Comment: " + comment + "\n");
		sb.append("Time: " + timestamp + "\n");
		return sb.toString();
	}

}
