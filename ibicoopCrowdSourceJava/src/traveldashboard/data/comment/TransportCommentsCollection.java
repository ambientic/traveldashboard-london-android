package traveldashboard.data.comment;

/**
 * Transport comments collection
 * @author khoo
 *
 */
public class TransportCommentsCollection {

	protected TransportComment[] comments;
	
	protected TransportCommentsCollection() {}
	
	/**
	 * Constructor of transport comments collection
	 * @param comments
	 */
	public TransportCommentsCollection(TransportComment[] comments) {
		this.comments = comments;
	}
	
	/**
	 * Get transport comments
	 * @return transport comments
	 */
	public TransportComment[] getTransportComments() {
		return comments;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < comments.length; i++) {
			sb.append(comments[i].toString() + "\n");
		}
		
		return sb.toString();
	}
}

