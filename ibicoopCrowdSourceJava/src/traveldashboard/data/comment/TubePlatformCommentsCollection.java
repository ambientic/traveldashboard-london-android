package traveldashboard.data.comment;

/**
 * Tube platform comments collection
 * @author khoo
 *
 */
public class TubePlatformCommentsCollection {

	protected TubePlatformComment[] comments;
	
	protected TubePlatformCommentsCollection() {}
	
	/**
	 * Constructor of tube platform comments collection
	 * @param comments
	 */
	public TubePlatformCommentsCollection(TubePlatformComment[] comments) {
		this.comments = comments;
	}
	
	/**
	 * Get tube platform comments
	 * @return tube platform comments
	 */
	public TubePlatformComment[] getTransportComments() {
		return comments;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < comments.length; i++) {
			sb.append(comments[i].toString() + "\n");
		}
		
		return sb.toString();
	}
}
