package traveldashboard.data.comment;


/**
 * Collection of station comments
 * @author khoo
 *
 */
public class StationCommentsCollection {

	StationComment[] comments;
	
	protected StationCommentsCollection() {}
	
	/**
	 * Constructor of station comments collection
	 * @param comments
	 */
	public StationCommentsCollection(StationComment[] comments) {
		this.comments = comments;
	}
	
	/**
	 * Get station comments
	 * @return station comments
	 */
	public StationComment[] getStationComments() {
		return comments;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < comments.length; i++) {
			sb.append(comments[i].toString() + "\n");
		}
		
		return sb.toString();
	}
}
