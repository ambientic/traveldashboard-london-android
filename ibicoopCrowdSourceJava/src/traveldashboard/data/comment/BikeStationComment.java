package traveldashboard.data.comment;

import traveldashboard.data.BikeStation;

/**
 * Bike station comment
 * @author khoo
 *
 */
public class BikeStationComment extends StationComment {

	protected BikeStationComment() {}
	
	/**
	 * Constructor of bikeStation comment
	 * @param user
	 * @param comment
	 * @param timestamp
	 * @param bikeStation
	 */
	public BikeStationComment(String user, String comment, long timestamp, BikeStation bikeStation) {
		super(user, comment, timestamp, bikeStation);
	}
	
	/**
	 * Get bike station
	 * @return bike station
	 */
	public BikeStation getBikeStation() {
		return (BikeStation) (station);
	}	
}