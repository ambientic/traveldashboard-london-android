package traveldashboard.data.comment;

/**
 * Bike station comments collection
 * @author khoo
 *
 */
public class BikeStationCommentsCollection {

	protected BikeStationComment[] comments;
	
	protected BikeStationCommentsCollection() {}
	
	/**
	 * Constructor of bike station comments collection
	 * @param comments
	 */
	public BikeStationCommentsCollection(BikeStationComment[] comments) {
		this.comments = comments;
	}
	
	/**
	 * Get bike station comments
	 * @return bike station comments
	 */
	public BikeStationComment[] getBikeStationComments() {
		return comments;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < comments.length; i++) {
			sb.append(comments[i].toString() + "\n");
		}
		
		return sb.toString();
	}
}

