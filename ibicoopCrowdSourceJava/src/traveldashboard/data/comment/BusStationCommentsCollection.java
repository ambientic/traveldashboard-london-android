package traveldashboard.data.comment;

/**
 * Bus station comments
 * @author khoo
 *
 */
public class BusStationCommentsCollection {

	protected BusStationComment[] comments;
	
	protected BusStationCommentsCollection() {}
	
	/**
	 * Constructor of bus station comments collection
	 * @param comments
	 */
	public BusStationCommentsCollection(BusStationComment[] comments) {
		this.comments = comments;
	}
	
	/**
	 * Get bus station comments
	 * @return bus station comments
	 */
	public BusStationComment[] getBusStationComments() {
		return comments;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < comments.length; i++) {
			sb.append(comments[i].toString() + "\n");
		}
		
		return sb.toString();
	}
}
