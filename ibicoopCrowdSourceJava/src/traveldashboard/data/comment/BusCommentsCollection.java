package traveldashboard.data.comment;

/**
 * Bus comments collection
 * @author khoo
 *
 */
public class BusCommentsCollection {
	
	protected BusComment[] comments;

	protected BusCommentsCollection() {}
	
	/**
	 * Constructor of bus comments collection
	 * @param comments
	 */
	public BusCommentsCollection(BusComment[] comments) {
		this.comments = comments;
	}
	
	/**
	 * Get bus comments
	 * @return bus comments
	 */
	public BusComment[] getBusComments() {
		return comments;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < comments.length; i++) {
			sb.append(comments[i].toString() + "\n");
		}
		
		return sb.toString();
	}
}
