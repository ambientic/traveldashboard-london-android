package traveldashboard.data.comment;

import traveldashboard.data.Station;

/**
 * Station comment
 * @author khoo
 *
 */
public class StationComment extends GeneralComment {
	
	protected Station station;
	
	protected StationComment() {}
	
	/**
	 * Constructor of station comment
	 * @param user
	 * @param comment
	 * @param timestamp
	 * @param station
	 */
	public StationComment(String user, String comment, long timestamp, Station station) {
		super(user, comment, timestamp);
		this.station = station;
	}
	
	/**
	 * Get station
	 * @return station
	 */
	public Station getStation() {
		return station;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Station: " + station.getName() + "\n");
		sb.append(super.toString());
		return sb.toString();
	}

}
