package traveldashboard.data.comment;

/**
 * Tube comments collection
 * @author khoo
 *
 */
public class TubeCommentsCollection {

	protected TubeComment[] comments;
	
	protected TubeCommentsCollection() {}
	
	/**
	 * Constructor of tube comments collection
	 * @param comments
	 */
	public TubeCommentsCollection(TubeComment[] comments) {
		this.comments = comments;
	}
	
	/**
	 * Get tube comments
	 * @return tube comments
	 */
	public TubeComment[] getTubeComments() {
		return comments;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < comments.length; i++) {
			sb.append(comments[i].toString() + "\n");
		}
		
		return sb.toString();
	}
}


