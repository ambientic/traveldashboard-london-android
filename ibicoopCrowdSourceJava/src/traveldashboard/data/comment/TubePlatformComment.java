package traveldashboard.data.comment;

import traveldashboard.data.TubePlatform;

/**
 * Tube platform comment
 * @author khoo
 *
 */
public class TubePlatformComment extends GeneralComment {
	
	protected TubePlatform platform;
	
	protected TubePlatformComment() {}
	
	/**
	 * Constructor of platform comment
	 * @param user
	 * @param comment
	 * @param timestamp
	 * @param platform
	 */
	public TubePlatformComment(String user, String comment, long timestamp, TubePlatform platform) {
		super(user, comment, timestamp);
		this.platform = platform;
	}
	
	/**
	 * Get platform
	 * @return platform
	 */
	public TubePlatform getStation() {
		return platform;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("TubePlatform: " + platform.getName() + "\n");
		sb.append(super.toString());
		return sb.toString();
	}

}