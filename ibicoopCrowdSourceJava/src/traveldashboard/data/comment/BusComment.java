package traveldashboard.data.comment;

import traveldashboard.data.Bus;

/**
 * Bus comment
 * @author khoo
 *
 */
public class BusComment extends TransportComment {

	protected BusComment() {}
	
	/**
	 * Constructor of bus comment
	 * @param user
	 * @param comment
	 * @param timestamp
	 * @param bus
	 */
	public BusComment(String user, String comment, long timestamp, Bus bus) {
		super(user, comment, timestamp, bus);
	}
	
	/**
	 * Get bus station
	 * @return bus station
	 */
	public Bus getBus() {
		return (Bus) (transport);
	}	
}
