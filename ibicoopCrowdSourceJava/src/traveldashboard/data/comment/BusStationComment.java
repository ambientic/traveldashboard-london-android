package traveldashboard.data.comment;

import traveldashboard.data.BusStation;

/**
 * Bus station comment
 * @author khoo
 *
 */
public class BusStationComment extends StationComment {

	protected BusStationComment() {}
	
	/**
	 * Constructor of busStation comment
	 * @param user
	 * @param comment
	 * @param timestamp
	 * @param busStation
	 */
	public BusStationComment(String user, String comment, long timestamp, BusStation busStation) {
		super(user, comment, timestamp, busStation);
	}
	
	/**
	 * Get bus station
	 * @return bus station
	 */
	public BusStation getBusStation() {
		return (BusStation) (station);
	}	
}
