package traveldashboard.data.comment;

/**
 * 
 * @author khoo
 *
 */
public class GeneralCommentsCollection {
	
	protected GeneralComment[] comments;
	
	protected GeneralCommentsCollection() {}
	
	/**
	 * Comments collection
	 * @param comments
	 */
	public GeneralCommentsCollection(GeneralComment[] comments) {
		this.comments = comments;
	}
	
	
	/**
	 * Get comments
	 * @return comments
	 */
	public GeneralComment[] getComments() {
		return comments;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < comments.length; i++) {
			sb.append(comments[i].getComment());
		}
		return sb.toString();
	}
}