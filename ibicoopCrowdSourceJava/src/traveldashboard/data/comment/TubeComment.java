package traveldashboard.data.comment;

import traveldashboard.data.Tube;

public class TubeComment extends GeneralComment {

	protected Tube tube;
	
	protected TubeComment() {}
	
	/**
	 * Constructor of tube comment
	 * @param user
	 * @param comment
	 * @param timestamp
	 * @param tube
	 */
	public TubeComment(String user, String comment, long timestamp, Tube tube) {
		super(user, comment, timestamp);
		this.tube = tube;
	}
	
	/**
	 * Get tube station
	 * @return tube station
	 */
	public Tube getTube() {
		return tube;
	}	
}