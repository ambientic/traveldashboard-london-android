package traveldashboard.data;

import java.util.ArrayList;
import java.util.List;

public class AreaBorder {

	private double maxLat = 0.0;
	private double minLat = 0.0;
	private double maxLon = 0.0;
	private double minLon = 0.0;
	
	public AreaBorder(double maxLat, double minLat, double maxLon, double minLon) {
		this.maxLat = maxLat;
		this.minLat = minLat;
		this.maxLon = maxLon;
		this.minLon = minLon;
	}
	
	public static AreaBorder getAreaBorder(double lat, double lon, double rad) {
        double maxLatitude = lat + (rad/100000);
        double minLatitude = lat - (rad/100000);
        double maxLongitude = lon + (rad/50000);
        double minLongitude = lon - (rad/50000);
        return new AreaBorder(maxLatitude, minLatitude, maxLongitude, minLongitude);
	}
	
	public static List<AreaBorder> decomposeAreaBorderInFourZones(AreaBorder initialAreaBorder) {
		List<AreaBorder> areaBorderList = new ArrayList<AreaBorder>();
		
		double init_maxlat = initialAreaBorder.getMaxLat();
		double init_minlat = initialAreaBorder.getMinLat();
		double init_maxlon = initialAreaBorder.getMaxLon();
		double init_minlon = initialAreaBorder.getMinLon();
		
		//Zone1
		double zone1_maxlat = init_maxlat;
		double zone1_minlat = (init_maxlat + init_minlat)*0.5;
		double zone1_maxlon = (init_maxlon + init_minlon)*0.5;
		double zone1_minlon = init_minlon;
		AreaBorder zone1 = new AreaBorder(zone1_maxlat, zone1_minlat, zone1_maxlon, zone1_minlon);
		
		//Zone2
		double zone2_maxlat = init_maxlat;
		double zone2_minlat = (init_maxlat + init_minlat)*0.5;
		double zone2_maxlon = init_maxlon;
		double zone2_minlon = (init_maxlon + init_minlon)*0.5;
		AreaBorder zone2 = new AreaBorder(zone2_maxlat, zone2_minlat, zone2_maxlon, zone2_minlon);
		
		//Zone3
		double zone3_maxlat = (init_maxlat + init_minlat)*0.5;
		double zone3_minlat = init_minlat;
		double zone3_maxlon = (init_maxlon + init_minlon)*0.5;
		double zone3_minlon = init_minlon;
		AreaBorder zone3 = new AreaBorder(zone3_maxlat, zone3_minlat, zone3_maxlon, zone3_minlon);
		
		//Zone4
		double zone4_maxlat = (init_maxlat + init_minlat)*0.5;
		double zone4_minlat = init_minlat;
		double zone4_maxlon = init_maxlon;
		double zone4_minlon = (init_maxlon + init_minlon)*0.5;
		AreaBorder zone4 = new AreaBorder(zone4_maxlat, zone4_minlat, zone4_maxlon, zone4_minlon);
		
		areaBorderList.add(zone1);
		areaBorderList.add(zone2);
		areaBorderList.add(zone3);
		areaBorderList.add(zone4);
		
		return areaBorderList;
	}
	
	
	public double getMaxLat() {return maxLat;}
	public double getMinLat() {return minLat;}
	public double getMaxLon() {return maxLon;}
	public double getMinLon() {return minLon;}
}
