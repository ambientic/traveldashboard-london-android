package traveldashboard.data;

/**
 * Transport type
 * @author khoo
 *
 */
public enum TransportType {
	BUS("Bus"), 
	TUBE("Tube"),
	TRAM("Tram");
	
	String type;
	
	private TransportType() {}
	
	private TransportType(String type) {
		this.type = type;
	}
	
	@Override
	public String toString() {
		return type;
	}
}
