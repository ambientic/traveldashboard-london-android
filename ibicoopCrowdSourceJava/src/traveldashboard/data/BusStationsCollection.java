package traveldashboard.data;

/**
 * Collection of bus stations
 * @author khoo
 *
 */
public class BusStationsCollection {
	
	protected BusStation[] busStations;
	
	protected BusStationsCollection() {}
	
	/**
	 * Constructor of bus stations collection
	 * @param busStations
	 */
	public BusStationsCollection(BusStation[] busStations) {
		this.busStations = busStations;
	}
	
	/**
	 * Get bus stations
	 * @return bus stations
	 */
	public BusStation[] getBusStations() {
		return busStations;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < busStations.length; i++) {
			sb.append(busStations[i].toString() + "\n");
		}
		return sb.toString();
	}
}
