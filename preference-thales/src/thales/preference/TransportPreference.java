package thales.preference;

import java.io.Serializable;

import org.cimst.annotation.AccessMapping;
import org.cimst.annotation.AccessTable;
import org.cimst.privacy.AccessMode;
import org.inbicoop.preference.TransportPreferenceIntf;


public class TransportPreference implements Serializable, TransportPreferenceIntf {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@AccessTable ( table={
			@AccessMapping(target=AccessMapping.SELF, mode=AccessMode.READ_WRITE),
			@AccessMapping (target=AccessMapping.OTHER, mode=AccessMode.READ_WRITE)
	} )
	private boolean bikePref;
	
	
	@AccessTable ( table={
			@AccessMapping(target=AccessMapping.SELF, mode=AccessMode.READ_WRITE),
			@AccessMapping (target=AccessMapping.OTHER, mode=AccessMode.READ_WRITE)
	} )
	private boolean busPref;
	
	
	@AccessTable ( table={
			@AccessMapping(target=AccessMapping.SELF, mode=AccessMode.READ_WRITE),
			@AccessMapping (target=AccessMapping.OTHER, mode=AccessMode.READ_WRITE)
	} )
	private boolean tramPref;
	
	
	@AccessTable ( table={
			@AccessMapping(target=AccessMapping.SELF, mode=AccessMode.READ_WRITE),
			@AccessMapping (target=AccessMapping.OTHER, mode=AccessMode.READ_WRITE)
	} )
	private boolean tubePref;
	
	
	@AccessTable ( table={
			@AccessMapping(target=AccessMapping.SELF, mode=AccessMode.READ_WRITE),
			@AccessMapping (target=AccessMapping.OTHER, mode=AccessMode.READ_WRITE)
	} )
	private boolean noisePref;
	
	
	@AccessTable ( table={
			@AccessMapping(target=AccessMapping.SELF, mode=AccessMode.READ_WRITE),
			@AccessMapping (target=AccessMapping.OTHER, mode=AccessMode.READ_WRITE)
	} )
	private boolean isTrackingCurrentPositionPref;
	
	
	@AccessTable ( table={
			@AccessMapping(target=AccessMapping.SELF, mode=AccessMode.READ_WRITE),
			@AccessMapping (target=AccessMapping.OTHER, mode=AccessMode.READ_WRITE)
	} )
	private boolean isSimulationModePref;
	
	public TransportPreference() {
		this.bikePref = true;
		this.busPref = true;
		this.tramPref = true;
		this.tubePref = true;
		this.noisePref = true;
		this.isTrackingCurrentPositionPref = false;
		this.isSimulationModePref = false;
	}
	
	public boolean getBikePref() {
		return bikePref;
	}
	
	public boolean getBusPref() {
		return busPref;
	}
	
	public boolean getTramPref() {
		return tramPref;
	}
	
	public boolean getTubePref() {
		return tubePref;
	}
	
	public boolean getNoisePref() {
		return noisePref;
	}
	
	public boolean isTrackingCurrentPosition() {
		return isTrackingCurrentPositionPref;
	}
	
	public boolean isSimulationMode() {
		return isSimulationModePref;
	}
	
	@Override
	public String toString() {
		return "bikePref = " + bikePref +
				", busPref = " + busPref +
				", tramPref = " + tramPref +
				", tubePref = " + tubePref +
				", noisePref = " + noisePref +
				", isTrackingCurrentPositionPref = " + isTrackingCurrentPositionPref +
				", isSimulationModePref = " + isSimulationModePref				
				;
	}

	@Override
	public void initTransportReference(boolean bikePref, boolean busPref,
			boolean tramPref, boolean tubePref, boolean noisePref,
			boolean isTrackingCurrentPositionPref, boolean isSimulationMode) {
		this.bikePref = bikePref;
		this.busPref = busPref;
		this.tramPref = tramPref;
		this.tubePref = tubePref;
		this.noisePref = noisePref;
		this.isTrackingCurrentPositionPref = isTrackingCurrentPositionPref;
		this.isSimulationModePref = isSimulationMode;
	}

}
