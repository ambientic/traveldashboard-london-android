package thales.preference;

import java.io.IOException;

import org.cimst.common.BindingCallback;
import org.inbicoop.preference.BindingCallbackIntf;
import org.inbicoop.preference.MainMapActivityCallback;
import org.inbicoop.preference.PreferenceConfigurationIntf;
import org.inbicoop.preference.TSLApplicationContextIntf;

public class BindingCallbackImpl implements BindingCallback, BindingCallbackIntf {

	private boolean debugMode;
	private MainMapActivityCallback mainMapActivityCallback;
	private TSLApplicationContextIntf tslApplicationContextIntf;
	
	@Override
	public void serviceConnected() {
		if (debugMode) System.out.println("Preference service connected");
		try {
			PreferenceConfigurationIntf preferenceConfiguration = tslApplicationContextIntf.getPreferenceConfiguration();
			
			if (preferenceConfiguration != null) {
				mainMapActivityCallback.processPreference(preferenceConfiguration.getTransportPreference());
			} else {
				if (debugMode) System.out.println("Preference configuration is null");
			}
			
		} catch (SecurityException e) {
			if (debugMode) e.printStackTrace();
		} catch (InstantiationException e) {
			if (debugMode) e.printStackTrace();
		} catch (IllegalAccessException e) {
			if (debugMode) e.printStackTrace();
		} catch (IOException e) {
			if (debugMode) e.printStackTrace();
		}
	}

	@Override
	public void serviceDisconnected() {
		if (debugMode) System.out.println("Preference service disconnected");
	}

	@Override
	public void initInterfaces(MainMapActivityCallback mainMapActivityCallback,
			TSLApplicationContextIntf tslApplicationContextIntf) {
		this.mainMapActivityCallback = mainMapActivityCallback;
		this.tslApplicationContextIntf = tslApplicationContextIntf;
	}

	@Override
	public void initMode(boolean debugMode) {
		this.debugMode = debugMode;
	}

}
