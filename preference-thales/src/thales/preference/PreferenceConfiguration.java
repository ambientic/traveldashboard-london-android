package thales.preference;

import java.io.IOException;
import java.io.Serializable;
import java.util.Properties;

import org.cimst.common.AppAccessInterpret;
import org.cimst.common.BindingCallback;
import org.cimst.common.Serviceable;
import org.cimst.common.StoredDataEditor;
import org.cimst.privacy.ContextPrivate;
import org.cimst.proxy.AbstractFactory;
import org.cimst.proxy.Api;
import org.inbicoop.preference.PreferenceConfigurationIntf;
import org.inbicoop.preference.TransportPreferenceIntf;
import org.inbicoop.preference.BindingCallbackIntf;
import org.theresis.cimst.semantic.Model;
import org.theresis.cimst.semantic.Resource;
import org.theresis.cimst.storage.Credential;
import org.theresis.cimst.storage.Storage;


import android.app.Activity;
import android.content.Context;

public class PreferenceConfiguration implements PreferenceConfigurationIntf {

	private Context context;
	
	private Model model;
	private Resource<TransportPreference> rc;
	private Credential credential;
	private Serviceable<Storage<Serializable>> serviceable;
	private Storage<Serializable> storage;
	private StoredDataEditor<Serializable, TransportPreference> storageHelper;

	private boolean debugMode;

	public void initParams(Context ctx, String prefDir, String prefUrl, String prefEntity, boolean debugMode) {
		this.debugMode = debugMode;
		if (debugMode) System.out.println("In the constructor of preference configuration with context = " + ctx);
		
		context = ctx;
		
		Properties props = new Properties();
		String storePath = context.getDir("Store", Context.MODE_PRIVATE).getAbsolutePath();

		props.setProperty(prefDir,
				storePath); // used by mock storage only
		
		AbstractFactory factory = Api.getAbstractFactory();
		
		final String ontologyURL = prefUrl;
		model = factory.getModelFactory().newModel(ontologyURL);
		
		final String entity = prefEntity;
		rc = model.newResource(entity, TransportPreference.class);

		credential= new ContextPrivate(context).getCredential();
		
		serviceable = factory.getSecureStorage(props);
		
		storage = serviceable.getServiceBase();
		
		storageHelper = new StoredDataEditor<Serializable, TransportPreference>(
				model, storage);
		
		storageHelper.setAccessInterpret(new AppAccessInterpret(context));
		storageHelper.setCredential(credential);
		
		if (debugMode) System.out.println("End of the constructor of preference configuration");
	}
	
	public Credential getCredential() {
		return credential;
	}
	
	
	public Serviceable<Storage<Serializable>> getServiceable() {
		return serviceable;
	}
	
	public Storage<Serializable> getStorage() {
		return storage;
	}
	
	public TransportPreferenceIntf getTransportPreference() throws SecurityException, InstantiationException, IllegalAccessException, IOException {
		if (debugMode) System.out.println("Checking preferences");

		TransportPreference stored = storageHelper.readData(TransportPreference.class.getName(), rc, false);
		
		if (stored == null) {
			if (debugMode) System.out.println("stored is null!");
			stored = new TransportPreference();
			storageHelper.writeData(stored, rc);
		}
		
		if (debugMode) {
			System.out.println("Current preferences......");
			System.out.println(stored.toString());
		}

		return stored;
	}
	
	public boolean setTransportPreference(TransportPreferenceIntf preference) throws IllegalArgumentException, SecurityException, IllegalAccessException, IOException, InstantiationException {
		boolean success = false;

		storageHelper.writeData((TransportPreference) preference, rc);
		
		if (debugMode) {
			System.out.println("Current preferences......");
			System.out.println(preference.toString());
		}
		
		TransportPreference stored = storageHelper.readData(TransportPreference.class.getName(), rc, false);
		
		if (stored != null) success = true;
		
		return success;
	}

	@Override
	public boolean isBoundForServiceBinder() {
		return getServiceable().getServiceBinder().isBound();
	}



	@Override
	public void bind(Activity activity, BindingCallbackIntf callback) {
		if (activity == null || callback == null) return;
		BindingCallback bc = (BindingCallback) callback;
		getServiceable().getServiceBinder().bind(activity, bc);
	}

	@Override
	public void unbind(Activity activity,
			BindingCallbackIntf callback) {
		if (activity == null || callback == null) return;
		BindingCallback bc = (BindingCallback) callback;
		getServiceable().getServiceBinder().unbind(activity, bc);
	}

}
