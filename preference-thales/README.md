travelsmartlondon-app
=====================

- To compile the code, please execute the following command:
  $ mvn clean install
  
- To install the application on your phone, please execute the following command:
  $ mvn android:deploy

- To open Eclipse with the application, please execute the following command:
  $ mvn eclipse:eclipse

- Note that the application use JAVA's version >= 1.6 because of some libraries compiled with the version.

Glossary:
* STC = STATION CODE = station code
* NLC = national road code in London


