package org.inbicoop.preference;

public interface TransportPreferenceIntf {

	/**
	 * Gets bike preference.
	 * @return
	 */
	boolean getBikePref();

	/**
	 * Gets bus preference.
	 * @return
	 */
	boolean getBusPref();

	/**
	 * Gets tram preference.
	 * @return
	 */
	boolean getTramPref();

	/**
	 * Gets tube preference.
	 * @return
	 */
	boolean getTubePref();

	/**
	 * Gets noise preference.
	 * @return
	 */
	boolean getNoisePref();

	/**
	 * Checks whether or not the application tracks the phone's position.
	 * @return
	 */
	boolean isTrackingCurrentPosition();

	/**
	 * Checks whether or not the application is in the simulation mode.
	 * @return
	 */
	boolean isSimulationMode();

	void initTransportReference(boolean bikePref, boolean busPref, 
			boolean tramPref, boolean tubePref, boolean noisePref,
			boolean isTrackingCurrentPositionPref, boolean isSimulationMode
			);
}
