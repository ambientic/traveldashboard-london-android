package org.inbicoop.preference;


public interface BindingCallbackIntf {

	void initInterfaces(MainMapActivityCallback mainMapActivityCallback, TSLApplicationContextIntf tslApplicationContextIntf);
	void initMode(boolean mode);
}
