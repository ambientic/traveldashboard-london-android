package org.inbicoop.preference;

import java.io.IOException;

import android.app.Activity;
import android.content.Context;

public interface PreferenceConfigurationIntf {

	TransportPreferenceIntf getTransportPreference() throws SecurityException, InstantiationException, IllegalAccessException, IOException;
	
	boolean setTransportPreference(TransportPreferenceIntf preference) throws IllegalArgumentException, SecurityException, IllegalAccessException, IOException, InstantiationException;

	boolean isBoundForServiceBinder();

	void initParams(Context context, String preferenceDir, String preferenceUrl, String prefEntity, boolean mode);

	void bind(Activity activity, BindingCallbackIntf callback);
	void unbind(Activity activity, BindingCallbackIntf callback);
}
