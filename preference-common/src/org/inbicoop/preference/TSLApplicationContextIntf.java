package org.inbicoop.preference;


public interface TSLApplicationContextIntf {

	PreferenceConfigurationIntf getPreferenceConfiguration();
}
