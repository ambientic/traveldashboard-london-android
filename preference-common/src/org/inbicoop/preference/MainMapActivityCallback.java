package org.inbicoop.preference;


public interface MainMapActivityCallback {

	void processPreference(TransportPreferenceIntf transportPref);
}
