travelsmartlondon-app
=====================
- At the moment, before compiling the module

 i) Please checkout IbicoopSensors:
  git clone https://yourBitbucketAccount@bitbucket.org/yourBitbucketAccount/newibicoopsensors.git
  for example git clone https://kinh@bitbucket.org/kinh/newibicoopsensors.git), then please do as the following:
   $ cd newibicoopsensors
   $ cd IbicoopSensorsJava
   $ mvn clean install
   $ cd ../IbicoopSensorsAndroid
   $ mvn clean install

  ii) Goto the POM parent, then edit the path to your Android SDK and its available version
   
- To compile the code, please execute the following command:
  $ mvn clean install
  
- To install the application on your phone, please execute the following command:
  $ mvn android:deploy

- To open Eclipse with the application, please execute the following command:
  $ mvn eclipse:eclipse

- Note that the application use JAVA's version >= 1.6 because of some libraries compiled with the version.

Glossary:
* STC = STATION CODE = station code
* NLC = national road code in London


