package traveldashboardlondon.preferences;

import org.inbicoop.preference.BindingCallbackIntf;
import org.inbicoop.preference.PreferenceConfigurationIntf;
import org.inbicoop.preference.TransportPreferenceIntf;

import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.utils.IntentConstants;
import traveldashboardlondon.app.MainMapActivity;
import traveldashboardlondon.context.TSLApplicationContext;
import android.app.Activity;
import android.content.Context;

public class PreferenceWrapper {

	public static final String PREF_THALES_PACKAGE_NAME = "thales.preference";

	public static final String TRANSPORT_PREF_IMPL_CLASS = PREF_THALES_PACKAGE_NAME + ".TransportPreference";
	public static final String PREF_CONFIG_IMPL_CLASS = PREF_THALES_PACKAGE_NAME + ".PreferenceConfiguration";
	public static final String BINDING_CALLBACK_IMPL_CLASS = PREF_THALES_PACKAGE_NAME + ".BindingCallbackImpl";

	private static BindingCallbackIntf bindingCallback;
	
	/**
	 * Creates an instance for the interface {@link TransportPreferenceIntf}
	 * @param bikePref
	 * @param busPref
	 * @param tramPref
	 * @param tubePref
	 * @param noisePref
	 * @param isTrackingCurrentPositionPref
	 * @param isSimulationModePref
	 * @return
	 */
	public static TransportPreferenceIntf createTransportPreference(boolean bikePref, boolean busPref, boolean tramPref, 
			boolean tubePref, boolean noisePref, boolean isTrackingCurrentPositionPref, boolean isSimulationModePref) {
		try {
			TransportPreferenceIntf pref = (TransportPreferenceIntf) Class.forName(
					TRANSPORT_PREF_IMPL_CLASS).newInstance();
			pref.initTransportReference(bikePref, busPref, tramPref, tubePref, noisePref,
					isTrackingCurrentPositionPref, isSimulationModePref);

			return pref;
		} catch (InstantiationException e) {
			if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
		} catch (IllegalAccessException e) {
			if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
		} catch (ClassNotFoundException e) {
			if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
		}
		return null;
	}

	public static PreferenceConfigurationIntf createPreferenceConfiguration(Context context) {
		try {
			PreferenceConfigurationIntf pc = (PreferenceConfigurationIntf) Class.forName(
					PREF_CONFIG_IMPL_CLASS).newInstance();
			pc.initParams(context, IntentConstants.PREFERENCE_DIR, IntentConstants.PREFERENCE_URL,
					IntentConstants.PREFERENCE_ENTITY, IbicoopDataConstants. DEBUG_MODE);

			return pc;
		} catch (InstantiationException e) {
			if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
		} catch (IllegalAccessException e) {
			if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
		} catch (ClassNotFoundException e) {
			if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
		}
		return null;
	}

	public static void createPreferenceCallbackBinder(final MainMapActivity mainMapActivity) {
		
		try {
			bindingCallback = (BindingCallbackIntf) Class.forName(
					BINDING_CALLBACK_IMPL_CLASS).newInstance();
			bindingCallback.initInterfaces(mainMapActivity, TSLApplicationContext.getInstance());
			bindingCallback.initMode(IbicoopDataConstants.DEBUG_MODE);
		} catch (InstantiationException e) {
			if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
		} catch (IllegalAccessException e) {
			if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
		} catch (ClassNotFoundException e) {
			if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
		}
	}


	public static void bind(PreferenceConfigurationIntf pc, Activity activity) {
		if (pc == null || activity == null || bindingCallback == null) return;
		pc.bind(activity, bindingCallback);
	}

	public static void unbind(PreferenceConfigurationIntf pc, Activity activity) {
		if (pc == null || activity == null || bindingCallback == null) return;
		pc.unbind(activity, bindingCallback);
	}

	private PreferenceWrapper() {
	}
}
