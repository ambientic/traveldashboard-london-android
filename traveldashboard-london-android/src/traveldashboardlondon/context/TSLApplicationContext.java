package traveldashboardlondon.context;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.ibicoop.broker.common.IbiTopic;
import org.inbicoop.preference.PreferenceConfigurationIntf;
import org.inbicoop.preference.TSLApplicationContextIntf;


import android.content.Context;

import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.data.LondonBikeDbManager;
import traveldashboard.data.LondonBusDbManager;
import traveldashboard.data.LondonMetroDbManager;
import traveldashboard.data.LondonMetroStopInfo;
import traveldashboard.data.ParisMetroDbManager;
import traveldashboard.data.ParisStopInfo;
import traveldashboard.ibicooptask.TSLContext;
import traveldashboard.data.TubeStation;


/**
 * TSLApplicationContext singleton context object.
 * <p>Stores session-related cached information</p>
 * @author kamilprzekwas
 *
 */
public class TSLApplicationContext implements TSLApplicationContextIntf, TSLContext {

	private static final TSLApplicationContext INSTANCE = new TSLApplicationContext();
	
	private Context context;
	
	private PreferenceConfigurationIntf preferenceConfiguration;
	
	private String emailAddress;
	
	private int versionCode;
	
	//Camera view city
	private String cameraViewCity;
	
	//True location city
	private String trueLocationCity;
	
	//Databases
	//Paris
	private ParisMetroDbManager parisMetroDbManager;
	
	//London
	private LondonMetroDbManager londonMetroDbManager;
	private LondonBikeDbManager londonBikeDbManager;
	private LondonBusDbManager londonBusDbManager;
	
	//Location
	//Camera location
	private double cameraLatitude;
	private double cameraLongitude;
	
	//True location
	private double trueLatitude;
	private double trueLongitude;
	
	//public static final String USER_EMAIL_ADDRESS = "username";	
		
	/**
	 * List of station topics to which we subscribed for receiving on-demand vote responses.
	 * Usually there should be only one topic.
	 */
	private ConcurrentHashMap<String, TubeStation> currentMetroStationTopicList = new ConcurrentHashMap<String, TubeStation>();
	

	/**
	 * Stores tokens for identifying own vote requests.
	 * This list is not persisted.
	 */
	public Set<String> pendingOnDemandRequests = Collections.newSetFromMap(new ConcurrentHashMap<String, Boolean>());
	
	/**
	 * List of region topics to which we have subscribed to receive on-demand vote requests.
	 * Usually there should be only one topic.
	 */
	public Set<IbiTopic> currentChannelTopicList = Collections.newSetFromMap(new ConcurrentHashMap<IbiTopic, Boolean>());
	
	
	
	/*
	 * Private Constructor to ensure that the application complies with the Singleton pattern
	 */
	private TSLApplicationContext() {
		cameraLatitude = 0.0;
		cameraLongitude = 0.0;
		trueLatitude = 0.0;
		trueLongitude = 0.0;
	}
	
	public static TSLApplicationContext getInstance() {
		return INSTANCE;
	}
	
	public void setContext(Context ctx) {
		context = ctx;
	}
	
	public Context getContext() {
		return context;
	}
	
	
	public Set<String> getPendingOnDemandRequests() {
		return pendingOnDemandRequests;
	}
	
	public void addPendingOnDemandRequests(String demandRequest) {
		pendingOnDemandRequests.add(demandRequest);
	}
	
	public void removePendingOnDemandRequests(String demandRequest) {
		pendingOnDemandRequests.remove(demandRequest);
	}
	
	public Set<IbiTopic> getCurrentChannelTopicList() {
		return currentChannelTopicList;
	}
	
	public void addChannelTopic(IbiTopic topic) {
		currentChannelTopicList.add(topic);
	}
	
	public void removeChannelTopic(IbiTopic topic) {
		currentChannelTopicList.remove(topic);
	}
	
	public ConcurrentHashMap<String, TubeStation> getCurrentMetroStationTopicList() {
		return currentMetroStationTopicList;
	}
	
	public void addTubeStation(String topic, TubeStation station) {
		currentMetroStationTopicList.put(topic, station);
	}
	
	public void removeTubeStation(String topic) {
		currentMetroStationTopicList.remove(topic);
	}
	
	public void setPreferenceConfiguration(PreferenceConfigurationIntf receivedPreferenceConfiguration) {
		preferenceConfiguration = receivedPreferenceConfiguration;
	}
	
	public PreferenceConfigurationIntf getPreferenceConfiguration() {
		return preferenceConfiguration;
	}
	
	public void setParisMetroDbManager(ParisMetroDbManager parisMetroDbManager) {
		this.parisMetroDbManager = parisMetroDbManager;
	}
	
	public ParisMetroDbManager getParisMetroDbManager() {
		return parisMetroDbManager;
	}
	
	public void setLondonBusDbManager(LondonBusDbManager londonBusDbManager) {
		this.londonBusDbManager = londonBusDbManager;
	}
	
	public LondonBusDbManager getLondonBusDbManager() {
		return londonBusDbManager;
	}
	
	public void setLondonMetroDbManager(LondonMetroDbManager londonMetroDbManager) {
		this.londonMetroDbManager = londonMetroDbManager;
	}
	
	public LondonMetroDbManager getLondonMetroDbManager() {
		return londonMetroDbManager;
	}
	
	public void setLondonBikeDbManager(LondonBikeDbManager londonBikeDbManager) {
		this.londonBikeDbManager = londonBikeDbManager;
	}
	
	public LondonBikeDbManager getLondonBikeDbManager() {
		return londonBikeDbManager;
	}

	public void setVersionCode(int version) {
		this.versionCode = version;
	}
	
	public int getVersionCode() {
		return this.versionCode;
	}
	
	public void setCameraViewCity(String city) {
		this.cameraViewCity = city;
	}
	
	public String getCameraViewCity() {
		return cameraViewCity;
	}
	
	public void setTrueLocationCity(String city) {
		this.trueLocationCity = city;
	}
	
	public String getTrueLocationCity() {
		return trueLocationCity;
	}
	
	public void setCameraLatitude(double cameraLat) {
		cameraLatitude = cameraLat;
	}
	
	public double getCameraLatitude() {
		return cameraLatitude;
	}
	
	public void setCameraLongitude(double cameraLon) {
		cameraLongitude = cameraLon;
	}
	
	public double getCameraLongitude() {
		return cameraLongitude;
	}
	
	public void setTrueLatitude(double trueLat) {
		trueLatitude = trueLat;
	}
	
	public double getTrueLatitude() {
		return trueLatitude;
	}
	
	public void setTrueLongitude(double trueLon) {
		trueLongitude = trueLon;
	}
	
	public double getTrueLongitude() {
		return trueLongitude;
	}
	
	/**
	 * This is for presentation purposes only and should not be sent to the outside world
	 * @return emailAddress
	 */
	public String currentEmailAddress() {
		return this.emailAddress;
	}
	
	public boolean checkIfUserIsLoggedIn() {
		return (this.emailAddress == null) ? false : true;
	}
	
	public void injectEmailAddressOnceUserLoggedIn(String email_) {
		this.emailAddress = email_;
	}
	
	public String currentSHA1EmailAddress() {
		return toSHA1(this.emailAddress.getBytes());
	}
	
	private static String toSHA1(byte[] convertme) {
	    MessageDigest md = null;
	    try {
	        md = MessageDigest.getInstance("SHA-1");
	    }
	    catch(NoSuchAlgorithmException e) {
	        e.printStackTrace();
	    } 
	    return byteArrayToHexString(md.digest(convertme));
	}
	
	private static String byteArrayToHexString(byte[] b) {
		  String result = "";
		  for (int i=0; i < b.length; i++) {
		    result +=
		          Integer.toString( ( b[i] & 0xff ) + 0x100, 16).substring( 1 );
		  }
		  return result;
	}
}
