package traveldashboardlondon.context;

public class CameraLocationContext {
	
	private static CameraLocationContext singleton;
	private double _latitude;
	private double _longitude;
	private float _zoomFactor;
	
	private CameraLocationContext() {
		_latitude = 0.0;
		_longitude = 0.0;
		_zoomFactor = 0f; // Default zooming factor
	}
	
	public static CameraLocationContext getInstance() {
		if (singleton == null) singleton = new CameraLocationContext();
		return singleton;
	}
	
	public void saveLatitude(double paramLatitude) {
		_latitude = paramLatitude;
	}

	public void saveLongitude(double paramLongitude) {
		_longitude = paramLongitude;
	}
	
	public void saveZoomFactor(float paramZoomFactor) {
		_zoomFactor = paramZoomFactor;
	}
	public double getLatitude() {
		return _latitude;
	}
	
	public double getLongitude() {
		return _longitude;
	}
	
	public float getZoomFactor() {
		return _zoomFactor;
	}
}
