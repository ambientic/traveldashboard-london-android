package traveldashboardlondon.entry;

public class CommentEntry {
	
	private String comment; 
	private String userName;
	private String type;
	private long timestamp;
	
	public CommentEntry(String comment, String type, long timestamp){
		this.comment = comment;
		this.type = type;
		this.timestamp = timestamp;
	}
	
	public void setUserName(String userName){
		this.userName = userName;
	}
	
	public String getUserID(){
		return this.userName;
	}
	
	public String getComment(){
		return this.comment;
	}
	
	public String getType(){
		return this.type;
	}
	
	public long getTimestamp() {
		return this.timestamp;
	}

}
