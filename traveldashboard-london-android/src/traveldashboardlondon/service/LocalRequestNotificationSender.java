package traveldashboardlondon.service;

import java.util.Random;

import com.google.gson.Gson;

import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.ibicooptask.IbicoopRegionChannelSubscriptionTask;
import traveldashboard.utils.IntentConstants;
import traveldashboardlondon.app.QuickReportCrowdActivity;
import traveldashboardlondon.app.R;
import traveldashboardlondon.context.TSLApplicationContext;
import traveldashboard.data.TubeStation;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

public class LocalRequestNotificationSender extends IntentService {

	private static final boolean USE_GLOBAL_CHANNEL = false;
	
	public LocalRequestNotificationSender() {
		super("BaS_notifier");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		
		String metroDesc = intent.getExtras().getString(
				IntentConstants.TUBE_STATION_GSON);		
		
		Gson gson = new Gson();
		TubeStation metroStop = gson.fromJson(metroDesc, TubeStation.class);
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("metroDesc = " + metroDesc);
		
		//Check if the we are in the real station
		if (!USE_GLOBAL_CHANNEL) {
			
			if (IbicoopDataConstants.SIMULATION_MODE) {
					
				double cameraLatitude = TSLApplicationContext.getInstance().getCameraLatitude();
				double cameraLongitude = TSLApplicationContext.getInstance().getCameraLongitude();
				
				float[] distanceResults = new float[3];
				Location.distanceBetween(cameraLatitude, cameraLongitude,
						metroStop.getLatitude(), metroStop.getLongitude(), distanceResults);
				
				float computedDistance = distanceResults[0];
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Simulation: computed distance = " + computedDistance);
				
				//Simulation location
				if (computedDistance > (IbicoopDataConstants.MONITORING_RADIUS * 2)) {
					//Not in the region
					if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Simulation: not in the region!");
					return;
				}
				
				String regionOfDevice = IbicoopRegionChannelSubscriptionTask.convertLocationToChannelName(cameraLatitude, cameraLongitude);
				String regionOfStation = IbicoopRegionChannelSubscriptionTask.convertLocationToChannelName(metroStop.getLatitude(), metroStop.getLongitude());
				
				if (!regionOfDevice.equals(regionOfStation)) {
					if (IbicoopDataConstants.DEBUG_MODE) System.err.println("Simulation: IGNORING demand! StationRegion = " + regionOfStation + " != DeviceRegion = " + regionOfDevice);
					return;
				}		
				
			} else {
				//True location
				
				double currentLatitude = 0.0;
				double currentLongitude = 0.0;				
				
				if (TSLApplicationContext.getInstance().getTrueLatitude() != 0.0 && TSLApplicationContext.getInstance().getTrueLongitude() != 0.0) {
					//We have application true location
					currentLatitude = TSLApplicationContext.getInstance().getTrueLatitude();
					currentLongitude = TSLApplicationContext.getInstance().getTrueLongitude();
				} else {
					//We don't have application true location
					LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
					Location loc = lm
							.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
					
					if (loc == null) {
						if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IGNORING demand! Location not available.");
						return;
					}
					
					long delay = System.currentTimeMillis() - loc.getTime();

					// location is too old or to inaccurate to receive this request
					if (loc.getAccuracy() > IbicoopDataConstants.MIN_ACCURACY_RADIUS_METERS
							|| delay > IbicoopDataConstants.MAX_FIX_DELAY_MILLIS) {
						if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IGNORING demand! Current location is either too old or too inaccurate.");
						return;
					}
					
					currentLatitude = loc.getLatitude();
					currentLongitude = loc.getLongitude();
				}
							
				if (currentLatitude == 0.0) return;
				if (currentLongitude == 0.0) return;
				
				float[] distanceResults = new float[3];
				Location.distanceBetween(currentLatitude, currentLongitude,
						metroStop.getLatitude(), metroStop.getLongitude(), distanceResults);
				
				float computedDistance = distanceResults[0];
				
				if (computedDistance > (IbicoopDataConstants.MONITORING_RADIUS * 2)) {
					//Not in the region
					if (IbicoopDataConstants.DEBUG_MODE) System.err.println("True location not in region");
					return;
				}
				
				String regionOfDevice = IbicoopRegionChannelSubscriptionTask.convertLocationToChannelName(currentLatitude, currentLongitude);
				String regionOfStation = IbicoopRegionChannelSubscriptionTask.convertLocationToChannelName(metroStop.getLatitude(), metroStop.getLongitude());
				
				if (!regionOfDevice.equals(regionOfStation)) {
					if (IbicoopDataConstants.DEBUG_MODE) System.err.println("IGNORING demand! StationRegion = " + regionOfStation + " != DeviceRegion = " + regionOfDevice);
					return;
				}				
			}
		}
		
		buildAndSendNotification(metroStop, metroDesc);
		
	}

	private void buildAndSendNotification(TubeStation metroStop, String metroDesc) {
		
		NotificationCompat.Builder builder = new NotificationCompat.Builder(
				this)
				.setSmallIcon(R.drawable.ic_launcher_notification)
				.setContentTitle(metroStop.getName() + " notification")
				.setContentText("We need your help to share crowd data, thank you")
				;

		Intent notificationIntent = new Intent(this, QuickReportCrowdActivity.class);
		notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
		notificationIntent.putExtra(IntentConstants.TUBE_STATION_GSON, metroDesc);		

		int random = new Random(System.currentTimeMillis()).nextInt();

		PendingIntent contentIntent = PendingIntent.getActivity(this, random,
				notificationIntent, 0);
		builder.setContentIntent(contentIntent);
		builder.setAutoCancel(true);
		builder.setLights(Color.BLUE, 500, 500);
		long[] pattern = { 500, 500, 500, 500 };
		builder.setVibrate(pattern);
		// builder.setStyle(new NotificationCompat.InboxStyle());
		// Add as notification
		NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		Uri alarmSound = RingtoneManager
				.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		builder.setSound(alarmSound);
		manager.notify(random, builder.build());
	}

}
