package traveldashboardlondon.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;

import traveldashboardlondon.app.R;

/**
 * Create notification
 * @author khoo
 *
 */
public class NotificationCreator {

	/**
	 * Create notification
	 * @param context
	 * @param pendingIntent
	 * @param when
	 * @return
	 */
	public static Notification createNotification(
			int id,
			Context context,
			String contentTitle,
			String contentText,
			PendingIntent pendingIntent, 
			long when) {

		NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
		builder.setSmallIcon(R.drawable.ic_launcher_notification);
		builder.setWhen(when);
		builder.setAutoCancel(true);
		builder.setLights(Color.WHITE, 1500, 1500);
		builder.setContentTitle(contentTitle);
		builder.setContentText(contentText);
		builder.setContentIntent(pendingIntent);
		builder.setTicker(contentTitle);
		builder.setDefaults(Notification.DEFAULT_VIBRATE);
		builder.setDefaults(Notification.DEFAULT_LIGHTS);
		builder.setDefaults(Notification.DEFAULT_SOUND);				
		
		return builder.build();
	}
}
