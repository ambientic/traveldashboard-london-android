package traveldashboardlondon.service;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.data.LondonBikeDbManager;
import traveldashboardlondon.context.TSLApplicationContext;

import android.app.IntentService;
import android.content.Intent;

public class UpdateBikeService extends IntentService {

	public static final String SERVICE_NAME = "UpdateBikeService";
	private static final String URL = "http://www.tfl.gov.uk/tfl/syndication/feeds/cycle-hire/livecyclehireupdates.xml";	
	
	private static final int CORE_POOL_SIZE = 10;
	private static final long UPDATE_TIME_MS = 1000*60*10;//10 minutes
		
	private ScheduledThreadPoolExecutor threadExecutor;
	private LondonBikeDbManager dbManager;

	public UpdateBikeService() {
		super(SERVICE_NAME);
		threadExecutor = new ScheduledThreadPoolExecutor(CORE_POOL_SIZE);
	}

	// Will be called asynchronously be Android
	@Override
	protected void onHandleIntent(Intent receivedIntent) {
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("UpdateBikeService: receive intent");
		dbManager = TSLApplicationContext.getInstance().getLondonBikeDbManager();
		if (threadExecutor != null) threadExecutor.scheduleAtFixedRate(new UpdateBikeThread(), 0, UPDATE_TIME_MS, TimeUnit.MILLISECONDS);		
	}
	
	public void stopTask() {
		if (threadExecutor != null) threadExecutor.shutdownNow();
		stopSelf();
	}
	
	private class UpdateBikeThread implements Runnable {
		@Override
		public void run() {
			//Feed from URI
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Update bike thread");
			try {
				HttpClient httpClient = new DefaultHttpClient();
				
				HttpGet httpGet = new HttpGet(URL);
				
				BasicResponseHandler responseHandler = new BasicResponseHandler();
			
				String xmlString = httpClient.execute(httpGet, responseHandler);
	
				dbManager.updateBikeInformation(xmlString);
			} catch(Exception e) {
				if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
			}
		}
	}
	
	@Override
	public void onDestroy() {
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Update bike service on destroy");
		if (threadExecutor != null) threadExecutor.shutdownNow();
		super.onDestroy();
	}
}
