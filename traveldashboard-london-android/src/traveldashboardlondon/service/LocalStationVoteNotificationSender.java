package traveldashboardlondon.service;

import java.util.Random;

import com.google.gson.Gson;

import traveldashboard.compat.AsyncTaskCompat;
import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.ibicooptask.IbicoopStationChannelSubscriptionTask;
import traveldashboard.utils.IntentConstants;
import traveldashboardlondon.app.QuickCrowdResponseActivity;
import traveldashboardlondon.app.R;
import traveldashboard.data.TubeStation;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

public class LocalStationVoteNotificationSender extends IntentService {

	public LocalStationVoteNotificationSender() {
		super("BaS_notifier");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		String metroDesc = intent.getExtras().getString(IntentConstants.TUBE_STATION_GSON);
		Gson gson = new Gson();
		TubeStation metroStop = gson.fromJson(metroDesc, TubeStation.class);
		int level = intent.getExtras().getInt(IbicoopDataConstants.OD_KEY_LEVEL);
		
		if (IbicoopDataConstants.DEBUG_MODE) {
			System.out.println("Receive station vote: metroDesc = " + metroDesc);
			System.out.println("Receive station vote: level = " + level);
		}
 		
		NotificationCompat.Builder builder = new NotificationCompat.Builder(
				this)
				.setSmallIcon(R.drawable.ic_launcher_notification)
				.setContentTitle(metroStop.getName() + " notification")
				.setContentText("Crowd level = " + level);

		Intent notificationIntent = new Intent(this, QuickCrowdResponseActivity.class);
		notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
		notificationIntent.putExtra(IntentConstants.TUBE_STATION_GSON, metroDesc);
		notificationIntent.putExtra(IbicoopDataConstants.OD_KEY_LEVEL, level);

		int random = new Random(System.currentTimeMillis()).nextInt();

		PendingIntent contentIntent = PendingIntent.getActivity(this, random,
				notificationIntent, 0);
		builder.setContentIntent(contentIntent);
		builder.setAutoCancel(true);
		builder.setLights(Color.BLUE, 500, 500);
		long[] pattern = { 500, 500, 500, 500 };
		builder.setVibrate(pattern);
		// builder.setStyle(new NotificationCompat.InboxStyle());
		// Add as notification
		NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		Uri alarmSound = RingtoneManager
				.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		builder.setSound(alarmSound);
		manager.notify(random, builder.build());
		
		unsubcribeStationChannel();
	}
	
	private void unsubcribeStationChannel() {
		//Unsubscribe all station channel
		IbicoopStationChannelSubscriptionTask stationChannelSubscriptionTask
			= new IbicoopStationChannelSubscriptionTask(this, null, true);
		AsyncTaskCompat.executeParallel(stationChannelSubscriptionTask);
	}

}
