package traveldashboardlondon.service;

import java.util.ArrayList;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.ibicoop.sensor.IbiSensor;

import traveldashboard.data.IbicoopDataConstants;

public class MockLocationSimulator {

	private static final int CORE_POOL_SIZE = 2;
	private static final ArrayList<MockLocationCoordinate> SIMU_LOCATIONS;
	private static final int SIMU_LOCATIONS_SIZE;
	
	static {
		SIMU_LOCATIONS = new ArrayList<MockLocationCoordinate>();
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Etoile", 48.87386, 2.29451));
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Kl�ber", 48.87143, 2.29327));
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Boissi�re", 48.86683, 2.29009));
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Trocad�ro", 48.86277, 2.28726));
		//Create long stop in trocad�ro to test noise data trigger timeout
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Trocad�ro", 48.86277, 2.28726));
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Trocad�ro", 48.86277, 2.28726));
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Trocad�ro", 48.86277, 2.28726));
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Passy", 48.85737, 2.28563));		
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Bir-Hakeim", 48.85393, 2.28928));
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Dupleix", 48.85043, 2.29348));
		SIMU_LOCATIONS.add(new MockLocationCoordinate("La Motte-Picquet Grenelle", 48.84902, 2.29795));
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Cambronne", 48.84749, 2.30279));
		SIMU_LOCATIONS.add(new MockLocationCoordinate("S�vres Lecourbe", 48.84554, 2.30940));
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Pasteur", 48.84277, 2.31254));
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Montparnasse-Bienvenue", 48.84294, 2.32305));
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Edgar Quinet", 48.84080, 2.32541));
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Raspail", 48.83885, 2.33073));
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Denfert-Rochereau", 48.83385, 2.33279));	
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Saint-Jacques", 48.83290, 2.33706));
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Glaci�re", 48.83095, 2.34363));
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Corvisart", 48.82975, 2.35058));
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Place d'Italie", 48.83140, 2.35560));
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Nationale", 48.83296, 2.36311));	
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Chevaleret", 48.83487, 2.36833));
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Quai de la Gare", 48.83713, 2.37305));
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Bercy", 48.84053, 2.37991));
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Dugommier", 48.83899, 2.39000));	
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Daumesnil", 48.83957, 2.39579));			
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Bel-Air", 48.84132, 2.40083));
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Picpus", 48.84506, 2.40122));
		SIMU_LOCATIONS.add(new MockLocationCoordinate("Nation", 48.84828, 2.39644));
		SIMU_LOCATIONS_SIZE = SIMU_LOCATIONS.size();
	}
	
	private ScheduledThreadPoolExecutor threadExecutor;
	private UpdateLocationThread updateLocationThread;
	private MockLocationProvider locationProvider;
	private IbiSensor locationSensor;
	private MockLocationUpdateCallback newLocationCallback;
	
	private int index = 0;
	private long updatePeriodMs = 0;
	private boolean increment = true;
	
	public MockLocationSimulator(IbiSensor ibiSensor, MockLocationProvider provider, long periodMs, MockLocationUpdateCallback callback) {
		locationSensor = ibiSensor;
		locationProvider = provider;
		updatePeriodMs = periodMs;
		newLocationCallback = callback;
		threadExecutor = new ScheduledThreadPoolExecutor(CORE_POOL_SIZE);
		updateLocationThread = new UpdateLocationThread();
	}
	
	public void startSimulation() {
		//Schedule thread
		threadExecutor.scheduleAtFixedRate(updateLocationThread, 0, updatePeriodMs, TimeUnit.MILLISECONDS);		
	}
	
	public void stopSimulation() {
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Stop simulation");
		threadExecutor.shutdownNow();
	}
	
	private class UpdateLocationThread implements Runnable {

		@Override
		public void run() {
			
			MockLocationCoordinate coordinate = SIMU_LOCATIONS.get(index);
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("UpdateLocationThread index = " + index + ", description : " + coordinate.toString());
			
			//Update new location
			locationProvider.pushLocation(locationSensor, coordinate.getLatitude(), coordinate.getLongitude());
			
			if (newLocationCallback != null) newLocationCallback.mockNewLocation(coordinate);
			
			if (increment) {
				index++;
			}
			else {
				index--;
			}
			
			//Change simulation direction
			if (index == -1) {
				increment = !increment;
				index++;
			} else if (index == SIMU_LOCATIONS_SIZE) {
				increment = !increment;
				index--;
			}
			
		}
		
	}
	
	public interface MockLocationUpdateCallback {
		public void mockNewLocation(MockLocationCoordinate coordinate);
	}
	
}
