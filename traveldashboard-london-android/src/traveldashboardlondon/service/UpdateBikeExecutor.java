package traveldashboardlondon.service;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.data.LondonBikeDbManager;
import traveldashboardlondon.context.TSLApplicationContext;

public class UpdateBikeExecutor {
	public static final String SERVICE_NAME = "UpdateBikeService";
	private static final String URL = "http://www.tfl.gov.uk/tfl/syndication/feeds/cycle-hire/livecyclehireupdates.xml";	
	
	private static final int CORE_POOL_SIZE = 10;
	private static final long UPDATE_TIME_MS = 1000*60*10;//10 minutes
		
	private ScheduledThreadPoolExecutor threadExecutor;
	private LondonBikeDbManager dbManager;

	public UpdateBikeExecutor() {
		threadExecutor = new ScheduledThreadPoolExecutor(CORE_POOL_SIZE);
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("UpdateBikeExecutor: new executor");
		dbManager = TSLApplicationContext.getInstance().getLondonBikeDbManager();
		threadExecutor.scheduleAtFixedRate(new UpdateBikeThread(), 0, UPDATE_TIME_MS, TimeUnit.MILLISECONDS);		
	}

	public void stopTask() {
		threadExecutor.shutdownNow();
	}
	
	private class UpdateBikeThread implements Runnable {
		@Override
		public void run() {
			//Feed from URI
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Update bike thread");
			try {
				HttpClient httpClient = new DefaultHttpClient();
				
				HttpGet httpGet = new HttpGet(URL);
				
				BasicResponseHandler responseHandler = new BasicResponseHandler();
			
				String xmlString = httpClient.execute(httpGet, responseHandler);
	
				dbManager.updateBikeInformation(xmlString);
			} catch(Exception e) {
				if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
			}	
		}
	}
}
