package traveldashboardlondon.service;

import com.google.gson.Gson;

import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.utils.IntentConstants;
import traveldashboardlondon.app.InfosDashboardActivity;
import traveldashboard.data.TransportArea;
import traveldashboard.data.TubeStation;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.support.v4.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;

public class MetroInfoNotificationService extends IntentService {

	public static final String SERVICE_NAME = "MetroInfoNotificationService";
	
	public MetroInfoNotificationService() {
		super(SERVICE_NAME);
	}

	// Will be called asynchronously be Android
	@Override
	protected void onHandleIntent(Intent receivedIntent) {
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("MetroInfoNotificationService: receive intent");
		
		//Get metro stop gson string
		String metroStopDesc = receivedIntent.getStringExtra(IntentConstants.TUBE_STATION_GSON);
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Metro stop desc = " + metroStopDesc);

		Gson gson = new Gson();
		TubeStation metroStop = gson.fromJson(metroStopDesc, TubeStation.class);

		//Launch notification
	    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	    
	    String city = TransportArea.getArea(metroStop.getLatitude(), metroStop.getLongitude());
		
	    int requestCode = 0;
		
		if (city.equals(IbicoopDataConstants.PARIS)) {
			requestCode = Integer.parseInt(metroStop.getId());				
		} else if (city.equals(IbicoopDataConstants.LONDON)) {
			requestCode = Integer.parseInt(metroStop.getLocationCode());
		}
		
		Intent intent = new Intent(this, InfosDashboardActivity.class);
		intent.putExtra(IntentConstants.TUBE_STATION_GSON, metroStopDesc);
		
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
		// Adds the back stack
		stackBuilder.addParentStack(InfosDashboardActivity.class);
		// Adds the intent to the top of the stack
		stackBuilder.addNextIntent(intent);
		
		//Gets a PendingIntent containg the entire back stack
		PendingIntent notifyIntent = stackBuilder.getPendingIntent(requestCode, PendingIntent.FLAG_UPDATE_CURRENT);
		
    	Notification notification = NotificationCreator.
    			createNotification(requestCode, 
    					this,
    					"TravelDashboard alert",
    					"Station " + metroStop.getName() + " info",
    					notifyIntent, 
    					System.currentTimeMillis());

    	notificationManager.notify(requestCode, notification);
	}
}
