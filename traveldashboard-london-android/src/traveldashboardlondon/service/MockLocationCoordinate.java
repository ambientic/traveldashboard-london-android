package traveldashboardlondon.service;

public class MockLocationCoordinate {

	private String placeName;
	private double latitude;
	private double longitude;
	
	public MockLocationCoordinate(String name, double lat, double lon) {
		placeName = name;
		latitude = lat;
		longitude = lon;
	}
	
	public String getName() {
		return placeName;
	}
	
	public double getLatitude() {
		return latitude;
	}
	
	public double getLongitude() {
		return longitude;
	}
	
	@Override
	public String toString() {
		return "Place name = " + placeName + ", latitude = " + latitude + ", longitude = " + longitude;
	}
}
