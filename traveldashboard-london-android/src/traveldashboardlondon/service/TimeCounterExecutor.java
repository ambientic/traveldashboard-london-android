package traveldashboardlondon.service;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import traveldashboard.data.IbicoopDataConstants;

public class TimeCounterExecutor {
	private static final int CORE_POOL_SIZE = 10;
		
	private ScheduledThreadPoolExecutor threadExecutor;
	private boolean firstTime;
	
	private TimeCounterCallback callback;
	
	public interface TimeCounterCallback {
		public void timeOut();
	}

	public TimeCounterExecutor(long time_counter_ms, TimeCounterCallback callback) {
		threadExecutor = new ScheduledThreadPoolExecutor(CORE_POOL_SIZE);
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("TimeCounterExecutor: new time counter executor");
		firstTime = true;
		this.callback = callback;
		threadExecutor.scheduleAtFixedRate(new TimeCounterThread(), 0, time_counter_ms, TimeUnit.MILLISECONDS);		
	}

	public void stopTask() {
		threadExecutor.shutdownNow();
	}
	
	private class TimeCounterThread implements Runnable {
		@Override
		public void run() {
			//Feed from URI
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Time counter thread!");
			
			if (firstTime) {
				//First time, do nothing
				firstTime = false;
			} else {
				//Callback
				if (callback != null) {
					callback.timeOut();
				}
			}
			
			try {
			} catch(Exception e) {
				if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
			}	
		}
	}
}

