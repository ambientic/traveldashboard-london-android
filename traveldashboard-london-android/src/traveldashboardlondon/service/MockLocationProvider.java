package traveldashboardlondon.service;

import org.ibicoop.sensor.IbiSensor;

import traveldashboard.data.IbicoopDataConstants;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Build;
import android.os.SystemClock;

/**
 * 
 * @author emil
 *
 */
public class MockLocationProvider {
    String providerName;
    Context ctx;

    public MockLocationProvider(String name, Context ctx) {
            this.providerName = name;
            this.ctx = ctx;

            LocationManager lm = (LocationManager) ctx
                            .getSystemService(Context.LOCATION_SERVICE);
            

            /*
            if (lm.getProvider(providerName) != null) {
            	//Remove the provider if already exist
            	shutdown();
            }*/
            
            lm.addTestProvider(providerName, false, false, false, false, true,
                            true, true, Criteria.POWER_LOW, Criteria.ACCURACY_FINE);
            lm.setTestProviderEnabled(providerName, true);
            lm.setTestProviderStatus(providerName, LocationProvider.AVAILABLE, null, System.currentTimeMillis());
    }

    public void pushLocation(IbiSensor locationSensor, double lat, double lon) {
            LocationManager lm = (LocationManager) ctx
                            .getSystemService(Context.LOCATION_SERVICE);

            Location mockLocation = new Location(providerName);
            mockLocation.setLatitude(lat);
            mockLocation.setLongitude(lon);
            mockLocation.setAltitude(0);
            mockLocation.setAccuracy(10);
            long time = System.currentTimeMillis();
            mockLocation.setTime(time);
    		if (IbicoopDataConstants.DEBUG_MODE) System.out.println(providerName + ": Push location : lat = " + lat + ", lon = " + lon + ", time = " + time);
           // lm.setTestProviderEnabled(providerName, true);
            //lm.setTestProviderStatus(providerName, LocationProvider.AVAILABLE, null, time);
            lm.setTestProviderLocation(providerName, mockLocation);
            //if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Request location updates");
            //lm.requestLocationUpdates(providerName, 0, 0, (IbiAndroidSensorLocation) locationSensor);
    }

    public void shutdown() {
            LocationManager lm = (LocationManager) ctx
                            .getSystemService(Context.LOCATION_SERVICE);
            lm.removeTestProvider(providerName);
    }
}