package traveldashboardlondon.utils;

import traveldashboard.data.happiness.Happiness;

public class HappinessWithResourceId extends Happiness {

	private int resId;

	public int getResId() {
		return resId;
	}

	public void setResId(int resId) {
		this.resId = resId;
	}
}
