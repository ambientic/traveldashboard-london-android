package traveldashboardlondon.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import traveldashboardlondon.preferences.PreferenceWrapper;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetManager;
import android.net.Uri;

public class ExternalApkInstall  {

	private static final String ANDROID_APPLICATION_TYPE = "application/vnd.android.package-archive";

	private ExternalApkInstall() {
	}
	
	/**
	 * Install Thales' application from assets
	 * @param activity
	 */
	public static void install(Activity activity) {
		// check whether Thales' dependencies exist
		Package pkg = Package.getPackage(PreferenceWrapper.PREF_THALES_PACKAGE_NAME);
		if (pkg == null) {
			return;
		}
		
		/*
		 * To install an application from an APK file:
		 * - Copy the file to Sdcard
		 * - Start activity with the type application/vnd.android.package-archive 
		 */
		
		String externalApkName = "OsgiDroid-rel1.0.apk";
		String outputPath = "/sdcard/" + externalApkName;

		// org.theresis.osgidroid belongs to Thales, check whether Thales' application installed
		if (checkPackageName(activity, "org.theresis.osgidroid")) {
			return;
		}

		AssetManager assetManager = activity.getAssets();

		if (copyToSdcard(assetManager, externalApkName, outputPath)) {
			Intent intent = new Intent(Intent.ACTION_VIEW);

			intent.setDataAndType(Uri.fromFile(new File(outputPath)),
					ANDROID_APPLICATION_TYPE);

			activity.startActivity(intent);
		}
	}

	/**
	 * Check whether or not the package name exists
	 * @param activity
	 * @param packageName
	 * @return
	 */
	private static boolean checkPackageName(Activity activity, String packageName) {
		android.content.pm.PackageManager mPm = activity.getPackageManager();
		PackageInfo info;
		try {
			info = mPm.getPackageInfo(packageName, 0);
			return info != null;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Copy an APK file from assets folder to SdCard
	 * @param assetManager
	 * @param externalApkName
	 * @param outputPath
	 * @return
	 */
	private static boolean copyToSdcard(AssetManager assetManager,
			String externalApkName, String outputPath) {
		try {
			InputStream in = assetManager.open(externalApkName);
			if (in == null) {
				return false;
			}
			OutputStream out = new FileOutputStream(outputPath);

			byte[] buffer = new byte[1024];

			int read;
			while((read = in.read(buffer)) != -1) {
				out.write(buffer, 0, read);
			}

			in.close();
			in = null;

			out.flush();
			out.close();
			out = null;
			return true;
		} catch (Exception e) {
		}
		return false;
	}
}
