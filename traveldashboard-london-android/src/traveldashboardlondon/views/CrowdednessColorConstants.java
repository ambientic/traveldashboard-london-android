package traveldashboardlondon.views;

import java.util.HashMap;

import traveldashboard.data.IbicoopDataConstants;

import android.graphics.Color;

public final class CrowdednessColorConstants {
	
	public static final HashMap<String, Integer> LEVEL_COLOR;
	
	static {
		LEVEL_COLOR = new HashMap<String, Integer>();
		LEVEL_COLOR.put(IbicoopDataConstants.EMPTY, Color.rgb(0, 255, 0));
		LEVEL_COLOR.put(IbicoopDataConstants.ALMOST_EMPTY, Color.rgb(0, 255, 0));
		LEVEL_COLOR.put(IbicoopDataConstants.NORMAL, Color.rgb(0, 255, 0));
		LEVEL_COLOR.put(IbicoopDataConstants.CROWDED, Color.rgb(255, 255, 0));
		LEVEL_COLOR.put(IbicoopDataConstants.VERY_CROWDED , Color.rgb(255, 140, 0));
		LEVEL_COLOR.put(IbicoopDataConstants.EXTREMELY_CROWDED, Color.rgb(238, 64, 0));	
	}
}
