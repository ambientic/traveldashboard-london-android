package traveldashboardlondon.views;

import java.util.List;

import traveldashboardlondon.app.R;
import traveldashboardlondon.utils.HappinessWithResourceId;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;

public class HappinessAdapter extends ArrayAdapter<HappinessWithResourceId> {

	private int resource;
	private HappinessWithResourceId selectedHappiness;

	public HappinessAdapter(Context context, int resource, List<HappinessWithResourceId> happinessList) {
		super(context, resource, happinessList);
		this.resource = resource;
	}

	@Override
	public View getView(final int position, View convertView, final ViewGroup parent) {
		HappinessWithResourceId hwri = getItem(position);

        LinearLayout happinessItemView;
         
        //Inflate the view
        if(convertView==null)
        {
        	happinessItemView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater vi;
            vi = (LayoutInflater)getContext().getSystemService(inflater);
            vi.inflate(resource, happinessItemView, true);
        }
        else
        {
        	happinessItemView = (LinearLayout) convertView;
        }

        final RadioButton radioButton = (RadioButton) happinessItemView.findViewById(R.id.happinessDescriptionRadio);
        radioButton.setText(hwri.getCrowdSourcedValue().getDescription());
        radioButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				clickOnItemsRelatedToRadio(radioButton, parent, position);
			}
		});

        ImageView iv = (ImageView) happinessItemView.findViewById(R.id.happinessDetailImgForPush);
        iv.setImageResource(hwri.getResId());

		return happinessItemView;
	}

	public HappinessWithResourceId getSelectedHappiness() {
		return selectedHappiness;
	}

	private void clickOnItemsRelatedToRadio(RadioButton selectedRb, ViewGroup parent, int position) {
		for (int i = 0; i < getCount(); i++) {
			View layoutView = parent.getChildAt(i);
			if (layoutView != null) {
				RadioButton rb = (RadioButton) layoutView.findViewById(R.id.happinessDescriptionRadio);
				rb.setChecked(false);
			}
		}
		selectedRb.setChecked(true);
		selectedHappiness = getItem(position);
	}

}
