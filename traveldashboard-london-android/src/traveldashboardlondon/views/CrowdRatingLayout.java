package traveldashboardlondon.views;

import traveldashboardlondon.app.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

@SuppressLint("NewApi")
public class CrowdRatingLayout extends LinearLayout {
	
	private Context context;
	private int rating = 0;
	
	private int[] imageViews = { R.id.currentImageview1, R.id.currentImageview2,
			R.id.currentImageview3, R.id.currentImageview4, R.id.currentImageview5 };
	
	private ImageView crowdOne, crowdTwo, crowdThree, crowdFour, crowdFive;
	
	public CrowdRatingLayout(Context context) {
		super(context);
		this.context = context;
		init();
	}
	
	public CrowdRatingLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		init();
	}
	
	public CrowdRatingLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
		init();
	}
	
	public void init() {
		CrowdRatingLayout.inflate(context, R.layout.custom_crowd_rating, this);
		//Initialize crowd layout
		CrowdHelper.setCrowdImages(this, imageViews, 0);
		
		crowdOne = (ImageView) findViewById(imageViews[0]);
		crowdTwo = (ImageView) findViewById(imageViews[1]);
		crowdThree = (ImageView) findViewById(imageViews[2]);
		crowdFour = (ImageView) findViewById(imageViews[3]);
		crowdFive = (ImageView) findViewById(imageViews[4]);
		
		crowdOne.setOnClickListener(crowdOneListener);
		crowdTwo.setOnClickListener(crowdTwoListener);
		crowdThree.setOnClickListener(crowdThreeListener);
		crowdFour.setOnClickListener(crowdFourListener);
		crowdFive.setOnClickListener(crowdFiveListener);
	}
	
	public int getRating() {
		return rating;
	}
	
	public void setClickable() {
		crowdOne.setClickable(true);
		crowdTwo.setClickable(true);
		crowdThree.setClickable(true);
		crowdFour.setClickable(true);
		crowdFive.setClickable(true);
	}
	
	public void setNonClickable() {
		crowdOne.setClickable(false);
		crowdTwo.setClickable(false);
		crowdThree.setClickable(false);
		crowdFour.setClickable(false);
		crowdFive.setClickable(false);
	}
	
	private View.OnClickListener crowdOneListener = new OnClickListener() {	
		@SuppressWarnings("deprecation")
		@Override
		public void onClick(View v) {
			crowdOne.setAlpha(255);
			crowdTwo.setAlpha(80);
			crowdThree.setAlpha(80);
			crowdFour.setAlpha(80);
			crowdFive.setAlpha(80);
			rating = 1;
		}
	};
	
	private View.OnClickListener crowdTwoListener = new OnClickListener() {	
		@SuppressWarnings("deprecation")
		@Override
		public void onClick(View v) {
			crowdOne.setAlpha(255);
			crowdTwo.setAlpha(255);
			crowdThree.setAlpha(80);
			crowdFour.setAlpha(80);
			crowdFive.setAlpha(80);
			rating = 2;
		}
	};
	
	private View.OnClickListener crowdThreeListener = new OnClickListener() {	
		@SuppressWarnings("deprecation")
		@Override
		public void onClick(View v) {
			crowdOne.setAlpha(255);
			crowdTwo.setAlpha(255);
			crowdThree.setAlpha(255);
			crowdFour.setAlpha(80);
			crowdFive.setAlpha(80);
			rating = 3;
		}
	};
	
	private View.OnClickListener crowdFourListener = new OnClickListener() {	
		@SuppressWarnings("deprecation")
		@Override
		public void onClick(View v) {
			crowdOne.setAlpha(255);
			crowdTwo.setAlpha(255);
			crowdThree.setAlpha(255);
			crowdFour.setAlpha(255);
			crowdFive.setAlpha(80);
			rating = 4;
		}
	};
	
	private View.OnClickListener crowdFiveListener = new OnClickListener() {	
		@SuppressWarnings("deprecation")
		@Override
		public void onClick(View v) {
			crowdOne.setAlpha(255);
			crowdTwo.setAlpha(255);
			crowdThree.setAlpha(255);
			crowdFour.setAlpha(255);
			crowdFive.setAlpha(255);
			rating = 5;
		}
	};
}
