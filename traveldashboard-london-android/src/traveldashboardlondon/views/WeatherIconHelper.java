package traveldashboardlondon.views;

import java.util.HashMap;

import traveldashboardlondon.app.R;

/**
 * Get weather icon by icon name
 * Reference of open weather map API:
 * http://bugs.openweathermap.org/projects/api/wiki/Weather_Condition_Codes 
 * @author khoo
 *
 */
public class WeatherIconHelper {
	
	//Weather icon map
	public static final HashMap<String, Integer> WEATHER_ICON_MAP;
	
	static {
		WEATHER_ICON_MAP = new HashMap<String, Integer>();
		
		//Sky is clear
		WEATHER_ICON_MAP.put("01d", R.drawable.weather_01d);
		WEATHER_ICON_MAP.put("01n", R.drawable.weather_01n);
		//Few clouds
		WEATHER_ICON_MAP.put("02d", R.drawable.weather_02d);
		WEATHER_ICON_MAP.put("02n", R.drawable.weather_02n);
		//Scattered clouds
		WEATHER_ICON_MAP.put("03d", R.drawable.weather_03d);
		WEATHER_ICON_MAP.put("03n", R.drawable.weather_03n);
		//Broken clouds
		WEATHER_ICON_MAP.put("04d", R.drawable.weather_04d);
		WEATHER_ICON_MAP.put("04n", R.drawable.weather_04n);
		//Shower rain
		WEATHER_ICON_MAP.put("09d", R.drawable.weather_09d);
		WEATHER_ICON_MAP.put("09n", R.drawable.weather_09n);
		//Rain
		WEATHER_ICON_MAP.put("10d", R.drawable.weather_10d);
		WEATHER_ICON_MAP.put("10n", R.drawable.weather_10n);
		//Thunderstorm
		WEATHER_ICON_MAP.put("11d", R.drawable.weather_11d);
		WEATHER_ICON_MAP.put("11n", R.drawable.weather_11n);
		//Snow
		WEATHER_ICON_MAP.put("13d", R.drawable.weather_13d);
		WEATHER_ICON_MAP.put("13n", R.drawable.weather_13n);
		//Mist
		WEATHER_ICON_MAP.put("50d", R.drawable.weather_50d);
		WEATHER_ICON_MAP.put("50n", R.drawable.weather_50n);		
	}
	
}
