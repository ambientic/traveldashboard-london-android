package traveldashboardlondon.views;

import java.util.ArrayList;
import java.util.List;

import traveldashboard.data.Station;
import traveldashboard.data.happiness.Happiness;
import traveldashboard.data.happiness.HappinessLevel;
import traveldashboardlondon.app.R;
import traveldashboardlondon.utils.HappinessWithResourceId;
import android.content.Context;
import android.widget.LinearLayout;
import android.widget.ListView;

public class HappinessPushLayout extends LinearLayout {

	private HappinessAdapter adapter;

	public HappinessPushLayout(Context context, Station station) {
		super(context);
		BusMarkerInfoLayout.inflate(context, R.layout.happiness_push, this);
		
		ListView lv = (ListView) findViewById(R.id.pushHappinessList);
		List<HappinessWithResourceId> list = new ArrayList<HappinessWithResourceId>();
		init(list, station);
		adapter = new HappinessAdapter(context, R.layout.happiness_detail, list);
		lv.setAdapter(adapter);
	}

	public Happiness getSelectedHappiness() {
		Happiness happiness = new Happiness();
		happiness.setCrowdSourcedValue(adapter.getSelectedHappiness().getCrowdSourcedValue());
		happiness.setHotSpot(adapter.getSelectedHappiness().getHotSpot());
		happiness.setTimeStamp(adapter.getSelectedHappiness().getTimeStamp());
		return happiness;
	}

	private void init(List<HappinessWithResourceId> list, Station station) {
		list.add(createHappiness(HappinessLevel.HAPPY, R.drawable.happy_icon, station));
		list.add(createHappiness(HappinessLevel.GLAD, R.drawable.glad_icon, station));
		list.add(createHappiness(HappinessLevel.OK, R.drawable.ok_icon, station));
		list.add(createHappiness(HappinessLevel.SAD, R.drawable.sad_icon, station));
		list.add(createHappiness(HappinessLevel.CRIED, R.drawable.cry_icon, station));
		
	}

	private HappinessWithResourceId createHappiness(HappinessLevel hl, int resId, Station station) {
		HappinessWithResourceId ret = new HappinessWithResourceId();
		ret.setHotSpot(station);
		ret.setResId(resId);
		ret.setTimeStamp(System.currentTimeMillis());
		ret.setCrowdSourcedValue(hl);
		return ret;
	}
}
