package traveldashboardlondon.views;

import java.util.List;
import java.util.Map;

import traveldashboard.data.IbicoopDataConstants;
import traveldashboardlondon.app.R;
import traveldashboard.data.BusStation;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

@SuppressLint("NewApi")
public class BusMarkerInfoLayout extends LinearLayout {
	
	private Context context;
	private String stopName = "";
	private TextView tv;
	private ImageView iv;
	private ListView lv;
	
	private String[] from = {
			IbicoopDataConstants.PARAM_KEY_ROUTE_NAME,
			IbicoopDataConstants.DESTINATION,
			IbicoopDataConstants.TIME
			};
	
	private int[] to = {R.id.tv1, R.id.tv2, R.id.tv3};
	private SimpleAdapter simpleAdapter;
	private List<Map<String, String>> info;
	private BusStation station;
	
	public BusMarkerInfoLayout(Context context) {
		super(context);
		this.context = context;
		init();
	}
	
	public BusMarkerInfoLayout(Context context, BusStation station, List<Map<String, String>> info) {
		super(context);
		this.context = context;
		this.stopName = station.getName();
		this.station = station;
		this.info = info;
		initWithRequiredInfo();
	}
	
	public BusMarkerInfoLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		init();
	}
	
	public BusMarkerInfoLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
		init();
	}
	
	
	public void initWithRequiredInfo() {
		BusMarkerInfoLayout.inflate(context, R.layout.custom_bus_marker_info,this);
		lv = (ListView) findViewById(R.id.busList);
		tv = (TextView) findViewById(R.id.busStopName);
		iv = (ImageView) findViewById(R.id.busMarkerLayoutIcon);
		iv.setImageResource(ImageResourceHelper.getBusStationImageResource(station, ImageResourceHelper.MARKER_INFO));
		tv.setText(stopName);
		simpleAdapter = new SimpleAdapter(context, info, R.layout.custom_marker_info_list_view, from, to);
		lv.setAdapter(simpleAdapter);
	}
	
	public void init() {
		BusMarkerInfoLayout.inflate(context, R.layout.custom_bus_marker_info,this);
	}
	
	
}
