package traveldashboardlondon.views;

import traveldashboardlondon.app.R;
import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;

@SuppressLint("NewApi")
public class HeatRatingLayout extends LinearLayout {
	
	private Context context;
	private int rating = 0;
	
	private int[] imageViews = { R.id.currentHeat1, R.id.currentHeat2,
			R.id.currentHeat3, R.id.currentHeat4, R.id.currentHeat5 };
	
	private ImageView heatOne, heatTwo, heatThree, heatFour, heatFive;
	
	public HeatRatingLayout(Context context) {
		super(context);
		this.context = context;
		init();
	}
	
	public HeatRatingLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		init();
	}
	
	public HeatRatingLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
		init();
	}
	
	public void init() {
		CrowdRatingLayout.inflate(context, R.layout.custom_heat_rating, this);
		//Initialize crowd layout
		CrowdHelper.setCrowdImages(this, imageViews, 0);
		
		heatOne = (ImageView) findViewById(imageViews[0]);
		heatTwo = (ImageView) findViewById(imageViews[1]);
		heatThree = (ImageView) findViewById(imageViews[2]);
		heatFour = (ImageView) findViewById(imageViews[3]);
		heatFive = (ImageView) findViewById(imageViews[4]);
		
		heatOne.setOnClickListener(heatOneListener);
		heatTwo.setOnClickListener(heatTwoListener);
		heatThree.setOnClickListener(heatThreeListener);
		heatFour.setOnClickListener(heatFourListener);
		heatFive.setOnClickListener(heatFiveListener);
	}
	
	public int getRating() {
		return rating;
	}
	
	public void setClickable() {
		heatOne.setClickable(true);
		heatTwo.setClickable(true);
		heatThree.setClickable(true);
		heatFour.setClickable(true);
		heatFive.setClickable(true);
	}
	
	public void setNonClickable() {
		heatOne.setClickable(false);
		heatTwo.setClickable(false);
		heatThree.setClickable(false);
		heatFour.setClickable(false);
		heatFive.setClickable(false);
	}
	
	private View.OnClickListener heatOneListener = new OnClickListener() {	
		@SuppressWarnings("deprecation")
		@Override
		public void onClick(View v) {
			heatOne.setAlpha(255);
			heatTwo.setAlpha(80);
			heatThree.setAlpha(80);
			heatFour.setAlpha(80);
			heatFive.setAlpha(80);
			rating = 1;
		}
	};
	
	private View.OnClickListener heatTwoListener = new OnClickListener() {	
		@SuppressWarnings("deprecation")
		@Override
		public void onClick(View v) {
			heatOne.setAlpha(255);
			heatTwo.setAlpha(255);
			heatThree.setAlpha(80);
			heatFour.setAlpha(80);
			heatFive.setAlpha(80);
			rating = 2;
		}
	};
	
	private View.OnClickListener heatThreeListener = new OnClickListener() {	
		@SuppressWarnings("deprecation")
		@Override
		public void onClick(View v) {
			heatOne.setAlpha(255);
			heatTwo.setAlpha(255);
			heatThree.setAlpha(255);
			heatFour.setAlpha(80);
			heatFive.setAlpha(80);
			rating = 3;
		}
	};
	
	private View.OnClickListener heatFourListener = new OnClickListener() {	
		@SuppressWarnings("deprecation")
		@Override
		public void onClick(View v) {
			heatOne.setAlpha(255);
			heatTwo.setAlpha(255);
			heatThree.setAlpha(255);
			heatFour.setAlpha(255);
			heatFive.setAlpha(80);
			rating = 4;
		}
	};
	
	private View.OnClickListener heatFiveListener = new OnClickListener() {	
		@SuppressWarnings("deprecation")
		@Override
		public void onClick(View v) {
			heatOne.setAlpha(255);
			heatTwo.setAlpha(255);
			heatThree.setAlpha(255);
			heatFour.setAlpha(255);
			heatFive.setAlpha(255);
			rating = 5;
		}
	};
}