package traveldashboardlondon.views;

import traveldashboardlondon.app.R;
import android.app.Activity;
import android.view.View;
import android.widget.ImageView;

public class CrowdHelper {

	
	public static void setNewCrowdImagesByImageViews(Activity activity, ImageView[] imageViews, int level) {
		
		for (int n = 0; n < level; n++) {
			ImageView imageview = imageViews[n];
			imageview.setImageResource(R.drawable.icon_crowd_white);
			imageview.setVisibility(View.VISIBLE);
		}
		
		for (int n = level; n < 5; n++) {
			ImageView imageview = imageViews[n];
			imageview.setImageResource(R.drawable.icon_crowd_white_transparent);
			imageview.setVisibility(View.VISIBLE);
		}
	}
	
	public static void setNewCrowdImagesByResourceIds(Activity activity, int[] imageViews, int level) {

		for (int n = 0; n < level; n++) {
			ImageView imageview = (ImageView) activity.findViewById(imageViews[n]);
			imageview.setImageResource(R.drawable.icon_crowd_white);
			imageview.setVisibility(View.VISIBLE);
		}
		
		for (int n = level; n < 5; n++) {
			ImageView imageview = (ImageView) activity.findViewById(imageViews[n]);
			imageview.setImageResource(R.drawable.icon_crowd_white_transparent);
			imageview.setVisibility(View.VISIBLE);
		}
	}
	
	@SuppressWarnings("deprecation")
	public static void setCrowdImages(Activity activity, int[] imageViews, int level) {

		for (int n = 0; n < level; n++) {
			ImageView imageview = (ImageView) activity.findViewById(imageViews[n]);
			imageview.setAlpha(255);
			imageview.setVisibility(View.VISIBLE);
		}
		
		for (int n = level; n < 5; n++) {
			ImageView imageview = (ImageView) activity.findViewById(imageViews[n]);
			imageview.setAlpha(80);
			imageview.setVisibility(View.VISIBLE);
		}
	}
	
	@SuppressWarnings("deprecation")
	public static void setCrowdImages(View view, int[] imageViews, int level) {

		for (int n = 0; n < level; n++) {
			ImageView imageview = (ImageView) view.findViewById(imageViews[n]);
			imageview.setAlpha(255);
			imageview.setVisibility(View.VISIBLE);
		}
		
		for (int n = level; n < 5; n++) {
			ImageView imageview = (ImageView) view.findViewById(imageViews[n]);
			imageview.setAlpha(80);
			imageview.setVisibility(View.VISIBLE);
		}
	}
	
}
