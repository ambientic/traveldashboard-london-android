package traveldashboardlondon.views;

import java.util.HashMap;

import traveldashboard.data.IbicoopDataConstants;


/**
 * Background resource for every line 
 * @author khoo
 *
 */
public final class TubeLineUiConstants {

	public static final HashMap<String, Integer> BACKGROUND_RESOURCE;
	
	static {
		BACKGROUND_RESOURCE = new HashMap<String, Integer>();
		BACKGROUND_RESOURCE.put(IbicoopDataConstants.BAKERLOO_LINE, traveldashboardlondon.app.R.drawable.gradient_bakerloo);
		BACKGROUND_RESOURCE.put(IbicoopDataConstants.CENTRAL_LINE, traveldashboardlondon.app.R.drawable.gradient_central);
		BACKGROUND_RESOURCE.put(IbicoopDataConstants.CIRCLE_LINE, traveldashboardlondon.app.R.drawable.gradient_circle);
		BACKGROUND_RESOURCE.put(IbicoopDataConstants.DISTRICT_LINE,traveldashboardlondon.app.R.drawable.gradient_district);
		BACKGROUND_RESOURCE.put(IbicoopDataConstants.DLR_LINE,traveldashboardlondon.app.R.drawable.gradient_dlr);		
		BACKGROUND_RESOURCE.put(IbicoopDataConstants.HAMMERSMITH_CIRCLE,traveldashboardlondon.app.R.drawable.gradient_hammersmith_circle);
		BACKGROUND_RESOURCE.put(IbicoopDataConstants.HAMMERSMITH_CITY_LINE,traveldashboardlondon.app.R.drawable.gradient_hammersmith);	
		BACKGROUND_RESOURCE.put(IbicoopDataConstants.JUBILEE_LINE,traveldashboardlondon.app.R.drawable.gradient_jubilee);		
		BACKGROUND_RESOURCE.put(IbicoopDataConstants.METROPOLITAN_LINE,traveldashboardlondon.app.R.drawable.gradient_metropolitan);	
		BACKGROUND_RESOURCE.put(IbicoopDataConstants.NORTHERN_LINE,traveldashboardlondon.app.R.drawable.gradient_northern);
		BACKGROUND_RESOURCE.put(IbicoopDataConstants.OVERGROUND_LINE,traveldashboardlondon.app.R.drawable.gradient_overground);
		BACKGROUND_RESOURCE.put(IbicoopDataConstants.PICCADILLY_LINE,traveldashboardlondon.app.R.drawable.gradient_piccadilly);	
		BACKGROUND_RESOURCE.put(IbicoopDataConstants.VICTORIA_LINE,traveldashboardlondon.app.R.drawable.gradient_victoria);
		BACKGROUND_RESOURCE.put(IbicoopDataConstants.WATERLOO_CITY_LINE,traveldashboardlondon.app.R.drawable.gradient_waterloo);
	}
	
}
