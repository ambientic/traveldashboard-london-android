package traveldashboardlondon.views;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import traveldashboard.data.IbicoopDataConstants;
import traveldashboardlondon.app.R;
import traveldashboard.data.TubeStation;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

@SuppressLint("NewApi")
public class TubeMarkerInfoLayout extends LinearLayout {

	private Context context;
	
	private SimpleAdapter simpleAdapter;
	private ListView lv;
	private TextView tv;
	private ImageView stationIcon;
	private int[] _imageViews = {R.id.img1, R.id.img2,
			R.id.img3, R.id.img4, R.id.img5};
	private String[] from = {IbicoopDataConstants.PARAM_KEY_ROUTE_NAME, IbicoopDataConstants.DESTINATION, IbicoopDataConstants.TIME};
	private int[] to = {R.id.tv1, R.id.tv2, R.id.tv3};
	
	private String currentTubeName = "";	
	private List<Map<String, String>> tubeCountdownInfo;
	private int crowdLevel;
	private TubeStation tubeStation;
	
	public TubeMarkerInfoLayout(Context context) {
		super(context);
		this.context = context;
		init();
	}
	
	public TubeMarkerInfoLayout(Context context, TubeStation station, List<Map<String, String>> tubeCountdownInfo, int crowdLevel) {
		super(context);
		this.context = context;
		this.tubeStation = station;
		this.currentTubeName = station.getName();
		this.tubeCountdownInfo = new ArrayList<Map<String,String>>();
		if (tubeCountdownInfo != null) {this.tubeCountdownInfo.addAll(tubeCountdownInfo);}
		this.crowdLevel = crowdLevel;
		initWithRequiredInformation();
	}
	
	public TubeMarkerInfoLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		init();
	}
	
	public TubeMarkerInfoLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
		init();
	}
	
	public void initWithRequiredInformation() {
		TubeMarkerInfoLayout.inflate(context, R.layout.custom_tube_marker_info,this);
		lv = (ListView) findViewById(R.id.list);
		tv = (TextView) findViewById(R.id.stationName);
		stationIcon = (ImageView) findViewById(R.id.infoStationIcon);
		stationIcon.setImageResource(ImageResourceHelper.getTubeStationImageResource(tubeStation, ImageResourceHelper.MARKER_INFO));
		/*
        if (TransportArea.getArea(tubeStation.getLatitude(), tubeStation.getLongitude()).equals(TransportArea.PARIS)) {
        	//In Paris
        	
			boolean hasRER = false;
			Tube[] tubes = tubeStation.getTubes();
			
			for (int i = 0; i < tubes.length; i++) {
				Tube tube = tubes[i];
				if (tube.getId().equals("RA") || tube.getId().equals("RB")) {
					hasRER = true;
					break;
				}
			}
			
			if (!hasRER) stationIcon.setImageResource(R.drawable.metro_small_icon);
			else stationIcon.setImageResource(R.drawable.rer_small_icon);
        } else {
        	//In London
			stationIcon.setImageResource(R.drawable.tube_icon_coloured);
        }*/
        
		tv.setText(currentTubeName);
		CrowdHelper.setCrowdImages(
				TubeMarkerInfoLayout.this, _imageViews, crowdLevel);
		
		System.out.println("Tube station = " + tubeStation.toString());
		System.out.println("Tube countdown info = " + tubeCountdownInfo.toString());
		
		simpleAdapter = new SimpleAdapter(context, tubeCountdownInfo, R.layout.custom_marker_info_list_view, from, to);
		lv.setAdapter(simpleAdapter);
	}
	
	public void init() {
		TubeMarkerInfoLayout.inflate(context, R.layout.custom_tube_marker_info,this);
	}

	
	
}
