package traveldashboardlondon.views;

import traveldashboardlondon.app.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

@SuppressLint("NewApi")
public class CrowdRatingSimplifiedLayout extends LinearLayout {
	
	private Context context;
	private int rating = 1;
	
	private int[] imageViews = { R.id.crowd1, R.id.crowd2,
			R.id.crowd3, R.id.crowd4, R.id.crowd5 };
	
	private ImageView crowdOne, crowdTwo, crowdThree, crowdFour, crowdFive;
	
	private TextView nbCrowd;
	
	public CrowdRatingSimplifiedLayout(Context context) {
		super(context);
		this.context = context;
		init();
	}
	
	public CrowdRatingSimplifiedLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		init();
	}
	
	public CrowdRatingSimplifiedLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
		init();
	}
	
	public void init() {
		CrowdRatingLayout.inflate(context, R.layout.custom_crowd_rating2, this);
		//Initialize crowd layout		
		crowdOne = (ImageView) findViewById(imageViews[0]);
		crowdTwo = (ImageView) findViewById(imageViews[1]);
		crowdThree = (ImageView) findViewById(imageViews[2]);
		crowdFour = (ImageView) findViewById(imageViews[3]);
		crowdFive = (ImageView) findViewById(imageViews[4]);
		
		//Default initial crowd level = 1
		
		nbCrowd = (TextView) findViewById(R.id.nbCrowd);
		nbCrowd.setText("1/5");
		//crowdOne.setOnClickListener(crowdOneListener);
		crowdOne.setImageResource(R.drawable.icon_crowd_white);		
		crowdTwo.setOnClickListener(crowdTwoListener);
		crowdThree.setOnClickListener(crowdThreeListener);
		crowdFour.setOnClickListener(crowdFourListener);
		crowdFive.setOnClickListener(crowdFiveListener);
	}
	
	public int getRating() {
		return rating;
	}
	
	public void setClickable() {
		crowdOne.setClickable(true);
		crowdTwo.setClickable(true);
		crowdThree.setClickable(true);
		crowdFour.setClickable(true);
		crowdFive.setClickable(true);
	}
	
	public void setNonClickable() {
		crowdOne.setClickable(false);
		crowdTwo.setClickable(false);
		crowdThree.setClickable(false);
		crowdFour.setClickable(false);
		crowdFive.setClickable(false);
	}
	
	//Default: initial crowd level = 1
	/*
	private View.OnClickListener crowdOneListener = new OnClickListener() {	
		@Override
		public void onClick(View v) {
			crowdOne.setImageResource(R.drawable.icon_crowd_white);
			crowdTwo.setImageResource(R.drawable.icon_crowd_white_transparent);
			crowdThree.setImageResource(R.drawable.icon_crowd_white_transparent);
			crowdFour.setImageResource(R.drawable.icon_crowd_white_transparent);
			crowdFive.setImageResource(R.drawable.icon_crowd_white_transparent);
			nbCrowd.setText("1/5");
			rating = 1;
		}
	};*/
	
	private View.OnClickListener crowdTwoListener = new OnClickListener() {	
		@Override
		public void onClick(View v) {
			crowdOne.setImageResource(R.drawable.icon_crowd_white);
			crowdTwo.setImageResource(R.drawable.icon_crowd_white);
			crowdThree.setImageResource(R.drawable.icon_crowd_white_transparent);
			crowdFour.setImageResource(R.drawable.icon_crowd_white_transparent);
			crowdFive.setImageResource(R.drawable.icon_crowd_white_transparent);
			nbCrowd.setText("2/5");
			rating = 2;
		}
	};
	
	private View.OnClickListener crowdThreeListener = new OnClickListener() {	
		@Override
		public void onClick(View v) {
			crowdOne.setImageResource(R.drawable.icon_crowd_white);
			crowdTwo.setImageResource(R.drawable.icon_crowd_white);
			crowdThree.setImageResource(R.drawable.icon_crowd_white);
			crowdFour.setImageResource(R.drawable.icon_crowd_white_transparent);
			crowdFive.setImageResource(R.drawable.icon_crowd_white_transparent);
			nbCrowd.setText("3/5");
			rating = 3;
		}
	};
	
	private View.OnClickListener crowdFourListener = new OnClickListener() {	
		@Override
		public void onClick(View v) {
			crowdOne.setImageResource(R.drawable.icon_crowd_white);
			crowdTwo.setImageResource(R.drawable.icon_crowd_white);
			crowdThree.setImageResource(R.drawable.icon_crowd_white);
			crowdFour.setImageResource(R.drawable.icon_crowd_white);
			crowdFive.setImageResource(R.drawable.icon_crowd_white_transparent);
			nbCrowd.setText("4/5");
			rating = 4;
		}
	};
	
	private View.OnClickListener crowdFiveListener = new OnClickListener() {	
		@Override
		public void onClick(View v) {
			crowdOne.setImageResource(R.drawable.icon_crowd_white);
			crowdTwo.setImageResource(R.drawable.icon_crowd_white);
			crowdThree.setImageResource(R.drawable.icon_crowd_white);
			crowdFour.setImageResource(R.drawable.icon_crowd_white);
			crowdFive.setImageResource(R.drawable.icon_crowd_white);
			nbCrowd.setText("5/5");
			rating = 5;
		}
	};
}
