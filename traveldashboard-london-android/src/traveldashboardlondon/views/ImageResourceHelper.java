package traveldashboardlondon.views;

import traveldashboardlondon.app.R;
import traveldashboard.data.BikeStation;
import traveldashboard.data.BusStation;
import traveldashboard.data.TransportArea;
import traveldashboard.data.Tube;
import traveldashboard.data.TubeStation;

public final class ImageResourceHelper {
	
	public static final String MARKER_INFO = "MarkerInfo";
	public static final String MARKER_MAP = "MarkerMap";
	public static final String NON_MARKER = "NonMarker";

	public static int getTramStationImageResource(String marker) {
		int resourceId = -1;
		if (marker.equals(MARKER_MAP)) {
			resourceId = traveldashboardlondon.app.R.drawable.tram_red;			
		} else {
			resourceId = traveldashboardlondon.app.R.drawable.tram;				
		}
		return resourceId;
	}
	
	public static int getBikeStationImageResource(BikeStation bikeStation) {
		int resourceId = -1;
		
        if (TransportArea.getArea(bikeStation.getLatitude(), bikeStation.getLongitude()).equals(TransportArea.PARIS)) {
        	//In Paris
			resourceId = traveldashboardlondon.app.R.drawable.velib_red;    	
        } else {
        	//In London
			resourceId = traveldashboardlondon.app.R.drawable.bike_icon_coloured;
        }	
		return resourceId;
	}		
	
	public static int getBusStationImageResource(BusStation busStation, String marker) {
		int resourceId = -1;
		
        if (TransportArea.getArea(busStation.getLatitude(), busStation.getLongitude()).equals(TransportArea.PARIS)) {
        	//In Paris

				if (marker.equals(MARKER_MAP)) {
					//Marker small icon on map
					resourceId = traveldashboardlondon.app.R.drawable.bus_paris_red_small_icon;
				}  
				else if (marker.equals(MARKER_INFO)) {
					//Marker small icon on info view
					resourceId = traveldashboardlondon.app.R.drawable.bus_paris_small_icon;
				} else {
					resourceId = traveldashboardlondon.app.R.drawable.bus_paris;
				}
        } else {
        	//In London
			if (marker.equals(MARKER_MAP) || marker.equals(MARKER_INFO)) {
				//Marker small icon
				resourceId = traveldashboardlondon.app.R.drawable.bus_icon_coloured;
			} else {
				resourceId = traveldashboardlondon.app.R.drawable.bus_icon_large;
			}
        }	
		return resourceId;
	}
	
	public static int getTubeStationImageResource(TubeStation tubeStation, String marker) {
		int resourceId = -1;
		
        if (TransportArea.getArea(tubeStation.getLatitude(), tubeStation.getLongitude()).equals(TransportArea.PARIS)) {
        	//In Paris
        	
			boolean hasRER = false;
			Tube[] tubes = tubeStation.getTubes();
			
			for (int i = 0; i < tubes.length; i++) {
				Tube tube = tubes[i];
				if (tube.isRER()) {
					hasRER = true;
					break;
				}
			}
			
			if (!hasRER) {
				//Metro
				if (marker.equals(MARKER_MAP)) {
					//Marker small icon on map
					resourceId = traveldashboardlondon.app.R.drawable.metro_small_icon_red;
				}  
				else if (marker.equals(MARKER_INFO)) {
					//Marker small icon on info view
					resourceId = traveldashboardlondon.app.R.drawable.metro_small_icon;
				} else {
					resourceId = traveldashboardlondon.app.R.drawable.metro;
				}
			}
			else {
				//Rer
				if (marker.equals(MARKER_MAP)) {
					//Marker small icon on map
					resourceId = traveldashboardlondon.app.R.drawable.rer_red_small_icon;
				}  
				else if (marker.equals(MARKER_INFO)) {
					//Marker small icon
					resourceId = traveldashboardlondon.app.R.drawable.rer_small_icon;
				} else {
					resourceId = traveldashboardlondon.app.R.drawable.rer;
				}
			}
        } else {
        	//In London
			if (marker.equals(MARKER_MAP) || marker.equals(MARKER_INFO)) {
				//Marker small icon
				resourceId = traveldashboardlondon.app.R.drawable.tube_icon_coloured;
			} else {
				resourceId = traveldashboardlondon.app.R.drawable.tube_red_color;
			}
        }	
		return resourceId;
	}

}
