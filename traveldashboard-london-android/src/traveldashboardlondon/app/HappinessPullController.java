package traveldashboardlondon.app;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import traveldashboard.compat.AsyncTaskCompat;
import traveldashboard.data.Station;
import traveldashboard.data.core.CrowdSourcedPullCallback;
import traveldashboard.data.happiness.HappinessLevel;
import traveldashboard.data.happiness.HappinessRequest;
import traveldashboard.data.happiness.HappinessVote;
import traveldashboard.utils.TravelDashboardCommonPullTask;
import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class HappinessPullController implements CrowdSourcedPullCallback<HappinessVote, Station, HappinessLevel> {

	private Activity activity;
	private HappinessVote happinessInfo;
	private Station station;

	public HappinessPullController(Activity view, Station station) {
		this.activity = view;
		this.station = station;
		pullData();
	}

	public void showOrHideView() {
		if (happinessInfo == null || activity == null) return;

		final ImageView expandImg = (ImageView) activity.findViewById(R.id.expandHappiness);
		expandImg.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				LinearLayout ll = (LinearLayout) activity.findViewById(R.id.listHappiness);
				if (ll.getVisibility() == View.GONE) {
					pullData();
					ll.setVisibility(View.VISIBLE);
					expandImg.setImageResource(R.drawable.icon_collapse);
				} else {
					ll.setVisibility(View.GONE);
					expandImg.setImageResource(R.drawable.icon_expand);
				}
			}
		});
		
		Date start = new Date(happinessInfo.getTimeStamp());
		Date end = new Date(happinessInfo.getTimeStamp() + HappinessRequest.INTERVAL);
		TextView timeTv = (TextView) activity.findViewById(R.id.happinessTime);
		timeTv.setText(getString(start) + " - " + getString(end));
		
		ImageView iv = (ImageView) activity.findViewById(R.id.happinessDetailIcon);
		TextView levelTv = (TextView) activity.findViewById(R.id.happinessDetailLevel);
		
		if (happinessInfo.getCrowdSourcedValue() != null) {
			if (happinessInfo.getCrowdSourcedValue().getLevel() == HappinessLevel.HAPPY.getLevel()) {
				iv.setImageResource(R.drawable.happy_icon);
			} else if (happinessInfo.getCrowdSourcedValue().getLevel() == HappinessLevel.GLAD.getLevel()) {
				iv.setImageResource(R.drawable.glad_icon);
			} else if (happinessInfo.getCrowdSourcedValue().getLevel() == HappinessLevel.OK.getLevel()) {
				iv.setImageResource(R.drawable.ok_icon);
			} else if (happinessInfo.getCrowdSourcedValue().getLevel() == HappinessLevel.SAD.getLevel()) {
				iv.setImageResource(R.drawable.sad_icon);
			} else if (happinessInfo.getCrowdSourcedValue().getLevel() == HappinessLevel.CRIED.getLevel()) {
				iv.setImageResource(R.drawable.cry_icon);
			}
			iv.setVisibility(View.VISIBLE);

			levelTv.setText("" + happinessInfo.getCrowdSourcedValue().getLevel() + "/5");
			levelTv.setVisibility(View.VISIBLE);
		} else {
			iv.setVisibility(View.GONE);
			levelTv.setVisibility(View.GONE);
		}

		TextView totalTv = (TextView) activity.findViewById(R.id.happinessTotalReports);
		totalTv.setText(happinessInfo.getNumberOfVotes() + " of " + happinessInfo.getTotalVotes() + " votes");

		
	}

	@Override
	public void failed() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(List<HappinessVote> crowdSourceds) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(HappinessVote crowdSourced) {
		this.happinessInfo = crowdSourced;
		showOrHideView();
	}

	private String getString(Date date) {
		SimpleDateFormat dt = new SimpleDateFormat("hh:mm"); 
		return dt.format(date);
	}

	private void pullData() {
		HappinessRequest csRequest = new HappinessRequest();
		csRequest.setStation(station);
		csRequest.setTimstamp(System.currentTimeMillis() - HappinessRequest.INTERVAL);
		TravelDashboardCommonPullTask<HappinessVote, Station, HappinessLevel, HappinessRequest> task =
				new TravelDashboardCommonPullTask<HappinessVote, Station, HappinessLevel, HappinessRequest>(
						activity, HappinessVote.class, csRequest, this);
		AsyncTaskCompat.executeParallel(task);
	}
}
