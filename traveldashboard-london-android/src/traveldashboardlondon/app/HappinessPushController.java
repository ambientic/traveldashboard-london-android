package traveldashboardlondon.app;

import traveldashboard.compat.AsyncTaskCompat;
import traveldashboard.data.Station;
import traveldashboard.data.core.CrowdSourcedPushCallback;
import traveldashboard.data.happiness.Happiness;
import traveldashboard.data.happiness.HappinessLevel;
import traveldashboard.utils.TravelDashboardCommonPushTask;
import traveldashboardlondon.compat.AlertDialogCompat;
import traveldashboardlondon.views.HappinessPushLayout;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.ImageView;

public class HappinessPushController implements CrowdSourcedPushCallback {

	private Activity activity;
	private Station station;

	public HappinessPushController(Activity activity, Station station) {
		this.activity = activity;
		this.station = station;
		init();
	}

	private void init() {
		ImageView happinessIcon = (ImageView) activity.findViewById(R.id.happinesslevel32);
		happinessIcon.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				showPushHappinessDialog();
			}
		});
	}

	private void showPushHappinessDialog() {
		AlertDialog.Builder builder = AlertDialogCompat.getAlertBuilder(activity);
		
		final HappinessPushLayout localLayout = new HappinessPushLayout(activity, station);
		builder.setView(localLayout);
		
		builder.setPositiveButton("Report happiness", new DialogInterface.OnClickListener() {
		      public void onClick(DialogInterface dialog, int which) {
		    	  		    	  
		    	Happiness selected = localLayout.getSelectedHappiness();
		    	
		    	TravelDashboardCommonPushTask<Happiness, Station, HappinessLevel> task = 
		    			new TravelDashboardCommonPushTask<Happiness, Station, HappinessLevel>(activity, selected, HappinessPushController.this);
		    	
		    	AsyncTaskCompat.executeParallel(task);
		    	
		    	dialog.dismiss();
		      }
		    });

		AlertDialog dialog = AlertDialogCompat.getAlertDialog(builder);
		dialog.show();
	}

	@Override
	public void failed() {
		// TODO Auto-generated method stub
		System.out.println("unsuccessfully push crowdsourced data -------------");
	}

	@Override
	public void success() {
		// TODO Auto-generated method stub
		System.out.println("successfully push crowdsourced data -------------");
	}
}
