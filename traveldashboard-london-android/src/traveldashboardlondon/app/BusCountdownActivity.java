package traveldashboardlondon.app;

//
//
//ActiobarSherlock library for using action bar in all Android versions
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Window;
import com.google.gson.Gson;

import traveldashboard.compat.AsyncTaskCompat;
import traveldashboard.data.IbicoopDataConstants;
//
//
//Traveldashboard packages
import traveldashboard.data.Bus;
import traveldashboard.data.BusStation;
import traveldashboard.data.TransportArea;
import traveldashboard.data.info.BusRealTimeInfo;
import traveldashboard.data.info.BusRealTimeInfosCollection;
import traveldashboard.ibicooptask.IbicoopBusCountdownCallback;
import traveldashboard.ibicooptask.IbicoopBusCountdownTask;
import traveldashboard.utils.IntentConstants;

//
//
//Android library

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class BusCountdownActivity extends SherlockActivity {
	
	//Constants
	private static final int MAX_VALUES = 50;
	
	
	//
	//
	//Android
	//UI
	private LinearLayout timeTableLyt;
	private LinearLayout busTimetableLyt;
	
	//
	//
	//Ibicoop task	
	private IbicoopBusCountdownTask busCountdownTask;

	//
	//
	//Station
	private String stopId;
	private String stopName;
	private String city;
	private BusStation busStation;
	
    @Override
    public void onCreate(Bundle savedInstanceState_) {
        super.onCreate(savedInstanceState_);
        setContentView(R.layout.activity_bus_countdown2);
		setTitle("");
		//Don't need action bar
		getSupportActionBar().hide();
        
        //Get intent from MainMapActivity
        Intent intent = getIntent();
        String busStationJsonString = intent.getStringExtra(IntentConstants.BUS_STATION_GSON);
        Gson gson = new Gson();
        busStation = gson.fromJson(busStationJsonString, BusStation.class);
        
        stopId = busStation.getId();
        stopName = busStation.getName();

		city = "Unknown";
		if (busStation != null) city = TransportArea.getArea(busStation.getLatitude(), busStation.getLongitude());
		
		timeTableLyt = (LinearLayout) findViewById(R.id.busCountdownSubInfoLyt);
        
		if (busStation != null) {
			startMultipleTimetableTask();
		}
    }
    
    @Override
    protected void onDestroy() {
    	
    	if (busCountdownTask != null) busCountdownTask.cancel(true);
    		
    	super.onDestroy();
    }
    
	private void startMultipleTimetableTask() {
		
		if (busStation != null) {
			
			timeTableLyt.removeAllViews();
			
			Bus[] buses = busStation.getBuses();
			
			String routeId = "null";
			
			if (city.equals(IbicoopDataConstants.LONDON)) {
				//FIXME: In London, we will get timetable for all buses of a station
				//So we don't need to specify which routeId
				routeId = "*"; 
			} else if (city.equals(IbicoopDataConstants.PARIS)) {
				routeId = buses[0].getId(); 
			}
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("startMultipleTimetableTask for routeId = " + routeId);
			
			LayoutInflater inflater = BusCountdownActivity.this.getLayoutInflater();
			
			busTimetableLyt = (LinearLayout) inflater.inflate(R.layout.custom_time_table_parent_layout,  null);	

			//Initialize line name (route name)
			TextView lineName = (TextView) busTimetableLyt.findViewById(R.id.lineName);
			lineName.setText(stopName);
				
			ProgressBar pb = (ProgressBar) busTimetableLyt.findViewById(R.id.timetablePb);
			pb.setVisibility(View.VISIBLE);
			
			timeTableLyt.addView(busTimetableLyt);
				
			if (busCountdownTask != null) busCountdownTask.cancel(true);
				
			busCountdownTask = new  IbicoopBusCountdownTask(this, city, stopId, routeId, MAX_VALUES, busCallback);

			AsyncTaskCompat.executeParallel(busCountdownTask);
				
			}
		}
	
	private IbicoopBusCountdownCallback busCallback = new IbicoopBusCountdownCallback() {
		@Override
		public void busCountdownRealTimeInfo(
				String routeId,
				BusRealTimeInfosCollection infos) {
			
			if (IbicoopDataConstants.DEBUG_MODE) {
				System.out.println("Get multiple bus countdown success for routeId = " + routeId);
				Toast.makeText(BusCountdownActivity.this, "Get multiple bus countdown success for routeId = " + routeId,
						Toast.LENGTH_SHORT).show();
			}

			LayoutInflater li = LayoutInflater.from(BusCountdownActivity.this);
			
			BusRealTimeInfo[] realTimeInfos = infos.getBusRealTimeInfos();

			ProgressBar subTimetablePb = (ProgressBar) busTimetableLyt.findViewById(R.id.timetablePb);
			subTimetablePb.setVisibility(View.GONE);
			
			LinearLayout subTimetableLyt = (LinearLayout) busTimetableLyt.findViewById(R.id.subTimeTableLyt);
			subTimetableLyt.setVisibility(View.VISIBLE);
			
			if (realTimeInfos.length > 0) {
				
				for (BusRealTimeInfo realTimeInfo: realTimeInfos) {
					LinearLayout ttLayout = (LinearLayout) li.inflate(R.layout.custom_layout_time_table2, null);
					TextView line = (TextView) ttLayout.findViewById(R.id.line);
					TextView time = (TextView) ttLayout.findViewById(R.id.time);
					TextView direction = (TextView) ttLayout.findViewById(R.id.direction);
					
					String lineString = realTimeInfo.getTransport().getName();
					String timeString = realTimeInfo.getTime();
					String directionString = realTimeInfo.getDestination();
					
					line.setText(lineString);
					time.setText(timeString);
					direction.setText(directionString);
					subTimetableLyt.addView(ttLayout);
				}
				
			} else {
				LinearLayout ttLayout = (LinearLayout) li.inflate(R.layout.custom_layout_time_table2, null);
				TextView line = (TextView) ttLayout.findViewById(R.id.line);
				TextView time = (TextView) ttLayout.findViewById(R.id.time);
				TextView direction = (TextView) ttLayout.findViewById(R.id.direction);
				line.setText("");
				time.setText("Not available");
				direction.setText("");
				subTimetableLyt.addView(ttLayout);				
			}
			
		}
		
		@Override
		public void busCountdownFailed(String routeId) {
			if (IbicoopDataConstants.DEBUG_MODE) {
				System.out.println("bus countdown failed");
				Toast.makeText(BusCountdownActivity.this, "Get bus countdown failed", Toast.LENGTH_SHORT).show();
			}

			LayoutInflater li = LayoutInflater.from(BusCountdownActivity.this);						
			LinearLayout ttLayout = (LinearLayout) li.inflate(R.layout.custom_layout_time_table2, null);
			TextView line = (TextView) ttLayout.findViewById(R.id.line);
			TextView time = (TextView) ttLayout.findViewById(R.id.time);
			TextView direction = (TextView) ttLayout.findViewById(R.id.direction);
			line.setText("");
			time.setText("Not available");
			direction.setText("");

			LinearLayout subTimetableLyt = (LinearLayout) busTimetableLyt.findViewById(R.id.subTimeTableLyt);
			subTimetableLyt.setVisibility(View.VISIBLE);
			ProgressBar subTimetablePb = (ProgressBar) busTimetableLyt.findViewById(R.id.timetablePb);
			subTimetablePb.setVisibility(View.GONE);							
			subTimetableLyt.addView(ttLayout);
		}
	};

}
