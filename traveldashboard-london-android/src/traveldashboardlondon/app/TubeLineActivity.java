package traveldashboardlondon.app;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Window;

import traveldashboard.compat.AsyncTaskCompat;
import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.ibicooptask.IbicoopAllTubeStatusCallback;
import traveldashboard.ibicooptask.IbicoopAllTubeStatusTask;
import traveldashboardlondon.context.TSLApplicationContext;
import traveldashboard.data.status.TubeStatus;
import traveldashboard.data.status.TubeStatusCollection;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class TubeLineActivity extends SherlockActivity {
	
	//
	//
	//Android
	//UI
	private LinearLayout tubeStatusLayout;
	private LinearLayout tubeStatusInfoLayout;
	
    //
    //
    //Ibicoop tasks
    private IbicoopAllTubeStatusTask allTubeStatusTask;
    
    private String city;

	@Override
	public void onRestoreInstanceState(Bundle restoreInstanceState_) {
		super.onRestoreInstanceState(restoreInstanceState_);
		getLineStatusUpdates();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState_) {
		super.onCreate(savedInstanceState_);
		setContentView(R.layout.activity_tube_line2);
	    setTitle("");
	    getSupportActionBar().hide();
	    tubeStatusInfoLayout = (LinearLayout) findViewById(R.id.tubeStatusSubInfoLyt);
		city = TSLApplicationContext.getInstance().getCameraViewCity();
		getLineStatusUpdates();
	}
	
	@Override
	protected void onDestroy() {
		
		if (allTubeStatusTask != null) allTubeStatusTask.cancel(true);
		
		super.onDestroy();
	}
	
	
	private void getLineStatusUpdates() {
		tubeStatusInfoLayout.removeAllViews();
		LayoutInflater inflater = TubeLineActivity.this.getLayoutInflater();
		tubeStatusLayout = (LinearLayout) inflater.inflate(R.layout.custom_time_table_parent_layout, null);	

		//Initialize line name (route name)
		TextView lineName = (TextView) tubeStatusLayout.findViewById(R.id.lineName);
		lineName.setText("Metro status");

		ProgressBar pb = (ProgressBar) tubeStatusLayout.findViewById(R.id.timetablePb);
		pb.setVisibility(View.VISIBLE);
		
		tubeStatusInfoLayout.addView(tubeStatusLayout);

		/**
		 * Get tube status through ibicoop
		 */
		if (allTubeStatusTask != null) allTubeStatusTask.cancel(true);
		allTubeStatusTask = new IbicoopAllTubeStatusTask(this, city, statusCallback);
		AsyncTaskCompat.executeParallel(allTubeStatusTask);
	}
	
    private IbicoopAllTubeStatusCallback statusCallback = new IbicoopAllTubeStatusCallback() {
		@Override
		public void tubeStatusFailed() {
			if (IbicoopDataConstants.DEBUG_MODE) {
				System.err.println("Get tube status failed");
				Toast.makeText(TubeLineActivity.this, "Get metro status failed", Toast.LENGTH_SHORT).show();
			}
			LayoutInflater li = LayoutInflater.from(TubeLineActivity.this);						
			LinearLayout ttLayout = (LinearLayout) li.inflate(R.layout.custom_layout_time_table2, null);
			TextView line = (TextView) ttLayout.findViewById(R.id.line);
			TextView time = (TextView) ttLayout.findViewById(R.id.time);
			TextView direction = (TextView) ttLayout.findViewById(R.id.direction);
			line.setText("");
			time.setText("Not available");
			direction.setText("");

			LinearLayout subTimetableLyt = (LinearLayout) tubeStatusLayout.findViewById(R.id.subTimeTableLyt);
			subTimetableLyt.setVisibility(View.VISIBLE);
			ProgressBar subTimetablePb = (ProgressBar) tubeStatusLayout.findViewById(R.id.timetablePb);
			subTimetablePb.setVisibility(View.GONE);							
			subTimetableLyt.addView(ttLayout);			
		}
		
		@Override
		public void tubeStatusCollectionInfo(TubeStatusCollection info) {
			if (IbicoopDataConstants.DEBUG_MODE) {
				System.out.println("Get tube status success");
				Toast.makeText(TubeLineActivity.this, "Get metro status success",
						Toast.LENGTH_SHORT).show();
			}
			
			TubeStatus[] statusArray = info.getTubeStatusCollection();
			
			LayoutInflater li = LayoutInflater.from(TubeLineActivity.this);

			ProgressBar subTimetablePb = (ProgressBar) tubeStatusLayout.findViewById(R.id.timetablePb);
			subTimetablePb.setVisibility(View.GONE);
			
			LinearLayout subTimetableLyt = (LinearLayout) tubeStatusLayout.findViewById(R.id.subTimeTableLyt);
			subTimetableLyt.setVisibility(View.VISIBLE);
			
			if (statusArray.length > 0) {
				
				List<String> routeNames = new ArrayList<String>();
				//key = route name, value = tube status
				HashMap<String, LinearLayout> statusMap = new HashMap<String, LinearLayout>();
				
				for (TubeStatus status: statusArray) {
					
					LinearLayout ttLayout = (LinearLayout) li.inflate(R.layout.custom_layout_time_table2, null);
					TextView line = (TextView) ttLayout.findViewById(R.id.line);
					TextView time = (TextView) ttLayout.findViewById(R.id.time);
					TextView direction = (TextView) ttLayout.findViewById(R.id.direction);
					
					String lineString = "";
					String timeString = "";
					String directionString = "";

					if (city.equals(IbicoopDataConstants.LONDON)) {
						lineString = status.getTube().getName();
						timeString = status.getStatus();
						directionString = status.getDescription();
					} else if (city.equals(IbicoopDataConstants.PARIS)) {			
						lineString = status.getTube().getName();
						timeString = status.getDescription();
						directionString = status.getStatus();
						if ((routeNames.contains(status.getTube().getName()))) continue;
					}

					line.setText(lineString);
					time.setText(timeString);
					direction.setText(directionString);
					routeNames.add(status.getTube().getName());
					statusMap.put(status.getTube().getName(), ttLayout);
				}
				
				List<String> allRouteNames = new ArrayList<String>();
				
				if (city.equals(IbicoopDataConstants.LONDON)) {
					allRouteNames.addAll(IbicoopDataConstants.LONDON_METRO_ROUTE_NAMES);
				} else if (city.equals(IbicoopDataConstants.PARIS)) {
					allRouteNames.addAll(IbicoopDataConstants.PARIS_METRO_ROUTE_NAMES);
				}
				
				for (String routeName : allRouteNames) {
					LinearLayout lyt = statusMap.get(routeName);
					
					if  (lyt != null) {
						
						if (!routeName.equals("N/A")) {
							subTimetableLyt.addView(lyt);
						}
					}
				}
				
			} else {
				LinearLayout ttLayout = (LinearLayout) li.inflate(R.layout.custom_layout_time_table2, null);
				TextView line = (TextView) ttLayout.findViewById(R.id.line);
				TextView time = (TextView) ttLayout.findViewById(R.id.time);
				TextView direction = (TextView) ttLayout.findViewById(R.id.direction);
				line.setText("");
				time.setText("Status not available");
				direction.setText("");
				subTimetableLyt.addView(ttLayout);				
			}
		}
	};
}
