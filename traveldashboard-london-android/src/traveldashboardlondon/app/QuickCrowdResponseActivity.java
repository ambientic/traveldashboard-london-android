package traveldashboardlondon.app;

import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.utils.IntentConstants;
import traveldashboard.data.TubeStation;
import traveldashboardlondon.views.CrowdHelper;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.google.gson.Gson;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class QuickCrowdResponseActivity extends SherlockActivity {

	private TubeStation metroStop;
	private String metroStopDesc;
	
	private String stationName;
	private int level;
	
	private TextView stationNameTv;
	private LinearLayout quickCrowdLyt;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		setContentView(R.layout.activity_quick_crowd_response);
		setTitle("");
		getSupportActionBar().setTitle("Crowd response");
	
		if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Quick crowd response!");
		
		//Get information from intent
		metroStopDesc = getIntent().getExtras().getString(IntentConstants.TUBE_STATION_GSON);
		
		if (IbicoopDataConstants. DEBUG_MODE) System.out.println("metroStopDesc = " + metroStopDesc);
		
		level = getIntent().getExtras().getInt(IbicoopDataConstants.OD_KEY_LEVEL);
		
		Gson gson = new Gson();
		metroStop = gson.fromJson(metroStopDesc, TubeStation.class);
		
		//Display station name
		stationNameTv = (TextView) findViewById(R.id.quickCrowdResponseStationName);
		
		if (metroStop != null) {
			stationName = metroStop.getName();
			stationNameTv.setText(stationName);
		} else {
			stationNameTv.setVisibility(View.GONE);
		}
		
		//Add crowd rating simplified layout
		quickCrowdLyt = (LinearLayout) findViewById(R.id.quickCrowdResponseLyt);
		
		LayoutInflater inflater = QuickCrowdResponseActivity.this.getLayoutInflater();
		
		LinearLayout generalCrowdParentLayout = (LinearLayout) inflater.inflate(R.layout.custom_crowd_layout2, null);
		
		ImageView[] generalCrowdImageViews = new ImageView[5];
		generalCrowdImageViews[0] = (ImageView) generalCrowdParentLayout.findViewById(R.id.customCrowd1);
		generalCrowdImageViews[1] = (ImageView) generalCrowdParentLayout.findViewById(R.id.customCrowd2);
		generalCrowdImageViews[2] = (ImageView) generalCrowdParentLayout.findViewById(R.id.customCrowd3);
		generalCrowdImageViews[3] = (ImageView) generalCrowdParentLayout.findViewById(R.id.customCrowd4);
		generalCrowdImageViews[4] = (ImageView) generalCrowdParentLayout.findViewById(R.id.customCrowd5);
		
		TextView nbGeneralCrowdTv = (TextView) generalCrowdParentLayout.findViewById(R.id.nbCustomCrowd);;
		nbGeneralCrowdTv.setText(level + "/5");
		CrowdHelper.setNewCrowdImagesByImageViews(QuickCrowdResponseActivity.this, generalCrowdImageViews, level);
		
		quickCrowdLyt.addView(generalCrowdParentLayout);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

}
