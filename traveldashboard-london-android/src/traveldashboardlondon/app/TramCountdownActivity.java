package traveldashboardlondon.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

import traveldashboard.compat.AsyncTaskCompat;
import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.ibicooptask.IbicoopTramCountdownCallback;
import traveldashboard.ibicooptask.IbicoopTramCountdownTask;
import traveldashboard.utils.IntentConstants;
import traveldashboardlondon.compat.ProgressDialogCompat;
import traveldashboardlondon.context.TSLApplicationContext;
import traveldashboard.data.Tram;
import traveldashboard.data.TramStation;
import traveldashboard.data.info.TramRealTimeInfo;
import traveldashboard.data.info.TramRealTimeInfosCollection;
import android.os.Bundle;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SimpleAdapter;

public class TramCountdownActivity extends ListActivity {

	//
	//
	//Constants
	private static final int MAX_VALUES = 50;
	
	
	//
	//
	//Android
	//UI
	private Button closeButton;
	
	
	
	//
	//
	//Ibicoop task
	private IbicoopTramCountdownTask tramCountdownTask;
	
	
	//
	//
	//Station
	private TramStation tramStation;
	private String stopId;
	private String stopName;
	private String routeId;

    @Override
    public void onCreate(Bundle savedInstanceState_) {
        super.onCreate(savedInstanceState_);
        setContentView(R.layout.activity_tram_countdown);

        Intent intent = getIntent();
        String jsonString = intent.getStringExtra(IntentConstants.TRAM_STATION_GSON);
        
        Gson gson = new Gson();
        tramStation = gson.fromJson(jsonString, TramStation.class);
        stopId = tramStation.getId();
        stopName = tramStation.getName();
        Tram[] trams = tramStation.getTrams();
        //We suppose that we have the only one tram line on each station
        Tram tram = trams[0];
        routeId = tram.getId();
		setTitle(stopName);

		this.closeButton = (Button) findViewById(R.id.close_button);
		this.closeButton.setBackgroundColor(Color.DKGRAY);
		this.closeButton.setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						finish();
					}
		});
			
		/**
		 * Get tram countdown through ibicoop
		 */
		if (tramCountdownTask != null) tramCountdownTask.cancel(true);
		
		tramCountdownTask =  new IbicoopTramCountdownTask(
				this, 
				TSLApplicationContext.getInstance().getCameraViewCity(), 
				stopId, 
				routeId, 
				MAX_VALUES, 
				tramCallback);
		
		AsyncTaskCompat.executeParallel(tramCountdownTask);
    }
    
    
    @Override
    public void onDestroy() {
		if (tramCountdownTask != null) tramCountdownTask.cancel(true);
    	super.onDestroy();
    }
    
     
    /**
     * Get tram countdown by ibicoop
     */
    private IbicoopTramCountdownCallback tramCallback = new IbicoopTramCountdownCallback() {

		@Override
		public void tramCountdownFailed() {
			if (IbicoopDataConstants. DEBUG_MODE) System.err.println("Get tram countdown failed");
		}

		@Override
		public void tramRealtimeInfo(TramRealTimeInfosCollection infos) {
			
			TramRealTimeInfo[] tramRealTimeInfos = infos.getTramRealTimeInfos();
			List<Map<String, String>> infosMap = new ArrayList<Map<String, String>>();
			
			for (int i = 0; i < tramRealTimeInfos.length; i++) {
				TramRealTimeInfo tramInfo = tramRealTimeInfos[i];
				Map<String, String> infoMap = new HashMap<String, String>();
				infoMap.put(IbicoopDataConstants.PARAM_KEY_ROUTE_NAME, tramInfo.getTransport().getName());
				infoMap.put(IbicoopDataConstants.TIME, tramInfo.getTime());
				infoMap.put(IbicoopDataConstants.DESTINATION, tramInfo.getDestination());
				infosMap.add(infoMap);
			}
			
			SimpleAdapter adapter = new SimpleAdapter(TramCountdownActivity.this,
	      			  infosMap,
	      			  R.layout.custom_row_view,
	      			  new String[] {IbicoopDataConstants.PARAM_KEY_ROUTE_NAME, IbicoopDataConstants.TIME, IbicoopDataConstants.DESTINATION},
	      			  new int[] {R.id.text1,R.id.text2, R.id.text3});
		    		setListAdapter(adapter);
		}
    };
 
}
