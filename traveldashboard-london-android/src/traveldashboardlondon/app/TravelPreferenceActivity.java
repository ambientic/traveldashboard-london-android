package traveldashboardlondon.app;

import java.io.IOException;

import org.inbicoop.preference.PreferenceConfigurationIntf;
import org.inbicoop.preference.TransportPreferenceIntf;


import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;

import traveldashboard.data.IbicoopDataConstants;
import traveldashboardlondon.context.TSLApplicationContext;
import traveldashboardlondon.preferences.PreferenceWrapper;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

public class TravelPreferenceActivity extends SherlockActivity {
	
	private CheckBox bikePrefBox;
	private CheckBox busPrefBox;
	private CheckBox tramPrefBox;
	private CheckBox tubePrefBox;
	private CheckBox noisePrefBox;
	private CheckBox isTrackingCurrentPositionPrefBox;
	private CheckBox isSimulationModeBox;
	private Button buttonSave;
	
	private PreferenceConfigurationIntf preferenceConfiguration;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_travel_preferences);
		setTitle("");
		getSupportActionBar().hide();
		
		bikePrefBox = (CheckBox) findViewById(R.id.bikePref);
		busPrefBox = (CheckBox) findViewById(R.id.busPref);
		tramPrefBox = (CheckBox) findViewById(R.id.tramPref);
		tubePrefBox = (CheckBox) findViewById(R.id.tubePref);
		noisePrefBox = (CheckBox) findViewById(R.id.noisePref);
		isTrackingCurrentPositionPrefBox = (CheckBox) findViewById(R.id.isTrackingCurrentPositionPref);
		//Simulation mode not used
		isSimulationModeBox = (CheckBox) findViewById(R.id.isSimulationModePref);
		isSimulationModeBox.setVisibility(View.GONE);
		
		preferenceConfiguration = TSLApplicationContext.getInstance().getPreferenceConfiguration();
		
		if (preferenceConfiguration != null) {
			
			if (preferenceConfiguration.isBoundForServiceBinder()) {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Preference service is bound");
				
				TransportPreferenceIntf pref = null;
				try {
					pref = preferenceConfiguration.getTransportPreference();
				} catch (SecurityException e) {
					// TODO Auto-generated catch block
					if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
				}
				
				if (pref != null) {
					if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Transport preference is not null");
					if (IbicoopDataConstants.DEBUG_MODE) System.out.println(pref.toString());
					bikePrefBox.setChecked(pref.getBikePref());
					busPrefBox.setChecked(pref.getBusPref());
					tramPrefBox.setChecked(pref.getTramPref());
					tubePrefBox.setChecked(pref.getTubePref());
					noisePrefBox.setChecked(pref.getNoisePref());
					isTrackingCurrentPositionPrefBox.setChecked(pref.isTrackingCurrentPosition());
					isSimulationModeBox.setChecked(pref.isSimulationMode());
				} else {
					if (IbicoopDataConstants.DEBUG_MODE) System.err.println("Preference is null");
				}
			} else {
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Preference service is unbound");
			}
		} else {
			if (IbicoopDataConstants.DEBUG_MODE) System.err.println("Preference configuration is null");
		}
		
		buttonSave = (Button) findViewById(R.id.savePref);
		buttonSave.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean save = savePreference();
				if (save) {
					if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Save ok");
					Toast.makeText(TravelPreferenceActivity.this, "Save preferences ok", Toast.LENGTH_SHORT).show();
				}
				else {
					if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Save not ok");
					Toast.makeText(TravelPreferenceActivity.this, "Save preferences not ok", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	private boolean savePreference() {
		boolean success = false;
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Save preferences");

		boolean bikePref =  bikePrefBox.isChecked();
		boolean busPref =  busPrefBox.isChecked();
		boolean tramPref =  tramPrefBox.isChecked();
		boolean tubePref =  tubePrefBox.isChecked();
		boolean noisePref = noisePrefBox.isChecked();
		boolean isTrackingCurrentPositionPref = isTrackingCurrentPositionPrefBox.isChecked();
		boolean isSimulationModePref = isSimulationModeBox.isChecked();
		
		// Write and read a dummy value. Both values must of course be the same
		// for the test to succeed.
		TransportPreferenceIntf pref = PreferenceWrapper.createTransportPreference(bikePref, busPref, tramPref, 
				tubePref, noisePref, isTrackingCurrentPositionPref, isSimulationModePref);
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println(pref.toString());
		
		if (preferenceConfiguration != null && pref != null) {
			
			if (preferenceConfiguration.isBoundForServiceBinder()) {
				try {
					success = preferenceConfiguration.setTransportPreference(pref);
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
				} catch (SecurityException e) {
					// TODO Auto-generated catch block
					if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					if (IbicoopDataConstants.DEBUG_MODE) e.printStackTrace();
				}
			}
		}
		
		return success;
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.travel_preferences, menu);
		return true;
	}

}
