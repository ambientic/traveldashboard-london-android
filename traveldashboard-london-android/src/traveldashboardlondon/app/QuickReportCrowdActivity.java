package traveldashboardlondon.app;

import org.ibicoop.init.IbicoopInit;

import traveldashboard.compat.AsyncTaskCompat;
import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.ibicooptask.IbicoopRegionChannelRequest;
import traveldashboard.ibicooptask.IbicoopReportCrowdTask;
import traveldashboard.ibicooptask.IbicoopStartCallback;
import traveldashboard.ibicooptask.IbicoopStartStopTask;
import traveldashboard.ibicooptask.IbicoopReportCrowdTask.IbicoopReportCrowdCallback;
import traveldashboard.utils.IntentConstants;
import traveldashboardlondon.app.InfosDashboardActivity.WaitForInitIbicoopTask;
import traveldashboardlondon.compat.AlertDialogCompat;
import traveldashboardlondon.context.TSLApplicationContext;
import traveldashboardlondon.service.LocalRequestNotificationSender;
import traveldashboardlondon.service.LocalStationVoteNotificationSender;
import traveldashboard.data.TransportArea;
import traveldashboard.data.TubeStation;
import traveldashboardlondon.views.CrowdRatingSimplifiedLayout;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.Window;
import com.google.gson.Gson;

public class QuickReportCrowdActivity extends SherlockActivity {
	
	private TubeStation metroStop;
	private String metroStopDesc;
	
	private String city;
	private String routeId;
	private String stationId;
	private String stationName;
	private String direction;
	
	private TextView stationNameTv;
	private LinearLayout quickCrowdLyt;
	private Button quickReportCrowdButton;
	private CrowdRatingSimplifiedLayout crowdRatingLayout;
	
	private TextView reportCrowdContentTv;
	
	private IbicoopReportCrowdTask reportCrowdTask;	

    //Start stop
    private IbicoopStartStopTask ibicoopStartStopTask;
    private WaitForInitIbicoopTask waitForInitIbicoopTask;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		setContentView(R.layout.activity_quick_report_crowd);
		setTitle("");
		getSupportActionBar().setTitle("Crowd reporting");
		
		//Get information from intent
		metroStopDesc = getIntent().getExtras().getString(IntentConstants.TUBE_STATION_GSON);
		
		if (IbicoopDataConstants. DEBUG_MODE) System.out.println("QuickReportCrowdActivity: metroStopDesc = " + metroStopDesc);
		
		Gson gson = new Gson();
		metroStop = gson.fromJson(metroStopDesc, TubeStation.class);
		
		city = TransportArea.getArea(metroStop.getLatitude(), metroStop.getLongitude());
		routeId = metroStop.getTubes()[0].getId();		
		stationId = metroStop.getId();		
		stationName = metroStop.getName();
		direction = metroStop.getTubes()[0].getDestination();		
		
		//Display station name
		stationNameTv = (TextView) findViewById(R.id.quickCrowdStationName);
		stationNameTv.setText(stationName);
		
		//Add crowd rating simplified layout
		quickCrowdLyt = (LinearLayout) findViewById(R.id.quickCrowdLyt);
		crowdRatingLayout = new CrowdRatingSimplifiedLayout(QuickReportCrowdActivity.this);
		quickCrowdLyt.addView(crowdRatingLayout);
		
		//Add listener
		quickReportCrowdButton = (Button) findViewById(R.id.quickReportCrowdButton);
		quickReportCrowdButton.setOnClickListener(quickReportCrowdButtonListener);
		
		if (waitForInitIbicoopTask != null) {waitForInitIbicoopTask.cancel(true);}
		
		waitForInitIbicoopTask = new WaitForInitIbicoopTask();
		AsyncTaskCompat.executeParallel(waitForInitIbicoopTask);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}
	
	class WaitForInitIbicoopTask extends AsyncTask<Void, Void, Void> {
		
		@Override
		protected Void doInBackground(Void... params) {

			try{
				//Waiting to reinitialize ibicoop in the case of notification callback
				Thread.sleep(1000);
			} catch (Exception exception) {
				if (IbicoopDataConstants. DEBUG_MODE) System.err.println(exception.getMessage());
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void param) {
			if (ibicoopStartStopTask != null) ibicoopStartStopTask.cancel(true);
			ibicoopStartStopTask = new IbicoopStartStopTask(QuickReportCrowdActivity.this, false, ibicoopStartCallback,
					TSLApplicationContext.getInstance(), LocalStationVoteNotificationSender.class, LocalRequestNotificationSender.class);
			AsyncTaskCompat.executeParallel(ibicoopStartStopTask);				
		}
	}
	
	private IbicoopStartCallback ibicoopStartCallback = new IbicoopStartCallback() {
		
		@Override
		public void ibicoopStartOk() {
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Ibicoop start ok");
		}
		
		@Override
		public void ibicoopStartFailed() {
			if (IbicoopDataConstants. DEBUG_MODE) System.err.println("Ibicoop start failed");
		}

	};
	
	
	private OnClickListener quickReportCrowdButtonListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			
			if (IbicoopInit.getInstance().isStarted()) {
		    	int rating = crowdRatingLayout.getRating();
			    
		    	if (reportCrowdTask != null) reportCrowdTask.cancel(true);
		    	
		        //In Paris we can report different crowd for diffrent lines
		        //FIXME: We can only report the same crowd for all lines in a tube station in London
		    	//FIXME: Start ibicoop in report crowd task
		    	reportCrowdTask = new IbicoopReportCrowdTask(
		    			QuickReportCrowdActivity.this,
		    			city,
		    			stationId, 
		    			routeId, 
		    			rating,
		    			reportCrowdCallback
		    			);
		    	
		    	AsyncTaskCompat.executeParallel(reportCrowdTask);
		    	
				showReportCrowdFinishDialog();
			} else {
				showReportCrowdNotAvailableDialog();
			}
		}
	};
	
	
	private void showReportCrowdFinishDialog() {
		AlertDialog.Builder builder = AlertDialogCompat.getAlertBuilder(QuickReportCrowdActivity.this);
		LayoutInflater inflater = QuickReportCrowdActivity.this.getLayoutInflater();
		
		ScrollView localSv = (ScrollView) inflater.inflate(R.layout.custom_incentive_layout, null);
		reportCrowdContentTv = (TextView) localSv.findViewById(R.id.incentiveContent);
		reportCrowdContentTv.setText("Thank you for reporting");
		builder.setTitle("Report crowd data");
		builder.setView(localSv);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
		      public void onClick(DialogInterface dialog, int which) {
		    	  dialog.dismiss();
		    	  finish();
		    }
		});
		
		AlertDialog dialog = AlertDialogCompat.getAlertDialog(builder);
		dialog.show();
	}		
	
	
	private void showReportCrowdNotAvailableDialog() {
		AlertDialog.Builder builder = AlertDialogCompat.getAlertBuilder(QuickReportCrowdActivity.this);
		LayoutInflater inflater = QuickReportCrowdActivity.this.getLayoutInflater();
		
		ScrollView localSv = (ScrollView) inflater.inflate(R.layout.custom_incentive_layout, null);
		reportCrowdContentTv = (TextView) localSv.findViewById(R.id.incentiveContent);
		reportCrowdContentTv.setText("Sorry, the crowd reporting service is currently not available. Please try again.");
		builder.setTitle("Report crowd data");
		builder.setView(localSv);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
		      public void onClick(DialogInterface dialog, int which) {
		    	  dialog.dismiss();
		    }
		});
		
		AlertDialog dialog = AlertDialogCompat.getAlertDialog(builder);
		dialog.show();
	}	
	
	
	private IbicoopReportCrowdCallback reportCrowdCallback = new IbicoopReportCrowdCallback() {
		@Override
		public void reportCrowdSuccess() {
			if (IbicoopDataConstants. DEBUG_MODE) {
				System.out.println("Report crowd success!");
				Toast.makeText(QuickReportCrowdActivity.this, "Report crowd success", Toast.LENGTH_SHORT).show();
			}
		}
		
		@Override
		public void reportCrowdFail() {
			if (IbicoopDataConstants. DEBUG_MODE) {
				System.err.println("Report crowd failed!");
				Toast.makeText(QuickReportCrowdActivity.this, "Report crowd failed", Toast.LENGTH_SHORT).show();
			}
		}
	};

}
