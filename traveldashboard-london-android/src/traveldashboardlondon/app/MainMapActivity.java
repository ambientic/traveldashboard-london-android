package traveldashboardlondon.app;

//
//
//Java native library
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;



import org.ibicoop.init.IbicoopInit;
//
//
//Ibicoop sensor library for collecting sensor data
import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.IbiSensorFactory;
import org.ibicoop.sensor.IbiSensorListener;
import org.ibicoop.sensor.IbiSensorType;
import org.ibicoop.sensor.data.IbiSensorData;
import org.ibicoop.sensor.data.IbiSensorDataLocation;
import org.ibicoop.sensor.data.IbiSensorDataMicrophone;
import org.ibicoop.sensor.exceptions.IbiSensorException;
import org.ibicoop.sensor.resolution.IbiSensorResolutionLocation;
import org.ibicoop.sensor.resolution.IbiSensorResolutionLocationAccuracyType;
import org.ibicoop.sensor.resolution.IbiSensorResolutionMicrophone;
import org.ibicoop.sensor.trigger.IbiSensorDataInRegionSystemTrigger;
import org.ibicoop.sensor.trigger.IbiSensorDataInRegionSystemTriggerListener;
import org.ibicoop.sensor.trigger.IbiSensorDataTimerNotAdaptativeNewDataTrigger;
import org.ibicoop.sensor.trigger.IbiSensorDataTrigger;
import org.ibicoop.sensor.trigger.IbiSensorDataTriggerListener;
import org.ibicoop.sensor.unit.IbiSensorUnitLocation;
import org.ibicoop.sensor.unit.IbiSensorUnitMicrophone;
import org.inbicoop.preference.MainMapActivityCallback;
import org.inbicoop.preference.PreferenceConfigurationIntf;
import org.inbicoop.preference.TransportPreferenceIntf;


import traveldashboard.compat.AsyncTaskCompat;
import traveldashboard.data.BoundingArea;
import traveldashboard.data.Geocoder;
import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.data.LondonBikeDbManager;
import traveldashboard.data.LondonBikeStopInfo;
import traveldashboard.data.LondonBusDbManager;
import traveldashboard.data.LondonBusStopInfo;
import traveldashboard.data.LondonMetroDbManager;
import traveldashboard.data.LondonMetroStopInfo;
import traveldashboard.data.ParisMetroDbManager;
import traveldashboard.data.ParisStopInfo;
import traveldashboard.data.LondonBikeDbManager.LondonBikeDbUpdateCallback;
import traveldashboard.ibicooptask.IbicoopAddAllLondonBikeStopsInDbTask;
import traveldashboard.ibicooptask.IbicoopAddAllLondonMetroStopsInDbTask;
import traveldashboard.ibicooptask.IbicoopAddAllParisMetroStopsInDbTask;
import traveldashboard.ibicooptask.IbicoopAddLondonStopsInRegionMonitoringTask;
import traveldashboard.ibicooptask.IbicoopAddParisStopsInRegionMonitoringTask;
import traveldashboard.ibicooptask.IbicoopRegionChannelSubscriptionTask;
import traveldashboard.ibicooptask.IbicoopRemoveLondonStopsFromRegionMonitoringTask;
import traveldashboard.ibicooptask.IbicoopRemoveParisStopsFromRegionMonitoringTask;
import traveldashboard.ibicooptask.IbicoopReportNoiseTask;
import traveldashboard.ibicooptask.IbicoopStartCallback;
import traveldashboard.ibicooptask.IbicoopStartStopTask;
import traveldashboard.ibicooptask.IbicoopAddAllLondonBikeStopsInDbTask.AddAllLondonBikeStopsInDbTaskCallback;
import traveldashboard.ibicooptask.IbicoopAddAllLondonMetroStopsInDbTask.AddAllLondonMetroStopsInDbTaskCallback;
import traveldashboard.ibicooptask.IbicoopAddAllParisMetroStopsInDbTask.AddAllParisStopsInDbTaskCallback;
import traveldashboard.ibicooptask.IbicoopAddLondonStopsInRegionMonitoringTask.AddLondonStopsInRegionMonitoringCallback;
import traveldashboard.ibicooptask.IbicoopAddParisStopsInRegionMonitoringTask.AddParisStopsInRegionMonitoringCallback;
import traveldashboard.ibicooptask.IbicoopRemoveLondonStopsFromRegionMonitoringTask.RemoveLondonStopsFromRegionMonitoringCallback;
import traveldashboard.ibicooptask.IbicoopRemoveParisStopsFromRegionMonitoringTask.RemoveParisStopsFromRegionMonitoringCallback;
import traveldashboard.ibicooptask.IbicoopReportNoiseTask.IbicoopReportNoiseCallback;
import traveldashboard.utils.IntentConstants;
//
//
//Traveldashboard packages
import traveldashboardlondon.compat.AlertDialogCompat;
import traveldashboardlondon.context.CameraLocationContext;
import traveldashboardlondon.context.TSLApplicationContext;
import traveldashboardlondon.preferences.PreferenceWrapper;
import traveldashboardlondon.service.LocalRequestNotificationSender;
import traveldashboardlondon.service.LocalStationVoteNotificationSender;
import traveldashboardlondon.service.MockLocationSimulator;
import traveldashboardlondon.service.UpdateBikeExecutor;
import traveldashboard.data.BikeStation;
import traveldashboard.data.Bus;
import traveldashboard.data.BusStation;
import traveldashboard.data.TransportArea;
import traveldashboard.data.Tube;
import traveldashboard.data.TubeStation;
import traveldashboardlondon.utils.ExternalApkInstall;


//
//
//Android library
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.app.SearchManager.OnCancelListener;
import android.app.SearchManager.OnDismissListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnCloseListener;
import android.widget.Toast;
import android.widget.ToggleButton;

//
//
//Android maps extensions library for marker clustering
import pl.mg6.android.maps.extensions.Circle;
import pl.mg6.android.maps.extensions.GoogleMap;
import pl.mg6.android.maps.extensions.GoogleMap.CancelableCallback;
import pl.mg6.android.maps.extensions.GoogleMap.OnInfoWindowClickListener;
import pl.mg6.android.maps.extensions.GoogleMap.OnMarkerClickListener;
import pl.mg6.android.maps.extensions.GoogleMap.OnCameraChangeListener;
import pl.mg6.android.maps.extensions.Marker;
import pl.mg6.android.maps.extensions.Polyline;
import pl.mg6.android.maps.extensions.SupportMapFragment;

import com.actionbarsherlock.app.ActionBar;
//
//
//ActionbarSherlock library for using action bar in all Android versions
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

//
//
//Google maps library for map
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

//
//
//Gson library
import com.google.gson.Gson;

public class MainMapActivity extends SherlockFragmentActivity
	implements OnMarkerClickListener,
				OnCameraChangeListener, SearchView.OnQueryTextListener,
				OnDismissListener, OnCancelListener, OnCloseListener, MainMapActivityCallback { 

	//Constants
	
	private static final boolean USE_GLOBAL_CHANNEL = false;
	
	
	//Metro stops xml file name
	private static final String PARIS_METRO_XML_FILE = "parisMetroStop.xml";	
	
	//Stations radius, fecth new stations when current location is over the radius
	private static final String RADIUS = "1000"; //1 km

	//Camera zoom latitude and longitude offset
	private static final int TWO_MINUTES = 1000 * 60 * 2;

	//For noise sensor
	private static final int MICRO_SAMPLING_RATE = 44100; //44.1 kHz
	private static final int RECORDING_MS = 1000; //1000 ms
	
	//Noise reporting period = 5 seconds
	private static final int SOUND_TRIGGER_DEFAULT_MS = 1000 * 5;
	//Noise reporting timeout = 60 seconds = 1 minute
	private static final int SOUND_TRIGGER_TIMEOUT_MS = 1000 * 60;

	//For location sensor
	private static final int LOCATION_TRIGGER_DEFAULT_MS = 1000;//*1000 ms
	private static final long LOCATION_UPDATE_MS = 1000; //1000 ms
	private static final float LOCATION_UPDATE_DISTANCE = 100f; //100 m

	//Default Paris location
	private static final double LAT_PARIS_INIT = 48.84681;
	private static final double LON_PARIS_INIT = 2.31640;
	
	//Default London location
	private static final double LAT_LONDON_INIT = 51.51121;
	private static final double LON_LONDON_INIT =-0.11982;
	
	//In region proximity alert active time
	private static final long PROXIMITY_ALERT_ACTIVE_TIME_MS = 1000 * 60 * 5; //5 minutes
	
	//In region proximity alert distance check
	private static final double PROXIMITY_ALERT_ACTIVE_DISTANCE_CHECK = IbicoopDataConstants.MONITORING_RADIUS * 2; //200 m
	
	//For simulation  used
	private static final long SIMULATION_PERIOD = 1000 * 60; //60 seconds

	private static final int MAP_ANIMATION_DURATION_MS = 1000; //1 second
	
	//
	//
	//Android
	//Context for ibicoop
	private Context ibicoopContext;
	
	//Asset manager
	private AssetManager assetManager;
	private InputStream parisMetroInputStream;
	private InputStream[] londonMetroInputStream;
	private InputStream londonBikeInputStream;
	
	//User interface
	private ToggleButton busToggle;
	private ToggleButton bikeToggle;
	private ToggleButton metroToggle;
	private ToggleButton tramToggle;
	
	
	private ListView addressListView;
	//private Integer weatherIconResId;
		
	//Search
	private	SearchManager searchManager;
    private SearchView searchView;
    
    
    //
    //
    //Google map markers
	private GoogleMap map;

	//Current position marker
	private Marker currentPositionMarker;
    //Current position marker circle
	private Circle currentPositionMarkerCircle;
	
	//Simulation position marker
	private Marker simuPositionMarker;
	//Simulation position marker circle
	private Circle simuPositionMarkerCircle;
	
	//Nearby marker list
	//Metro
	private List<Marker> nearbyMetroMarkersList = new ArrayList<Marker>();
	private HashMap<Marker, TubeStation> nearbyMetroStopsMap = new HashMap<Marker, TubeStation>();
	
	//Bike
	private List<Marker> nearbyBikeMarkersList = new ArrayList<Marker>();
	private HashMap<Marker, BikeStation> nearbyBikeStopsMap = new HashMap<Marker, BikeStation>();
	
	//Update bike executor
	private UpdateBikeExecutor updateBikeExecutor;
	
	//Bus
	private List<Marker> nearbyBusMarkersList = new ArrayList<Marker>();
	private HashMap<Marker, BusStation> nearbyBusStopsMap = new HashMap<Marker, BusStation>();	

	
	//Nearby marker circle
	private Circle nearbyMarkerCircle;
	private Polyline nearbyMarkerRectangle;
	
	
    //
    //
	//Actionbarsherlock
	private ActionBar actionBar;
	private	MenuItem refreshItem;	
	
	//
	//
	//Ibicoop
	private IbicoopReportNoiseTask reportNoiseTask;
	
	//Ibicoop sensors
	private IbiSensor noiseSensor;
	private IbiSensor locationSensor;
	private IbiSensorListener sensorListener;
	private IbiSensorDataTimerNotAdaptativeNewDataTrigger noiseDataTrigger;
	private IbiSensorDataTimerNotAdaptativeNewDataTrigger locationDataTrigger;
	private IbiSensorDataTriggerListener noiseDataTriggerListener;
	private IbiSensorDataTriggerListener locationDataTriggerListener;
    private IbiSensorDataInRegionSystemTrigger sysInRegionTrigger;
    private IbiSensorDataInRegionSystemTriggerListener inRegionListener;
    
	//Ibicoop tasks
    //Start stop
    private IbicoopStartStopTask ibicoopStartStopTask;
    
    //Metro
    private IbicoopAddAllParisMetroStopsInDbTask ibicoopAddAllParisMetroStopsInDbTask;
    private IbicoopAddAllLondonMetroStopsInDbTask ibicoopAddAllLondonMetroStopsInDbTask;
    
    //Bike
    private IbicoopAddAllLondonBikeStopsInDbTask ibicoopAddAllLondonBikeStopsInDbTask;
    
   
    //Region monitoring
    //Paris
	private IbicoopAddParisStopsInRegionMonitoringTask addParisStopsInRegionTask;
	private IbicoopRemoveParisStopsFromRegionMonitoringTask removeParisStopsFromRegionTask;
	
	//London
	private IbicoopAddLondonStopsInRegionMonitoringTask addLondonStopsInRegionTask;
	private IbicoopRemoveLondonStopsFromRegionMonitoringTask removeLondonStopsFromRegionTask;

	
	//Region channel subscription
	private IbicoopRegionChannelSubscriptionTask regionChannelSubscriptionTask;
	

	//Init preference
	private InitPreferencesTask initPreferencesTask;
	
    //Save in monitoring location data
	//FIXME: Might be in concurrent access!
    private ArrayList<IbiSensorDataLocation> locationDatasList;
    
    
	//
	//
	//Variables
	//For the gestion of tube, bus and bike menu check box, initially checked	
	private boolean isTubeChecked = true;
	private boolean isBusChecked = true;
	private boolean isBikeChecked = true;
	private boolean isTramChecked = true;
	private boolean isNoiseChecked = true;
	private boolean isTrackingCurrentPosition = false;
	private boolean isSimulationMode = false;
	
	private boolean isConnected;

	
	//Camera view latitude and longitude
	private String cameraViewLatitude;
	private String cameraViewLongitude;
	
	//The true location latitude and longitude
	private String trueLocationLatitude;
	private String trueLocationLongitude;	
	
	//City
	private String cameraViewCity;
	private String trueLocationCity;

	private LinkedHashMap<Integer, Address> targetIntegerAddressMap;
	private LinkedHashMap<String, Address> targetStringAddressMap;
	
	//Store in region stop ids
	private List<String> inRegionStopIdsList = new ArrayList<String>();
	
	//Use hashmap to store in region trigger stop id and corresponding timestamp
	//Key = stopId, value = triggering timestamp
	private HashMap<String, Long> inRegionStopIds_Timestamp = new HashMap<String, Long>();
	//Use hashmap to store out of region trigger stop id and corresponding timestamp
	//Key = stopId, value = triggering timestamp
	private HashMap<String, Long> outOfRegionStopIds_Timestamp = new HashMap<String, Long>();

	
	//IsRefreshing variable
	private boolean isRefreshing = false;
	//In the case whetre refresh item has not been created but we ask for refreshing
	private boolean isWaitingForRefresh = false;
	
	//Paris metro stops is in db
	private boolean parisMetroStopsIsInDb = false;
	
	//London metro stops is in db
	private boolean londonMetroStopsIsInDb = false;
	private boolean londonBikeStopsIsInDb = false;

	//
	//
	//Services
	//We use our own Geocoder which uses Google Geocoding Api!
	private Geocoder geoCoder;
	private CameraLocationContext cameraLocationContext;
	
	//Mock location
	//private MockLocationProvider mockLocationProvider;
	private MockLocationSimulator mockLocationSimulator;
	
	//Database
	private ParisMetroDbManager parisMetroDbManager;
	
	private LondonMetroDbManager londonMetroDbManager;
	private LondonBikeDbManager londonBikeDbManager;
	private LondonBusDbManager londonBusDbManager;
	
	//Bounding area
	private BoundingArea boundingArea;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		ExternalApkInstall.install(this);

		//Set application title
		setTitle("");
		setContentView(R.layout.activity_map);
		actionBar = getSupportActionBar();
		
		//
		//
		// Set up map
		map = ((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map)).getExtendedMap();
		
		map.setMyLocationEnabled(true);
		map.getUiSettings().setZoomControlsEnabled(true);
		map.getUiSettings().setCompassEnabled(false);
		
		map.setOnMarkerClickListener(this);

		map.setOnCameraChangeListener(this);
		map.setOnInfoWindowClickListener(infoWindowListener);
		
		//Set version code
	    TSLApplicationContext.getInstance().setVersionCode(Build.VERSION.SDK_INT);
	    
	    
	    addressListView = (ListView) findViewById(R.id.addressLv);
	    addressListView.setOnItemClickListener(addressItemClickListener);

		//
		//
		//Set up mock location provider
	    //mockLocationProvider = new MockLocationProvider(LocationManager.GPS_PROVIDER, this);
	  
		if (cameraViewLatitude == null || cameraViewLongitude == null) {
			
			cameraViewLatitude = String.valueOf(LAT_LONDON_INIT);
			cameraViewLongitude = String.valueOf(LON_LONDON_INIT);
			
			cameraViewCity = TransportArea.getArea(Double.parseDouble(cameraViewLatitude), Double.parseDouble(cameraViewLongitude));
			
			TSLApplicationContext.getInstance().setCameraViewCity(cameraViewCity);
			TSLApplicationContext.getInstance().setCameraLatitude(Double.parseDouble(cameraViewLatitude));
			TSLApplicationContext.getInstance().setCameraLongitude(Double.parseDouble(cameraViewLongitude));
			
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Default location data : " +
					"latitude = " + cameraViewLatitude + ", " +
					"longitude = " + cameraViewLongitude + ", " +
					"city = " + cameraViewCity
					);
		} 
	    
	    
		assetManager = getAssets();
		
		
		//
		//
		//Set up context
		//Context might be set by notification callback activity: InfosDashboardActivity
		ibicoopContext = TSLApplicationContext.getInstance().getContext();
		if (ibicoopContext == null) ibicoopContext = this;
		
		
	    
	    //
	    //
	    //Set up database for london
	    londonMetroDbManager = TSLApplicationContext.getInstance().getLondonMetroDbManager();
	    
	    if (londonMetroDbManager == null) {
		    londonMetroDbManager = new LondonMetroDbManager(this);
		    TSLApplicationContext.getInstance().setLondonMetroDbManager(londonMetroDbManager);
	    }

	    if (IbicoopDataConstants. DEBUG_MODE) System.out.println("db count for london = " + londonMetroDbManager.getStopInfoDbCount());
	    
	    //Test if paris metros stops in db
	    if (londonMetroDbManager.getStopInfoDbCount() >= 254) {
	    	if (IbicoopDataConstants. DEBUG_MODE) System.out.println("London db manager is in db");
	    	londonMetroStopsIsInDb = true;
	    } else {
			try {
		    	if (IbicoopDataConstants. DEBUG_MODE) System.out.println("London db manager is not in db");
		    	//Insert london metro stops in db
		    	londonMetroInputStream = new InputStream[8];
		    	
		    	for (int k = 0; k <= 7; k++) {
		    		londonMetroInputStream[k] = assetManager.open("stations" + (k + 1) + ".csv");
		    	}
		    	
		    	if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Open input stream ok");
		    	
		    	if (cameraViewCity.equals(IbicoopDataConstants.LONDON)) {
		    		launchRefresh();
		    	}
				
		    	ibicoopAddAllLondonMetroStopsInDbTask = new IbicoopAddAllLondonMetroStopsInDbTask(londonMetroDbManager, londonMetroInputStream, allLondonMetroStopsInDbCallback);
		    	
		    	AsyncTaskCompat.executeParallel(ibicoopAddAllLondonMetroStopsInDbTask);
		    	
			} catch (Exception exception) {
				if (IbicoopDataConstants. DEBUG_MODE) System.err.println("Error opening paris metro xml file : " + exception.getMessage());
			}
	    	
	    }
	    
	    
	    //bus
	    londonBusDbManager = TSLApplicationContext.getInstance().getLondonBusDbManager();
	    
	    if (londonBusDbManager == null) {
	    	if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Creating london bus db manager");
		    londonBusDbManager  = new LondonBusDbManager(this);	
		    TSLApplicationContext.getInstance().setLondonBusDbManager(londonBusDbManager);
	    }
	    
	    if (IbicoopDataConstants. DEBUG_MODE) System.out.println("db count for london bus = " + londonBusDbManager.getStopInfoDbCount());
	    
	    //bike
	    londonBikeDbManager = TSLApplicationContext.getInstance().getLondonBikeDbManager();
	    
	    if (londonBikeDbManager == null) {
	    	if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Creating london bike db manager");	    	
		    londonBikeDbManager = new LondonBikeDbManager(this, bikeDbUpdateCallback);
		    TSLApplicationContext.getInstance().setLondonBikeDbManager(londonBikeDbManager);
	    }
	    
	    if (IbicoopDataConstants. DEBUG_MODE) System.out.println("db count for london = " + londonBikeDbManager.getStopInfoDbCount());
	    
	    //Test if paris metros stops in db
	    if (londonBikeDbManager.getStopInfoDbCount() >= 258) {
	    	if (IbicoopDataConstants. DEBUG_MODE) System.out.println("London bike db manager is in db");
	    	londonBikeStopsIsInDb = true;
	    	
	    	startUpdateBikeExecutor();
	    } else {
			try {
		    	if (IbicoopDataConstants. DEBUG_MODE) System.out.println("London bike db manager is not in db");
		    	//Insert london metro stops in db
		    	londonBikeInputStream = assetManager.open("london_bike.xml");
		    	
		    	if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Open bike input stream ok");
				
		    	ibicoopAddAllLondonBikeStopsInDbTask = new IbicoopAddAllLondonBikeStopsInDbTask(londonBikeDbManager, londonBikeInputStream, addAllLondonBikeStopsInDbTaskCallback);
		    	
		    	AsyncTaskCompat.executeParallel(ibicoopAddAllLondonBikeStopsInDbTask);
		    	
			} catch (Exception exception) {
				if (IbicoopDataConstants. DEBUG_MODE) System.err.println("Error opening london bike xml file : " + exception.getMessage());
			}
	    	
	    }	    
	    
	    
	
	    //
	    //
	    //Set up database for paris
		//This manager might be set by notification callback activity: InfosDashboardActivity
		parisMetroDbManager = TSLApplicationContext.getInstance().getParisMetroDbManager();
		
		if (parisMetroDbManager == null) {
			//Create database if it doesn't exist
		    parisMetroDbManager = new ParisMetroDbManager(this);
		    if (IbicoopDataConstants. DEBUG_MODE) System.out.println("db count for paris = " + parisMetroDbManager.getStopInfoDbCount());
		    TSLApplicationContext.getInstance().setParisMetroDbManager(parisMetroDbManager);			
		}

	    //Test if paris metros stops in db
	    if (parisMetroDbManager.getStopInfoDbCount() >= 759) {
	    	if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Paris metro stops is in db");
	    	parisMetroStopsIsInDb = true;
	    } else {
	    	
	    	//Insert paris metro stops in db
			try {
				parisMetroInputStream = assetManager.open(PARIS_METRO_XML_FILE);
				
		    	if (cameraViewCity.equals(IbicoopDataConstants.PARIS)) {
		    		launchRefresh();
		    	}
				
		    	if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Open paris metro input stream ok");
		    	
		    	ibicoopAddAllParisMetroStopsInDbTask = new IbicoopAddAllParisMetroStopsInDbTask(parisMetroDbManager, parisMetroInputStream, allParisMetroStopsInDbCallback);
		    	
		    	AsyncTaskCompat.executeParallel(ibicoopAddAllParisMetroStopsInDbTask);
		    	
			} catch (Exception exception) {
				if (IbicoopDataConstants. DEBUG_MODE) System.err.println("Error opening paris metro xml file : " + exception.getMessage());
			}
	    }
	    
	    
	    //
	    //
	    //Set up in system monitoring location data list
	    locationDatasList = new ArrayList<IbiSensorDataLocation>();
	    
		//
		//
		//Initiatialize ibisensors
		initIbiSensors();
		
		
		isConnected = checkInternetConnection();

		if(!isConnected){
			showNoInternetDialog();
		}
	    
		geoCoder = new Geocoder(this);
		
		busToggle = (ToggleButton) findViewById(R.id.bus_toggle);
		busToggle.setChecked(true);

		bikeToggle = (ToggleButton) findViewById(R.id.bike_toggle);
		bikeToggle.setChecked(true);

		metroToggle = (ToggleButton) findViewById(R.id.metro_toggle);
		metroToggle.setChecked(true);
		
		tramToggle = (ToggleButton ) findViewById(R.id.tram_toggle);
		tramToggle.setChecked(true);

		busToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					for(Marker marker: nearbyBusMarkersList){
						marker.setVisible(true);
					}
					
					busToggle.setButtonDrawable(R.drawable.toggle_button_bus);
					
				} else {
					for(Marker marker: nearbyBusMarkersList){
						marker.setVisible(false);
					}
					busToggle.setButtonDrawable(R.drawable.toggle_button_bus_transparent);
				}
			}
		});

		bikeToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					for(Marker marker: nearbyBikeMarkersList){
						marker.setVisible(true);
					}
						buttonView.setButtonDrawable(R.drawable.toggle_button_bike);

				} else {
					for(Marker marker:  nearbyBikeMarkersList){
						marker.setVisible(false);
					}
					
					buttonView.setButtonDrawable(R.drawable.toggle_button_bike_transparent);
				}
			}
		});

		//Metro toggle button
		metroToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				
				if (isChecked) {
					//On
					for(Marker marker: nearbyMetroMarkersList){
						marker.setVisible(true);
					}
					
					buttonView.setButtonDrawable(R.drawable.toggle_button_train);

				} else {
					
					for(Marker marker: nearbyMetroMarkersList){
						marker.setVisible(false);
					}
					
					buttonView.setButtonDrawable(R.drawable.toggle_button_train_transparent);
				}
			}
		});

		//Tram toggle button
		tramToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				
				if (isChecked) {					
					buttonView.setButtonDrawable(R.drawable.toggle_button_tram);

				} else {
					buttonView.setButtonDrawable(R.drawable.toggle_button_tram_transparent);
				}
			}
		});
		
    	//Refresh current metro stops
		cameraViewCity = TransportArea.getArea(Double.parseDouble(cameraViewLatitude), Double.parseDouble(cameraViewLongitude));
		TSLApplicationContext.getInstance().setCameraViewCity(cameraViewCity);
		refreshAllNearbyStops(cameraViewCity);
	} //End of on create bundle


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        completeRefresh();
    }
	
	private LondonBikeDbUpdateCallback bikeDbUpdateCallback = new LondonBikeDbUpdateCallback() {
		@Override
		public void londonBikeDbUpdate() {
			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					if (IbicoopDataConstants. DEBUG_MODE) System.out.println("London bike db update callback");
					refreshNearbyBikeStops(cameraViewCity);
				}
			});
		}
	};
	
	
	//
	//
	//Callback when all metro stops in paris had been added
	//Paris
	private AddAllParisStopsInDbTaskCallback allParisMetroStopsInDbCallback = new AddAllParisStopsInDbTaskCallback() {
		
		@Override
		public void addAllStopsInDbOk() {
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Add all paris stops in db ok");
			
		    if (parisMetroDbManager.getStopInfoDbCount() >= 759) {
		    	if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Paris db manager info count = " + parisMetroDbManager.getStopInfoDbCount());
		    	parisMetroStopsIsInDb = true;
		    	
		    	//Refresh current metro stops
				cameraViewCity = TransportArea.getArea(Double.parseDouble(cameraViewLatitude), Double.parseDouble(cameraViewLongitude));
				TSLApplicationContext.getInstance().setCameraViewCity(cameraViewCity);
				
				if (cameraViewCity.equals(IbicoopDataConstants.PARIS)) {
					refreshNearbyMetroStops(IbicoopDataConstants.PARIS);
				}
		    }
		    
		    if (cameraViewCity.equals(IbicoopDataConstants.PARIS)) {
		    	completeRefresh();
		    }
		}
		
		@Override
		public void addAllStopsInDbFailed() {
			if (IbicoopDataConstants. DEBUG_MODE) System.err.println("Add all stops in db failed");

		    if (cameraViewCity.equals(IbicoopDataConstants.PARIS)) {
		    	completeRefresh();
		    }
		}
	};
	
	private void startUpdateBikeExecutor() {
		if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Start update bike service");
		
    	if (updateBikeExecutor == null) updateBikeExecutor = new UpdateBikeExecutor();
    	
	}
	
	private void stopUpdateBikeExecutor() {
		if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Stop update bike executor");
		
		if (updateBikeExecutor != null) {
				updateBikeExecutor.stopTask();
		}
	}
	
	private AddAllLondonBikeStopsInDbTaskCallback addAllLondonBikeStopsInDbTaskCallback = new AddAllLondonBikeStopsInDbTaskCallback() {
		
		@Override
		public void addAllStopsInDbOk() {
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Add all london bike stops in db ok");

		    if (londonBikeDbManager.getStopInfoDbCount() >= 258) {
		    	
		    	if (IbicoopDataConstants. DEBUG_MODE) System.out.println("London db bike manager info count = " + londonBikeDbManager.getStopInfoDbCount());
		    	londonBikeStopsIsInDb = true;
		    	
		    	//Refresh current metro stops
				cameraViewCity = TransportArea.getArea(Double.parseDouble(cameraViewLatitude), Double.parseDouble(cameraViewLongitude));
				TSLApplicationContext.getInstance().setCameraViewCity(cameraViewCity);
				
				
				
				if (cameraViewCity.equals(IbicoopDataConstants.LONDON)) {
					refreshNearbyBikeStops(IbicoopDataConstants.LONDON);
				}
				
				startUpdateBikeExecutor();
		    }
			
		}
		
		@Override
		public void addAllStopsInDbFailed() {
			if (IbicoopDataConstants. DEBUG_MODE) System.err.println("Add all bike stops in db failed");
			
		}
	};
	
	//London
	private AddAllLondonMetroStopsInDbTaskCallback allLondonMetroStopsInDbCallback = new AddAllLondonMetroStopsInDbTaskCallback() {
		
		@Override
		public void addAllStopsInDbOk() {
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Add all london stops in db ok");
			
		    if (londonMetroDbManager.getStopInfoDbCount() >= 254) {
		    	
		    	if (IbicoopDataConstants. DEBUG_MODE) System.out.println("London db manager info count = " + londonMetroDbManager.getStopInfoDbCount());
		    	londonMetroStopsIsInDb = true;
		    	
		    	//Refresh current metro stops
				cameraViewCity = TransportArea.getArea(Double.parseDouble(cameraViewLatitude), Double.parseDouble(cameraViewLongitude));
				TSLApplicationContext.getInstance().setCameraViewCity(cameraViewCity);
				
				if (cameraViewCity.equals(IbicoopDataConstants.LONDON)) {
					refreshNearbyMetroStops(IbicoopDataConstants.LONDON);
					completeRefresh();
				}
		    }
		    
		    if (cameraViewCity.equals(IbicoopDataConstants.LONDON)) {
		    	completeRefresh();
		    }
		}
		
		@Override
		public void addAllStopsInDbFailed() {
			if (IbicoopDataConstants. DEBUG_MODE) System.err.println("Add all stops in db failed");
			
		    if (cameraViewCity.equals(IbicoopDataConstants.LONDON)) {
		    	completeRefresh();
		    }
		}
	};	
	
	
	
	//Refresh all nearby stops
	public void refreshAllNearbyStops(String city) {
    	refreshNearbyMetroStops(city);
    	refreshNearbyBikeStops(city);
    	refreshNearbyBusStops(city);
	}
	
	
	//
	//
	//Refresh nearby metro stops
	public void refreshNearbyMetroStops(String city) {
		
		if (city.equals(IbicoopDataConstants.PARIS)) {

			ArrayList<ParisStopInfo> parisStopInfos = new ArrayList<ParisStopInfo>();
			
			parisStopInfos.addAll(parisMetroDbManager.readFromDbByCoord(
        			Double.parseDouble(cameraViewLatitude), 
        			Double.parseDouble(cameraViewLongitude), 
        			Double.parseDouble(RADIUS)
        			));
			//Remove previous marker
			for (Marker marker: nearbyMetroMarkersList) {
				marker.remove();
			}
			
			nearbyMetroMarkersList.clear();
			nearbyMetroStopsMap.clear();
			
			Bitmap bitmap = null;
			BitmapDescriptor bitmapDescriptor = null;
			
			if(parisStopInfos.size() > 0) {
				bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.icon_place_train);
				bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(bitmap);
			}
			
			
			double lonOffset = 0.00025;
			
	    	for (ParisStopInfo stop : parisStopInfos) {
	    		
	    		lonOffset = lonOffset * -1;
	    		
	    		Marker nearbyMarker = map.addMarker(new MarkerOptions().position(
						new LatLng(stop.getLatitude(), stop.getLongitude() + lonOffset))
						.title(stop.getStopName() + " >")
						.icon(bitmapDescriptor)
						.draggable(false)
	    				);
	    		
	    		
				nearbyMetroMarkersList.add(nearbyMarker);
				
				//Create tube
				Tube metro = new Tube(
						stop.getRouteId(), 
						stop.getRouteName(), 
						"" + stop.getDirection());
				
				Tube[] metros = {metro};
				//Create tube station
				TubeStation station = new TubeStation(
						stop.getStopId(), 
						stop.getStopName(), 
						stop.getStopId(), 
						stop.getLatitude(), 
						stop.getLongitude(), 
						metros);
				//Insert tube station into marker map
				nearbyMetroStopsMap.put(nearbyMarker, station);
	    	}			

		} else if (city.equals(IbicoopDataConstants.LONDON)) {
			
			ArrayList<LondonMetroStopInfo> londonMetroStopInfos = new ArrayList<LondonMetroStopInfo>();
			
			londonMetroStopInfos.addAll(londonMetroDbManager.readFromDbByCoord(
        			Double.parseDouble(cameraViewLatitude), 
        			Double.parseDouble(cameraViewLongitude), 
        			Double.parseDouble(RADIUS)
        			));
			//Remove previous marker
			for (Marker marker: nearbyMetroMarkersList) {
				marker.remove();
			}
			
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Refresh london nearby stops");
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("London stop infos size = " + londonMetroStopInfos.size());
			
			nearbyMetroMarkersList.clear();
			nearbyMetroStopsMap.clear();
			
			Bitmap bitmap = null;
			BitmapDescriptor bitmapDescriptor = null;
			
			if(londonMetroStopInfos.size() > 0) {
				bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.icon_place_train);
				bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(bitmap);
			}
			
			
	    	for (LondonMetroStopInfo stop : londonMetroStopInfos) {
	    		Marker nearbyMarker = map.addMarker(new MarkerOptions().position(
						new LatLng(stop.getLatitude(), stop.getLongitude()))
						.title(stop.getStopName() + " >")
						.icon(bitmapDescriptor)
						.draggable(false).visible(metroToggle.isChecked())
	    				);
	    		
				nearbyMetroMarkersList.add(nearbyMarker);
				
				HashMap<String, String> routeIdsWithNamesMap = new HashMap<String, String>();
				routeIdsWithNamesMap.putAll(stop.getRouteIdsWithNamesMap());
				
				HashMap<String, String> routeIdsWithDirectionsMap = new HashMap<String, String>();
				routeIdsWithDirectionsMap.putAll(stop.getRouteIdsWithDirectionsMap());
				
				List<String> routeIds = new ArrayList<String>();
				routeIds = stop.getRouteIds();
				
				List<Tube> metrosList = new ArrayList<Tube>();
				
				for (String routeId: routeIds) {

					if (!(routeId.equals("*"))) {
						//Create tube
						Tube metro = new Tube(
								routeId, 
								routeIdsWithNamesMap.get(routeId), 
								routeIdsWithDirectionsMap.get(routeId)
								);
						metrosList.add(metro);
					}
				}
				
				Tube[] metros = new Tube[metrosList.size()];
				
				for (int h = 0; h < metrosList.size(); h++) {
					metros[h] = metrosList.get(h);
				}

				//Create tube station
				TubeStation station = new TubeStation(
						stop.getStopId(), 
						stop.getStopName(), 
						stop.getStopNlc(), 
						stop.getLatitude(), 
						stop.getLongitude(), 
						metros);
				//Insert tube station into marker map
				nearbyMetroStopsMap.put(nearbyMarker, station);
	    	}			
		}
		
	}
	
	
	public void refreshNearbyBusStops(String city) {
	
		if (city.equals(IbicoopDataConstants.LONDON)) {
			
			ArrayList<LondonBusStopInfo> londonBusStopInfos = new ArrayList<LondonBusStopInfo>();
			
			londonBusStopInfos.addAll(londonBusDbManager.readFromDbByCoord(
        			Double.parseDouble(cameraViewLatitude), 
        			Double.parseDouble(cameraViewLongitude), 
        			Double.parseDouble(RADIUS)
        			));
			
			//Remove previous marker
			for (Marker marker: nearbyBusMarkersList) {
				marker.remove();
			}
			
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Refresh london nearby bus stops");
			
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("London bus stop infos size = " + londonBusStopInfos.size());
			
			nearbyBusMarkersList.clear();
			nearbyBusStopsMap.clear();
			
			Bitmap bitmap = null;
			BitmapDescriptor bitmapDescriptor = null;
			
			if(londonBusStopInfos.size() > 0) {
				bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.icon_place_bus);
				bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(bitmap);
			}
			
	    	for (LondonBusStopInfo stop : londonBusStopInfos) {
	    		Marker nearbyMarker = map.addMarker(new MarkerOptions().position(
						new LatLng(stop.getLatitude(), stop.getLongitude()))
						.title(stop.getStopName() + " >")
						.icon(bitmapDescriptor)
						.draggable(false).visible(busToggle.isChecked())
	    				);
	    		
				nearbyBusMarkersList.add(nearbyMarker);
				
				HashMap<String, String> routeIdsWithNamesMap = new HashMap<String, String>();
				routeIdsWithNamesMap.putAll(stop.getRouteIdsWithNamesMap());
				
				HashMap<String, String> routeIdsWithDirectionsMap = new HashMap<String, String>();
				routeIdsWithDirectionsMap.putAll(stop.getRouteIdsWithDirectionsMap());
				
				List<String> routeIds = new ArrayList<String>();
				routeIds = stop.getRouteIds();
				
				List<Bus> busList = new ArrayList<Bus>();
				
				for (String routeId: routeIds) {
					if (!(routeId.equals("*"))) {
						//Create bus
						Bus bus = new Bus(
								routeId, 
								routeIdsWithNamesMap.get(routeId), 
								routeIdsWithDirectionsMap.get(routeId)
								);
						busList.add(bus);
					}
				
				Bus[] buses = new Bus[busList.size()];
				
				for (int h = 0; h < busList.size(); h++) {
					buses[h] = busList.get(h);
				}

				//Create tube station
				BusStation station = new BusStation(
						stop.getStopId(), 
						stop.getStopName(), 
						stop.getLatitude(), 
						stop.getLongitude(), 
						buses);
				//Insert bus station into marker map
				nearbyBusStopsMap.put(nearbyMarker, station);
				}			
	    	}			
		}
	}
	
	//
	//
	//Refresh nearby metro stops
	public void refreshNearbyBikeStops(String city) {
		
		if (city.equals(IbicoopDataConstants.LONDON)) {
			
			ArrayList<LondonBikeStopInfo> londonBikeStopInfos = new ArrayList<LondonBikeStopInfo>();
			
			londonBikeStopInfos.addAll(londonBikeDbManager.readFromDbByCoord(
        			Double.parseDouble(cameraViewLatitude), 
        			Double.parseDouble(cameraViewLongitude), 
        			Double.parseDouble(RADIUS)
        			));
			
			//Remove previous marker
			for (Marker marker: nearbyBikeMarkersList) {
				marker.remove();
			}
			
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Refresh london nearby bike stops");
			
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("London stop infos size = " + londonBikeStopInfos.size());
			
			nearbyBikeMarkersList.clear();
			nearbyBikeStopsMap.clear();
			
			Bitmap bitmap = null;
			BitmapDescriptor bitmapDescriptor = null;
			
			if(londonBikeStopInfos.size() > 0) {
				bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.icon_place_bike);
				bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(bitmap);
			}
			
	    	for (LondonBikeStopInfo stop : londonBikeStopInfos) {
	    		Marker nearbyMarker = map.addMarker(new MarkerOptions().position(
						new LatLng(stop.getLatitude(), stop.getLongitude()))
						.title(stop.getStopName())
						.snippet(stop.getNbBikes() + "/" +  stop.getNbDocks() + " bikes available")
						.icon(bitmapDescriptor)
						.draggable(false).visible(bikeToggle.isChecked())
	    				);
	    		
				nearbyBikeMarkersList.add(nearbyMarker);

				BikeStation station = new BikeStation(
						stop.getStopId(), 
						stop.getStopName(), 
						stop.getLatitude(), 
						stop.getLongitude(), 
						Integer.parseInt(stop.getNbBikes()), 
						Integer.parseInt(stop.getNbEmptyDocks()), 
						Integer.parseInt(stop.getNbDocks()), 
						Boolean.valueOf(stop.getLocked()));
				
				//Insert tube station into marker map
				nearbyBikeStopsMap.put(nearbyMarker, station);
	    	}			
		}
		
	}
		
	
	//
	//
	//Ibisensors
	/**
	 * Initialize ibisensors
	 */
	private void initIbiSensors() {
				
		try {
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Initialize sensor listener!");
			
			sensorListener = new IbiSensorListener() {
				
				@Override
				public void stopped(IbiSensor ibiSensor) {
					try {
						if (IbicoopDataConstants. DEBUG_MODE) System.out.println(ibiSensor.getProperties().getName() + ": stopped");					
					} catch (IbiSensorException ibiSensorException) {
						ibiSensorException.printStackTrace();
					}
				}
				
				@Override
				public void started(IbiSensor ibiSensor) {
					try {
						if (IbicoopDataConstants. DEBUG_MODE) System.out.println(ibiSensor.getProperties().getName() + ": started");					
					} catch (IbiSensorException ibiSensorException) {
						ibiSensorException.printStackTrace();
					}
				}
				
				@Override
				public void resumed(IbiSensor ibiSensor) {
					try {
						if (IbicoopDataConstants. DEBUG_MODE) System.out.println(ibiSensor.getProperties().getName() + ": resumed");					
					} catch (IbiSensorException ibiSensorException) {
						ibiSensorException.printStackTrace();
					}
				}
				
				@Override
				public void paused(IbiSensor ibiSensor) {
					try {
						if (IbicoopDataConstants. DEBUG_MODE) System.out.println(ibiSensor.getProperties().getName() + ": paused");					
					} catch (IbiSensorException ibiSensorException) {
						ibiSensorException.printStackTrace();
					}	
				}
			};
			
            //
            //
            //Noise sensor
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Initialize noise sensor!");
			//Launch noise sensor
			noiseSensor = IbiSensorFactory.getInstance().getSensor(
					MainMapActivity.this, 
					IbiSensorType.MICROPHONE, 
					new IbiSensorResolutionMicrophone(MICRO_SAMPLING_RATE, RECORDING_MS), 
					sensorListener);
			
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Initialize noise data trigger listener!");
			
			noiseDataTriggerListener = new IbiSensorDataTriggerListener() {
				
				@Override
				public void ibiSensorDataTrigger(IbiSensor ibiSensor,
						IbiSensorDataTrigger dataTrigger, IbiSensorData ibiSensorData) {
					//Reporting the noise to the server
					//Get noise level
					
					if (isNoiseChecked) {
						//Report noise only if the noise preference is checked
						IbiSensorDataMicrophone microData = (IbiSensorDataMicrophone) ibiSensorData;
						int noiseLevel = (int) microData.getAmplitude();
						
						if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Report noise : " + noiseLevel);
						
						//Cancel the previous noise reporting
						if (reportNoiseTask != null) reportNoiseTask.cancel(true);
						
						//Report noise
						reportNoiseTask = new IbicoopReportNoiseTask((
								Context) MainMapActivity.this, 
								Double.parseDouble(trueLocationLatitude), 
								Double.parseDouble(trueLocationLongitude),
								noiseLevel,
								reportNoiseCallback
								);
						AsyncTaskCompat.executeParallel(reportNoiseTask);
					}
					//End ibisensordatatrigger
				}
			};
			
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Initialize location sensor!");
            // Launch location sensor
			// We use network provider to reduce power consumption
            locationSensor = IbiSensorFactory.getInstance().getSensor(
            		MainMapActivity.this, 
            		IbiSensorType.LOCATION,
            		new IbiSensorResolutionLocation(
            				IbiSensorResolutionLocationAccuracyType.ACCURACY_COARSE, 
            				LOCATION_UPDATE_MS, 
            				LOCATION_UPDATE_DISTANCE), 
            		sensorListener);
			
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Initialize location data trigger listener!");
            
			
            locationDataTriggerListener = new IbiSensorDataTriggerListener() {
				
				@Override
				public void ibiSensorDataTrigger(IbiSensor ibiSensor,
						IbiSensorDataTrigger dataTrigger, IbiSensorData ibiSensorData) {
					//Convert ibisensor data to location data
					IbiSensorDataLocation locationData = ((IbiSensorDataLocation) ibiSensorData);
					//Get latitude and longitude
					trueLocationLatitude = String.valueOf(locationData.getLatitude());
					trueLocationLongitude = String.valueOf(locationData.getLongitude());
					
					trueLocationCity = TransportArea.getArea(Double.parseDouble(trueLocationLatitude), Double.parseDouble(trueLocationLongitude));
					
					TSLApplicationContext.getInstance().setTrueLocationCity(trueLocationCity);
					TSLApplicationContext.getInstance().setTrueLatitude(Double.parseDouble(trueLocationLatitude));
					TSLApplicationContext.getInstance().setTrueLongitude(Double.parseDouble(trueLocationLongitude));					
					
					if (IbicoopDataConstants. DEBUG_MODE) System.out.println("IbiSensorDataTrigger : New location data : " +
							"latitude = " + trueLocationLatitude + ", " +
							"longitude = " + trueLocationLongitude + ", " +
							"city = " + trueLocationCity);
					
					//RegionChannelSubscription
					if (!IbicoopDataConstants.SIMULATION_MODE) {
						if (IbicoopInit.getInstance().isStarted()) {
							if (regionChannelSubscriptionTask != null) regionChannelSubscriptionTask.cancel(true);
							regionChannelSubscriptionTask = new IbicoopRegionChannelSubscriptionTask(
									MainMapActivity.this, Double.parseDouble(trueLocationLatitude), 
									Double.parseDouble(trueLocationLongitude), USE_GLOBAL_CHANNEL,
									TSLApplicationContext.getInstance(), LocalStationVoteNotificationSender.class, LocalRequestNotificationSender.class);
							AsyncTaskCompat.executeParallel(regionChannelSubscriptionTask);
						}
					}
					
					if (trueLocationCity.equals(IbicoopDataConstants.PARIS)) {
						
						if (parisMetroDbManager != null) {
							//
							//
							//Remove paris stops from previous monitoring
							if (parisMetroDbManager.getStopInfoDbCount() > 0) {
								if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Remove previous paris stops from monitoring!");
								if (removeParisStopsFromRegionTask != null) removeParisStopsFromRegionTask.cancel(true);					
								removeParisStopsFromRegionTask = new IbicoopRemoveParisStopsFromRegionMonitoringTask(
										sysInRegionTrigger, 
										locationDatasList, 
										removeParisStopsCallback);
								
								AsyncTaskCompat.executeParallel(removeParisStopsFromRegionTask);						
								
								
								//
								//
								//Add paris stops in monitoring
								if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Add all paris stops in monitoring!");					
								
								ArrayList<ParisStopInfo> infos = parisMetroDbManager.readFromDbByCoord(Double.parseDouble(trueLocationLatitude), Double.parseDouble(trueLocationLongitude), Double.parseDouble(RADIUS));
								
								if (addParisStopsInRegionTask != null) addParisStopsInRegionTask.cancel(true);
								
								addParisStopsInRegionTask = 
										new IbicoopAddParisStopsInRegionMonitoringTask(
												sysInRegionTrigger, 
												infos,
												IbicoopDataConstants.MONITORING_RADIUS,
												addParisStopsCallback
												);
								
								AsyncTaskCompat.executeParallel(addParisStopsInRegionTask);							
							} //End if size > 0
							
						} //End paris db manager not null
						
					} else if (trueLocationCity.equals(IbicoopDataConstants.LONDON)) {
						
						if (londonMetroDbManager != null) {
							
							if (londonMetroDbManager.getStopInfoDbCount() > 0) {
								//
								//
								//Remove london stops from previous monitoring
								if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Remove previous london stops from monitoring!");
								
								if (removeLondonStopsFromRegionTask != null) removeLondonStopsFromRegionTask.cancel(true);					
								removeLondonStopsFromRegionTask = new IbicoopRemoveLondonStopsFromRegionMonitoringTask(
										sysInRegionTrigger, 
										locationDatasList, 
										removeLondonStopsCallback);
								
								AsyncTaskCompat.executeParallel(removeLondonStopsFromRegionTask);						
								
								
								//
								//
								//Add paris stops in monitoring
								if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Add all london stops in monitoring!");					
								
								ArrayList<LondonMetroStopInfo> infos = londonMetroDbManager.readFromDbByCoord(Double.parseDouble(trueLocationLatitude), Double.parseDouble(trueLocationLongitude), Double.parseDouble(RADIUS));
								
								if (addLondonStopsInRegionTask != null) addLondonStopsInRegionTask.cancel(true);
								
								addLondonStopsInRegionTask = 
										new IbicoopAddLondonStopsInRegionMonitoringTask(
												sysInRegionTrigger, 
												infos,
												IbicoopDataConstants.MONITORING_RADIUS,
												addLondonStopsCallback
												);
								
								AsyncTaskCompat.executeParallel(addLondonStopsInRegionTask);							
							} //End size > 0
							
						}//End london db manager not null
						
					} //End true location check
					
					runOnUiThread(new Runnable() {
						
						@Override
						public void run() {						
							//We don't want to move to current position to not preventing users
							//from navigation
							if (isTrackingCurrentPosition) {
								map.moveCamera(CameraUpdateFactory.newLatLngZoom(
										new LatLng(Double.parseDouble(trueLocationLatitude),
												Double.parseDouble(trueLocationLongitude)), 
												cameraLocationContext.getZoomFactor()));
							}			
							
							//Draw current position
							if (currentPositionMarker != null) {
								currentPositionMarker.remove();
							}
							
							if (currentPositionMarkerCircle != null) {
								currentPositionMarkerCircle.remove();
							}
							
							currentPositionMarker = map.addMarker(new MarkerOptions().position(
		        						new LatLng(Double.parseDouble(trueLocationLatitude), Double.parseDouble(trueLocationLongitude)))
		        						.title("Your current position").
		        						snippet("latitude = " + trueLocationLatitude + ", longitude = " + trueLocationLongitude)
		        						.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
							
							currentPositionMarkerCircle = map.addCircle(
									new CircleOptions()
									.center(new LatLng(Double.parseDouble(trueLocationLatitude), Double.parseDouble(trueLocationLongitude)))
									.radius(IbicoopDataConstants.MONITORING_RADIUS)
									.strokeWidth(2.5f)
									);
						}
					});

				}
			};//End new location data trigger
			
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Initialize location data trigger!");

			//Create new in region stop ids list		
			IbiSensorDataLocation initialLocationThreshold = new IbiSensorDataLocation(
					0.0, 0.0, 0.0, 0f, 0f, System.currentTimeMillis(), null, new IbiSensorUnitLocation());
			
			locationDataTrigger = new IbiSensorDataTimerNotAdaptativeNewDataTrigger(
					locationSensor, 
					initialLocationThreshold, 
					locationDataTriggerListener, 
					LOCATION_TRIGGER_DEFAULT_MS,
					-1);
	
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Initialize location sensor system in region data trigger listener!");
            // Initialize system in region data trigger listener
			
			inRegionListener = new IbiSensorDataInRegionSystemTriggerListener() {
				
				@Override
				public void ibiSensorDataInRegionTrigger(IbiSensor ibiSensor,
						IbiSensorDataInRegionSystemTrigger dataTrigger,
						IbiSensorDataLocation ibiSensorData) {
					
					//Get location data
					IbiSensorDataLocation locationData = ((IbiSensorDataLocation) ibiSensorData);
					//Get parameters which contain stops information
                    Map<String, String> parameters = locationData.getExtraParameters();
					

                    if (parameters != null) {
                    	
                    	TubeStation stationTempo = null;
                    	String stopIdTempo = null;
                    	String stopNameTempo = null;
                    	String latTempo = null;
                    	String lonTempo = null;
                    	
                    	if (trueLocationCity.equals(IbicoopDataConstants.PARIS)) {
                        	latTempo = parameters.get(IbicoopDataConstants.PARAM_KEY_STOP_LAT);
                        	lonTempo = parameters.get(IbicoopDataConstants.PARAM_KEY_STOP_LON);
                    	} else if (trueLocationCity.equals(IbicoopDataConstants.LONDON)) {
                        	latTempo = parameters.get("lat");
                        	lonTempo = parameters.get("lng");                  		
                    	}
                    	
                    	//Check if the trigger station is less than 200 m from the current location
    					if (distFrom(Double.parseDouble(latTempo), Double.parseDouble(lonTempo), 
    							Double.parseDouble(trueLocationLatitude), Double.parseDouble(trueLocationLongitude)) 
    							<= PROXIMITY_ALERT_ACTIVE_DISTANCE_CHECK) {
    						
	    					if (trueLocationCity.equals(IbicoopDataConstants.PARIS)) {
	    						//Location in region!
	    						if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Location in paris region!");
	    						
	                        	stopIdTempo = parameters.get(IbicoopDataConstants.PARAM_VALUE_STOP_ID);
	                        	stopNameTempo = parameters.get(IbicoopDataConstants.PARAM_KEY_STOP_NAME);
	                        	String routeId = parameters.get(IbicoopDataConstants.PARAM_KEY_ROUTE_ID);
	                        	String routeName = parameters.get(IbicoopDataConstants.PARAM_KEY_ROUTE_NAME);
	                        	String direction = parameters.get(IbicoopDataConstants.PARAM_KEY_DIRECTION_ID); 
	
	        			    	
								//Create tube
								Tube metro = new Tube(
										routeId, 
										routeName, 
										direction);
								
								Tube[] metros = {metro};
								//Create tube station
								stationTempo = new TubeStation(
										stopIdTempo, 
										stopNameTempo, 
										stopIdTempo, 
										Double.parseDouble(latTempo), 
										Double.parseDouble(lonTempo), 
										metros);
	
	    					} else if (trueLocationCity.equals(IbicoopDataConstants.LONDON)) {
	    						//Location in region!
	    						if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Location in london region!");
	                        	
	    						stopIdTempo = parameters.get("stc");
	                        	stopNameTempo = parameters.get("id");
	                        	final String stopNlc = parameters.get("nlc");
	                        	                   	
	                        	List<Tube> metrosList = new ArrayList<Tube>();
	                        	
	                        	for (int i = 1; i <= 5; i++) {
	                            	String routeId = parameters.get("line" + i);
	                            	String routeName = parameters.get("lineName" + i);
	                            	String direction = parameters.get("lineDirection" + i);
	                            	
	                            	if (!routeId.equals("*")) {
	                            		Tube metro = new Tube(routeId, routeName, direction);
	                            		metrosList.add(metro);
	                            	}
	                        	}
	                        	
	                        	final Tube[] metrosArray = new Tube[metrosList.size()];
	                        	
	                        	for (int j = 0; j < metrosList.size(); j++) {
	                        		metrosArray[j] = metrosList.get(j);
	                        	}
	                        	
								//Create tube station
								stationTempo = new TubeStation(
										stopIdTempo, 
										stopNameTempo,
										stopNlc, 
										Double.parseDouble(latTempo), 
										Double.parseDouble(lonTempo), 
										metrosArray);
	                        	
	    					} //End if city equals to London or Paris
    					
	                    	final TubeStation station = stationTempo;
	                    	final String stopId = stopIdTempo;
	                    	final String stopName = stopNameTempo;
	                    	final String lat = latTempo;
	                    	final String lon = lonTempo;
	                    	
    						//if the trigger station is 150 m away from the current location, real trigger
    						String info = "stopId = " + stopId + ", stopName = " + stopName;
    						
    						if (IbicoopDataConstants. DEBUG_MODE) {
    							System.out.println("In region trigger: " + info);
    							Toast.makeText(MainMapActivity.this, "In region trigger: " + info, Toast.LENGTH_SHORT).show();
    						}
    						
	                    	inRegionStopIdsList.add(stopId);
        					
        					if (isNoiseChecked) {
                                //Stop previous noise data trigger
                                //Launch noise reporting task / noise data trigger!
                    			//Create initial data threshold
                    			IbiSensorDataMicrophone initialNoiseThreshold = new IbiSensorDataMicrophone(
                                		0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0f, 
                                		System.currentTimeMillis(), 
                                		new IbiSensorUnitMicrophone());
                    			
                    			if (noiseDataTrigger != null) noiseDataTrigger.stop();
                    			
                    			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Create noise data trigger!");
                    			
                    			noiseDataTrigger =  new IbiSensorDataTimerNotAdaptativeNewDataTrigger(
                    					noiseSensor, 
                    					initialNoiseThreshold, noiseDataTriggerListener, SOUND_TRIGGER_DEFAULT_MS, SOUND_TRIGGER_TIMEOUT_MS);    						
        					} //End if noise checked

    					} //End if the trigger station is 150 m away from the current location
                    } //End if parameter is not null
				}

				@Override
				public void ibiSensorDataOutOfRegionTrigger(
						IbiSensor ibiSensor,
						IbiSensorDataInRegionSystemTrigger dataTrigger,
						IbiSensorDataLocation ibiSensorData) {

					//Get location data
					IbiSensorDataLocation locationData = ((IbiSensorDataLocation) ibiSensorData);
					//Get parameters which contain stops information
                    Map<String, String> parameters = locationData.getExtraParameters();

                    if (parameters != null) {

                    	String latTempo = null;
                    	String lonTempo = null;
                    	String stopIdTempo = null;
                    	
                    	if (trueLocationCity.equals(IbicoopDataConstants.PARIS)) {
                    		stopIdTempo = parameters.get(IbicoopDataConstants.PARAM_VALUE_STOP_ID);
                        	latTempo = parameters.get(IbicoopDataConstants.PARAM_KEY_STOP_LAT);
                        	lonTempo = parameters.get(IbicoopDataConstants.PARAM_KEY_STOP_LON);
                    	} else if (trueLocationCity.equals(IbicoopDataConstants.LONDON)) {
                    		stopIdTempo = parameters.get("stc");
                        	latTempo = parameters.get("lat");
                        	lonTempo = parameters.get("lng");                    		
                    	}
                    	
                    	//Check if the trigger station is more than 200 m from the current location
    					if (distFrom(Double.parseDouble(latTempo), Double.parseDouble(lonTempo), 
    							Double.parseDouble(trueLocationLatitude), Double.parseDouble(trueLocationLongitude)) 
    							> PROXIMITY_ALERT_ACTIVE_DISTANCE_CHECK) {
  
                        	final String stopId = stopIdTempo;
                        	
    						if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Stop noise data in region trigger when in region stop id list size = 0 or timeout");
    						
    						if (inRegionStopIdsList.contains(stopId)) {
    							inRegionStopIdsList.remove(stopId);
    						}
    						
    						if (inRegionStopIdsList.size() == 0)
    						{
    							if (noiseDataTrigger != null) {
    								if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Stop noise data trigger!");
    								noiseDataTrigger.stop();
    							}
    							
    						} //End in region stop ids list size = 0   						

    					} //End if the trigger station is more than 150 m from the current location

                    } //End if parameters not null			
				}
			};
			
			
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Initialize location sensor system in region data trigger!");
            // Initialize system in region data trigger
            sysInRegionTrigger = IbiSensorDataInRegionSystemTrigger.getInstance();
            sysInRegionTrigger.init(locationSensor, PROXIMITY_ALERT_ACTIVE_TIME_MS, inRegionListener);
            
			
            //if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Push location: lat = " + LAT_PARIS_TROCADERO + ", lon = " + LON_PARIS_TROCADERO);
            //mockLocationProvider.pushLocation(locationSensor, LAT_PARIS_TROCADERO, LON_PARIS_TROCADERO);

		} catch (Exception exception) {
			if (IbicoopDataConstants. DEBUG_MODE) System.err.println(exception.getMessage());
		}
	}
	
	private IbicoopReportNoiseCallback reportNoiseCallback = new IbicoopReportNoiseCallback() {
		
		@Override
		public void reportNoiseSuccess() {
			if (IbicoopDataConstants. DEBUG_MODE) {
				System.out.println("Report noise success");
				Toast.makeText(MainMapActivity.this, "Report noise success!", Toast.LENGTH_SHORT).show();
			} 
		}
		
		@Override
		public void reportNoiseFailed() {
			if (IbicoopDataConstants. DEBUG_MODE) {
				System.out.println("Report noise failed");
				Toast.makeText(MainMapActivity.this, "Report noise failed!", Toast.LENGTH_SHORT).show();	
			}
		}
	};
	
	/*
	private MockLocationUpdateCallback newLocationUpdateCallback = new MockLocationUpdateCallback() {
		
		@Override
		public void mockNewLocation(final MockLocationCoordinate coordinate) {
			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					cameraViewlLatitude = "" + coordinate.getLatitude();
					cameraViewLongitude = "" + coordinate.getLongitude();
					
					cameraViewCity = TransportArea.getArea(Double.parseDouble(cameraViewlLatitude), Double.parseDouble(cameraViewLongitude));
					TSLApplicationContext.getInstance().setCameraViewCity(cameraViewCity);
					
					//?Move to current location? on UI thread
					map.moveCamera(CameraUpdateFactory.newLatLngZoom(
							new LatLng(Double.parseDouble(cameraViewlLatitude),
									Double.parseDouble(cameraViewLongitude)), 
									cameraLocationContext.getZoomFactor()));
					
					//Draw current mock position
					if (currentPositionMarker != null) {
						currentPositionMarker.remove();
					}
					
					if (currentPositionMarkerCircle != null) {
						currentPositionMarkerCircle.remove();
					}
						
					currentPositionMarker = map.addMarker(new MarkerOptions().position(
        						new LatLng(Double.parseDouble(cameraViewlLatitude), Double.parseDouble(cameraViewLongitude)))
        						.title("Your current position").
        						snippet("latitude = " + cameraViewlLatitude + ", longitude = " + cameraViewLongitude)
        						.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
					
					currentPositionMarkerCircle = map.addCircle(
							new CircleOptions()
							.center(new LatLng(Double.parseDouble(cameraViewlLatitude), Double.parseDouble(cameraViewLongitude)))
							.radius(IbicoopDataConstants.MONITORING_RADIUS)
							);

		        	//Create or update bounding area
		        	if ((boundingArea == null) || (boundingArea.outOfTheArea(Double.parseDouble(cameraViewlLatitude), Double.parseDouble(cameraViewLongitude)))) {
		        		
		        		if (boundingArea == null) {
		        			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Create new bounding area");
		        			boundingArea = new BoundingArea(Double.parseDouble(cameraViewlLatitude), Double.parseDouble(cameraViewLongitude), Double.parseDouble(RADIUS));
		        		} else {
		        			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Out of bounding area, update bounding area");
		        			boundingArea.updateBoundingArea(Double.parseDouble(cameraViewlLatitude), Double.parseDouble(cameraViewLongitude), Double.parseDouble(RADIUS));
		        		}
		        		
		        		if (nearbyMarkerCircle != null) nearbyMarkerCircle.remove();
		        		
						nearbyMarkerCircle = map.addCircle(
								new CircleOptions()
								.center(new LatLng(Double.parseDouble(cameraViewlLatitude), Double.parseDouble(cameraViewLongitude)))
								.radius(Double.parseDouble(RADIUS))
								);
		        		
	        			//Remove previous marker circle
			        	if (nearbyMarkerRectangle != null) nearbyMarkerRectangle.remove();
			        	
			        	nearbyMarkerRectangle = map.addPolyline(new PolylineOptions()
			        		.add(
			        				new LatLng(boundingArea.getMinLat(), boundingArea.getMinLon()),
			        				new LatLng(boundingArea.getMinLat(), boundingArea.getMaxLon()),
			        				new LatLng(boundingArea.getMaxLat(), boundingArea.getMaxLon()),
			        				new LatLng(boundingArea.getMaxLat(), boundingArea.getMinLon()),
			        				new LatLng(boundingArea.getMinLat(), boundingArea.getMinLon())
			        				)
			        			);
			        	
						//Remove previous marker
						for (Marker marker: nearbyMetroMarkersList) {
							marker.remove();
						}
						
						
						//
						//
						//Remove stops from previous monitoring
						if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Remove previous stops from monitoring!");
						if (removeParisStopsFromRegionTask != null) removeParisStopsFromRegionTask.cancel(true);					
						removeParisStopsFromRegionTask = new IbicoopRemoveParisStopsFromRegionMonitoringTask(
								sysInRegionTrigger, 
								locationDatasList, 
								removeParisStopsCallback);
						
						AsyncTaskCompat.executeParallel(removeParisStopsFromRegionTask);
						
						
						//
						//
						//Add stops in monitoring
						if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Add stops in monitoring!");
						ArrayList<ParisStopInfo> infos = parisMetroDbManager.readFromDbByCoord(Double.parseDouble(cameraViewlLatitude), Double.parseDouble(cameraViewLongitude), Double.parseDouble(RADIUS));
						
						if (addParisStopsInRegionTask != null) addParisStopsInRegionTask.cancel(true);
						
						addParisStopsInRegionTask = 
								new IbicoopAddParisStopsInRegionMonitoringTask(
										sysInRegionTrigger, 
										infos,
										IbicoopDataConstants.MONITORING_RADIUS,
										addParisStopsCallback
										);
						
						AsyncTaskCompat.executeParallel(addParisStopsInRegionTask);
						
						//Fix on same latitude and longitude problem
						double lonOffset = 0.00015;
						
			        	for (ParisStopInfo stop : infos) {
			        		//Inverse the sign
			        		lonOffset = lonOffset*-1.0;
			        		
							Marker nearbyMarker = map.addMarker(new MarkerOptions().position(
		    						new LatLng(stop.getLatitude(), (stop.getLongitude() + lonOffset)))
		    						.title(stop.getStopName() +  " >").
		    						snippet("latitude = " + stop.getLatitude() + ", longitude = " + stop.getLongitude())
		    						.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
							nearbyMetroMarkersList.add(nearbyMarker);
							//Create tube
							Tube metro = new Tube(
									stop.getRouteId(), 
									stop.getRouteName(), 
									"" + stop.getDirection());
							Tube[] metros = {metro};
							//Create tube station
							TubeStation station = new TubeStation(
									stop.getStopId(), 
									stop.getStopName(), 
									stop.getStopId(), 
									stop.getLatitude(), 
									stop.getLongitude(), 
									metros);
							//Insert tube station into marker map
							nearbyMetroStopsMap.put(nearbyMarker, station);
			        	}
		        	} 
		        	
				}
			});
			
		}
	};
	*/
	
	private AddLondonStopsInRegionMonitoringCallback addLondonStopsCallback = new AddLondonStopsInRegionMonitoringCallback() {
		
		@Override
		public void addStopsInRegionMonitoringOK(ArrayList<IbiSensorDataLocation> locationDatas) {
			if (IbicoopDataConstants. DEBUG_MODE) {
				System.out.println("Add london stops in region monitoring ok");
			}
			locationDatasList.addAll(locationDatas);
		}
		
		@Override
		public void addStopsInRegionMonitoringFailed() {
			if (IbicoopDataConstants. DEBUG_MODE) System.err.println("Add london stops in region monitoring failed");
			}
	};
	
	private RemoveLondonStopsFromRegionMonitoringCallback removeLondonStopsCallback = new RemoveLondonStopsFromRegionMonitoringCallback() {
		
		@Override
		public void removeStopsFromRegionMonitoringOK() {
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Remove london stops from region monitoring ok");
			
			//Clear in monitoring location data list
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Clear in london monitoting data list");
			locationDatasList.clear();
		}
		
		@Override
		public void removeStopsFromRegionMonitoringFailed() {
			if (IbicoopDataConstants. DEBUG_MODE) System.err.println("Remove london stops from region monitoring failed");		}
	};	
	
	
	
	private AddParisStopsInRegionMonitoringCallback addParisStopsCallback = new AddParisStopsInRegionMonitoringCallback() {
		
		@Override
		public void addStopsInRegionMonitoringOK(ArrayList<IbiSensorDataLocation> locationDatas) {
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Add paris stops in region monitoring ok");
			locationDatasList.addAll(locationDatas);
		}
		
		@Override
		public void addStopsInRegionMonitoringFailed() {
			if (IbicoopDataConstants. DEBUG_MODE) System.err.println("Add paris stops in region monitoring failed");
		}
	};
	
	private RemoveParisStopsFromRegionMonitoringCallback removeParisStopsCallback = new RemoveParisStopsFromRegionMonitoringCallback() {
		
		@Override
		public void removeStopsFromRegionMonitoringOK() {
			if (IbicoopDataConstants. DEBUG_MODE) {
				System.out.println("Remove paris stops from region monitoring ok");	
				//Clear in monitoring location data list
				if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Clear in paris monitoting data list");
			}

			locationDatasList.clear();
		}
		
		@Override
		public void removeStopsFromRegionMonitoringFailed() {
			if (IbicoopDataConstants. DEBUG_MODE) System.err.println("Remove paris stops from region monitoring failed");		}
	};
	
    private IbicoopStartCallback ibicoopStartCallback = new IbicoopStartCallback() {
		
		@Override
		public void ibicoopStartOk() {
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Ibicoop start ok");
			
			//Simulation only
			if (IbicoopDataConstants.SIMULATION_MODE) {
				
				if (regionChannelSubscriptionTask != null) regionChannelSubscriptionTask.cancel(true);
				regionChannelSubscriptionTask = new IbicoopRegionChannelSubscriptionTask(
						MainMapActivity.this, Double.parseDouble(cameraViewLatitude), Double.parseDouble(cameraViewLongitude),
						USE_GLOBAL_CHANNEL, TSLApplicationContext.getInstance(), LocalStationVoteNotificationSender.class,
						LocalRequestNotificationSender.class);
				AsyncTaskCompat.executeParallel(regionChannelSubscriptionTask);
				
				if (cameraViewLatitude != null && cameraViewLongitude != null) {
					//Draw current simulation position
					if (simuPositionMarker != null) {
						simuPositionMarker.remove();
					}
					
					if (simuPositionMarkerCircle != null) {
						simuPositionMarkerCircle.remove();
					}
					
					simuPositionMarker = map.addMarker(new MarkerOptions().position(
								new LatLng(Double.parseDouble(cameraViewLatitude), Double.parseDouble(cameraViewLongitude)))
								.title("Your simulation position").
								snippet("latitude = " + cameraViewLatitude + ", longitude = " + cameraViewLongitude)
								.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
					
					simuPositionMarkerCircle = map.addCircle(
							new CircleOptions()
							.center(new LatLng(Double.parseDouble(cameraViewLatitude), Double.parseDouble(cameraViewLongitude)))
							.radius(IbicoopDataConstants.MONITORING_RADIUS)
							.strokeWidth(2.5f)
							);
				}
			} else {
				if (trueLocationLatitude != null && trueLocationLongitude != null) {
					if (regionChannelSubscriptionTask != null) regionChannelSubscriptionTask.cancel(true);
					regionChannelSubscriptionTask = new IbicoopRegionChannelSubscriptionTask(
							MainMapActivity.this, Double.parseDouble(trueLocationLatitude), Double.parseDouble(trueLocationLongitude),
							USE_GLOBAL_CHANNEL, TSLApplicationContext.getInstance(), LocalStationVoteNotificationSender.class,
							LocalRequestNotificationSender.class);
					AsyncTaskCompat.executeParallel(regionChannelSubscriptionTask);
				}
			}
		}
		
		@Override
		public void ibicoopStartFailed() {
			if (IbicoopDataConstants. DEBUG_MODE) System.err.println("Ibicoop start failed");
		}
	};
    
	
        
	//
	//
	//Thales preferences
	/**
	 * Initalize Thales preferences
	 */
	private void initPreferences() {
		if (initPreferencesTask != null) initPreferencesTask.cancel(true);
		PreferenceWrapper.createPreferenceCallbackBinder(this);
		initPreferencesTask =  new InitPreferencesTask();
		AsyncTaskCompat.executeParallel(initPreferencesTask);
	}

	class InitPreferencesTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			
			try {
				
				PreferenceConfigurationIntf _preferenceConfiguration = TSLApplicationContext.getInstance().getPreferenceConfiguration();
				
				if (_preferenceConfiguration == null) {
					
					_preferenceConfiguration = PreferenceWrapper.createPreferenceConfiguration(MainMapActivity.this);
					if (_preferenceConfiguration == null) return null;
					
					
					TSLApplicationContext.getInstance().setPreferenceConfiguration(_preferenceConfiguration);
				}
				
				if (IbicoopDataConstants. DEBUG_MODE) System.out.println("preferenceConfiguration = " + _preferenceConfiguration);

				
				if (_preferenceConfiguration.isBoundForServiceBinder()) {
					
					if (IbicoopDataConstants. DEBUG_MODE) System.out.println("binder is bound");
					
					processPreference(_preferenceConfiguration.getTransportPreference());
					
				} else {
					
					if (IbicoopDataConstants. DEBUG_MODE) System.out.println("binder is not bound");
					
//					binder.bind(MainMapActivity.this, bindingCallback);
					PreferenceWrapper.bind(_preferenceConfiguration, MainMapActivity.this);

				}
				
			} catch (Exception exception) {
				if (IbicoopDataConstants. DEBUG_MODE) System.err.println("InitPreferences exception: " + exception.getMessage());
			}
			
			return null;
		}
	}
	
	public void processPreference(TransportPreferenceIntf preference) {
		if (IbicoopDataConstants. DEBUG_MODE) {
			System.out.println("Process preference");
			System.out.println(preference.toString());
		}

		isBikeChecked = preference.getBikePref();
		isBusChecked = preference.getBusPref();
		isTramChecked = preference.getTramPref();
		isTubeChecked = preference.getTubePref();
		isNoiseChecked = preference.getNoisePref();
		isTrackingCurrentPosition = preference.isTrackingCurrentPosition();
		isSimulationMode = preference.isSimulationMode();
		
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				refreshTransportDisplay();				
			}
		});
	}
	
	private void refreshTransportDisplay() {
		
		if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Refresh transport display");
		
    	//Show marker
		bikeToggle.setChecked(isBikeChecked);
		for(Marker marker: nearbyBikeMarkersList){
			marker.setVisible(isBikeChecked);
		}
		
		busToggle.setChecked(isBusChecked);
		for(Marker marker: nearbyBusMarkersList){
			marker.setVisible(isBusChecked);
		}
		
		tramToggle.setChecked(isTramChecked);
		//FIXME
		
		metroToggle.setChecked(isTubeChecked);
		for(Marker marker: nearbyMetroMarkersList){
			marker.setVisible(isTubeChecked);
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main_map, menu);
		getSupportMenuInflater().inflate(R.menu.main_map, menu);
		// Associate searchable configuration with the SearchView
	    searchManager =
	           (SearchManager) getSystemService(Context.SEARCH_SERVICE);
	    
	    searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
	    //Enable submit button
	    //searchView.setSubmitButtonEnabled(true);
	    searchView.setQueryHint("Address");
	    	    
	   	//Only can be used by sdk version >= 11
	   	searchManager.setOnDismissListener(this);
	    searchManager.setOnCancelListener(this);
	    searchView.setOnCloseListener(this);
	    searchView.setOnQueryTextListener(this);
	    searchView.setSearchableInfo(
	            searchManager.getSearchableInfo(getComponentName()));

	    //Refresh item
	    refreshItem = menu.findItem(R.id.action_refresh);
	    
	    if (isWaitingForRefresh) {
	    	launchRefresh();
	    }
	    
		return true;
	}
	
	@SuppressLint("NewApi")
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case R.id.action_search:
	            //openSearch();
	        	if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Action search");
	            return true;
	        case R.id.action_refresh:
	        	//Relaunch all tasks
	        	if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Action refresh");
	        	refreshAllNearbyStops(cameraViewCity);
	            return true;
	        case R.id.action_route_planner:
	        	//Relaunch all tasks
	        	if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Action route planner");
    	
				if (checkInternetConnection()) {
					//Got internet connection
					if (!IbicoopInit.getInstance().isStarted()) {
						if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Start ibicoop!");
						if (ibicoopStartStopTask != null) ibicoopStartStopTask.cancel(true);
						ibicoopStartStopTask = createIbicoopStartStopTask(ibicoopContext, false, ibicoopStartCallback);
						AsyncTaskCompat.executeParallel(ibicoopStartStopTask);
					}
					
		        	Intent routePlannerIntent = new Intent(this, RoutePlannerActivity.class);
		        	startActivity(routePlannerIntent);
		        	
				} else {
					//No internet connection
					showNoInternetDialog();
				}
				
	            return true;	            
	        case R.id.action_preferences:
	        	if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Action preferences");
	        	Intent intent = new Intent(this, TravelPreferenceActivity.class);
	        	startActivity(intent);
	            return true;	            
	        case R.id.action_status:
	        	if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Action metro status");
	        	
				if (checkInternetConnection()) {
					//Got internet connection
					if (!IbicoopInit.getInstance().isStarted()) {
						if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Start ibicoop!");
						if (ibicoopStartStopTask != null) ibicoopStartStopTask.cancel(true);
						ibicoopStartStopTask = createIbicoopStartStopTask(ibicoopContext, false, ibicoopStartCallback);
						AsyncTaskCompat.executeParallel(ibicoopStartStopTask);
					}
					
		        	Intent statusIntent = new Intent(this, TubeLineActivity.class);
		        	startActivity(statusIntent);
		        	
				} else {
					//No internet connection
					showNoInternetDialog();
				}
				
	            return true;	            
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	
	@Override
	public void onResume() {
		super.onResume();
		
		
		if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Init preferences");
		//Get preference
		initPreferences();

		 cameraLocationContext = CameraLocationContext.getInstance();
		
		if ((cameraLocationContext.getLatitude() != 0.0) && (cameraLocationContext.getLongitude() != 0.0)) {
			//Location already saved
			map.moveCamera(CameraUpdateFactory.newLatLngZoom(
					new LatLng(cameraLocationContext.getLatitude(), cameraLocationContext.getLongitude()), cameraLocationContext.getZoomFactor()));
		}
		else {
			//Location not yet saved
			cameraLocationContext.saveLatitude(Double.parseDouble(cameraViewLatitude));
			cameraLocationContext.saveLongitude(Double.parseDouble(cameraViewLongitude));
			float zoom = map.getCameraPosition().zoom;
			if (zoom < 15) {
				cameraLocationContext.saveZoomFactor(15);
			} else {
				cameraLocationContext.saveZoomFactor(zoom);
			}

			map.moveCamera(CameraUpdateFactory.newLatLngZoom(
					new LatLng(Double.parseDouble(cameraViewLatitude),Double.parseDouble(cameraViewLongitude)), cameraLocationContext.getZoomFactor()));			
		}
		
		/**
		 * Start ibicoop
		 */
		if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Start ibicoop!");
		if (ibicoopStartStopTask != null) ibicoopStartStopTask.cancel(true);
		ibicoopStartStopTask = createIbicoopStartStopTask(ibicoopContext, false, ibicoopStartCallback);
		AsyncTaskCompat.executeParallel(ibicoopStartStopTask);
	}
	
	@Override
	protected void onDestroy() {
		
		//Stop Thales preference service
		PreferenceConfigurationIntf preferenceConfiguration = TSLApplicationContext.getInstance().getPreferenceConfiguration();
		
		if (preferenceConfiguration != null) {
			
			if (preferenceConfiguration.isBoundForServiceBinder()) {
				
				if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Unbound the service");
				
				PreferenceWrapper.unbind(preferenceConfiguration, MainMapActivity.this);
				
			} 
		}

		
		stopUpdateBikeExecutor();
		
		/**
		 * Stop ibicoop
		 */
		if (regionChannelSubscriptionTask != null) regionChannelSubscriptionTask.cancel(true);
		
		if (ibicoopAddAllParisMetroStopsInDbTask != null) ibicoopAddAllParisMetroStopsInDbTask.cancel(true);
		
		if (addParisStopsInRegionTask != null) addParisStopsInRegionTask.cancel(true);
		if (ibicoopAddAllLondonMetroStopsInDbTask != null) ibicoopAddAllLondonMetroStopsInDbTask.cancel(true);

		
		if (locationDatasList.size() > 0) {
			
			if (trueLocationCity.equals(IbicoopDataConstants.LONDON)) {
				
				if (removeLondonStopsFromRegionTask != null) removeLondonStopsFromRegionTask.cancel(true);	
				
				removeLondonStopsFromRegionTask = new IbicoopRemoveLondonStopsFromRegionMonitoringTask(
						sysInRegionTrigger, 
						locationDatasList, 
						removeLondonStopsCallback);
				
				AsyncTaskCompat.executeParallel(removeLondonStopsFromRegionTask);
				
			} else if (trueLocationCity.equals(IbicoopDataConstants.PARIS)) {
				
				if (removeParisStopsFromRegionTask != null) removeParisStopsFromRegionTask.cancel(true);
				
				removeParisStopsFromRegionTask = new IbicoopRemoveParisStopsFromRegionMonitoringTask(
						sysInRegionTrigger, 
						locationDatasList, 
						removeParisStopsCallback);					
				
				//Remove all paris stops from in region triggering
				AsyncTaskCompat.executeParallel(removeParisStopsFromRegionTask);
			}
		}


		
		if (mockLocationSimulator != null) {
			mockLocationSimulator.stopSimulation();
		}
		
		if (noiseDataTrigger != null) {
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Stop noise data trigger");
			noiseDataTrigger.stop();
		}
		
		if (noiseSensor != null) {
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Stop noise sensor");
			try {
				noiseSensor.stop();
			} catch (Exception exception) {
				if (IbicoopDataConstants. DEBUG_MODE) System.err.println(exception.getMessage());
			}
		}
				
		if (locationDataTrigger != null) {
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Stop location data trigger");
			locationDataTrigger.stop();
		}
		
		if (locationSensor != null) {
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Stop location sensor");
			try {
				locationSensor.stop();
			} catch (Exception exception) {
				if (IbicoopDataConstants. DEBUG_MODE) System.err.println(exception.getMessage());
			}
		}
		
		TSLApplicationContext.getInstance().setContext(null);
		
		if (ibicoopStartStopTask != null) ibicoopStartStopTask.cancel(true);
		ibicoopStartStopTask = createIbicoopStartStopTask(ibicoopContext, true, null);
		AsyncTaskCompat.executeParallel(ibicoopStartStopTask);
		
		if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Stop MainMapActivity");
		
		super.onDestroy();
	}
	
	public void launchRefresh() {
		
		if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Launch refresh");
		
		if (!isRefreshing) {
	    	//Set image view animation
	        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	        ImageView iv = (ImageView) inflater.inflate(R.layout.refresh_action_view, null);
	        Animation rotation = AnimationUtils.loadAnimation(this, R.drawable.refresh_animation);
	        rotation.setRepeatCount(Animation.INFINITE);
	        iv.startAnimation(rotation);
	        if (refreshItem != null) {
	        	refreshItem.setActionView(iv);
	        	isRefreshing = true;
				if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Is Refeshing ok!");
	        } else {
	        	if (IbicoopDataConstants. DEBUG_MODE) System.err.println("Refresh item is null");
	        	isWaitingForRefresh = true;
	        }
	        
		}
	}
	
	public void completeRefresh() {
		
		if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Complete refresh");
		
		if (isRefreshing) {
			if (refreshItem != null) {
			    View actionView = refreshItem.getActionView();
			    if (actionView != null) {
			    	actionView.clearAnimation();
				    refreshItem.setActionView(null);
					isRefreshing = false;
		        	isWaitingForRefresh = false;
					if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Complete refresh ok");
			    } 
			} else {
				if (IbicoopDataConstants. DEBUG_MODE) System.err.println("Refresh item is null");  
			}
		}
	}
	
	public void zoomToAddress(double latitude, double longitude, float zoom) {
        
       CancelableCallback callback =  new CancelableCallback() {
			@Override
			public void onCancel() {}

			@Override
			public void onFinish() {}	
       };
   	
        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(latitude, longitude)).zoom(zoom).build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), MAP_ANIMATION_DURATION_MS, callback);
	}

	public void onCameraChange(final CameraPosition position) {
		
		double newLatitude = position.target.latitude;
		double newLongitude = position.target.longitude;
		double dist = distFrom(newLatitude, newLongitude, Double.parseDouble(cameraViewLatitude), Double.parseDouble(cameraViewLongitude));
	
		TSLApplicationContext.getInstance().setCameraLatitude(newLatitude);
		TSLApplicationContext.getInstance().setCameraLongitude(newLongitude);		
		
		// Save current camera position coordinate
		if (cameraLocationContext != null) {
			cameraLocationContext.saveLatitude(newLatitude); 
			cameraLocationContext.saveLongitude(newLongitude);
			cameraLocationContext.saveZoomFactor(position.zoom);
		} 
		
		//Simulation only
		if (IbicoopDataConstants.SIMULATION_MODE) {

			if (IbicoopInit.getInstance().isStarted()) {
				if (regionChannelSubscriptionTask != null) regionChannelSubscriptionTask.cancel(true);
				regionChannelSubscriptionTask = new IbicoopRegionChannelSubscriptionTask(this, newLatitude, newLongitude, USE_GLOBAL_CHANNEL,
						TSLApplicationContext.getInstance(), LocalStationVoteNotificationSender.class, LocalRequestNotificationSender.class);
				AsyncTaskCompat.executeParallel(regionChannelSubscriptionTask);
			}
			
			//Draw current simulation position
			if (simuPositionMarker != null) {
				simuPositionMarker.remove();
			}
			
			if (simuPositionMarkerCircle != null) {
				simuPositionMarkerCircle.remove();
			}
			
			simuPositionMarker = map.addMarker(new MarkerOptions().position(
						new LatLng(newLatitude, newLongitude))
						.title("Your simulation position").
						snippet("latitude = " + newLatitude + ", longitude = " + newLongitude)
						.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
			
			simuPositionMarkerCircle = map.addCircle(
					new CircleOptions()
					.center(new LatLng(newLatitude, newLongitude))
					.radius(IbicoopDataConstants.MONITORING_RADIUS)
					.strokeWidth(2.5f)
					);
		}
		
		if(dist > Double.parseDouble(RADIUS)) {	
			
			cameraViewLatitude = Double.toString(newLatitude);	
			cameraViewLongitude = Double.toString(newLongitude);

			cameraViewCity = TransportArea.getArea(Double.parseDouble(cameraViewLatitude), Double.parseDouble(cameraViewLongitude));
			TSLApplicationContext.getInstance().setCameraViewCity(cameraViewCity);	
			
			refreshAllNearbyStops(cameraViewCity);
		}
	}
	
	private static double distFrom(double lat1, double lng1, double lat2, double lng2) {
	    double earthRadius = 6371000;
	    double dLat = Math.toRadians(lat2-lat1);
	    double dLng = Math.toRadians(lng2-lng1);
	    double sindLat = Math.sin(dLat / 2);
	    double sindLng = Math.sin(dLng / 2);
	    double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
	            * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	    double dist = earthRadius * c;

	    return dist;
	    }
	
	public void showNoInternetDialog(){
		AlertDialog.Builder dialog = AlertDialogCompat.getAlertBuilder(this);
		
		dialog.setTitle("Internet connection!");
		dialog.setMessage("No internet connection was dectected.Please enable mobile internet or wifi to use map capabilities");
		dialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {}
		});

		dialog.show();
	}

	public boolean checkInternetConnection(){
		ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext()
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		boolean wifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
				.isConnectedOrConnecting();

		boolean internet = false;
		if(!wifi) { 
			if(connectivityManager.getActiveNetworkInfo() != null){
			internet  = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
				.isConnectedOrConnecting();
			}
		}

		if(!internet && !wifi)
		{
			return false;
		}

		return true;
	}

	protected boolean isBetterLocation(Location newLocation_, Location currentBestLocation_) {
		if (currentBestLocation_ == null) {
			// A new location is always better than no location
			return true;
		}

		// Check whether the new location fix is newer or older
		long timeDelta = newLocation_.getTime() - currentBestLocation_.getTime();
		boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
		boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
		boolean isNewer = timeDelta > 0;

		// If it's been more than two minutes since the current location, use the new location
		// because the user has likely moved
		if (isSignificantlyNewer) {
			return true;
			// If the new location is more than two minutes older, it must be worse
		} else if (isSignificantlyOlder) {
			return false;
		}

		// Check whether the new location fix is more or less accurate
		int accuracyDelta = (int) (newLocation_.getAccuracy() - currentBestLocation_.getAccuracy());
		boolean isLessAccurate = accuracyDelta > 0;
		boolean isMoreAccurate = accuracyDelta < 0;
		boolean isSignificantlyLessAccurate = accuracyDelta > 200;

		// Determine location quality using a combination of timeliness and accuracy
		if (isMoreAccurate) {
			return true;
		} else if (isNewer && !isLessAccurate) {
			return true;
		} else if (isNewer && !isSignificantlyLessAccurate) {
			return true;
		}
		return false;
	}

	
	@Override
	public boolean onMarkerClick(Marker marker) {
	
		if (IbicoopDataConstants. DEBUG_MODE) System.out.println("On marker click");
		
		if (nearbyMetroMarkersList.contains(marker)) {
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Nearby metro marker");	
			marker.showInfoWindow();
		}
		
		if (nearbyBusMarkersList.contains(marker)) {
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Nearby bus marker");
			marker.showInfoWindow();
		}
		
		if (nearbyBikeMarkersList.contains(marker)) {
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Nearby bike marker");
			marker.showInfoWindow();
		}

		if (currentPositionMarker != null) {
			if (currentPositionMarker.equals(marker)) {
				if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Current position marker");
				marker.showInfoWindow();		
			}
		}

		return true;
	}
	
	//On info window click listener
	private OnInfoWindowClickListener infoWindowListener = new OnInfoWindowClickListener() {

		@Override
		public void onInfoWindowClick(Marker marker) {
			
			if (nearbyMetroMarkersList.contains(marker)) {
				
				if (IbicoopDataConstants. DEBUG_MODE) System.out.println("On metro info window click");
				
				TubeStation metroStop = nearbyMetroStopsMap.get(marker);
				Gson gson = new Gson();
				String metroStopDesc = gson.toJson(metroStop);
				if (IbicoopDataConstants. DEBUG_MODE) System.out.println("MainMapActivity : Metro stop desc = " + metroStopDesc);
				
				if (checkInternetConnection()) {
					//Got internet connection
					
					if (!IbicoopInit.getInstance().isStarted()) {
						if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Start ibicoop!");
						if (ibicoopStartStopTask != null) ibicoopStartStopTask.cancel(true);
						ibicoopStartStopTask = createIbicoopStartStopTask(ibicoopContext, false, ibicoopStartCallback);
						AsyncTaskCompat.executeParallel(ibicoopStartStopTask);
					}
					
					Intent intent = new Intent(MainMapActivity.this, InfosDashboardActivity.class);
					intent.putExtra(IntentConstants.TUBE_STATION_GSON, metroStopDesc);
					startActivity(intent);

				} else {
					//No internet connection
					showNoInternetDialog();
				}
			}
			
			if (nearbyBusMarkersList.contains(marker)) {
				
				if (IbicoopDataConstants. DEBUG_MODE) System.out.println("On bus info window click");
				
				BusStation busStop = nearbyBusStopsMap.get(marker);
				Gson gson = new Gson();
				String busStopDesc = gson.toJson(busStop);
				if (IbicoopDataConstants. DEBUG_MODE) System.out.println("MainMapActivity : Bus stop desc = " + busStopDesc);
				
				if (checkInternetConnection()) {
					//Got internet connection
					
					if (!IbicoopInit.getInstance().isStarted()) {
						if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Start ibicoop!");
						if (ibicoopStartStopTask != null) ibicoopStartStopTask.cancel(true);
						ibicoopStartStopTask = createIbicoopStartStopTask(ibicoopContext, false, ibicoopStartCallback);
						AsyncTaskCompat.executeParallel(ibicoopStartStopTask);
					}
					
					Intent intent = new Intent(MainMapActivity.this, BusCountdownActivity.class);
					intent.putExtra(IntentConstants.BUS_STATION_GSON, busStopDesc);
					startActivity(intent);

				} else {
					//No internet connection
					showNoInternetDialog();
				}				
			}
		}
	};
	
	/**
	 * Address list on item click listener
	 */
	OnItemClickListener addressItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("On item click");
			
			if ((targetStringAddressMap != null) && (targetStringAddressMap.size() > 0)) {
				Address address = targetIntegerAddressMap.get(position);
				
				if (address == null) return;
				
				String addressString = address.getFeatureName();
				
				if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Address = " + addressString);
				
				searchView.setQuery(addressString, true);
				
				addressListView.setAdapter(null);
				addressListView.setVisibility(View.GONE);
				
				
				//Hide the keyboard when the user press on go or search button
				InputMethodManager imm = (InputMethodManager)getSystemService(
					      Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(searchView.getWindowToken(), 0);
				
				//Zoom to the address
				float zoom = map.getCameraPosition().zoom;
				zoomToAddress(address.getLatitude(), address.getLongitude(), zoom);
			}
		}
	};
	
	/**
	 * SearchView.OnQueryTestListener override method
	 */
	@Override
	public boolean onQueryTextChange(String newText) {
		if (IbicoopDataConstants. DEBUG_MODE) System.out.println("onQueryTextChange = " + newText);
		
		//We give suggestion only when we have at least four letters
		if (newText.length() < 4) {
			addressListView.setAdapter(null);
			addressListView.setVisibility(View.GONE);
			//Return true to indicate that we hace handled the action
        	return true;
		}
		
		try {
			
			if (checkInternetConnection()) {
		        final List<Address> addresses = geoCoder.getFromLocationName(newText, 10);
		        
		        //Return when there is no address found
		        if (addresses.size() <= 0) {
					addressListView.setAdapter(null);
					addressListView.setVisibility(View.GONE);
					//Return true to indicate that we hace handled the action
		        	return true;
		        }
		        
		        targetStringAddressMap = new LinkedHashMap<String, Address>();
		        targetIntegerAddressMap = new LinkedHashMap<Integer, Address>();
		        
				//Show dialog to make more choices
				//CharSequence[] adrs = new CharSequence[addresses.size()];
		        List<String> adrs = new ArrayList<String>();
				
				for (int j = 0; j < addresses.size(); j++) {
					adrs.add(addresses.get(j).getFeatureName());
					//Insert target address in the list
					targetIntegerAddressMap.put(j, addresses.get(j));
					targetStringAddressMap.put(addresses.get(j).getFeatureName(), addresses.get(j));
				}

				ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainMapActivity.this, R.layout.custom_search, adrs);
				addressListView.setAdapter(adapter);
				addressListView.setVisibility(View.VISIBLE);				
			}
			
		} catch (Exception exception) {
			if (IbicoopDataConstants. DEBUG_MODE) System.err.println(exception.getMessage());
		}

		//Return true to indicate that we hace handled the action
		return true;
	}

	@SuppressLint("NewApi")
	@Override
	public boolean onQueryTextSubmit(String query) {
		if (IbicoopDataConstants. DEBUG_MODE) System.out.println("onQueryTextSubmit = " + query);
		Address address = targetStringAddressMap.get(query);
		
		if (address != null) {
			
			if (checkInternetConnection()) {

				searchView.setQuery("", false);
				searchView.onActionViewCollapsed();
				
				//Hide the keyboard when the user press on go or search button
				InputMethodManager imm = (InputMethodManager)getSystemService(
					      Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(searchView.getWindowToken(), 0);
				
				//Zoom to the address
				float zoom = map.getCameraPosition().zoom;
				zoomToAddress(address.getLatitude(), address.getLongitude(), zoom);
				
			} else {
				showNoInternetDialog();
			}

		} else {
			//Wrong address
			Toast.makeText(MainMapActivity.this, "Invalid address!", Toast.LENGTH_LONG).show();
		}		
		
		//Make sure the keyboard is really hidden
		//searchView.setQuery("", false);
		
		//Return true to indicare that we have handled the submit request
		return true;
	}


	/**
	 * Get search dialog close event
	 */
	@Override
	public void onDismiss() {
		if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Search dialog is dismissed!");
	}


	/**
	 * Get search dialog cancel event
	 */
	@Override
	public void onCancel() {
		if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Search dialog is canceled!");
	}

	/**
	 * Get search dialog close event
	 */
	@Override
	public boolean onClose() {
		if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Search dialog is on close!");
		//True if the listener wants to override the default behavior of clearing the text field
		return false;
	}

	private IbicoopStartStopTask createIbicoopStartStopTask(Context ibicoopContext, boolean isStopping, IbicoopStartCallback ibicoopStartCallback) {
		return new IbicoopStartStopTask(ibicoopContext, isStopping, ibicoopStartCallback,
				TSLApplicationContext.getInstance(), LocalStationVoteNotificationSender.class, LocalRequestNotificationSender.class);
	}
}