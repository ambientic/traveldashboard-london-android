package traveldashboardlondon.app;

import java.io.IOException;
import java.util.List;

import org.inbicoop.preference.PreferenceConfigurationIntf;
import org.inbicoop.preference.TransportPreferenceIntf;


import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;

import traveldashboard.compat.AsyncTaskCompat;
import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.data.LondonMetroDbManager;
import traveldashboard.ibicooptask.IbicoopRoutePlannerCallback;
import traveldashboard.ibicooptask.IbicoopRoutePlannerTask;
import traveldashboard.ibicooptask.IbicoopStartCallback;
import traveldashboard.ibicooptask.IbicoopStartStopTask;
import traveldashboardlondon.compat.AlertDialogCompat;
import traveldashboardlondon.context.TSLApplicationContext;
import traveldashboard.routeplanner.ItdDateTime;
import traveldashboard.routeplanner.ItdItinerary;
import traveldashboard.routeplanner.ItdMeansOfTransport;
import traveldashboard.routeplanner.ItdPartialRoute;
import traveldashboard.routeplanner.ItdPoint;
import traveldashboard.routeplanner.ItdRoute;
import traveldashboard.routeplanner.MotType;
import traveldashboardlondon.service.LocalRequestNotificationSender;
import traveldashboardlondon.service.LocalStationVoteNotificationSender;
import traveldashboardlondon.views.CrowdHelper;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class RoutePlannerActivity extends SherlockActivity {
	
	//Context
	private Context ibicoopContext;
	
	private IbicoopRoutePlannerTask routePlannerTask;
	
	private TextView itineraryTv;
	
	private TextView depDateTv;
	private TextView depTimeTv;
	
	private Button goButton;
	
	private AutoCompleteTextView depStopsAutoCompleteTv;
	private AutoCompleteTextView arrStopsAutoCompleteTv;
	private ArrayAdapter<String> depStopsAdapter;
	private ArrayAdapter<String> arrStopsAdapter;
	
	private LondonMetroDbManager londonMetroDbManager;
	private List<String> londonMetroStopNames;
	
	private String city = "London";
	
	private String depStopName = "";
	private String arrStopName = "";
	
	private String depDate = "";
	private String depTime = "";
	
	private DatePicker depDatePicker;
	private TimePicker depTimePicker;
	
	private CheckBox undergroundCheckBox;
	private CheckBox busCheckBox;
	
	private boolean isUndergroundSelected;
	private boolean isBusSelected;
	
	private int[] motTypeSelected;
	
	private LinearLayout newItineraryLayout;
	private Button newItineraryButton;
	
	private LinearLayout itineraryMainLayout;
	private LinearLayout itineraryMainContentLayout;
	private ProgressBar itineraryPb;
	
	
    //Start stop
    private IbicoopStartStopTask ibicoopStartStopTask;
    private WaitForInitIbicoopTask waitForInitIbicoopTask;
    
	private PreferenceConfigurationIntf preferenceConfiguration;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("");
		setContentView(R.layout.activity_route_planner);
		
		getSupportActionBar().setTitle("Route planner");
		
		ibicoopContext = TSLApplicationContext.getInstance().getContext();
		if (ibicoopContext == null) ibicoopContext = this;
		
		newItineraryLayout = (LinearLayout) findViewById(R.id.newItineraryLyt);
		newItineraryLayout.setVisibility(View.VISIBLE);
		
		newItineraryButton = (Button) findViewById(R.id.newItineraryButton);
		newItineraryButton.setVisibility(View.GONE);
		newItineraryButton.setOnClickListener(onNewItineraryClickListener);
		
		depDateTv = (TextView) findViewById(R.id.depDate);
		depDateTv.setOnClickListener(depDateOnClickListener);
		
		depTimeTv = (TextView) findViewById(R.id.depTime);
		depTimeTv.setOnClickListener(depTimeOnClickListener);
		
		depStopsAutoCompleteTv = (AutoCompleteTextView) findViewById(R.id.depStation);
		arrStopsAutoCompleteTv = (AutoCompleteTextView) findViewById(R.id.arrStation);
		
		londonMetroDbManager = TSLApplicationContext.getInstance().getLondonMetroDbManager();
		
		if (londonMetroDbManager != null) {
			//Get all metro stop names
			londonMetroStopNames = londonMetroDbManager.getAllLondonMetroStopNames();
			
	        // For departure station
	        depStopsAdapter = new ArrayAdapter<String>(this,
	        		android.R.layout.simple_dropdown_item_1line, londonMetroStopNames);

	        depStopsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	 
	        depStopsAutoCompleteTv.setAdapter(depStopsAdapter);
	        
	        depStopsAutoCompleteTv.setOnItemSelectedListener(depStopsOnItemClickListener);
	        
	        // For arrival station
	        arrStopsAdapter = new ArrayAdapter<String>(this,
	                android.R.layout.simple_dropdown_item_1line, londonMetroStopNames);
	 
	        arrStopsAutoCompleteTv.setAdapter(arrStopsAdapter);
	        
	        arrStopsAutoCompleteTv.setOnItemSelectedListener(arrStopsOnItemClickListener);
		}
		
		undergroundCheckBox = (CheckBox) findViewById(R.id.motTypeUnderground);
		busCheckBox = (CheckBox) findViewById(R.id.motTypeBus);
		
		preferenceConfiguration = TSLApplicationContext.getInstance().getPreferenceConfiguration();
		
		if (preferenceConfiguration != null) {
			
			if (preferenceConfiguration.isBoundForServiceBinder()) {
				if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Preference service is bound");
				
				TransportPreferenceIntf pref = null;
				try {
					pref = preferenceConfiguration.getTransportPreference();
				} catch (SecurityException e) {
					// TODO Auto-generated catch block
					if (IbicoopDataConstants. DEBUG_MODE) e.printStackTrace();
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					if (IbicoopDataConstants. DEBUG_MODE) e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					if (IbicoopDataConstants. DEBUG_MODE) e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					if (IbicoopDataConstants. DEBUG_MODE) e.printStackTrace();
				}

				if (pref != null) {
					if (IbicoopDataConstants. DEBUG_MODE) {
						System.out.println("Transport preference is not null");
						System.out.println(pref.toString());
					}
					undergroundCheckBox.setChecked(pref.getTubePref());
					busCheckBox.setChecked(pref.getBusPref());
				}
			}
		}

		goButton = (Button) findViewById(R.id.go);
		goButton.setOnClickListener(onGoClickListener);
		
		itineraryMainLayout = (LinearLayout) findViewById(R.id.itineraryMainLayout);
		itineraryMainLayout.setVisibility(View.GONE);
		
		itineraryMainContentLayout = (LinearLayout) findViewById(R.id.itineraryMainContentLayout);
		
		itineraryTv = (TextView) findViewById(R.id.itinerary);
		itineraryTv.setVisibility(View.GONE);
		itineraryPb = (ProgressBar) findViewById(R.id.itineraryPb);
		itineraryPb.setVisibility(View.GONE);
		
		if (waitForInitIbicoopTask != null) {waitForInitIbicoopTask.cancel(true);}
		
		waitForInitIbicoopTask = new WaitForInitIbicoopTask();
		AsyncTaskCompat.executeParallel(waitForInitIbicoopTask);
		
	}
	
	@Override
	protected void onDestroy() {
		
		if (routePlannerTask != null) routePlannerTask.cancel(true);
		
		super.onDestroy();
	}
	
	class WaitForInitIbicoopTask extends AsyncTask<Void, Void, Void> {
		
		@Override
		protected Void doInBackground(Void... params) {

			try{
				//Waiting to reinitialize ibicoop in the case of notification callback
				Thread.sleep(1000);
			} catch (Exception exception) {
				if (IbicoopDataConstants. DEBUG_MODE) System.err.println(exception.getMessage());
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void param) {
			if (ibicoopStartStopTask != null) ibicoopStartStopTask.cancel(true);
			ibicoopStartStopTask = new IbicoopStartStopTask(ibicoopContext, false, ibicoopStartCallback,
					TSLApplicationContext.getInstance(), LocalStationVoteNotificationSender.class, LocalRequestNotificationSender.class);
			AsyncTaskCompat.executeParallel(ibicoopStartStopTask);				
		}
		
	}
	
	private IbicoopStartCallback ibicoopStartCallback = new IbicoopStartCallback() {
		@Override
		public void ibicoopStartOk() {
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Ibicoop start ok");
		}
		
		@Override
		public void ibicoopStartFailed() {
			if (IbicoopDataConstants. DEBUG_MODE) System.err.println("Ibicoop start failed");
		}

	};
	
	private OnClickListener depDateOnClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Departure date on click listener");
			showSelectDateDialog();
		}
	};
	
	
	private OnClickListener depTimeOnClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Departure time on click listener");
			showSelectTimeDialog();
		}
	};		
	
	private void showSelectDateDialog() {
		AlertDialog.Builder builder = AlertDialogCompat.getAlertBuilder(RoutePlannerActivity.this);
		
		LayoutInflater inflater = RoutePlannerActivity.this.getLayoutInflater();
		
		LinearLayout depDatePickerLayout = (LinearLayout) inflater.inflate(R.layout.custom_route_planner_datepicker,  null);
		
		depDatePicker = (DatePicker) depDatePickerLayout.findViewById(R.id.depDatePicker);

		TextView titleView = (TextView) inflater.inflate(R.layout.custom_alarm_picker_title, null);
		titleView.setText("Leaving date");
		
		builder.setCustomTitle(titleView);
		builder.setView(depDatePickerLayout);
		
		String buttonText = "Select";
		
		builder.setNegativeButton(buttonText, new DialogInterface.OnClickListener() {
		      public void onClick(DialogInterface dialog, int which) {
		    	  
		    	  int year = depDatePicker.getYear();
		    	  
		    	  int month = (depDatePicker.getMonth() + 1); //month index from 0 to 11
		    	  
		    	  int day = depDatePicker.getDayOfMonth();
		    	  
		    	  if (IbicoopDataConstants. DEBUG_MODE) System.out.println("year = " + year + ", month = " + month + ", day = " + day);
		    	  
		    	  String yearDesc = String.valueOf(year);
		    	  
		    	  if (year < 1000) yearDesc = "20" + yearDesc;
		    	  
		    	  String monthDesc = String.valueOf(month);
		    	  
		    	  if (month < 10) monthDesc = "0" + monthDesc;
		    	  
		    	  String dayDesc = String.valueOf(day);
		    	  
		    	  if (day < 10) dayDesc = "0" + dayDesc;
		    	  
		    	  depDate = yearDesc + monthDesc + dayDesc;
		    	  
		    	  if (IbicoopDataConstants. DEBUG_MODE) System.out.println("depDate = " + depDate);
		    	  
		    	  String depDateOnUi = dayDesc + "/" + monthDesc + "/" + yearDesc;
		    	  
		    	  depDateTv.setText(depDateOnUi);   	  
		    	  
		    }
		});
		
		AlertDialog dialog = AlertDialogCompat.getAlertDialog(builder);
		dialog.show();
	}
	
	private void showSelectTimeDialog() {
		AlertDialog.Builder builder = AlertDialogCompat.getAlertBuilder(RoutePlannerActivity.this);
		
		LayoutInflater inflater = RoutePlannerActivity.this.getLayoutInflater();
		
		LinearLayout depTimePickerLayout = (LinearLayout) inflater.inflate(R.layout.custom_route_planner_timepicker,  null);
		
		depTimePicker = (TimePicker) depTimePickerLayout.findViewById(R.id.depTimePicker);
		depTimePicker.setIs24HourView(true);

		TextView titleView = (TextView) inflater.inflate(R.layout.custom_alarm_picker_title, null);
		titleView.setText("Leaving time");
		
		builder.setCustomTitle(titleView);
		builder.setView(depTimePickerLayout);
		
		String buttonText = "Select";
		
		builder.setNegativeButton(buttonText, new DialogInterface.OnClickListener() {
		      public void onClick(DialogInterface dialog, int which) {
		    	  
		    	  int hour = depTimePicker.getCurrentHour();
		    	  
		    	  int minute = depTimePicker.getCurrentMinute();
		    	  
		    	  if (IbicoopDataConstants. DEBUG_MODE) System.out.println("hour = " + hour + ", minute = " + minute);
		    	  
		    	  String hourDesc = String.valueOf(hour);
		    	  
		    	  if (hour < 10) hourDesc = "0" + hourDesc;
		    	  
		    	  String minuteDesc = String.valueOf(minute);
		    	  
		    	  if (minute < 10) minuteDesc = "0" + minuteDesc;
		    	  
		    	  depTime = hourDesc + minuteDesc;
		    	  
		    	  if (IbicoopDataConstants. DEBUG_MODE) System.out.println("depTime = " + depTime);
		    	  
		    	  String depTimeOnUi = hourDesc + ":" + minuteDesc;
		    	  
		    	  depTimeTv.setText(depTimeOnUi);
		    }
		});
		
		AlertDialog dialog = AlertDialogCompat.getAlertDialog(builder);
		dialog.show();
	}
	
	private OnItemSelectedListener depStopsOnItemClickListener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
			depStopName = parent.getItemAtPosition(position).toString();
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Select departure stop name = " + depStopName);
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
			depStopName = parent.getItemAtPosition(0).toString();
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Select departure stop name = " + depStopName);
		}
		
	};
	
	private OnItemSelectedListener arrStopsOnItemClickListener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
			arrStopName = parent.getItemAtPosition(position).toString();
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Select arrival stop name = " + arrStopName);
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
			arrStopName = parent.getItemAtPosition(0).toString();
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("Select arrival stop name = " + depStopName);
		}
		
	};	
	
	private OnClickListener onNewItineraryClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("On new itinerary click listener");
			newItineraryLayout.setVisibility(View.VISIBLE);
			newItineraryButton.setVisibility(View.GONE);
			itineraryMainLayout.setVisibility(View.GONE);
		}
	};
	
	private OnClickListener onGoClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			if (IbicoopDataConstants. DEBUG_MODE) System.out.println("On go click listener");
			//Launch route planner task
			if ((depStopsAutoCompleteTv.getText() == null) || depStopsAutoCompleteTv.getText().toString().equals("")) {
			
				Toast.makeText(RoutePlannerActivity.this, "Please insert the departure station name!", Toast.LENGTH_SHORT).show();
				
				return;
			}
			
			if ((arrStopsAutoCompleteTv.getText() == null) || arrStopsAutoCompleteTv.getText().toString().equals("")) {
				
				Toast.makeText(RoutePlannerActivity.this, "Please insert the arrival station name!", Toast.LENGTH_SHORT).show();
				
				return;
			}
			
			if ((depDate == null) || depDate.equals("")) {
				
				Toast.makeText(RoutePlannerActivity.this, "Please select leaving date!", Toast.LENGTH_SHORT).show();
				
				return;
			}
			
			if ((depTime == null) || depTime.equals("")) {
				
				Toast.makeText(RoutePlannerActivity.this, "Please select leaving time!", Toast.LENGTH_SHORT).show();
				
				return;
			}
			
			
			depStopName = depStopsAutoCompleteTv.getText().toString() + " underground station";
			arrStopName = arrStopsAutoCompleteTv.getText().toString() + " underground station";
			
			//motTypeSelected
			isUndergroundSelected = undergroundCheckBox.isChecked();
			isBusSelected = busCheckBox.isChecked();
			
			if (isUndergroundSelected && isBusSelected) {
				motTypeSelected = new int[2];
				motTypeSelected[0] = MotType.UNDERGROUND.getId();
				motTypeSelected[1] = MotType.CITY_BUS.getId();
			} else if (isUndergroundSelected && !isBusSelected) {
				motTypeSelected = new int[1];
				motTypeSelected[0] = MotType.UNDERGROUND.getId();
			} else if (!isUndergroundSelected && isBusSelected) {
				motTypeSelected = new int[1];
				motTypeSelected[0] = MotType.CITY_BUS.getId();			
			} else {
				motTypeSelected = new int[0];
			}
			
			if (routePlannerTask != null) routePlannerTask.cancel(true);
			
			
			newItineraryLayout.setVisibility(View.GONE);
			newItineraryButton.setVisibility(View.VISIBLE);
			
			itineraryMainLayout.setVisibility(View.VISIBLE);
			
			itineraryTv.setVisibility(View.GONE);
			itineraryPb.setVisibility(View.VISIBLE);
			
			itineraryMainContentLayout.removeAllViews();
			
			routePlannerTask = new  IbicoopRoutePlannerTask(
					RoutePlannerActivity.this, city, depStopName, arrStopName, depDate, depTime, motTypeSelected, callback);
			
			AsyncTaskCompat.executeParallel(routePlannerTask);
		}
	};
	
	private IbicoopRoutePlannerCallback callback = new IbicoopRoutePlannerCallback() {
		
		@Override
		public void getItinerarySucess(ItdItinerary itdItinerary) {
			if (IbicoopDataConstants. DEBUG_MODE) {
				System.out.println("Get itinerary sucess!");
				System.out.println(itdItinerary.toString());
				Toast.makeText(RoutePlannerActivity.this,"Get itinerary sucess!", Toast.LENGTH_SHORT).show();
			}
			
			itineraryPb.setVisibility(View.GONE);
			itineraryTv.setVisibility(View.GONE);
			
			//Get inflater
			LayoutInflater inflater = RoutePlannerActivity.this.getLayoutInflater();
			
			//Start to add view in itinerary main content layout

			List<ItdRoute> itdRouteList = itdItinerary.getItdRouteList();

			int routeIndex = 1;
			
			for (ItdRoute itdRoute : itdRouteList) {
				
				//Create custom_route_planner_route_parent
				LinearLayout routeParentLayout = (LinearLayout) inflater.inflate(R.layout.custom_route_planner_route_parent,  null);
				
				TextView routeTripIndexTv = (TextView) routeParentLayout.findViewById(R.id.routeTripIndex);
				TextView routeTripIndexDurationTv = (TextView) routeParentLayout.findViewById(R.id.routeTripIndexDuration);
				
				//Set route trip index and duration
				String routeTripIndex = String.valueOf(itdRoute.getRouteTripIndex());
				String routeTripIndexDuration = itdRoute.getPublicDuration();
				
				if (IbicoopDataConstants. DEBUG_MODE) {
					System.out.println("route trip index = " + routeTripIndex);
					System.out.println("route trip index duration = " + routeTripIndexDuration);
				}
				
				//routeTripIndexTv.setText("Route " + routeTripIndex);
				routeTripIndexTv.setText("Route " + routeIndex);
				routeIndex++;
				routeTripIndexDurationTv.setText(routeTripIndexDuration);
				
				LinearLayout routeTripLayout = (LinearLayout) routeParentLayout.findViewById(R.id.routeTripLayout);
				
				List<ItdPartialRoute> itdPartialRouteList = itdRoute.getItdPartialRouteList();
				
				for (ItdPartialRoute itdPartialRoute : itdPartialRouteList) {
					//Create custom_route_planner_route_children
					LinearLayout routeChildrenLayout = (LinearLayout) inflater.inflate(R.layout.custom_route_planner_route_children,  null);
					
					ImageView motTypeIcon = (ImageView) routeChildrenLayout.findViewById(R.id.motTypeIcon);
					motTypeIcon.setVisibility(View.VISIBLE);
					
					TextView journeyTimeTv = (TextView) routeChildrenLayout.findViewById(R.id.journeyTimeTv);
					String timeMinute = String.valueOf(itdPartialRoute.getTimeMinute()) + " mins";
					journeyTimeTv.setText(timeMinute);
					
					//Extract depart point
					TextView depTimeTv = (TextView) routeChildrenLayout.findViewById(R.id.depTimeTv);
					TextView depStationTv = (TextView) routeChildrenLayout.findViewById(R.id.depStationTv);
					
					ItdPoint departPoint = itdPartialRoute.getDeparturePoint();
					ItdDateTime depDateTime = departPoint.getItdDateTime();
					
					int depHour = depDateTime.getHour();
					String depHourDesc = String.valueOf(depHour);
					if (depHour < 10) depHourDesc = "0" + depHourDesc;
					
					int depMin = depDateTime.getMinute();
					String depMinDesc = String.valueOf(depMin);
					if (depMin < 10) depMinDesc = "0" + depMinDesc;
					
					depTimeTv.setText(depHourDesc + ":" + depMinDesc);
					
					String depStationName = departPoint.getStationName();
					depStationTv.setText(depStationName);
					
					//For bus
					String depPlatformName = departPoint.getPlatformName();
					
					//Extract departure crowd
					LinearLayout depCrowdImageViewsLyt = (LinearLayout) routeChildrenLayout.findViewById(R.id.depParentCustomCrowdImages);
					
					int depCrowdLevel = departPoint.getCrowdLevel();
					
					if (depCrowdLevel > 0) {
						ImageView[] depCrowdImageViews = new ImageView[5];
						
						depCrowdImageViews[0] = (ImageView) routeChildrenLayout.findViewById(R.id.depCustomCrowd1);
						depCrowdImageViews[1] = (ImageView) routeChildrenLayout.findViewById(R.id.depCustomCrowd2);
						depCrowdImageViews[2] = (ImageView) routeChildrenLayout.findViewById(R.id.depCustomCrowd3);
						depCrowdImageViews[3] = (ImageView) routeChildrenLayout.findViewById(R.id.depCustomCrowd4);
						depCrowdImageViews[4] = (ImageView) routeChildrenLayout.findViewById(R.id.depCustomCrowd5);
						
						CrowdHelper.setNewCrowdImagesByImageViews(RoutePlannerActivity.this, depCrowdImageViews, depCrowdLevel);
						
						TextView depNbCrowdTv = (TextView) routeChildrenLayout.findViewById(R.id.nbDepCustomCrowd);
						
						depNbCrowdTv.setText("" + depCrowdLevel + "/5");
						
						//Make the crowd level view visible if the crowd data is present
						depCrowdImageViewsLyt.setVisibility(View.VISIBLE);
						
					} else {
						//Make the crowd level view gone if the crowd data is absent
						depCrowdImageViewsLyt.setVisibility(View.GONE);
					}
					
					//Noise data
					LinearLayout depNoiseLyt = (LinearLayout) routeChildrenLayout.findViewById(R.id.depNoiseLyt);
					TextView depNoiseLevelTv = (TextView) routeChildrenLayout.findViewById(R.id.depNoiseLevelText);
					ProgressBar depNoisePb = (ProgressBar) routeChildrenLayout.findViewById(R.id.depNoiseLevelPb);
					depNoisePb.setMax(IbicoopDataConstants.NOISE_MAX_VALUE);
					TextView depNoiseLevelMaxTv = (TextView) routeChildrenLayout.findViewById(R.id.depNoiseLevelMaxText);
					depNoiseLevelMaxTv.setText("" + IbicoopDataConstants.NOISE_MAX_VALUE);
					
					int depNoiseLevel = departPoint.getNoiseLevel();
					
					if (depNoiseLevel > 0) {
						
						if (depNoiseLevel > IbicoopDataConstants.NOISE_MAX_VALUE) {
							depNoiseLevel = IbicoopDataConstants.NOISE_MAX_VALUE;
						}
						
						depNoiseLevelTv.setText("" + depNoiseLevel);
						depNoisePb.setProgress(depNoiseLevel);
						depNoiseLyt.setVisibility(View.VISIBLE);
					} else {
						//Don't show if noise level = 0
						depNoiseLyt.setVisibility(View.GONE);
					}
					
					
					//Extract arrival point
					TextView arrTimeTv = (TextView) routeChildrenLayout.findViewById(R.id.arrTimeTv);
					TextView arrStationTv = (TextView) routeChildrenLayout.findViewById(R.id.arrStationTv);
					
					ItdPoint arrPoint = itdPartialRoute.getArrivalPoint();
					ItdDateTime arrDateTime = arrPoint.getItdDateTime();
					
					int arrHour = arrDateTime.getHour();
					String arrHourDesc = String.valueOf(arrHour);
					if (arrHour < 10) arrHourDesc = "0" + arrHourDesc;
					
					int arrMin = arrDateTime.getMinute();
					String arrMinDesc = String.valueOf(arrMin);
					if (arrMin < 10) arrMinDesc = "0" + arrMinDesc;
					
					arrTimeTv.setText(arrHourDesc + ":" + arrMinDesc);
					
					String arrStationName = arrPoint.getStationName();
					arrStationTv.setText(arrStationName);
					
					//Extract arrival crowd
					LinearLayout arrCrowdImageViewsLyt = (LinearLayout) routeChildrenLayout.findViewById(R.id.arrParentCustomCrowdImages);
					
					int arrCrowdLevel = arrPoint.getCrowdLevel();
					
					if (arrCrowdLevel > 0) {
						ImageView[] arrCrowdImageViews = new ImageView[5];
						
						arrCrowdImageViews[0] = (ImageView) routeChildrenLayout.findViewById(R.id.arrCustomCrowd1);
						arrCrowdImageViews[1] = (ImageView) routeChildrenLayout.findViewById(R.id.arrCustomCrowd2);
						arrCrowdImageViews[2] = (ImageView) routeChildrenLayout.findViewById(R.id.arrCustomCrowd3);
						arrCrowdImageViews[3] = (ImageView) routeChildrenLayout.findViewById(R.id.arrCustomCrowd4);
						arrCrowdImageViews[4] = (ImageView) routeChildrenLayout.findViewById(R.id.arrCustomCrowd5);
						
						CrowdHelper.setNewCrowdImagesByImageViews(RoutePlannerActivity.this, arrCrowdImageViews, arrCrowdLevel);
						
						TextView arrNbCrowdTv = (TextView) routeChildrenLayout.findViewById(R.id.nbArrCustomCrowd);
						
						arrNbCrowdTv.setText("" + arrCrowdLevel + "/5");
						
						//Make the crowd level view visible if the crowd data is present
						arrCrowdImageViewsLyt.setVisibility(View.VISIBLE);
						
					} else {
						//Make the crowd level view gone if the crowd data is absent
						arrCrowdImageViewsLyt.setVisibility(View.GONE);
					}
					
					//Arrival noise data
					//Noise data
					LinearLayout arrNoiseLyt = (LinearLayout) routeChildrenLayout.findViewById(R.id.arrNoiseLyt);
					TextView arrNoiseLevelTv = (TextView) routeChildrenLayout.findViewById(R.id.arrNoiseLevelText);
					ProgressBar arrNoisePb = (ProgressBar) routeChildrenLayout.findViewById(R.id.arrNoiseLevelPb);
					arrNoisePb.setMax(IbicoopDataConstants.NOISE_MAX_VALUE);
					TextView arrNoiseLevelMaxTv = (TextView) routeChildrenLayout.findViewById(R.id.arrNoiseLevelMaxText);
					arrNoiseLevelMaxTv.setText("" + IbicoopDataConstants.NOISE_MAX_VALUE);
					
					int arrNoiseLevel = arrPoint.getNoiseLevel();
					
					if (arrNoiseLevel > 0) {
						
						if (arrNoiseLevel > IbicoopDataConstants.NOISE_MAX_VALUE) {
							arrNoiseLevel = IbicoopDataConstants.NOISE_MAX_VALUE;
						}
						
						arrNoiseLevelTv.setText("" + arrNoiseLevel);
						arrNoisePb.setProgress(arrNoiseLevel);
						arrNoiseLyt.setVisibility(View.VISIBLE);
					} else {
						//Don't show if noise level = 0
						arrNoiseLyt.setVisibility(View.GONE);
					}


					TextView journeyDescTv = (TextView) routeChildrenLayout.findViewById(R.id.journeyDescTv);
					journeyDescTv.setText("Description not available");
					
					if (IbicoopDataConstants. DEBUG_MODE) {
						System.out.println("depStationName = " + depStationName);
						System.out.println("arrStationName = " + arrStationName);
					}
					
					//Extract itd means of transport
					List<ItdMeansOfTransport> itdMeansOfTransportList = itdPartialRoute.getItdMeansOfTransportList();
					
					if (itdMeansOfTransportList.size() > 0) {
												
						ItdMeansOfTransport firstItdMeansOfTransport = itdMeansOfTransportList.get(0);
						MotType motType = firstItdMeansOfTransport.getMode();
						
						if (motType.equals(MotType.UNDERGROUND)) {
							motTypeIcon.setImageResource(R.drawable.tube_icon);
						} else if (motType.equals(MotType.CITY_BUS)) {
							motTypeIcon.setImageResource(R.drawable.bus_icon);
						} else if (motType.equals(MotType.OTHER)) {
							motTypeIcon.setImageResource(R.drawable.walking_icon_small);
						} else  if (motType.equals(MotType.TRAIN)) {
							motTypeIcon.setImageResource(R.drawable.train_icon_small);
						} else {
							motTypeIcon.setVisibility(View.INVISIBLE);
						}
						
						String info = "";

						int index = 0;
						
						for (ItdMeansOfTransport itdMeansOfTransport : itdMeansOfTransportList) {
							
							String lineName = itdMeansOfTransport.getName();
							String destination = itdMeansOfTransport.getDestination();
							MotType lineMotType = itdMeansOfTransport.getMode();
							
							if (IbicoopDataConstants. DEBUG_MODE) System.out.println("lineName = " + lineName + ", destination = " + destination);
	
							if (lineMotType.equals(MotType.UNDERGROUND)) {
								//For tube
								if (index == 0) info = info + "Take the " + lineName + " Line towards " + destination;
								else info = info + "\nor " + lineName + " Line towards " + destination;
							 } else if (lineMotType.equals(MotType.CITY_BUS)) {
								//For bus
								if (index == 0) info = info + "Take the Route Bus " + lineName + 
										" from Stop: " +  depPlatformName + " towards " + destination;
								else info = info + "\nor Route Bus " + lineName + 
										" from Stop: " +  depPlatformName + " towards " + destination;
							 } else if (lineMotType.equals(MotType.TRAIN)) {
									//For train
									if (index == 0) info = info + lineName + 
											" towards " + destination;
									else info = info + "\nor " + lineName + " towards " + destination;
							 }  else if (lineMotType.equals(MotType.OTHER)) {
									//For waling
									if (index == 0) info = info + "Walk to " + arrStationName;
									else info = info + "\nor walk to " + arrStationName;
								 }

							index++;
						}
						
						journeyDescTv.setText(info);
						
					} //End itdMeansOfTransportList.size() > 0
					
					//Add children to parent
					routeTripLayout.addView(routeChildrenLayout);
				} //End of partial route

				//Add parent to main content
				itineraryMainContentLayout.addView(routeParentLayout);
				
			}//End for (ItdRoute itdRoute : itdRouteList)
			
		}
		
		@Override
		public void getItineraryFailed() {
			if (IbicoopDataConstants. DEBUG_MODE) {
				System.out.println("Get itinerary failed!");
				Toast.makeText(RoutePlannerActivity.this,"Get itinerary failed!", Toast.LENGTH_SHORT).show();
			}
		
			itineraryPb.setVisibility(View.GONE);
			itineraryTv.setText("Itinerary not available");
			itineraryTv.setVisibility(View.VISIBLE);
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.route_planner, menu);
		return true;
	}

}
