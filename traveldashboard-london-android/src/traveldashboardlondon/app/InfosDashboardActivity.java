package traveldashboardlondon.app;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;


import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.google.gson.Gson;

import traveldashboard.compat.AsyncTaskCompat;
import traveldashboard.data.IbicoopDataConstants;
import traveldashboard.data.LondonMetroDbManager;
import traveldashboard.data.ParisMetroDbManager;
import traveldashboard.data.Station;
import traveldashboard.ibicooptask.IbicoopClassifiedCrowdednessCallback;
import traveldashboard.ibicooptask.IbicoopClassifiedCrowdednessGetterTask;
import traveldashboard.ibicooptask.IbicoopClassifiedNoiseCallback;
import traveldashboard.ibicooptask.IbicoopClassifiedNoiseGetterTask;
import traveldashboard.ibicooptask.IbicoopCommentCallback;
import traveldashboard.ibicooptask.IbicoopCommentGetterTask;
import traveldashboard.ibicooptask.IbicoopFacebookCommentCallback;
import traveldashboard.ibicooptask.IbicoopFacebookCommentsTask;
import traveldashboard.ibicooptask.IbicoopGeneralCrowdednessCallback;
import traveldashboard.ibicooptask.IbicoopGeneralCrowdednessGetterTask;
import traveldashboard.ibicooptask.IbicoopIncentiveCallback;
import traveldashboard.ibicooptask.IbicoopIncentiveGetterTask;
import traveldashboard.ibicooptask.IbicoopNoiseCallback;
import traveldashboard.ibicooptask.IbicoopNoiseGetterTask;
import traveldashboard.ibicooptask.IbicoopRegionChannelRequest;
import traveldashboard.ibicooptask.IbicoopReportCommentTask;
import traveldashboard.ibicooptask.IbicoopReportCrowdTask;
import traveldashboard.ibicooptask.IbicoopStartCallback;
import traveldashboard.ibicooptask.IbicoopStartStopTask;
import traveldashboard.ibicooptask.IbicoopStationChannelSubscriptionTask;
import traveldashboard.ibicooptask.IbicoopTubeCountdownCallback;
import traveldashboard.ibicooptask.IbicoopTubeCountdownTask;
import traveldashboard.ibicooptask.IbicoopTwitterCommentCallback;
import traveldashboard.ibicooptask.IbicoopTwitterCommentGetterTask;
import traveldashboard.ibicooptask.IbicoopReportCommentTask.IbicoopReportCommentCallback;
import traveldashboard.ibicooptask.IbicoopReportCrowdTask.IbicoopReportCrowdCallback;
import traveldashboard.utils.IntentConstants;
import traveldashboard.utils.TravelDashboardCommonPullTask;
import traveldashboardlondon.compat.AlertDialogCompat;
import traveldashboardlondon.compat.ProgressDialogCompat;
import traveldashboardlondon.context.TSLApplicationContext;
import traveldashboardlondon.service.LocalRequestNotificationSender;
import traveldashboardlondon.service.LocalStationVoteNotificationSender;
import traveldashboardlondon.service.MetroInfoNotificationService;
import traveldashboardlondon.service.TimeCounterExecutor;
import traveldashboardlondon.service.TimeCounterExecutor.TimeCounterCallback;
import traveldashboard.data.TransportArea;
import traveldashboard.data.Tube;
import traveldashboard.data.TubeStation;
import traveldashboard.data.comment.TubeStationComment;
import traveldashboard.data.comment.TubeStationCommentsCollection;
import traveldashboard.data.crowd.TubeStationCrowd;
import traveldashboard.data.crowd.TubeStationCrowdsCollection;
import traveldashboard.data.happiness.HappinessLevel;
import traveldashboard.data.happiness.HappinessRequest;
import traveldashboard.data.happiness.HappinessVote;
import traveldashboard.data.incentive.Incentive;
import traveldashboard.data.incentive.IncentiveType;
import traveldashboard.data.info.TubeRealTimeInfo;
import traveldashboard.data.info.TubeRealTimeInfosCollection;
import traveldashboard.data.noise.NoiseData;
import traveldashboard.data.noise.NoiseDatasCollection;
import traveldashboardlondon.views.CrowdHelper;
import traveldashboardlondon.views.CrowdRatingSimplifiedLayout;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Handler.Callback;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class InfosDashboardActivity extends SherlockActivity {
	
	//General crowd time interval: 2 hour = 1000 ms * 60 * 60 * 2
	private static final int GENERAL_CROWD_HOUR = 2;
	private static final long GENERAL_CROWD_INTERVAL = 1000 * 60 * 60 * GENERAL_CROWD_HOUR;
	private static final int GENERAL_CROWD_INTERVAL_MAX_MULTIPLE = 6;
	private static final int NOISE_HOUR = 2;
	private static final long NOISE_INTERVAL = GENERAL_CROWD_INTERVAL;
	private static final int NOISE_INTERVAL_MAX_MULTIPLE = GENERAL_CROWD_INTERVAL_MAX_MULTIPLE;

	//UnsubcribeStationChannel
	private static final long UNSUBSCRIBE_STATION_CHANNEL_TIMEOUT_MS = 1000 * 60 * 10; //10 minutes 
	
	
	//Context
	private Context ibicoopContext;
	
	
	//Action bar
	private ActionBar actionBar;
    //For menu item
	private	MenuItem expandItem;
	private MenuItem signInItem;
	private MenuItem alarmItem;
	
	private boolean allIsExpanded = false;

	
	//Station
	private TextView stationName;
	
	//For alarm
	//We should save this alarm information in a database
	private TimePicker timePicker;
	private LinearLayout alarmSetLayout;
	private TextView hourTv;
	private TextView minuteTv;
	private SetInfoAlarmTask setInfoAlarmTask;
	private RemoveInfoAlarmTask removeInfoAlarmTask;
	
	
	
	//
	//
	//Incentive
	private TextView incentiveContentTv;
	private String incentiveContent;
	private String[] incentiveContentArray;

	
	//
	//
	//Noise
	private ImageView expandNoiseButton;
	private LinearLayout noiseExpandLyt;
	private LinearLayout noiseInfoLyt;
	private boolean noiseIsExpanded = false;
	
	//General
	private HashMap<Integer, LinearLayout> noiseLayouts;
	
	//Classified
	private LinearLayout noiseClassifiedInfoLyt;
	private LinearLayout classfiedNoiseInfoContentLyt;
	private ProgressBar classifiedNoiseLevelPb;
	private ProgressBar classifiedNoisePb;
	private LinearLayout classifiedNoiseLevelContentLyt;
	private TextView classifiedNoiseNotAvailableView;
	private TextView classifiedNoiseDescription;
	private TextView classifiedNoiseText;
	private TextView classifiedNoiseMaxText;
	private TextView classifiedNoiseReportsText; 
	private String classifiedNoiseInterval = "";
	private TimePicker timePickerForNoise;
	private LinearLayout noiseSetLayout;
	private ImageView changeNoiseTimeButton;
	
	
	//
	//
	//Crowd
	
	private ImageView reportCrowdButton;
	private ImageView demandCrowdButton;
	private ImageView expandCrowdButton;
	private LinearLayout crowdLyt;
	private LinearLayout crowdExpandLyt;
	private boolean crowdIsExpanded = false;
	private TimePicker timePickerForCrowd;
	private LinearLayout crowdSetLayout;
	private ImageView changeCrowdTimeButton;
	
	//Classfied

	private LinearLayout crowdRatingLyt;
	private TextView nbCrowdTv;
	private TextView crowdNotAvailableTv;
	private TextView crowdIntervalTv;
	private TextView crowdNbVotesTv;
	private ImageView[] crowdImageViews;
	private ProgressBar crowdPb;
	private String classifiedCrowdInterval = "";
	
	private LinearLayout classifiedCrowdParentLayout;
	
	//General
	//private List<LinearLayout> generalCrowdParentLayouts;
	private HashMap<Integer, LinearLayout> generalCrowdLayouts;
	private TextView demandCrowdContentTv;
	
	
	//
	//
	//Timetable
	private ImageView expandTimeTableButton;
	private LinearLayout timeTableLyt;
	private LinearLayout timeTableExpandLyt;
	//private LinearLayout timeTableSubLyt;
	//private ProgressBar timeTablePb;
	private boolean timeTableIsExpanded = false;
	
	
	//
	//
	//Comments
	private LinearLayout commentExpandLyt;
	private ImageView reportCommentButton;
	private ImageView commentExpandButton;
	private boolean commentIsExpanded = false;
	
	private LinearLayout twitComExpandLyt;
	private LinearLayout twitCommentLyt;
	private LinearLayout twitCommentSubLyt;
	private TextView twitComExpandLine;
	private ImageView twitComExpandButton;
	private ProgressBar twitComPb;
	private boolean twitComIsExpanded = false;
	
	private LinearLayout fbComExpandLyt;
	private LinearLayout fbComLyt;
	private LinearLayout fbComSubLyt;
	private TextView fbComExpandLine;	
	private ImageView fbComExpandButton;
	private ProgressBar fbComPb;
	private boolean fbComIsExpanded = false;
	
	private LinearLayout userComExpandLyt;
	private LinearLayout userComLyt;
	private LinearLayout userComSubLyt;
	private TextView userComExpandLine;	
	private ImageView userComExpandButton;
	private ProgressBar userComPb;
	private boolean userComIsExpanded = false;
	
	private LinearLayout userNoComView;
	
	private TubeStation metroStop;
	
	private IbicoopIncentiveGetterTask incentiveTask;
	
	private IbicoopNoiseGetterTask noiseTask;
	private IbicoopClassifiedNoiseGetterTask classifiedNoiseTask;
	
	private IbicoopClassifiedCrowdednessGetterTask classifiedCrowdTask;
	private IbicoopGeneralCrowdednessGetterTask generalCrowdTask;
	
	private IbicoopRegionChannelRequest regionChannelRequestTask;
	private IbicoopStationChannelSubscriptionTask stationChannelSubscriptionTask;
	
	private IbicoopReportCrowdTask reportCrowdTask;
	private IbicoopReportCommentTask reportCommentTask;
	
	
	//For multiple lines
	private IbicoopTubeCountdownTask[] metroCountdownTasks;
	private IbicoopTubeCountdownCallback[] metroCountdownCallbacks;
	//key = routeId, value = corresponding tube countdown tasks
	private HashMap<String, IbicoopTubeCountdownTask> metroTasksMap;
	//key = routeId, value = corresponding tube countdown callbacks
	private HashMap<String, IbicoopTubeCountdownCallback> metroCallbacksMap;
	//key = routeId, value = corresponding tube countdown linear layout
	private HashMap<String, LinearLayout> metroLytMap;
	
	
	
	private IbicoopFacebookCommentsTask fbTask;
	private IbicoopTwitterCommentGetterTask twitterTask;
	private IbicoopCommentGetterTask commentTask;
	
	private String metroStopDesc;
	private String city;
	
	private ParisMetroDbManager parisMetroDbManager;
	private LondonMetroDbManager londonMetroDbManager;
	
    //Start stop
    private IbicoopStartStopTask ibicoopStartStopTask;
    private WaitForInitIbicoopTask waitForInitIbicoopTask;
	
    private boolean isNewIntent = false;
    
    
    //
    //
    //Sign in
	public static final String AUTHORISATION_SCOPE_MANAGE_YOUR_TASKS = "Manage your tasks";
	public static final String ACCOUNT_TYPE_GOOGLE = "com.google";    
	private TextView email;
	private TextView signInText;
	private Button login;
	private String userId;
	private ProgressDialog pd;	
	
	private TimeCounterExecutor timeCounterExecutor;

	private HappinessPullController happinessController;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setTitle("");
		
		setContentView(R.layout.activity_infos_dashboard);
		
		//
		//
		//actionBar
		actionBar = getSupportActionBar();
		actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.sprinkles));
		
		Intent receivedIntent = getIntent();
		
		isNewIntent = false;

		handlerIntent(receivedIntent);
	}
	
	@Override
	public void onResume() {
		
		if (signInItem != null) {
			if (TSLApplicationContext.getInstance().checkIfUserIsLoggedIn()) {
				signInItem.setTitle("Sign out");
			} else {
				signInItem.setTitle("Sign in");
			}		
		}
		
		super.onResume();
	}
	
	@Override
	protected void onNewIntent(Intent intent) {
		
		super.onNewIntent(intent);
		
		isNewIntent = true;
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("On new intent");
		
		setIntent(intent);
		
		handlerIntent(intent);
	}
	
	@Override
	protected void onDestroy() {
		//Cancel all async task
		stopAllTasks();
		super.onDestroy();
	}
	
	private void handlerIntent(Intent intent) {
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Handler intent");
		
		ibicoopContext = TSLApplicationContext.getInstance().getContext();
		if (ibicoopContext == null) ibicoopContext = this;
		
		metroStopDesc = intent.getStringExtra(IntentConstants.TUBE_STATION_GSON);
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("InfosDashboardActivity : metroStopDesc = " + metroStopDesc);

		Gson gson = new Gson();
		metroStop = gson.fromJson(metroStopDesc, TubeStation.class); 
		
		String stationNameDesc = "Unknown";
		
		if (metroStop != null) stationNameDesc = metroStop.getName();
		
		//city
		city = "Unknown";
		if (metroStop != null) city = TransportArea.getArea(metroStop.getLatitude(), metroStop.getLongitude());
		
		//
		//
		//Check if metro db manager exists
		//Paris
		parisMetroDbManager = TSLApplicationContext.getInstance().getParisMetroDbManager();
		
		if (parisMetroDbManager == null) {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Paris metro db manager is null!");
			
			parisMetroDbManager = new ParisMetroDbManager(this);
			TSLApplicationContext.getInstance().setParisMetroDbManager(parisMetroDbManager);
		}

		//London
		londonMetroDbManager = TSLApplicationContext.getInstance().getLondonMetroDbManager();
		
		if (londonMetroDbManager == null) {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("London metro db manager is null!");
			
			londonMetroDbManager = new LondonMetroDbManager(this);
			TSLApplicationContext.getInstance().setLondonMetroDbManager(londonMetroDbManager);			
		}
		
		//
		//
		//Station name
		stationName = (TextView) findViewById(R.id.stationName);
		stationName.setText(stationNameDesc);
		
		
		LayoutInflater inflater = InfosDashboardActivity.this.getLayoutInflater();
		
		//
		//
		//Noise
		expandNoiseButton = (ImageView) findViewById(R.id.noiseExpand);
		noiseExpandLyt = (LinearLayout) findViewById(R.id.noiseExpandLyt);
		noiseExpandLyt.setOnClickListener(expandNoiseListener);
		
		changeNoiseTimeButton = (ImageView) findViewById(R.id.noiseChangeTime);
		changeNoiseTimeButton.setOnClickListener(changeNoiseTimeButtonListener);
		
		noiseInfoLyt = (LinearLayout) findViewById(R.id.noiseLyt);
		noiseInfoLyt.setVisibility(View.GONE);
		noiseClassifiedInfoLyt = (LinearLayout) findViewById(R.id.classifiedNoiseLyt);
		noiseClassifiedInfoLyt.setVisibility(View.GONE);
		
		classfiedNoiseInfoContentLyt = (LinearLayout) inflater.inflate(R.layout.custom_noise_layout, null);
		classifiedNoiseLevelPb = (ProgressBar) classfiedNoiseInfoContentLyt.findViewById(R.id.noiseLevelPb);
		classifiedNoiseLevelPb.setMax(IbicoopDataConstants.NOISE_MAX_VALUE);
		classifiedNoisePb = (ProgressBar) classfiedNoiseInfoContentLyt.findViewById(R.id.noisePb);
		classifiedNoiseLevelContentLyt = (LinearLayout) classfiedNoiseInfoContentLyt.findViewById(R.id.noiseLevelContentLyt);
		classifiedNoiseNotAvailableView = (TextView) classfiedNoiseInfoContentLyt.findViewById(R.id.noiseNotAvailableView);
		classifiedNoiseDescription = (TextView) classfiedNoiseInfoContentLyt.findViewById(R.id.noiseLevelDescription);
		classifiedNoiseText = (TextView) classfiedNoiseInfoContentLyt.findViewById(R.id.noiseLevelText);
		classifiedNoiseMaxText = (TextView) classfiedNoiseInfoContentLyt.findViewById(R.id.noiseLevelMaxText);
		classifiedNoiseReportsText = (TextView) classfiedNoiseInfoContentLyt.findViewById(R.id.noiseReportsText); 
		
		classifiedNoisePb.setVisibility(View.VISIBLE);
		classifiedNoiseLevelContentLyt.setVisibility(View.GONE);
		classifiedNoiseLevelPb.setVisibility(View.GONE);
		classifiedNoiseDescription.setVisibility(View.GONE);
		classifiedNoiseText.setVisibility(View.GONE);
		classifiedNoiseMaxText.setVisibility(View.GONE);
		classifiedNoiseReportsText.setVisibility(View.GONE);
		classifiedNoiseNotAvailableView.setVisibility(View.GONE);	
		
		noiseClassifiedInfoLyt.addView(classfiedNoiseInfoContentLyt);
		
		noiseLayouts = new HashMap<Integer, LinearLayout>();
		
		for (int j = 0; j <  NOISE_INTERVAL_MAX_MULTIPLE; j++) {
			LinearLayout noiseInfoContentLyt = (LinearLayout) inflater.inflate(R.layout.custom_noise_layout, null);
			ProgressBar noiseLevelPb = (ProgressBar) noiseInfoContentLyt.findViewById(R.id.noiseLevelPb);
			noiseLevelPb.setMax(IbicoopDataConstants.NOISE_MAX_VALUE);
			ProgressBar noisePb = (ProgressBar) noiseInfoContentLyt.findViewById(R.id.noisePb);
			LinearLayout noiseLevelContentLyt = (LinearLayout) noiseInfoContentLyt.findViewById(R.id.noiseLevelContentLyt);
			TextView noiseNotAvailableView = (TextView) noiseInfoContentLyt.findViewById(R.id.noiseNotAvailableView);
			TextView noiseDescription = (TextView) noiseInfoContentLyt.findViewById(R.id.noiseLevelDescription);
			TextView noiseText = (TextView) noiseInfoContentLyt.findViewById(R.id.noiseLevelText);
			TextView noiseMaxText = (TextView) noiseInfoContentLyt.findViewById(R.id.noiseLevelMaxText);
			TextView noiseReportsText = (TextView) noiseInfoContentLyt.findViewById(R.id.noiseReportsText); 
			
			noisePb.setVisibility(View.VISIBLE);
			noiseLevelContentLyt.setVisibility(View.GONE);
			noiseLevelPb.setVisibility(View.GONE);
			noiseDescription.setVisibility(View.GONE);
			noiseText.setVisibility(View.GONE);
			noiseMaxText.setVisibility(View.GONE);
			noiseReportsText.setVisibility(View.GONE);
			noiseNotAvailableView.setVisibility(View.GONE);
			
			noiseInfoLyt.addView(noiseInfoContentLyt);
			noiseLayouts.put(j, noiseInfoContentLyt);
		}
		
		
		//
		//
		//Crowd
		changeCrowdTimeButton = (ImageView) findViewById(R.id.crowdChangeTime);
		changeCrowdTimeButton.setOnClickListener(changeCrowdTimeButtonListener);
		
		reportCrowdButton = (ImageView) findViewById(R.id.crowdReport);
		reportCrowdButton.setOnClickListener(reportCrowdButtonListener);
		
		demandCrowdButton = (ImageView) findViewById(R.id.crowdDemand);
		demandCrowdButton.setOnClickListener(demandCrowdButtonListener);
		
		expandCrowdButton = (ImageView) findViewById(R.id.crowdExpand);

		crowdExpandLyt = (LinearLayout) findViewById(R.id.crowdExpandLyt);
		crowdExpandLyt.setOnClickListener(expandCrowdListener);		
		
		crowdLyt = (LinearLayout) findViewById(R.id.crowdLyt);
		crowdLyt.setVisibility(View.GONE);
		
		//Classified
		classifiedCrowdParentLayout = (LinearLayout) inflater.inflate(R.layout.custom_crowd_layout, null);	
		
		crowdImageViews = new ImageView[5];
		crowdImageViews[0] = (ImageView) classifiedCrowdParentLayout.findViewById(R.id.customCrowd1);
		crowdImageViews[1] = (ImageView) classifiedCrowdParentLayout.findViewById(R.id.customCrowd2);
		crowdImageViews[2] = (ImageView) classifiedCrowdParentLayout.findViewById(R.id.customCrowd3);
		crowdImageViews[3] = (ImageView) classifiedCrowdParentLayout.findViewById(R.id.customCrowd4);
		crowdImageViews[4] = (ImageView) classifiedCrowdParentLayout.findViewById(R.id.customCrowd5);
		
		nbCrowdTv = (TextView) classifiedCrowdParentLayout.findViewById(R.id.nbCustomCrowd);
		nbCrowdTv.setVisibility(View.GONE);
		
		crowdIntervalTv = (TextView) classifiedCrowdParentLayout.findViewById(R.id.customCrowdTimeInterval);
		crowdIntervalTv.setVisibility(View.GONE);
		
		crowdNbVotesTv = (TextView) classifiedCrowdParentLayout.findViewById(R.id.customCrowdNbVotes);
		crowdNbVotesTv.setVisibility(View.GONE);

		crowdNotAvailableTv = (TextView) classifiedCrowdParentLayout.findViewById(R.id.customCrowdNotAvailableView);
		crowdNotAvailableTv.setVisibility(View.GONE);
		
		crowdRatingLyt = (LinearLayout) classifiedCrowdParentLayout.findViewById(R.id.customCrowd);
		crowdRatingLyt.setVisibility(View.GONE);
		
		crowdPb = (ProgressBar) classifiedCrowdParentLayout.findViewById(R.id.customCrowdPb);
		crowdPb.setVisibility(View.VISIBLE);
		
		crowdLyt.addView(classifiedCrowdParentLayout);
		
		//General
		generalCrowdLayouts = new HashMap<Integer, LinearLayout>();
		
		for (int j = 0; j <  GENERAL_CROWD_INTERVAL_MAX_MULTIPLE; j++) {
			
			//General
			LinearLayout generalCrowdParentLayout = (LinearLayout) inflater.inflate(R.layout.custom_crowd_layout, null);
			
			ImageView[] generalCrowdImageViews = new ImageView[5];
			generalCrowdImageViews[0] = (ImageView) generalCrowdParentLayout.findViewById(R.id.customCrowd1);
			generalCrowdImageViews[1] = (ImageView) generalCrowdParentLayout.findViewById(R.id.customCrowd2);
			generalCrowdImageViews[2] = (ImageView) generalCrowdParentLayout.findViewById(R.id.customCrowd3);
			generalCrowdImageViews[3] = (ImageView) generalCrowdParentLayout.findViewById(R.id.customCrowd4);
			generalCrowdImageViews[4] = (ImageView) generalCrowdParentLayout.findViewById(R.id.customCrowd5);
			
			TextView nbGeneralCrowdTv = (TextView) generalCrowdParentLayout.findViewById(R.id.nbCustomCrowd);
			nbGeneralCrowdTv.setVisibility(View.GONE);
			
			TextView generalCrowdIntervalTv = (TextView) generalCrowdParentLayout.findViewById(R.id.customCrowdTimeInterval);
			generalCrowdIntervalTv.setVisibility(View.GONE);
			
			TextView generalCrowdNbVotesTv = (TextView) generalCrowdParentLayout.findViewById(R.id.customCrowdNbVotes);
			generalCrowdNbVotesTv.setVisibility(View.GONE);

			TextView generalCrowdNotAvailableTv = (TextView) generalCrowdParentLayout.findViewById(R.id.customCrowdNotAvailableView);
			generalCrowdNotAvailableTv.setVisibility(View.GONE);
			
			LinearLayout generalCrowdRatingLyt = (LinearLayout) generalCrowdParentLayout.findViewById(R.id.customCrowd);
			generalCrowdRatingLyt.setVisibility(View.GONE);
			
			ProgressBar generalCrowdPb = (ProgressBar) generalCrowdParentLayout.findViewById(R.id.customCrowdPb);
			generalCrowdPb.setVisibility(View.VISIBLE);
			
			crowdLyt.addView(generalCrowdParentLayout);
			
			generalCrowdLayouts.put(j, generalCrowdParentLayout);
		}
		
		
		//
		//
		//Timetable
		//
		if (metroStop != null) {
			int tubesLength = metroStop.getTubes().length;
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Tubes length = " + tubesLength);
			metroCountdownTasks = new IbicoopTubeCountdownTask[tubesLength];
			metroCountdownCallbacks = new IbicoopTubeCountdownCallback[tubesLength];
			//key = routeId, value = corresponding tube countdown tasks
			metroTasksMap = new HashMap<String, IbicoopTubeCountdownTask>();
			//key = routeId, value = corresponding tube countdown callbacks
			metroCallbacksMap = new HashMap<String, IbicoopTubeCountdownCallback>();
			//key = routeId, value = corresponding tube countdown linear layout
			metroLytMap = new HashMap<String, LinearLayout>();	
		}
		
		expandTimeTableButton = (ImageView) findViewById(R.id.timetableExpand);
		
		timeTableExpandLyt = (LinearLayout) findViewById(R.id.timetableExpandLyt);
		timeTableExpandLyt.setOnClickListener(expandTimeTableListener);
		

		timeTableLyt = (LinearLayout) findViewById(R.id.timetableSubInfoLyt);
		timeTableLyt.setVisibility(View.GONE);
		
		//
		//
		//Comment
		reportCommentButton = (ImageView) findViewById(R.id.reportComment);
		reportCommentButton.setOnClickListener(reportCommentListener);
		
		commentExpandButton = (ImageView) findViewById(R.id.commentExpand);
		
		commentExpandLyt = (LinearLayout) findViewById(R.id.commentExpandLyt);
		commentExpandLyt.setOnClickListener(commentExpandListener);

		
		//
		//
		//Twitter comment
		twitComExpandButton = (ImageView) findViewById(R.id.twitterExpand);
		
		twitComExpandLyt = (LinearLayout) findViewById(R.id.twitterExpandLyt);
		twitComExpandLyt.setVisibility(View.GONE);
		twitComExpandLyt.setOnClickListener(expandTwitComListener);
		
		twitCommentLyt = (LinearLayout) findViewById(R.id.twitterCommentLyt);
		twitCommentLyt.setVisibility(View.GONE);
		
		twitCommentSubLyt = (LinearLayout) findViewById(R.id.subTwitterCommentLyt);
		twitCommentSubLyt.setVisibility(View.GONE);
		
		twitComExpandLine = (TextView) findViewById(R.id.twitterExpandLine);
		twitComExpandLine.setVisibility(View.GONE);
		
		twitComPb = (ProgressBar) findViewById(R.id.twitterCommentPb);
		twitComPb.setVisibility(View.VISIBLE);
		
		//
		//
		//Facebook comment
		fbComExpandButton = (ImageView) findViewById(R.id.facebookExpand);
		
		fbComExpandLyt = (LinearLayout) findViewById(R.id.facebookExpandLyt);
		fbComExpandLyt.setVisibility(View.GONE);
		fbComExpandLyt.setOnClickListener(expandFbComListener);
		
		
		fbComLyt = (LinearLayout) findViewById(R.id.facebookCommentLyt);
		fbComLyt.setVisibility(View.GONE);
		
		fbComSubLyt = (LinearLayout) findViewById(R.id.subFacebookCommentLyt);
		fbComSubLyt.setVisibility(View.GONE);		

		fbComExpandLine = (TextView) findViewById(R.id.facebookExpandLine);
		fbComExpandLine.setVisibility(View.GONE);
		
		fbComPb = (ProgressBar) findViewById(R.id.facebookCommentPb);
		fbComPb.setVisibility(View.VISIBLE);

		
		//
		//
		//User comment
		userComExpandButton = (ImageView) findViewById(R.id.userExpand);
		
		userComExpandLyt = (LinearLayout) findViewById(R.id.userExpandLyt);
		userComExpandLyt.setVisibility(View.GONE);
		userComExpandLyt.setOnClickListener(expandUserComListener);
		
		userComLyt = (LinearLayout) findViewById(R.id.userCommentLyt);
		userComLyt.setVisibility(View.GONE);
		
		userComSubLyt = (LinearLayout) findViewById(R.id.subUserCommentLyt);
		userComSubLyt.setVisibility(View.GONE);
		
		userComExpandLine = (TextView) findViewById(R.id.userExpandLine);
		userComExpandLine.setVisibility(View.GONE);
		
		userComPb = (ProgressBar) findViewById(R.id.userCommentPb);
		userComPb.setVisibility(View.VISIBLE);
		
		Station stationHS = new Station(metroStop.getId(), metroStop.getTubes()[0].getId(), metroStop.getLatitude(), metroStop.getLongitude());
		happinessController = new HappinessPullController(this, stationHS);
		
		// XXX:
		new HappinessPushController(this, stationHS);

		if (metroStop != null) {
			//Make sure that Ibicoop has been started
			if (isNewIntent) {
				
				if (expandItem != null) {
					allIsExpanded = false;
					expandItem.setIcon(R.drawable.icon_expand_blue_small);
	        		closeAllInfos();
				}
			}

			if (waitForInitIbicoopTask != null) {waitForInitIbicoopTask.cancel(true);}
			
			waitForInitIbicoopTask = new WaitForInitIbicoopTask();
			AsyncTaskCompat.executeParallel(waitForInitIbicoopTask);
		
		} else {
			showAllNotAvailable();
		}
	}
	
	class WaitForInitIbicoopTask extends AsyncTask<Void, Void, Void> {
		
		@Override
		protected Void doInBackground(Void... params) {

			try{
				//Waiting to reinitialize ibicoop in the case of notification callback
				Thread.sleep(1000);
			} catch (Exception exception) {
				if (IbicoopDataConstants.DEBUG_MODE) System.err.println(exception.getMessage());
			}
			
			return null;
		}
		
		
		@Override
		protected void onPostExecute(Void param) {
			if (ibicoopStartStopTask != null) ibicoopStartStopTask.cancel(true);
			ibicoopStartStopTask = new IbicoopStartStopTask(ibicoopContext, false, ibicoopStartCallback,
					TSLApplicationContext.getInstance(), LocalStationVoteNotificationSender.class, LocalRequestNotificationSender.class);
			AsyncTaskCompat.executeParallel(ibicoopStartStopTask);				
		}
		
	}
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main_map, menu);
		getSupportMenuInflater().inflate(R.menu.menu_infos_dashboard, menu);
		
		expandItem = menu.findItem(R.id.action_expand);
		
		if (isNewIntent) {
			allIsExpanded = false;
			expandItem.setIcon(R.drawable.icon_expand_blue_small);
    		closeAllInfos();
		}
		
		signInItem = menu.findItem(R.id.action_preference);
		
		if (TSLApplicationContext.getInstance().checkIfUserIsLoggedIn()) {
			signInItem.setTitle("Sign out");
		} else {
			signInItem.setTitle("Sign in");
		}
		
		alarmItem = menu.findItem(R.id.action_alarm);
		
		if (metroStop != null) {
	
			boolean alarmIsSet = false;
			
			if (city.equals(IbicoopDataConstants.PARIS)) {
				alarmIsSet = (parisMetroDbManager.readAlarmSet(metroStop.getId()) == 1);
			} else if (city.equals(IbicoopDataConstants.LONDON)) {
				alarmIsSet = (londonMetroDbManager.readAlarmSet(metroStop.getId()) == 1);			
			}
			
			if (alarmIsSet) {
				//Alarm already set
				alarmItem.setIcon(R.drawable.icon_alarm_blue_small);
			} else {
				//Alarm not yet set
				alarmItem.setIcon(R.drawable.icon_alarm_black_small);
			}
		}
		
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case R.id.action_preference:
	        	if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Action sign in");
	        	showSignInDialog();
	            return true;
	        case R.id.action_expand:
	        	if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Action expand");
	        	allIsExpanded = !allIsExpanded;
	        	
	        	if (allIsExpanded) {
	        		expandItem.setIcon(R.drawable.icon_collapse_blue_small);
	        		openAllInfos();
	        	} else {
	        		expandItem.setIcon(R.drawable.icon_expand_blue_small);
	        		closeAllInfos();
	        	}
	        	
	            return true;
	        case R.id.action_alarm:
	        	if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Action alarm");
	        	//Show date picker here
	        	if (metroStop != null) {
	        		showAlarmSetterDialog();
	        	}
	            return true;		            
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}	
	
	private IbicoopStartCallback ibicoopStartCallback = new IbicoopStartCallback() {
		
		@Override
		public void ibicoopStartOk() {
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Ibicoop start ok");
			
			startAllTasks();
		}
		
		@Override
		public void ibicoopStartFailed() {
			if (IbicoopDataConstants. DEBUG_MODE) System.err.println("Ibicoop start failed");
		}

	};
	
	private void startAllTasks() {
		//Unsubscribe all station channel		
		unsubcribeStationChannel();
		
		//Noise
		startClassifiedNoiseTask();
		
		startNoiseTask();
		
		//Crowd
		startClassifiedCrowdTask();
		
		startGeneralCrowdTask();
		
		//Timetable
		startMultipleTimetableTask();
		
		//Twitter comment
		startTwitterTask();
		
		//Facebook comment
		startFacebookTask();
		
		//User comment
		startUserCommentTask();
		
		//Incentive
		startIncentiveTask();

	}
	
	private void unsubcribeStationChannel() {
		//Unsubscribe all station channel
		stationChannelSubscriptionTask
			= new IbicoopStationChannelSubscriptionTask(InfosDashboardActivity.this, null, true);
		
		AsyncTaskCompat.executeParallel(stationChannelSubscriptionTask);
	}
	
	private void stopAllTasks() {
		
		if (timeCounterExecutor != null) timeCounterExecutor.stopTask();
		
		//For region channel request
		if (regionChannelRequestTask != null) regionChannelRequestTask.cancel(true); 
		
		//Unsubscribe all station channel
		unsubcribeStationChannel();
		
		//Noise
		if (classifiedNoiseTask != null) noiseTask.cancel(true);
		
		if (noiseTask != null) noiseTask.cancel(true);
		
		//Crowd
		if (classifiedCrowdTask != null) classifiedCrowdTask.cancel(true);
		
		if (generalCrowdTask != null) generalCrowdTask.cancel(true);
		
		//Timetable
		if (metroCountdownTasks != null) {
			for (int i = 0; i < metroCountdownTasks.length; i++) {
				if (metroCountdownTasks[i] != null) metroCountdownTasks[i].cancel(true);
			}
		}

		
		//Twitter comment
		if (twitterTask != null) twitterTask.cancel(true);
		
		//Facebook comment
		if (fbTask != null) fbTask.cancel(true);
		
		//User comment
		if (commentTask != null) commentTask.cancel(true);
		
		//Incentive
		if (incentiveTask != null) incentiveTask.cancel(true);
	}
	
	
	private void startIncentiveTask() {
		
		if (incentiveTask != null) incentiveTask.cancel(true);
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("startIncentiveTask");
		
		if (IbicoopDataConstants.USE_JOKE) {
			incentiveTask = new IbicoopIncentiveGetterTask(this, city, metroStop.getId(), IncentiveType.JOKE, incentiveCallback);		
		} else {
			incentiveTask = new IbicoopIncentiveGetterTask(this, city, metroStop.getId(), IncentiveType.FACT, incentiveCallback);
		}
		
		AsyncTaskCompat.executeParallel(incentiveTask);
	}
	
	private IbicoopIncentiveCallback incentiveCallback = new IbicoopIncentiveCallback() {
		
		@Override
		public void getIncentiveSuccess(Incentive incentive) {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Get incentive success!");
			
			if (incentive != null) {
				incentiveContent = incentive.getStopIncentiveContent();
				//Split with dot
				if (!IbicoopDataConstants.USE_JOKE) {
					incentiveContentArray = incentiveContent.split("\\.");
				}
			}
		}
		
		@Override
		public void getIncentiveFailed() {
			if (IbicoopDataConstants. DEBUG_MODE) System.err.println("Get incentive failed!");
			
		}
	};
	
	private void startClassifiedNoiseTask() {
		
		if (classifiedNoiseTask != null) noiseTask.cancel(true);
		
		Time currentTime = new Time();
		currentTime.setToNow();
		String timeValue = currentTime.toString();	
		timeValue = timeValue.substring(9, 13);

		startClassifiedNoiseTask(timeValue);
	}
	
	private void startClassifiedNoiseTask(String timeValue) {
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("startClassifiedNoiseTask: timeValue = " + timeValue);
		
		if (classifiedNoiseTask != null) noiseTask.cancel(true);
				
		classifiedNoiseInterval = convertQueryTimeToInterval(city, timeValue);
		
		classifiedNoiseTask = new IbicoopClassifiedNoiseGetterTask(this, city, metroStop.getId(), metroStop.getTubes()[0].getId(), timeValue, classifiedNoiseCallback);
		
		classifiedNoisePb.setVisibility(View.VISIBLE);
		classifiedNoiseLevelContentLyt.setVisibility(View.GONE);
		classifiedNoiseLevelPb.setVisibility(View.GONE);
		classifiedNoiseDescription.setVisibility(View.GONE);
		classifiedNoiseText.setVisibility(View.GONE);
		classifiedNoiseMaxText.setVisibility(View.GONE);
		classifiedNoiseReportsText.setVisibility(View.GONE);
		classifiedNoiseNotAvailableView.setVisibility(View.GONE);
		
		AsyncTaskCompat.executeParallel(classifiedNoiseTask);
	}	
	
	
	private IbicoopClassifiedNoiseCallback classifiedNoiseCallback = new IbicoopClassifiedNoiseCallback() {
		
		@Override
		public void getNoiseInfo(NoiseData noiseData) {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Get classified noise data success");
				
				int noiseLevel = noiseData.getNoiseLevel();
				int noiseReports = noiseData.getNbVotes();
				
				//Show noise level
				if (noiseLevel > IbicoopDataConstants.NOISE_MAX_VALUE) noiseLevel = IbicoopDataConstants.NOISE_MAX_VALUE;
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Noise level = " + noiseLevel);
				
				classifiedNoiseLevelPb.setMax(IbicoopDataConstants.NOISE_MAX_VALUE);
	
				if (noiseLevel > 0) {
					classifiedNoisePb.setVisibility(View.GONE);
					classifiedNoiseLevelContentLyt.setVisibility(View.VISIBLE);
					classifiedNoiseLevelPb.setVisibility(View.VISIBLE);
					classifiedNoiseLevelPb.setProgress(noiseLevel);
					classifiedNoiseDescription.setVisibility(View.VISIBLE);
					classifiedNoiseDescription.setText(classifiedNoiseInterval);
					classifiedNoiseText.setVisibility(View.VISIBLE);
					classifiedNoiseText.setText("" + noiseLevel);
					classifiedNoiseMaxText.setVisibility(View.VISIBLE);
					classifiedNoiseMaxText.setText("" + IbicoopDataConstants.NOISE_MAX_VALUE);
					classifiedNoiseReportsText.setVisibility(View.VISIBLE);
					
					String noiseReportsString = String.valueOf(noiseReports);
					
					if (noiseReports > 1) {
						noiseReportsString = noiseReportsString + " reports";
					} else {
						noiseReportsString = noiseReportsString + " report";
					}
					
					classifiedNoiseReportsText.setText("" + noiseReportsString);
					
					classifiedNoiseNotAvailableView.setVisibility(View.GONE);
				} else {
					classifiedNoisePb.setVisibility(View.GONE);
					classifiedNoiseLevelPb.setVisibility(View.GONE);
					classifiedNoiseDescription.setVisibility(View.VISIBLE);
					classifiedNoiseDescription.setText(classifiedNoiseInterval + ":\nNo noise reporting");
					classifiedNoiseLevelContentLyt.setVisibility(View.GONE);
					classifiedNoiseText.setVisibility(View.GONE);
					classifiedNoiseMaxText.setVisibility(View.GONE);
					classifiedNoiseReportsText.setVisibility(View.GONE);
					classifiedNoiseNotAvailableView.setVisibility(View.GONE);
				}
	
		}
		
		@Override
		public void getNoiseFailed() {
			showClassifiedNoiseNotAvailable();
		}
	};
	
	private void startNoiseTask() {
		
		if (noiseTask != null) noiseTask.cancel(true);
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("startNoiseTask");
		
		noiseTask = new IbicoopNoiseGetterTask(this, city, metroStop.getId(), metroStop.getTubes()[0].getId(), NOISE_INTERVAL, noiseCallback);
		
		AsyncTaskCompat.executeParallel(noiseTask);
	}
	
	private IbicoopNoiseCallback noiseCallback = new IbicoopNoiseCallback() {
		
		@Override
		public void getNoiseInfos(NoiseDatasCollection noiseDatasCollection) {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Get noise success");
			
			NoiseData[] noiseDatas = noiseDatasCollection.getNoiseDatas();
			
			if (noiseDatas.length > 0) {
				
				
				int noiseDatasLength = NOISE_INTERVAL_MAX_MULTIPLE;
				
				if (noiseDatasLength > noiseDatas.length) {
					noiseDatasLength = noiseDatas.length;
				}
				
				for (int j = 0;  j < noiseDatasLength; j++) {
					
					String noiseInterval = "";
					
					if (j == 0) {
						noiseInterval = "Between now and " + NOISE_HOUR + " hours ago";
					} else {
						noiseInterval = "Between " + (NOISE_HOUR*j)  + " and " + NOISE_HOUR*(j + 1) + " hours ago";
					}
					
					//Got noise data
					int noiseLevel = noiseDatas[j].getNoiseLevel();
					int noiseReports = noiseDatas[j].getNbVotes();
					
					//Show noise level
					if (noiseLevel > IbicoopDataConstants.NOISE_MAX_VALUE) noiseLevel = IbicoopDataConstants.NOISE_MAX_VALUE;
					
					if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Noise level = " + noiseLevel);
					
					
					LinearLayout noiseInfoContentLyt = noiseLayouts.get(j);
					ProgressBar noiseLevelPb = (ProgressBar) noiseInfoContentLyt.findViewById(R.id.noiseLevelPb);
					noiseLevelPb.setMax(IbicoopDataConstants.NOISE_MAX_VALUE);
					ProgressBar noisePb = (ProgressBar) noiseInfoContentLyt.findViewById(R.id.noisePb);
					LinearLayout noiseLevelContentLyt = (LinearLayout) noiseInfoContentLyt.findViewById(R.id.noiseLevelContentLyt);
					TextView noiseNotAvailableView = (TextView) noiseInfoContentLyt.findViewById(R.id.noiseNotAvailableView);
					TextView noiseDescription = (TextView) noiseInfoContentLyt.findViewById(R.id.noiseLevelDescription);
					TextView noiseText = (TextView) noiseInfoContentLyt.findViewById(R.id.noiseLevelText);
					TextView noiseMaxText = (TextView) noiseInfoContentLyt.findViewById(R.id.noiseLevelMaxText);
					TextView noiseReportsText = (TextView) noiseInfoContentLyt.findViewById(R.id.noiseReportsText);
		
					if (noiseLevel > 0) {
						noisePb.setVisibility(View.GONE);
						noiseLevelContentLyt.setVisibility(View.VISIBLE);
						noiseLevelPb.setVisibility(View.VISIBLE);
						noiseLevelPb.setProgress(noiseLevel);
						noiseDescription.setVisibility(View.VISIBLE);
						noiseDescription.setText(noiseInterval);
						noiseText.setVisibility(View.VISIBLE);
						noiseText.setText("" + noiseLevel);
						noiseMaxText.setVisibility(View.VISIBLE);
						noiseMaxText.setText("" + IbicoopDataConstants.NOISE_MAX_VALUE);
						noiseReportsText.setVisibility(View.VISIBLE);
						
						String noiseReportsString = String.valueOf(noiseReports);
						
						if (noiseReports > 1) {
							noiseReportsString = noiseReportsString + " reports";
						} else {
							noiseReportsString = noiseReportsString + " report";
						}
						
						noiseReportsText.setText("" + noiseReportsString);
						
						noiseNotAvailableView.setVisibility(View.GONE);
					} else {
						noisePb.setVisibility(View.GONE);
						noiseLevelPb.setVisibility(View.GONE);
						noiseDescription.setVisibility(View.VISIBLE);
						noiseDescription.setText(noiseInterval + ":\nNo noise reporting");
						noiseLevelContentLyt.setVisibility(View.GONE);
						noiseText.setVisibility(View.GONE);
						noiseMaxText.setVisibility(View.GONE);
						noiseReportsText.setVisibility(View.GONE);
						noiseNotAvailableView.setVisibility(View.GONE);
					}
					
				}


			} else {
				
				for (int j = 0;  j < NOISE_INTERVAL_MAX_MULTIPLE; j++) {
					
					String noiseInterval = "";
					
					if (j == 0) {
						noiseInterval = "Between now and " + NOISE_HOUR + " hours ago";
					} else {
						noiseInterval = "Between " + (NOISE_HOUR*j)  + " and " + NOISE_HOUR*(j + 1) + " hours ago";
					}
					
					LinearLayout noiseInfoContentLyt = noiseLayouts.get(j);
					ProgressBar noiseLevelPb = (ProgressBar) noiseInfoContentLyt.findViewById(R.id.noiseLevelPb);
					noiseLevelPb.setMax(IbicoopDataConstants.NOISE_MAX_VALUE);
					ProgressBar noisePb = (ProgressBar) noiseInfoContentLyt.findViewById(R.id.noisePb);
					LinearLayout noiseLevelContentLyt = (LinearLayout) noiseInfoContentLyt.findViewById(R.id.noiseLevelContentLyt);
					TextView noiseNotAvailableView = (TextView) noiseInfoContentLyt.findViewById(R.id.noiseNotAvailableView);
					TextView noiseDescription = (TextView) noiseInfoContentLyt.findViewById(R.id.noiseLevelDescription);
					TextView noiseText = (TextView) noiseInfoContentLyt.findViewById(R.id.noiseLevelText);
					TextView noiseMaxText = (TextView) noiseInfoContentLyt.findViewById(R.id.noiseLevelMaxText);
					TextView noiseReportsText = (TextView) noiseInfoContentLyt.findViewById(R.id.noiseReportsText);
					
					noiseDescription.setText(noiseInterval + ":\nLatest noise information not available");
					noisePb.setVisibility(View.GONE);
					noiseLevelPb.setVisibility(View.GONE);
					noiseDescription.setVisibility(View.VISIBLE);
					noiseLevelContentLyt.setVisibility(View.GONE);
					noiseText.setVisibility(View.GONE);
					noiseMaxText.setVisibility(View.GONE);
					noiseReportsText.setVisibility(View.GONE);
					noiseNotAvailableView.setVisibility(View.GONE);
				}
			}
		}
		
		@Override
		public void getNoiseFailed() {
			if (IbicoopDataConstants. DEBUG_MODE) System.err.println("Get noise failed");
			showNoiseDataNotAvailable();
		}
	};
	
	//timeValue: HHmm
	private void startClassifiedCrowdTask(String timeValue) {
		if (classifiedCrowdTask != null) classifiedCrowdTask.cancel(true);

		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("startCrowdTask(timeValue): timeValue = " + timeValue);
		
        //In Paris we can have different crowd for diffrent lines
        //FIXME: We can only have the same crowd for all lines in a tube station in London
		classifiedCrowdTask = new IbicoopClassifiedCrowdednessGetterTask(this, city, metroStop.getId(), metroStop.getTubes()[0].getId(), timeValue, classifiedCrowdCallback);
		
		crowdPb.setVisibility(View.VISIBLE);
		crowdRatingLyt.setVisibility(View.GONE);
		nbCrowdTv.setVisibility(View.GONE);
		crowdIntervalTv.setVisibility(View.GONE);
		crowdNbVotesTv.setVisibility(View.GONE);
		crowdNotAvailableTv.setVisibility(View.GONE);		
		
		//classifiedCrowdInterval = convertTimestampToInterval(city, System.currentTimeMillis());
		classifiedCrowdInterval = convertQueryTimeToInterval(city, timeValue);
		AsyncTaskCompat.executeParallel(classifiedCrowdTask);
	}

	private void startClassifiedCrowdTask() {
		//Launch get classfied crowd task
		Time currentTime = new Time();
		currentTime.setToNow();
		String timeValue = currentTime.toString();	
		timeValue = timeValue.substring(9, 13);
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println("startCrowdTask(): timeValue = " + timeValue);
		startClassifiedCrowdTask(timeValue);
	}
	
	private void startGeneralCrowdTask() {
		if (generalCrowdTask != null) generalCrowdTask.cancel(true);
		
        //In Paris we can have different crowd for diffrent lines
        //FIXME: We can only have the same crowd for all lines in a tube station in London
		generalCrowdTask = new IbicoopGeneralCrowdednessGetterTask(this, city, metroStop.getId(), metroStop.getTubes()[0].getId(), GENERAL_CROWD_INTERVAL, generalCrowdCallback);
		
		resetAllGeneralCrowdViews();
		
		AsyncTaskCompat.executeParallel(generalCrowdTask);
	}
	
	private void resetAllGeneralCrowdViews() {
		
		for (int j = 0; j <  GENERAL_CROWD_INTERVAL_MAX_MULTIPLE; j++) {
			
			//General
			LinearLayout generalCrowdParentLayout = generalCrowdLayouts.get(j);

			TextView nbGeneralCrowdTv = (TextView) generalCrowdParentLayout.findViewById(R.id.nbCustomCrowd);
			nbGeneralCrowdTv.setVisibility(View.GONE);
			
			TextView generalCrowdIntervalTv = (TextView) generalCrowdParentLayout.findViewById(R.id.customCrowdTimeInterval);
			generalCrowdIntervalTv.setVisibility(View.GONE);
			
			TextView generalCrowdNbVotesTv = (TextView) generalCrowdParentLayout.findViewById(R.id.customCrowdNbVotes);
			generalCrowdNbVotesTv.setVisibility(View.GONE);

			TextView generalCrowdNotAvailableTv = (TextView) generalCrowdParentLayout.findViewById(R.id.customCrowdNotAvailableView);
			generalCrowdNotAvailableTv.setVisibility(View.GONE);
			
			LinearLayout generalCrowdRatingLyt = (LinearLayout) generalCrowdParentLayout.findViewById(R.id.customCrowd);
			generalCrowdRatingLyt.setVisibility(View.GONE);
			
			ProgressBar generalCrowdPb = (ProgressBar) generalCrowdParentLayout.findViewById(R.id.customCrowdPb);
			generalCrowdPb.setVisibility(View.VISIBLE);
		}
	}
	
	private void startMultipleTimetableTask() {
		
		if (metroStop != null) {
			
			timeTableLyt.removeAllViews();
			
			Tube[] tubes = metroStop.getTubes();
			
			for (int i = 0; i < tubes.length; i++) {			
				Tube tube = tubes[i];
				
				String routeId = tube.getId();
				String routeName = tube.getName();
				
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("startMultipleTimetableTask : " + i + ", routeId = " + routeId + ", routeName = " + routeName);
				
				LayoutInflater inflater = InfosDashboardActivity.this.getLayoutInflater();
				
				LinearLayout metroTimetableLyt = (LinearLayout) inflater.inflate(R.layout.custom_time_table_parent_layout,  null);	
				
				//Initialize line name (route name)
				TextView lineName = (TextView) metroTimetableLyt.findViewById(R.id.lineName);
				lineName.setText(routeName);
				
				ProgressBar pb = (ProgressBar) metroTimetableLyt.findViewById(R.id.timetablePb);
				pb.setVisibility(View.VISIBLE);

				metroLytMap.put(routeId, metroTimetableLyt);
				
				timeTableLyt.addView(metroTimetableLyt);

				IbicoopTubeCountdownCallback metroCallback = metroCountdownCallbacks[i];
	
					if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Initialize metro callback");
					
					metroCallback = new IbicoopTubeCountdownCallback() {
						
						@Override
						public void tubeRealtimeInfo(String routeId, TubeRealTimeInfosCollection info) {

							if (IbicoopDataConstants.DEBUG_MODE) {
								System.out.println("Get multiple metro countdown success for routeId = " + routeId);
								Toast.makeText(InfosDashboardActivity.this, "Get multiple metro countdown success for routeId = " + routeId,
										Toast.LENGTH_SHORT).show();
							}

							LayoutInflater li = LayoutInflater.from(InfosDashboardActivity.this);
							
							TubeRealTimeInfo[] realTimeInfos = info.getTubeRealTimeInfos();
							LinearLayout parentLyt = metroLytMap.get(routeId);

							ProgressBar subTimetablePb = (ProgressBar) parentLyt.findViewById(R.id.timetablePb);
							subTimetablePb.setVisibility(View.GONE);
							
							LinearLayout subTimetableLyt = (LinearLayout) parentLyt.findViewById(R.id.subTimeTableLyt);
							subTimetableLyt.setVisibility(View.VISIBLE);
							
							if (realTimeInfos.length > 0) {
								
								for (TubeRealTimeInfo realTimeInfo: realTimeInfos) {
									
									if (!realTimeInfo.getTransport().getId().equals(routeId)) {
										continue;
									}
									
									LinearLayout ttLayout = (LinearLayout) li.inflate(R.layout.custom_layout_time_table2, null);
									TextView line = (TextView) ttLayout.findViewById(R.id.line);
									TextView time = (TextView) ttLayout.findViewById(R.id.time);
									TextView direction = (TextView) ttLayout.findViewById(R.id.direction);
									
									String lineString = realTimeInfo.getTransport().getName();
									
									if (city.equals(IbicoopDataConstants.LONDON)) {
										//Check if the received route id correspond to the callback route id					
										//Get platform
										lineString = lineString + " - " + realTimeInfo.getTubePlatform().getName();
									}

									String timeString = realTimeInfo.getTime();
									String directionString = realTimeInfo.getDestination();
									
									line.setText(lineString);
									time.setText(timeString);
									direction.setText(directionString);
									subTimetableLyt.addView(ttLayout);
								}
								
							} else {
								LinearLayout ttLayout = (LinearLayout) li.inflate(R.layout.custom_layout_time_table2, null);
								TextView line = (TextView) ttLayout.findViewById(R.id.line);
								TextView time = (TextView) ttLayout.findViewById(R.id.time);
								TextView direction = (TextView) ttLayout.findViewById(R.id.direction);
								line.setText("");
								time.setText("Not available");
								direction.setText("");
								subTimetableLyt.addView(ttLayout);				
							}			
						}
						
						@Override
						public void tubeCountdownFailed(String routeId) {
							if (IbicoopDataConstants.DEBUG_MODE) {
								System.out.println("Get metro countdown failed");
								Toast.makeText(InfosDashboardActivity.this, "Get metro countdown failed", Toast.LENGTH_SHORT).show();
							}
							
							LayoutInflater li = LayoutInflater.from(InfosDashboardActivity.this);						
							LinearLayout ttLayout = (LinearLayout) li.inflate(R.layout.custom_layout_time_table2, null);
							TextView line = (TextView) ttLayout.findViewById(R.id.line);
							TextView time = (TextView) ttLayout.findViewById(R.id.time);
							TextView direction = (TextView) ttLayout.findViewById(R.id.direction);
							line.setText("");
							time.setText("Not available");
							direction.setText("");

							LinearLayout parentLyt = metroLytMap.get(routeId);
							LinearLayout subTimetableLyt = (LinearLayout) parentLyt.findViewById(R.id.subTimeTableLyt);
							subTimetableLyt.setVisibility(View.VISIBLE);
				
							ProgressBar subTimetablePb = (ProgressBar) parentLyt.findViewById(R.id.timetablePb);
							subTimetablePb.setVisibility(View.GONE);							
							
							subTimetableLyt.addView(ttLayout);
						}
					};
				
				IbicoopTubeCountdownTask metroTask = metroCountdownTasks[i];
				
				if (metroTask != null) metroTask.cancel(true);
				
				metroTask = new  IbicoopTubeCountdownTask(this, city, metroStop.getId(), routeId, 3, metroCallback);

				metroCountdownTasks[i] = metroTask;
				metroCountdownCallbacks[i] = metroCallback;
				metroTasksMap.put(routeId, metroTask);
				metroCallbacksMap.put(routeId, metroCallback);
			}//End of i-th iteration
			
			for (int i = 0; i < metroCountdownTasks.length; i++) {
				AsyncTaskCompat.executeParallel(metroCountdownTasks[i]);
			}
		}
	}
	
	private void startTwitterTask() {
		if (twitterTask != null) twitterTask.cancel(true);
		
		twitterTask = new IbicoopTwitterCommentGetterTask(this, city, metroStop.getId(), 5, twitterCallback);
		
		twitComPb.setVisibility(View.VISIBLE);	
		twitCommentSubLyt.setVisibility(View.GONE);
		twitCommentSubLyt.removeAllViews();
		
		AsyncTaskCompat.executeParallel(twitterTask);		
	}
	
	private void startFacebookTask() {
		if (fbTask != null) fbTask.cancel(true);
		
		fbTask = new IbicoopFacebookCommentsTask(this, city, metroStop.getId(), 5, facebookCallback);
		
		fbComPb.setVisibility(View.VISIBLE);
		fbComSubLyt.setVisibility(View.GONE);
		fbComSubLyt.removeAllViews();
		
		AsyncTaskCompat.executeParallel(fbTask);		
	}
	
	private void startUserCommentTask() {
		if (commentTask != null) commentTask.cancel(true);
		
        //In Paris we can have different comment for diffrent lines
        //FIXME: We can only have the same comment for all lines in a tube station in London		
		commentTask = new IbicoopCommentGetterTask(this, city,  metroStop.getId(), metroStop.getTubes()[0].getId(), 5, commentCallback);

		userComPb.setVisibility(View.VISIBLE);
		userComSubLyt.setVisibility(View.GONE);
		userComSubLyt.removeAllViews();
		
		AsyncTaskCompat.executeParallel(commentTask);		
	}
	
	private void openAllInfos() {
		//Open noise info
		noiseIsExpanded = true;
		expandNoiseButton.setImageResource(R.drawable.icon_collapse);
		noiseInfoLyt.setVisibility(View.VISIBLE);
		noiseClassifiedInfoLyt.setVisibility(View.VISIBLE);
		
		//Open crowd info
		crowdIsExpanded = true;
		expandCrowdButton.setImageResource(R.drawable.icon_collapse);
		crowdLyt.setVisibility(View.VISIBLE);
		
		//Open timetable info
		timeTableIsExpanded = true;
		expandTimeTableButton.setImageResource(R.drawable.icon_collapse);
		
		timeTableLyt.setVisibility(View.VISIBLE);
		
		//Open comments
		commentIsExpanded = true;
		commentExpandButton.setImageResource(R.drawable.icon_collapse);
		
		twitComExpandLyt.setVisibility(View.VISIBLE);
		fbComExpandLyt.setVisibility(View.VISIBLE);
		userComExpandLyt.setVisibility(View.VISIBLE);
		
		twitComExpandLine.setVisibility(View.VISIBLE);
		fbComExpandLine.setVisibility(View.VISIBLE);
		userComExpandLine.setVisibility(View.VISIBLE);
		
		twitComIsExpanded = true;
		twitComExpandButton.setImageResource(R.drawable.icon_collapse);
		twitCommentLyt.setVisibility(View.VISIBLE);		
		
		fbComIsExpanded = true;
		fbComExpandButton.setImageResource(R.drawable.icon_collapse);
		fbComLyt.setVisibility(View.VISIBLE);
		
		userComIsExpanded = true;
		userComExpandButton.setImageResource(R.drawable.icon_collapse);
		userComLyt.setVisibility(View.VISIBLE);
	}
	
	private void closeAllInfos() {
		
		//Close noise info
		noiseIsExpanded = false;
		expandNoiseButton.setImageResource(R.drawable.icon_expand);	
		noiseInfoLyt.setVisibility(View.GONE);
		noiseClassifiedInfoLyt.setVisibility(View.GONE);

		
		//Close crowd info
		crowdIsExpanded = false;
		expandCrowdButton.setImageResource(R.drawable.icon_expand);
		crowdLyt.setVisibility(View.GONE);
		
		//Close timetable info
		timeTableIsExpanded = false;
		expandTimeTableButton.setImageResource(R.drawable.icon_expand);
		
		timeTableLyt.setVisibility(View.GONE);
		
		//Close comments
		commentIsExpanded = false;
		commentExpandButton.setImageResource(R.drawable.icon_expand);
		
		twitComExpandLyt.setVisibility(View.GONE);
		fbComExpandLyt.setVisibility(View.GONE);
		userComExpandLyt.setVisibility(View.GONE);
		
		twitComExpandLine.setVisibility(View.GONE);
		fbComExpandLine.setVisibility(View.GONE);
		userComExpandLine.setVisibility(View.GONE);
		
		twitComIsExpanded = false;
		twitComExpandButton.setImageResource(R.drawable.icon_expand);
		twitCommentLyt.setVisibility(View.GONE);		
		
		fbComIsExpanded = false;
		fbComExpandButton.setImageResource(R.drawable.icon_expand);
		fbComLyt.setVisibility(View.GONE);
		
		userComIsExpanded = false;
		userComExpandButton.setImageResource(R.drawable.icon_expand);
		userComLyt.setVisibility(View.GONE);
		
	}
	
	private View.OnClickListener changeNoiseTimeButtonListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Change noise interval");
			
			if (metroStop != null) {
				showChangeNoiseTimeDialog();
			}
		}
	};	
	
	
	private View.OnClickListener changeCrowdTimeButtonListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Change crowd interval");
			
			if (metroStop != null) {
				showChangeCrowdTimeDialog();
			}
		}
	};
	
	private View.OnClickListener demandCrowdButtonListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Demand crowd");
			
			if (metroStop != null) {
				showShowDemandCrowdDialog();
			}
		}
	};	
	
	private void showShowDemandCrowdDialog() {
		AlertDialog.Builder builder = AlertDialogCompat.getAlertBuilder(InfosDashboardActivity.this);
		LayoutInflater inflater = InfosDashboardActivity.this.getLayoutInflater();
		
		ScrollView localSv = (ScrollView) inflater.inflate(R.layout.custom_incentive_layout, null);
		demandCrowdContentTv = (TextView) localSv.findViewById(R.id.incentiveContent);
		demandCrowdContentTv.setText("Demand crowd data from another user in the region?");
		builder.setTitle("Demand crowd data");
		builder.setView(localSv);
		builder.setPositiveButton("Demand", new DialogInterface.OnClickListener() {
		      public void onClick(DialogInterface dialog, int which) {
		    	  
		    	  //Demand crowd here
		    	  if (regionChannelRequestTask != null)  regionChannelRequestTask.cancel(true);
		    	  
		    	  if (metroStop != null) {
			    	  regionChannelRequestTask = new IbicoopRegionChannelRequest(
			    			  InfosDashboardActivity.this, metroStop, TSLApplicationContext.getInstance(),
			    			  LocalStationVoteNotificationSender.class, LocalRequestNotificationSender.class);
			    	  AsyncTaskCompat.executeParallel(regionChannelRequestTask); 
		    	  }

				//Launch counter of 10 minutes after making request
				if (timeCounterExecutor != null) timeCounterExecutor.stopTask();
				timeCounterExecutor = new TimeCounterExecutor(UNSUBSCRIBE_STATION_CHANNEL_TIMEOUT_MS, timeCounterCallback);
				
				showShowDemandCrowdFinishDialog();
				
				dialog.dismiss();
		    }
		});
		
		AlertDialog dialog = AlertDialogCompat.getAlertDialog(builder);
		dialog.show();
	}

	private void showShowDemandCrowdFinishDialog() {
		AlertDialog.Builder builder = AlertDialogCompat.getAlertBuilder(InfosDashboardActivity.this);
		LayoutInflater inflater = InfosDashboardActivity.this.getLayoutInflater();
		
		ScrollView localSv = (ScrollView) inflater.inflate(R.layout.custom_incentive_layout, null);
		demandCrowdContentTv = (TextView) localSv.findViewById(R.id.incentiveContent);
		demandCrowdContentTv.setText("You might receive crowd response from another user soon. Please be patience.");
		builder.setTitle("Waiting for crowd response");
		builder.setView(localSv);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
		      public void onClick(DialogInterface dialog, int which) {
		    	  dialog.dismiss();
		    }
		});
		
		AlertDialog dialog = AlertDialogCompat.getAlertDialog(builder);
		dialog.show();
	}	
	
	
	private TimeCounterCallback timeCounterCallback = new TimeCounterCallback() {
		
		@Override
		public void timeOut() {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Time counter callback timeout");
			//Unsubscribe from station channel
			unsubcribeStationChannel();
			timeCounterExecutor.stopTask();
		}
	};
	
	private View.OnClickListener reportCrowdButtonListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Report crowd");
			
			if (metroStop != null) {
				if (TSLApplicationContext.getInstance().checkIfUserIsLoggedIn()) {
					showShowReportCrowdDialog();
				} else {
					showPleaseSignInDialog();
				}				
			}
		}
	};
	
	private void showIncentiveDialog() {
		AlertDialog.Builder builder = AlertDialogCompat.getAlertBuilder(InfosDashboardActivity.this);
		
		LayoutInflater inflater = InfosDashboardActivity.this.getLayoutInflater();
		
		ScrollView localSv = (ScrollView) inflater.inflate(R.layout.custom_incentive_layout, null);
		incentiveContentTv = (TextView) localSv.findViewById(R.id.incentiveContent);
		
		if (incentiveContent != null) {
			//Set incentive content
			if ((incentiveContentArray != null) && (incentiveContentArray.length > 0)) {
				int min = 0;
				int max = incentiveContentArray.length;
				int randomIndex = (int) (min + (Math.random() * (max - min)));
				if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Incentive array: min = " + min + ", max = " + max + ", random index = " + randomIndex);
				String contentText = incentiveContentArray[randomIndex].replaceAll("^\\s+", "");
				contentText = Character.toUpperCase(contentText.charAt(0)) + contentText.substring(1);
				incentiveContentTv.setText(contentText);
			} else {
				//Show initial content
				incentiveContentTv.setText(incentiveContent);		
			}

		} else {
			incentiveContentTv.setText("Thank you for reporting");
		}
		
		builder.setTitle("Thank you for reporting");
		builder.setView(localSv);
		builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
		      public void onClick(DialogInterface dialog, int which) {
		    	  startIncentiveTask();
		    	  dialog.dismiss();
		    }
		});
		
		AlertDialog dialog = AlertDialogCompat.getAlertDialog(builder);
		dialog.show();
	}
	
	
	private void showChangeNoiseTimeDialog() {
		
		AlertDialog.Builder builder = AlertDialogCompat.getAlertBuilder(InfosDashboardActivity.this);
		
		LayoutInflater inflater = InfosDashboardActivity.this.getLayoutInflater();
		
		LinearLayout alarmPickerLayout = (LinearLayout) inflater.inflate(R.layout.custom_alarm_picker,  null);
		timePickerForNoise = (TimePicker) alarmPickerLayout.findViewById(R.id.timePicker);
		timePickerForNoise.setIs24HourView(true);
		
		noiseSetLayout = (LinearLayout) alarmPickerLayout.findViewById(R.id.timeAlreadySetLyt);
		
		TextView titleView = (TextView) inflater.inflate(R.layout.custom_alarm_picker_title, null);
		titleView.setText("Noise interval");
		
		builder.setCustomTitle(titleView);
		builder.setView(alarmPickerLayout);
		
		String buttonText = "Select";

		String format = "HHmm";
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String date = sdf.format(new Date());

		if (timePickerForNoise != null) {
			timePickerForNoise.setCurrentHour(Integer.parseInt(date.substring(0, 2)));
			timePickerForNoise.setCurrentMinute(Integer.parseInt(date.substring(2)));
			timePickerForNoise.setVisibility(View.VISIBLE);
		}
		
		if (noiseSetLayout != null) {
			noiseSetLayout.setVisibility(View.GONE);
		}
		
		builder.setNegativeButton(buttonText, new DialogInterface.OnClickListener() {
		      public void onClick(DialogInterface dialog, int which) {
		    	  
		    	  int hour = timePickerForNoise.getCurrentHour();

		    	  String hourString = String.valueOf(hour);
		    	  
		    	  if (hour < 10) {
		    		  hourString = "0" + hourString; 
		    	  }
		    	  
		    	  int minute = timePickerForNoise.getCurrentMinute();
		    	  
		    	  String minuteString = String.valueOf(minute);
		    	  
		    	  if (minute < 10) {
		    		  minuteString = "0" + minuteString; 
		    	  }
		    	  
		    	  String timeValue = hourString + minuteString;
		    	  startClassifiedNoiseTask(timeValue);
		    }
		});
		
		AlertDialog dialog = AlertDialogCompat.getAlertDialog(builder);
		dialog.show();
	}
	
	private void showChangeCrowdTimeDialog() {
		
		AlertDialog.Builder builder = AlertDialogCompat.getAlertBuilder(InfosDashboardActivity.this);
		
		LayoutInflater inflater = InfosDashboardActivity.this.getLayoutInflater();
		
		LinearLayout alarmPickerLayout = (LinearLayout) inflater.inflate(R.layout.custom_alarm_picker,  null);
		timePickerForCrowd = (TimePicker) alarmPickerLayout.findViewById(R.id.timePicker);
		timePickerForCrowd.setIs24HourView(true);
		
		crowdSetLayout = (LinearLayout) alarmPickerLayout.findViewById(R.id.timeAlreadySetLyt);
		
		TextView titleView = (TextView) inflater.inflate(R.layout.custom_alarm_picker_title, null);
		titleView.setText("Crowd interval");
		
		builder.setCustomTitle(titleView);
		builder.setView(alarmPickerLayout);
		
		String buttonText = "Select";

		String format = "HHmm";
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String date = sdf.format(new Date());

		if (timePickerForCrowd != null) {
			timePickerForCrowd.setCurrentHour(Integer.parseInt(date.substring(0, 2)));
			timePickerForCrowd.setCurrentMinute(Integer.parseInt(date.substring(2)));
			timePickerForCrowd.setVisibility(View.VISIBLE);
		}
		
		if (crowdSetLayout != null) {
			crowdSetLayout.setVisibility(View.GONE);
		}
		
		builder.setNegativeButton(buttonText, new DialogInterface.OnClickListener() {
		      public void onClick(DialogInterface dialog, int which) {
		    	  
		    	  int hour = timePickerForCrowd.getCurrentHour();

		    	  String hourString = String.valueOf(hour);
		    	  
		    	  if (hour < 10) {
		    		  hourString = "0" + hourString; 
		    	  }
		    	  
		    	  int minute = timePickerForCrowd.getCurrentMinute();
		    	  
		    	  String minuteString = String.valueOf(minute);
		    	  
		    	  if (minute < 10) {
		    		  minuteString = "0" + minuteString; 
		    	  }
		    	  
		    	  String timeValue = hourString + minuteString;
		    	  startClassifiedCrowdTask(timeValue);
		    }
		});
		
		AlertDialog dialog = AlertDialogCompat.getAlertDialog(builder);
		dialog.show();	
	}
	
	private void showAlarmSetterDialog() {
		AlertDialog.Builder builder = AlertDialogCompat.getAlertBuilder(InfosDashboardActivity.this);
		
		LayoutInflater inflater = InfosDashboardActivity.this.getLayoutInflater();
		
		LinearLayout alarmPickerLayout = (LinearLayout) inflater.inflate(R.layout.custom_alarm_picker,  null);
		timePicker = (TimePicker) alarmPickerLayout.findViewById(R.id.timePicker);
		timePicker.setIs24HourView(true);
		
		alarmSetLayout = (LinearLayout) alarmPickerLayout.findViewById(R.id.timeAlreadySetLyt);
		hourTv = (TextView) alarmPickerLayout.findViewById(R.id.timeHour);
		minuteTv = (TextView) alarmPickerLayout.findViewById(R.id.timeMinute);
		
		TextView titleView = (TextView) inflater.inflate(R.layout.custom_alarm_picker_title, null);
		
		builder.setCustomTitle(titleView);
		builder.setView(alarmPickerLayout);
		
		String buttonText = "Activate";
		
		boolean alarmIsSet = false;
		String time = "";
		
		if (city.equals(IbicoopDataConstants.PARIS)) {
			
			alarmIsSet = (parisMetroDbManager.readAlarmSet(metroStop.getId()) == 1);
			time = parisMetroDbManager.readAlarmTime(metroStop.getId());
			
		} else if (city.equals(IbicoopDataConstants.LONDON)) {
			
			alarmIsSet = (londonMetroDbManager.readAlarmSet(metroStop.getId()) == 1);
			time = londonMetroDbManager.readAlarmTime(metroStop.getId());
			
		}
		
		
		if (alarmIsSet) {
			//Alarm already set
			buttonText = "Unactivate";

			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Alarm time = " + time);
			
			if (timePicker != null) {
				timePicker.setCurrentHour(Integer.parseInt(time.substring(0, 2)));
				timePicker.setCurrentMinute(Integer.parseInt(time.substring(2)));
				timePicker.setVisibility(View.GONE);
			}
			
			if (alarmSetLayout != null) {
				hourTv.setText(time.substring(0, 2));
				minuteTv.setText(time.substring(2));
				alarmSetLayout.setVisibility(View.VISIBLE);
			}
			
		} else {
			//Alarm not yet set
			String format = "HHmm";
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			String date = sdf.format(new Date());
			
			if (timePicker != null) {
				timePicker.setCurrentHour(Integer.parseInt(date.substring(0, 2)));
				timePicker.setCurrentMinute(Integer.parseInt(date.substring(2)));
				timePicker.setVisibility(View.VISIBLE);				
			}
			
			if (alarmSetLayout != null) {
				hourTv.setText(date.substring(0, 2));
				minuteTv.setText(date.substring(2));
				alarmSetLayout.setVisibility(View.GONE);
			}
		}
		
		builder.setNegativeButton(buttonText, new DialogInterface.OnClickListener() {
		      public void onClick(DialogInterface dialog, int which) {
		    	 
		    boolean alarmIsSet = false;
		    
		    if (city.equals(IbicoopDataConstants.PARIS)) {
		    	alarmIsSet = (parisMetroDbManager.readAlarmSet(metroStop.getId()) == 0);	    	
		    } else if (city.equals(IbicoopDataConstants.LONDON)) {
		    	alarmIsSet = (londonMetroDbManager.readAlarmSet(metroStop.getId()) == 0);
		    }
		    
		    	  
			if (alarmIsSet) {
				//Alarm not yet set
	    	  if (timePicker != null) {
	    		  if (setInfoAlarmTask != null) setInfoAlarmTask.cancel(true);

	    		  setInfoAlarmTask = new SetInfoAlarmTask(timePicker.getCurrentHour(), timePicker.getCurrentMinute());
	    		  
	    		  AsyncTaskCompat.executeParallel(setInfoAlarmTask);
	    	  }
			}
			else {
				//Alarm already set
				if (removeInfoAlarmTask != null) removeInfoAlarmTask.cancel(true);
				
				removeInfoAlarmTask =  new RemoveInfoAlarmTask();
				AsyncTaskCompat.executeParallel(removeInfoAlarmTask);
			}	 
			
		    }
		});
		
		AlertDialog dialog = AlertDialogCompat.getAlertDialog(builder);
		dialog.show();		
	}
	
	class SetInfoAlarmTask extends AsyncTask<Void, Void, Void> {
		
		int hour;
		int minute;
		
		public SetInfoAlarmTask(int hour, int minute) {
			this.hour = hour;
			this.minute = minute;
		}
		

		@Override
		protected Void doInBackground(Void... params) {
			
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Set alarm for " + hour + ":" + minute);
			
			Calendar calendar = Calendar.getInstance();
			
			calendar.set(Calendar.HOUR_OF_DAY, hour);
			calendar.set(Calendar.MINUTE, minute);
			calendar.set(Calendar.SECOND, 0);
			
			//Use stop id as request code
			int requestCode = 0;
			
			if (city.equals(IbicoopDataConstants.PARIS)) {
				requestCode = Integer.parseInt(metroStop.getId());				
			} else if (city.equals(IbicoopDataConstants.LONDON)) {
				requestCode = Integer.parseInt(metroStop.getLocationCode());
			}

			Intent intent = new Intent(InfosDashboardActivity.this, MetroInfoNotificationService.class);
			intent.putExtra(IntentConstants.TUBE_STATION_GSON, metroStopDesc);
			PendingIntent pendingIntent = PendingIntent.getService(InfosDashboardActivity.this, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
			AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
			alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
			
			if (city.equals(IbicoopDataConstants.PARIS)) {
				parisMetroDbManager.updateAlarmSet(metroStop.getId(), 1);
				parisMetroDbManager.updateAlarmTime(metroStop.getId(), hour, minute);				
			} else {
				londonMetroDbManager.updateAlarmSet(metroStop.getId(), 1);
				londonMetroDbManager.updateAlarmTime(metroStop.getId(), hour, minute);				
			}

			return null;
		}
		
		@Override 
		protected void onPostExecute(Void param) {
			
			boolean alarmIsSet = false;
			
			if (city.equals(IbicoopDataConstants.PARIS)) {
				alarmIsSet = (parisMetroDbManager.readAlarmSet(metroStop.getId()) == 1);
			} else {
				alarmIsSet = (londonMetroDbManager.readAlarmSet(metroStop.getId()) == 1);
			}
			
			if (alarmIsSet) {
				//Alarm already set
				alarmItem.setIcon(R.drawable.icon_alarm_blue_small);
			} else {
				//Alarm not yet set
				alarmItem.setIcon(R.drawable.icon_alarm_black_small);
			}
			
			if (alarmSetLayout != null) {
				String hourString = String.valueOf(hour);
				String minuteString = String.valueOf(minute);
				
				if (hour < 10) hourString = "0" + hourString;
				if (minute < 10) minuteString = "0" + minuteString;
				
				hourTv.setText(hourString);
				minuteTv.setText(minuteString);
				alarmSetLayout.setVisibility(View.VISIBLE);
			}
	
			if (timePicker != null) {
				timePicker.setVisibility(View.GONE);
			}
			
		}
	}
	
	class RemoveInfoAlarmTask  extends AsyncTask<Void, Void, Void> {
	
		@Override
		protected Void doInBackground(Void... params) {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Remove alarm");
			
			//Use stop id as request code
			int requestCode = 0;
			
			if (city.equals(IbicoopDataConstants.PARIS)) {
				requestCode = Integer.parseInt(metroStop.getId());				
			} else if (city.equals(IbicoopDataConstants.LONDON)) {
				requestCode = Integer.parseInt(metroStop.getLocationCode());
			}
			
			Intent intent = new Intent(InfosDashboardActivity.this, MetroInfoNotificationService.class);
			intent.putExtra(IntentConstants.TUBE_STATION_GSON, metroStopDesc);
			PendingIntent pendingIntent = PendingIntent.getService(InfosDashboardActivity.this, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
			AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
			alarmManager.cancel(pendingIntent);
			
			if (city.equals(IbicoopDataConstants.PARIS)) {
				parisMetroDbManager.updateAlarmSet(metroStop.getId(), 0);				
			} else if (city.equals(IbicoopDataConstants.LONDON)) {
				londonMetroDbManager.updateAlarmSet(metroStop.getId(), 0);					
			}

			return null;
		}
		
		@Override 
		protected void onPostExecute(Void param) {
			
			boolean alarmIsSet = false;
			
			if (city.equals(IbicoopDataConstants.PARIS)) {
				alarmIsSet = (parisMetroDbManager.readAlarmSet(metroStop.getId()) == 1);
			} else if (city.equals(IbicoopDataConstants.LONDON)) {
				alarmIsSet = (londonMetroDbManager.readAlarmSet(metroStop.getId()) == 1);				
			}
			
			if (alarmIsSet) {
				//Alarm already set
				alarmItem.setIcon(R.drawable.icon_alarm_blue_small);
			} else {
				//Alarm not yet set
				alarmItem.setIcon(R.drawable.icon_alarm_black_small);
			}
			
			if (alarmSetLayout != null) {
				alarmSetLayout.setVisibility(View.GONE);
			}
	
			String format = "HHmm";
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			String date = sdf.format(new Date());
			
			if (timePicker != null) {
				timePicker.setCurrentHour(Integer.parseInt(date.substring(0, 2)));
				timePicker.setCurrentMinute(Integer.parseInt(date.substring(2)));
				timePicker.setVisibility(View.VISIBLE);				
			}
			
		}
	}
	
	
	
	private void showShowReportCrowdDialog() {
		AlertDialog.Builder builder = AlertDialogCompat.getAlertBuilder(InfosDashboardActivity.this);
		
		final CrowdRatingSimplifiedLayout localLayout = new CrowdRatingSimplifiedLayout(InfosDashboardActivity.this);
		builder.setView(localLayout);
		
		builder.setPositiveButton("Report", new DialogInterface.OnClickListener() {
		      public void onClick(DialogInterface dialog, int which) {
		    	  		    	  
		    	int rating = localLayout.getRating();
		    
		    	if (reportCrowdTask != null) reportCrowdTask.cancel(true);
		    	
		        //In Paris we can report different crowd for diffrent lines
		        //FIXME: We can only report the same crowd for all lines in a tube station in London
		    	reportCrowdTask = new IbicoopReportCrowdTask(
		    			InfosDashboardActivity.this, 
		    			city, 
		    			metroStop.getId(), 
		    			metroStop.getTubes()[0].getId(), 
		    			rating,
		    			reportCrowdCallback    			
		    			);
		    	
		    	AsyncTaskCompat.executeParallel(reportCrowdTask);
		    	
		    	showIncentiveDialog();
		    	
		    	dialog.dismiss();
		      }
		    });

		AlertDialog dialog = AlertDialogCompat.getAlertDialog(builder);
		dialog.show();
	}		

	private IbicoopReportCrowdCallback reportCrowdCallback = new IbicoopReportCrowdCallback() {
		
		@Override
		public void reportCrowdSuccess() {
			if (IbicoopDataConstants.DEBUG_MODE) {
				System.out.println("Report crowd success!");
				Toast.makeText(InfosDashboardActivity.this, "Report crowd success", Toast.LENGTH_SHORT).show();
			}
			startClassifiedCrowdTask();
			startGeneralCrowdTask();
		}
		
		@Override
		public void reportCrowdFail() {
			if (IbicoopDataConstants.DEBUG_MODE) {
			System.err.println("Report crowd failed!");
			Toast.makeText(InfosDashboardActivity.this, "Report crowd failed", Toast.LENGTH_SHORT).show();
			}
		}
	};
	
	private View.OnClickListener expandNoiseListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			//TODO: Show or hide crowd
			noiseIsExpanded = !noiseIsExpanded;
			
			if (noiseIsExpanded) {
				//Show collapse icon
				expandNoiseButton.setImageResource(R.drawable.icon_collapse);
				
				noiseInfoLyt.setVisibility(View.VISIBLE);
				noiseClassifiedInfoLyt.setVisibility(View.VISIBLE);
				
				
			} else {
				//Show expand icon
				expandNoiseButton.setImageResource(R.drawable.icon_expand);
				
				noiseInfoLyt.setVisibility(View.GONE);
				noiseClassifiedInfoLyt.setVisibility(View.GONE);
				
			}
			
			if (allIsExpanded && (!noiseIsExpanded) && (!crowdIsExpanded) && (!timeTableIsExpanded) && (!commentIsExpanded)) {
				allIsExpanded = false;
				expandItem.setIcon(R.drawable.icon_expand_blue_small);
			}
			
		}
	};	
	
	
	
	private View.OnClickListener expandCrowdListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			//TODO: Show or hide crowd
			crowdIsExpanded = !crowdIsExpanded;
			
			if (crowdIsExpanded) {
				//Show collapse icon
				expandCrowdButton.setImageResource(R.drawable.icon_collapse);
				
				crowdLyt.setVisibility(View.VISIBLE);
				
				
			} else {
				//Show expand icon
				expandCrowdButton.setImageResource(R.drawable.icon_expand);
				
				crowdLyt.setVisibility(View.GONE);
				
			}
			
			if (allIsExpanded && (!noiseIsExpanded) && (!crowdIsExpanded) && (!timeTableIsExpanded) && (!commentIsExpanded)) {
				allIsExpanded = false;
				expandItem.setIcon(R.drawable.icon_expand_blue_small);
			}
			
		}
	};
	
	
	private View.OnClickListener expandTimeTableListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			timeTableIsExpanded = !timeTableIsExpanded;
			
			if (timeTableIsExpanded) {
				//Show collapse icon
				expandTimeTableButton.setImageResource(R.drawable.icon_collapse);
				
				timeTableLyt.setVisibility(View.VISIBLE);
				
				
			} else {
				//Show expand icon
				expandTimeTableButton.setImageResource(R.drawable.icon_expand);
				
				timeTableLyt.setVisibility(View.GONE);
				
			}
			
			if (allIsExpanded && (!noiseIsExpanded) && (!crowdIsExpanded) && (!timeTableIsExpanded) && (!commentIsExpanded)) {
				allIsExpanded = false;
				expandItem.setIcon(R.drawable.icon_expand_blue_small);
			}
			
		}
	};
	
	
	private View.OnClickListener reportCommentListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Report comment");
			
			if (metroStop != null) {
				if (TSLApplicationContext.getInstance().checkIfUserIsLoggedIn()) {
					showReportCommentDialog();
				}  else {
					showPleaseSignInDialog();
				}				
			}
		}
	};
		
	private void showReportCommentDialog() {
		AlertDialog.Builder builder = AlertDialogCompat.getAlertBuilder(InfosDashboardActivity.this);
		
		final LayoutInflater inflater = InfosDashboardActivity.this.getLayoutInflater();
		LinearLayout localLayout = (LinearLayout) inflater.inflate(R.layout.custom_comment_dialog_view, null);
		builder.setView(localLayout);
		
		final EditText commentText = (EditText) localLayout.findViewById(R.id.comment);
		
		builder.setPositiveButton("Send", new DialogInterface.OnClickListener() {
		      public void onClick(DialogInterface dialog, int which) {
				LinearLayout commentLayout = (LinearLayout) inflater.inflate(R.layout.custom_row_view2, null);
				TextView user = (TextView) commentLayout.findViewById(R.id.user);
				TextView time = (TextView) commentLayout.findViewById(R.id.time);
				TextView comment = (TextView) commentLayout.findViewById(R.id.comment);
				String userString = TSLApplicationContext.getInstance().currentEmailAddress();
				String format = "dd/MM/yyyy HH:mm";
				Locale local = Locale.getDefault();
				
				if (city.equals(IbicoopDataConstants.PARIS)) {
					local = Locale.FRANCE;
				} else if (city.equals(IbicoopDataConstants.LONDON)) {
					local = Locale.UK;
				}
				
				SimpleDateFormat sdf = new SimpleDateFormat(format, local);
				String timeString = sdf.format(new Date());
				String commentString = commentText.getEditableText().toString();
				user.setText(userString);
				time.setText(timeString);
				comment.setText(commentString);
				
				if (reportCommentTask != null) reportCommentTask.cancel(true);
				
		        //In Paris we can report different comment for diffrent lines
		        //FIXME: We can only report the same comment for all lines in a tube station in London
				reportCommentTask = new IbicoopReportCommentTask(
						InfosDashboardActivity.this, 
		    			city, 
		    			metroStop.getId(), 
		    			metroStop.getTubes()[0].getId(),
		    			userString, 
		    			commentString,
		    			reportCommentCallback
						);
				
				AsyncTaskCompat.executeParallel(reportCommentTask);				

		    	dialog.dismiss();
		      }
		    });

		AlertDialog dialog = AlertDialogCompat.getAlertDialog(builder);
		dialog.show();		
	}
	
	private IbicoopReportCommentCallback reportCommentCallback = new IbicoopReportCommentCallback() {
		
		@Override
		public void reportCommentSuccess() {
			if (IbicoopDataConstants.DEBUG_MODE) {
				System.out.println("Report comment success");
				Toast.makeText(InfosDashboardActivity.this, "Report comment success", Toast.LENGTH_SHORT).show();
			}
			
			startUserCommentTask();
		}
		
		@Override
		public void reportCommentFailed() {
			if (IbicoopDataConstants.DEBUG_MODE) {
				System.out.println("Report comment failed");
				Toast.makeText(InfosDashboardActivity.this, "Report comment failed", Toast.LENGTH_SHORT).show();
			}			
		}
	};
	
	private View.OnClickListener commentExpandListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			commentIsExpanded = !commentIsExpanded;
			
			if (commentIsExpanded) {
				//Show collapse icon
				commentExpandButton.setImageResource(R.drawable.icon_collapse);
				
				twitComExpandLyt.setVisibility(View.VISIBLE);
				fbComExpandLyt.setVisibility(View.VISIBLE);
				userComExpandLyt.setVisibility(View.VISIBLE);
				
				twitComExpandLine.setVisibility(View.VISIBLE);
				fbComExpandLine.setVisibility(View.VISIBLE);
				userComExpandLine.setVisibility(View.VISIBLE);
				
			} else {
				//Show expand icon
				commentExpandButton.setImageResource(R.drawable.icon_expand);
				
				twitComExpandLyt.setVisibility(View.GONE);
				fbComExpandLyt.setVisibility(View.GONE);
				userComExpandLyt.setVisibility(View.GONE);
				
				twitComExpandLine.setVisibility(View.GONE);
				fbComExpandLine.setVisibility(View.GONE);
				userComExpandLine.setVisibility(View.GONE);
				
				if (twitComIsExpanded) {
					twitCommentLyt.setVisibility(View.GONE);
					twitComExpandButton.setImageResource(R.drawable.icon_expand);
					twitComIsExpanded = false;
				}
				
				if (fbComIsExpanded) {
					fbComLyt.setVisibility(View.GONE);
					fbComExpandButton.setImageResource(R.drawable.icon_expand);
					fbComIsExpanded = false;
				}
				
				if (userComIsExpanded) {
					userComLyt.setVisibility(View.GONE);
					userComExpandButton.setImageResource(R.drawable.icon_expand);
					userComIsExpanded = false;
				}
			}
			
			if (allIsExpanded && (!noiseIsExpanded) && (!crowdIsExpanded) && (!timeTableIsExpanded) && (!commentIsExpanded)) {
				allIsExpanded = false;
				expandItem.setIcon(R.drawable.icon_expand_blue_small);
			}
			
		}
	};
	
	private View.OnClickListener expandTwitComListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			
			twitComIsExpanded = !twitComIsExpanded;
			
			if (twitComIsExpanded) {
				//Show collapse icon
				twitComExpandButton.setImageResource(R.drawable.icon_collapse);
				
				twitCommentLyt.setVisibility(View.VISIBLE);
				
				
			} else {
				//Show expand icon
				twitComExpandButton.setImageResource(R.drawable.icon_expand);
				
				twitCommentLyt.setVisibility(View.GONE);
			}
			
		}
	};
	
	private View.OnClickListener expandFbComListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			
			fbComIsExpanded = !fbComIsExpanded;
			
			if (fbComIsExpanded) {
				//Show collapse icon
				fbComExpandButton.setImageResource(R.drawable.icon_collapse);
				
				fbComLyt.setVisibility(View.VISIBLE);
				
				
			} else {
				//Show expand icon
				fbComExpandButton.setImageResource(R.drawable.icon_expand);
				
				fbComLyt.setVisibility(View.GONE);
			}
			
		}
	};
	
	private View.OnClickListener expandUserComListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			userComIsExpanded = !userComIsExpanded;
			
			if (userComIsExpanded) {
				//Show collapse icon
				userComExpandButton.setImageResource(R.drawable.icon_collapse);
				
				userComLyt.setVisibility(View.VISIBLE);
				
				
			} else {
				//Show expand icon
				userComExpandButton.setImageResource(R.drawable.icon_expand);
				
				userComLyt.setVisibility(View.GONE);
			}
			
		}
	};
	
	private void showAllNotAvailable() {
		showNoiseDataNotAvailable();
		showClassifiedNoiseNotAvailable();
		showClassifiedCrowdNotAvailable();
		showGeneralCrowdNotAvailable();
		showTimetableNotAvailable();
		showTwitNotAvailable();
		showFbNotAvailable();
		showUserCommentNotAvailable();	
		

	}
	
	private void showClassifiedNoiseNotAvailable() {
		classifiedNoisePb.setVisibility(View.GONE);
		classifiedNoiseLevelPb.setVisibility(View.GONE);
		classifiedNoiseDescription.setVisibility(View.GONE);
		classifiedNoiseLevelContentLyt.setVisibility(View.GONE);
		classifiedNoiseText.setVisibility(View.GONE);
		classifiedNoiseMaxText.setVisibility(View.GONE);
		classifiedNoiseReportsText.setVisibility(View.GONE);
		classifiedNoiseNotAvailableView.setVisibility(View.VISIBLE);
	}
	
	private void showNoiseDataNotAvailable() {
		for (int j = 0;  j < NOISE_INTERVAL_MAX_MULTIPLE; j++) {
			
			String noiseInterval = "";
			
			if (j == 0) {
				noiseInterval = "Between now and " + NOISE_HOUR + " hours ago";
			} else {
				noiseInterval = "Between " + (NOISE_HOUR*j)  + " and " + NOISE_HOUR*(j + 1) + " hours ago";
			}
			
			LinearLayout noiseInfoContentLyt = noiseLayouts.get(j);
			ProgressBar noiseLevelPb = (ProgressBar) noiseInfoContentLyt.findViewById(R.id.noiseLevelPb);
			noiseLevelPb.setMax(IbicoopDataConstants.NOISE_MAX_VALUE);
			ProgressBar noisePb = (ProgressBar) noiseInfoContentLyt.findViewById(R.id.noisePb);
			TextView noiseNotAvailableView = (TextView) noiseInfoContentLyt.findViewById(R.id.noiseNotAvailableView);
			
			TextView noiseDescription = (TextView) noiseInfoContentLyt.findViewById(R.id.noiseLevelDescription);
			TextView noiseText = (TextView) noiseInfoContentLyt.findViewById(R.id.noiseLevelText);
			TextView noiseMaxText = (TextView) noiseInfoContentLyt.findViewById(R.id.noiseLevelMaxText);
			TextView noiseReportsText = (TextView) noiseInfoContentLyt.findViewById(R.id.noiseReportsText);
			LinearLayout noiseLevelContentLyt = (LinearLayout) noiseInfoContentLyt.findViewById(R.id.noiseLevelContentLyt);				
			noiseDescription.setText(noiseInterval + ":\nLatest noise information not available");
			noisePb.setVisibility(View.GONE);
			noiseLevelPb.setVisibility(View.GONE);
			noiseDescription.setVisibility(View.VISIBLE);
			noiseLevelContentLyt.setVisibility(View.GONE);
			noiseText.setVisibility(View.GONE);
			noiseMaxText.setVisibility(View.GONE);
			noiseReportsText.setVisibility(View.GONE);
			noiseNotAvailableView.setVisibility(View.GONE);
		}
	}
	
	private void showClassifiedCrowdNotAvailable() {
		crowdPb.setVisibility(View.GONE);
		crowdNotAvailableTv.setVisibility(View.VISIBLE);
		crowdNotAvailableTv.setText(classifiedCrowdInterval + ": Crowd not available");
	}
	
	private void showGeneralCrowdNotAvailable() {
		
		for (int j = 0; j <  GENERAL_CROWD_INTERVAL_MAX_MULTIPLE; j++) {
			//General
			LinearLayout generalCrowdParentLayout = generalCrowdLayouts.get(j);

			TextView nbGeneralCrowdTv = (TextView) generalCrowdParentLayout.findViewById(R.id.nbCustomCrowd);
			nbGeneralCrowdTv.setVisibility(View.GONE);
			
			TextView generalCrowdIntervalTv = (TextView) generalCrowdParentLayout.findViewById(R.id.customCrowdTimeInterval);
			generalCrowdIntervalTv.setVisibility(View.GONE);
			
			TextView generalCrowdNbVotesTv = (TextView) generalCrowdParentLayout.findViewById(R.id.customCrowdNbVotes);
			generalCrowdNbVotesTv.setVisibility(View.GONE);

			TextView generalCrowdNotAvailableTv = (TextView) generalCrowdParentLayout.findViewById(R.id.customCrowdNotAvailableView);
			generalCrowdNotAvailableTv.setVisibility(View.VISIBLE);
			
			String generalCrowdInterval = "";
			
			if (j == 0) {
				generalCrowdInterval = "Between now and " + GENERAL_CROWD_HOUR + " hours ago";
			} else {
				generalCrowdInterval = "Between " + (GENERAL_CROWD_HOUR*j)  + " and " + GENERAL_CROWD_HOUR*(j + 1) + " hours ago";
			}
			
			generalCrowdNotAvailableTv.setText(generalCrowdInterval + ":\n" + "Latest crowd information not available");
			
			LinearLayout generalCrowdRatingLyt = (LinearLayout) generalCrowdParentLayout.findViewById(R.id.customCrowd);
			generalCrowdRatingLyt.setVisibility(View.GONE);
			
			ProgressBar generalCrowdPb = (ProgressBar) generalCrowdParentLayout.findViewById(R.id.customCrowdPb);
			generalCrowdPb.setVisibility(View.GONE);
		}
	}
	
	private void showTimetableNotAvailable() {
		//timeTablePb.setVisibility(View.GONE);
		//timeTableSubLyt.setVisibility(View.VISIBLE);
		
		LayoutInflater li = LayoutInflater.from(InfosDashboardActivity.this);			
		LinearLayout commentLayout = (LinearLayout) li.inflate(R.layout.custom_layout_time_table2, null);
		TextView line = (TextView) commentLayout.findViewById(R.id.line);
		TextView time = (TextView) commentLayout.findViewById(R.id.time);
		TextView direction = (TextView) commentLayout.findViewById(R.id.direction);
		line.setText("");
		time.setText("Not available");
		direction.setText("");
		
		//timeTableSubLyt.addView(commentLayout);	
	}
	
	private void showTwitNotAvailable() {
		twitComPb.setVisibility(View.GONE);
		LayoutInflater li = LayoutInflater.from(InfosDashboardActivity.this);			
		LinearLayout commentLayout = (LinearLayout) li.inflate(R.layout.custom_row_view2, null);
		TextView user = (TextView) commentLayout.findViewById(R.id.user);
		TextView time = (TextView) commentLayout.findViewById(R.id.time);
		TextView comment = (TextView) commentLayout.findViewById(R.id.comment);
		user.setText("");
		time.setText("");
		comment.setText("Not available");
		twitCommentSubLyt.addView(commentLayout);
		twitCommentSubLyt.setVisibility(View.VISIBLE);
	}
	
	private void showFbNotAvailable() {
		fbComPb.setVisibility(View.GONE);	
		LayoutInflater li = LayoutInflater.from(InfosDashboardActivity.this);
		LinearLayout commentLayout = (LinearLayout) li.inflate(R.layout.custom_row_view2, null);
		TextView user = (TextView) commentLayout.findViewById(R.id.user);
		TextView time = (TextView) commentLayout.findViewById(R.id.time);
		TextView comment = (TextView) commentLayout.findViewById(R.id.comment);
		user.setText("");
		time.setText("");
		comment.setText("Not available");
		fbComSubLyt.addView(commentLayout);
		fbComSubLyt.setVisibility(View.VISIBLE);		
	}
	
	private void showUserCommentNotAvailable() {
		userComPb.setVisibility(View.GONE);
		LayoutInflater li = LayoutInflater.from(InfosDashboardActivity.this);
		LinearLayout commentLayout = (LinearLayout) li.inflate(R.layout.custom_row_view2, null);
		TextView user = (TextView) commentLayout.findViewById(R.id.user);
		TextView time = (TextView) commentLayout.findViewById(R.id.time);
		TextView comment = (TextView) commentLayout.findViewById(R.id.comment);
		user.setText("");
		time.setText("");
		comment.setText("Not available");
		userNoComView = commentLayout;
		userComSubLyt.addView(userNoComView);
		userComSubLyt.setVisibility(View.VISIBLE);		
	}
	
	private String convertQueryTimeToInterval(String city, String queryTime) {
		long initialTimestamp = convertDateStringToTimestamp(extractQueryTimeToCurrentTime(queryTime));
		return extractQueryTimeToInterval(convertTimestampToTimeQueryString(convertTimestampAccordingToTimeZone(city, initialTimestamp)));
	}
	
	private String extractQueryTimeToInterval(String initialTime) {
		String time = "";
		
		int hour = Integer.parseInt(initialTime.substring(0, 2));
		int minute = Integer.parseInt(initialTime.substring(2));
		int interval = (int) (Math.floor(minute/15)*15); 
		
		if (interval != 45) {
			time = intToTime(hour) + ":" + intToTime(interval) + " - " + intToTime(hour) + ":" + intToTime(interval + 15);
		} else {
			time = intToTime(hour) + ":" + intToTime(interval) + " - " + intToTime(hour + 1) + ":" + "00";
		}
		
		return time;
	}
	
	private String intToTime(int timeInt) {

		String time = "00";
		
		if (timeInt < 10) {
			time = "0" + timeInt;
		} else {
			time = String.valueOf(timeInt);
		}
		
		return time;
	}
	
	private String convertTimestampToTimeQueryString(long timestamp) {
		SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
		String date = sdf.format(new Date(timestamp));
		return date;
	}
	
	private long convertTimestampAccordingToTimeZone(String city, long initialTimestamp) {
		long timestamp = initialTimestamp;
		
		String timeZone = "";
		
		if (city.equals(IbicoopDataConstants.PARIS)) {
			timeZone = "Europe/Paris";
			
		} else if (city.equals(IbicoopDataConstants.LONDON)) {
			timeZone = "Europe/London";
		}
		
		//Set to local timestamp
		timestamp = timestamp - TimeZone.getDefault().getRawOffset() + 
    	TimeZone.getTimeZone(timeZone).getRawOffset();
		
		return timestamp;
	}
	
	private String extractQueryTimeToCurrentTime(String initialTime) {
		String newTime = "";
			
		String hour = initialTime.substring(0, 2);
		String minute = initialTime.substring(2);

		String formatDate = "yyyy-MM-dd'T'";
		SimpleDateFormat sdfDate = new SimpleDateFormat(formatDate);
		String date = sdfDate.format(new Date());
		
		String formatSecond = ":ss.SSS'Z'";
		SimpleDateFormat sdfSecond = new SimpleDateFormat(formatSecond);
		String second = sdfSecond.format(new Date());
		
		newTime = date + hour + ":" + minute + second;
		
		if (IbicoopDataConstants.DEBUG_MODE) System.out.println(">>>>New time = " + newTime);
		
		return newTime;
	}
	
	private long convertDateStringToTimestamp(String dateString) {
		long timestamp = -1l;
		
		try {
			if (IbicoopDataConstants.DEBUG_MODE) System.out.println("Date string = " + dateString);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			Date date = sdf.parse(dateString);
			timestamp = date.getTime();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return timestamp;
	}
	
	IbicoopClassifiedCrowdednessCallback classifiedCrowdCallback = new IbicoopClassifiedCrowdednessCallback() {
		
		@Override
		public void tubeStationCrowdInfo(TubeStationCrowd crowd) {
			if (IbicoopDataConstants.DEBUG_MODE) {
				System.out.println("Get classified crowd success");
				Toast.makeText(InfosDashboardActivity.this, "Get classified crowd success", Toast.LENGTH_SHORT).show();
			}
			
			int level = crowd.getCrowd().getCrowdLevel().getLevel();
			int nbVotes = crowd.getCrowd().getNbVotes();

			//Dismiss crowd progress bar
			crowdPb.setVisibility(View.GONE);
			
			if (level > 0) {
				//Information available
				crowdRatingLyt.setVisibility(View.VISIBLE);
				CrowdHelper.setNewCrowdImagesByImageViews(InfosDashboardActivity.this, crowdImageViews, level);
				nbCrowdTv.setText(level + "/5");
				nbCrowdTv.setVisibility(View.VISIBLE);
				//FIXME: It will show london time if we post it in paris!
				crowdIntervalTv.setText(classifiedCrowdInterval);
				crowdIntervalTv.setVisibility(View.VISIBLE);
				String voteTxt = " vote";
				if (nbVotes > 1) voteTxt = voteTxt + "s";
				crowdNbVotesTv.setText(nbVotes + voteTxt);
				crowdNbVotesTv.setVisibility(View.VISIBLE);
			} else {
				//No information
				crowdNotAvailableTv.setVisibility(View.VISIBLE);
				crowdNotAvailableTv.setText(classifiedCrowdInterval + ":\nNo vote for the crowd");
			}
			
		}
		
		@Override
		public void crowdednessFailed() {
			
			if (IbicoopDataConstants.DEBUG_MODE) {
				System.err.println("Get classified crowd failed");
				Toast.makeText(InfosDashboardActivity.this, "Get classified crowd failed", Toast.LENGTH_SHORT).show();
			}
			
			showClassifiedCrowdNotAvailable();
		}
	};
	
	IbicoopGeneralCrowdednessCallback generalCrowdCallback = new IbicoopGeneralCrowdednessCallback() {
		
		@Override
		public void tubeStationCrowdInfo(TubeStationCrowdsCollection crowdsCollection) {
			if (IbicoopDataConstants.DEBUG_MODE) {
				System.out.println("Get general crowd success");
				Toast.makeText(InfosDashboardActivity.this, "Get general crowd success", Toast.LENGTH_SHORT).show();
			}
			
			TubeStationCrowd[] stationCrowds = crowdsCollection.getTubeStationCrowds();
			
			int length = GENERAL_CROWD_INTERVAL_MAX_MULTIPLE;
			
			if (length > stationCrowds.length) {
				length = stationCrowds.length;
			}
			
			for (int j = 0; j < length; j++) {
				
				//General
				LinearLayout generalCrowdParentLayout = generalCrowdLayouts.get(j);
				
				ImageView[] generalCrowdImageViews = new ImageView[5];
				generalCrowdImageViews[0] = (ImageView) generalCrowdParentLayout.findViewById(R.id.customCrowd1);
				generalCrowdImageViews[1] = (ImageView) generalCrowdParentLayout.findViewById(R.id.customCrowd2);
				generalCrowdImageViews[2] = (ImageView) generalCrowdParentLayout.findViewById(R.id.customCrowd3);
				generalCrowdImageViews[3] = (ImageView) generalCrowdParentLayout.findViewById(R.id.customCrowd4);
				generalCrowdImageViews[4] = (ImageView) generalCrowdParentLayout.findViewById(R.id.customCrowd5);
				
				TextView nbGeneralCrowdTv = (TextView) generalCrowdParentLayout.findViewById(R.id.nbCustomCrowd);				
				TextView generalCrowdIntervalTv = (TextView) generalCrowdParentLayout.findViewById(R.id.customCrowdTimeInterval);
				TextView generalCrowdNbVotesTv = (TextView) generalCrowdParentLayout.findViewById(R.id.customCrowdNbVotes);
				TextView generalCrowdNotAvailableTv = (TextView) generalCrowdParentLayout.findViewById(R.id.customCrowdNotAvailableView);
				LinearLayout generalCrowdRatingLyt = (LinearLayout) generalCrowdParentLayout.findViewById(R.id.customCrowd);
				
				ProgressBar generalCrowdPb = (ProgressBar) generalCrowdParentLayout.findViewById(R.id.customCrowdPb);
					
				String generalCrowdInterval = "";
				
				if (j == 0) {
					generalCrowdInterval = "Between " + GENERAL_CROWD_HOUR + " hours ago and now";
				} else {
					generalCrowdInterval = "Between " + (GENERAL_CROWD_HOUR*j)  + " and " + GENERAL_CROWD_HOUR*(j + 1) + " hours ago";
				}
				
				TubeStationCrowd crowd = stationCrowds[j];
				
				int level = crowd.getCrowd().getCrowdLevel().getLevel();
				int nbVotes = crowd.getCrowd().getNbVotes();

				//Dismiss crowd progress bar
				generalCrowdPb.setVisibility(View.GONE);

				if (level > 0) {
					//Information available
					generalCrowdRatingLyt.setVisibility(View.VISIBLE);
					CrowdHelper.setNewCrowdImagesByImageViews(InfosDashboardActivity.this, generalCrowdImageViews, level);
					nbGeneralCrowdTv.setText(level + "/5");
					nbGeneralCrowdTv.setVisibility(View.VISIBLE);
					//FIXME: It will show london time if we post it in paris!
					generalCrowdIntervalTv.setText(generalCrowdInterval);
					generalCrowdIntervalTv.setVisibility(View.VISIBLE);
					String voteTxt = " vote";
					if (nbVotes > 1) voteTxt = voteTxt + "s";
					generalCrowdNbVotesTv.setText(nbVotes + voteTxt);
					generalCrowdNbVotesTv.setVisibility(View.VISIBLE);
				} else {
					//No information
					generalCrowdNotAvailableTv.setVisibility(View.VISIBLE);
					generalCrowdNotAvailableTv.setText(generalCrowdInterval + ":\nNo vote for crowd");
				}				
			}

		}
		
		@Override
		public void crowdednessFailed() {
			if (IbicoopDataConstants.DEBUG_MODE) {
				System.err.println("Get general crowd failed");
				Toast.makeText(InfosDashboardActivity.this, "Get general crowd failed", Toast.LENGTH_SHORT).show();
			}

			showGeneralCrowdNotAvailable();
		}
	};
	
	private IbicoopTwitterCommentCallback twitterCallback = new IbicoopTwitterCommentCallback() {
		
		@Override
		public void twitterCommentsCollection(TubeStationCommentsCollection info) {
			
			if (IbicoopDataConstants.DEBUG_MODE) {
				System.out.println("Get twitter comments success");
				Toast.makeText(InfosDashboardActivity.this, "Get twitter comments success", Toast.LENGTH_SHORT).show();
			}
			
			//Dismiss twitter progress bar
			twitComPb.setVisibility(View.GONE);
			twitCommentSubLyt.setVisibility(View.VISIBLE);
			
			LayoutInflater li = LayoutInflater.from(InfosDashboardActivity.this);

			TubeStationComment[] twitterComments =  info.getTubeStationComments();
			
			if (twitterComments.length > 0) {
				
				String format = "yyyy-MM-dd' 'HH:mm:ss";
				SimpleDateFormat sdf = new SimpleDateFormat(format);
				
				for (TubeStationComment userComment : twitterComments) {
					LinearLayout commentLayout = (LinearLayout) li.inflate(R.layout.custom_row_view2, null);
					TextView user = (TextView) commentLayout.findViewById(R.id.user);
					TextView time = (TextView) commentLayout.findViewById(R.id.time);
					TextView comment = (TextView) commentLayout.findViewById(R.id.comment);
					String userString = userComment.getUser();
					String timeString = sdf.format(new Date(userComment.getTimestamp()));
					String commentString = userComment.getComment();
					user.setText(userString);
					time.setText(timeString);
					comment.setText(commentString);
					twitCommentSubLyt.addView(commentLayout);
				}
				
			} else {
				LinearLayout commentLayout = (LinearLayout) li.inflate(R.layout.custom_row_view2, null);
				TextView user = (TextView) commentLayout.findViewById(R.id.user);
				TextView time = (TextView) commentLayout.findViewById(R.id.time);
				TextView comment = (TextView) commentLayout.findViewById(R.id.comment);
				user.setText("");
				time.setText("");
				comment.setText("No comment");
				twitCommentSubLyt.addView(commentLayout);
			}
		}
		
		@Override
		public void twitterCommentFailed() {
			
			if (IbicoopDataConstants.DEBUG_MODE) {
				System.err.println("Get twitter comments failed");
				Toast.makeText(InfosDashboardActivity.this, "Get twitter comments failed", Toast.LENGTH_SHORT).show();
			}

			showTwitNotAvailable();
		}
	};
		
	private IbicoopFacebookCommentCallback facebookCallback = new IbicoopFacebookCommentCallback() {
		
		@Override
		public void tubeStationFacebookCommentsCollection(
				TubeStationCommentsCollection info) {
			
			if (IbicoopDataConstants.DEBUG_MODE) {
				System.out.println("Get facebook comments success");
				Toast.makeText(InfosDashboardActivity.this, "Get facebook comments success", Toast.LENGTH_SHORT).show();
			}
			
			//Dismiss facebook progress bar
			fbComPb.setVisibility(View.GONE);
			fbComSubLyt.setVisibility(View.VISIBLE);

			TubeStationComment[] userComments =  info.getTubeStationComments();
			
			LayoutInflater li = LayoutInflater.from(InfosDashboardActivity.this);
			
			if (userComments.length > 0) {
				String format = "yyyy-MM-dd' 'HH:mm:ss";
				SimpleDateFormat sdf = new SimpleDateFormat(format);
				
				for (TubeStationComment userComment : userComments) {
					LinearLayout commentLayout = (LinearLayout) li.inflate(R.layout.custom_row_view2, null);
					TextView user = (TextView) commentLayout.findViewById(R.id.user);
					TextView time = (TextView) commentLayout.findViewById(R.id.time);
					TextView comment = (TextView) commentLayout.findViewById(R.id.comment);
					String userString = userComment.getUser();
					String timeString = sdf.format(new Date(userComment.getTimestamp()));
					String commentString = userComment.getComment();
					user.setText(userString);
					time.setText(timeString);
					comment.setText(commentString);
					fbComSubLyt.addView(commentLayout);
				}
			} else {
				LinearLayout commentLayout = (LinearLayout) li.inflate(R.layout.custom_row_view2, null);
				TextView user = (TextView) commentLayout.findViewById(R.id.user);
				TextView time = (TextView) commentLayout.findViewById(R.id.time);
				TextView comment = (TextView) commentLayout.findViewById(R.id.comment);
				user.setText("");
				time.setText("");
				comment.setText("No comment");
				fbComSubLyt.addView(commentLayout);
			}
		}
		
		
		@Override
		public void tubeStationFacebookCommentsCollectionFailed() {
			if (IbicoopDataConstants.DEBUG_MODE) {
				System.err.println("Get facebook comments failed");
				Toast.makeText(InfosDashboardActivity.this, "Get facebook comments failed", Toast.LENGTH_SHORT).show();
			}
			
			showFbNotAvailable();
		}
	};
	
	private IbicoopCommentCallback commentCallback = new IbicoopCommentCallback() {
		
		@Override
		public void tubeStationCommentsCollection(TubeStationCommentsCollection info) {
			
			if (IbicoopDataConstants.DEBUG_MODE) {
				System.out.println("Get user comments success");
				Toast.makeText(InfosDashboardActivity.this, "Get user comments success", Toast.LENGTH_SHORT).show();
			}
						
			//Dismiss time table progress bar
			userComPb.setVisibility(View.GONE);
			userComSubLyt.setVisibility(View.VISIBLE);
			
			LayoutInflater li = LayoutInflater.from(InfosDashboardActivity.this);

			TubeStationComment[] userComments =  info.getTubeStationComments();
			
			if (userComments.length > 0)
			{
				String format = "yyyy-MM-dd' 'HH:mm:ss";
				SimpleDateFormat sdf = new SimpleDateFormat(format);
				
				for (TubeStationComment userComment : userComments) {
					LinearLayout commentLayout = (LinearLayout) li.inflate(R.layout.custom_row_view2, null);
					TextView user = (TextView) commentLayout.findViewById(R.id.user);
					TextView time = (TextView) commentLayout.findViewById(R.id.time);
					TextView comment = (TextView) commentLayout.findViewById(R.id.comment);
					String userString = userComment.getUser();
					String timeString = sdf.format(new Date(userComment.getTimestamp()));
					String commentString = userComment.getComment();
					user.setText(userString);
					time.setText(timeString);
					comment.setText(commentString);
					userComSubLyt.addView(commentLayout);
				}				
			} else {
				LinearLayout commentLayout = (LinearLayout) li.inflate(R.layout.custom_row_view2, null);
				TextView user = (TextView) commentLayout.findViewById(R.id.user);
				TextView time = (TextView) commentLayout.findViewById(R.id.time);
				TextView comment = (TextView) commentLayout.findViewById(R.id.comment);
				user.setText("");
				time.setText("");
				comment.setText("No comment");
				userNoComView = commentLayout;
				userComSubLyt.addView(userNoComView);
			}
		}
		
		@Override
		public void commentFailed() {
			if (IbicoopDataConstants.DEBUG_MODE) {
				System.err.println("Get user comments failed");
				 Toast.makeText(InfosDashboardActivity.this, "Get user comments failed", Toast.LENGTH_SHORT).show();
			}
			showUserCommentNotAvailable();
		}
	};
	
	private void showPleaseSignInDialog() {
		AlertDialog.Builder builder = AlertDialogCompat.getAlertBuilder(InfosDashboardActivity.this);
		
		LayoutInflater inflater = InfosDashboardActivity.this.getLayoutInflater();
		
		LinearLayout localLayout = (LinearLayout) inflater.inflate(R.layout.please_login_dialog2, null);
		builder.setTitle("Please sign in");
		builder.setView(localLayout);
		builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
		      public void onClick(DialogInterface dialog, int which) {
		    	  showSignInDialog();
		    	  dialog.dismiss();
		    }
		});
		
		AlertDialog dialog = AlertDialogCompat.getAlertDialog(builder);
		dialog.show();	
	}

	private void showSignInDialog() {
		
		LayoutInflater li = LayoutInflater.from(InfosDashboardActivity.this);
		LinearLayout loginLayout = (LinearLayout) li.inflate(R.layout.activity_new_login2, null);
		
		//Set dialog view here
		AlertDialog.Builder builder = AlertDialogCompat.getAlertBuilder(InfosDashboardActivity.this);
		builder.setTitle("Your account");
		builder.setView(loginLayout);
		AlertDialog dialog = AlertDialogCompat.getAlertDialog(builder);
		dialog.show();
		
		email = (TextView) loginLayout.findViewById(R.id.mail);	
		email.setEnabled(false);
		
		signInText = (TextView) loginLayout.findViewById(R.id.signInText);
		
		login = (Button) loginLayout.findViewById(R.id.signInButton);
		login.setClickable(true);
		login.setOnClickListener(login_listener);
		
		if (TSLApplicationContext.getInstance().checkIfUserIsLoggedIn()) {
			//User logged in already
			userId = TSLApplicationContext.getInstance().currentEmailAddress();
			signInText.setText("You are already signed in!");
			email.setText(TSLApplicationContext.getInstance().currentEmailAddress());
	        email.setTextColor(getResources().getColor(android.R.color.darker_gray));
			login.setText("Sign out");
		} else {
			signInText.setText("Sign in with Gmail");		
			login.setText("Sign in");
			userId = "Email";
			searchGoogleAccount();
		}
	}
	
	private void searchGoogleAccount() {
		if (pd != null) pd.dismiss();
		pd = ProgressDialogCompat.getProgressDialog(this, "Searching available account...");
		pd.show();
		
		AccountManager am = AccountManager.get(this);
		Bundle options = new Bundle();
		Account[] accounts = am.getAccountsByType(ACCOUNT_TYPE_GOOGLE);
		
		if (accounts.length == 0) {
			
			pd.dismiss();
			
			AlertDialog.Builder builder = AlertDialogCompat.getAlertBuilder(this);
			
			builder.setMessage("Sorry, there is no valid google account on your phone")
		       .setTitle("Google account not available");
			builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
			      public void onClick(DialogInterface dialog, int which) {	  
			    	  dialog.dismiss();
			    } });

			AlertDialog dialog = AlertDialogCompat.getAlertDialog(builder);
			dialog.show();
			return;
		}

		am.getAuthToken(
		    accounts[0],            				// Account retrieved using getAccountsByType()
		    AUTHORISATION_SCOPE_MANAGE_YOUR_TASKS,  // Authorization scope
		    options,                       			// Authenticator-specific options
		    this,                           		// Your activity
		    new OnTokenAcquired(),      		// Callback called when a token is successfully acquired
		    new Handler(new OnError()));    		// Callback called if an error occurs
	}
	
	private class OnError implements Callback {

		@Override
		public boolean handleMessage(Message msg) {
			
			AlertDialog.Builder builder = AlertDialogCompat.getAlertBuilder(InfosDashboardActivity.this);

			builder.setMessage("Permission not granted")
		       .setTitle("Sorry, permission is not granted for your account");
			builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
			      public void onClick(DialogInterface dialog, int which) {
			    	  dialog.dismiss();
			    }});
			
			AlertDialog dialog = builder.create();
			dialog.show();
			
			return true;
		}
		
	}
	
	private class OnTokenAcquired implements AccountManagerCallback<Bundle> {

		@Override
	    public void run(AccountManagerFuture<Bundle> result_) {
						
	        Bundle bundle = null;
			try {
				bundle = result_.getResult();
			} catch (OperationCanceledException e) {
				if (IbicoopDataConstants. DEBUG_MODE) e.printStackTrace();
			} catch (AuthenticatorException e) {
				if (IbicoopDataConstants. DEBUG_MODE) e.printStackTrace();
			} catch (IOException e) {
				if (IbicoopDataConstants. DEBUG_MODE) e.printStackTrace();
			}
	    
			pd.dismiss();
			
	        // The token is a named value in the bundle. The name of the value
	        // is stored in the constant AccountManager.KEY_AUTHTOKEN.
			if(bundle != null) {
	        //String token = bundle.getString(AccountManager.KEY_AUTHTOKEN);//not used
	        String emailAddress = bundle.getString(AccountManager.KEY_ACCOUNT_NAME);
	        email.setText(emailAddress);
	        email.setTextColor(getResources().getColor(android.R.color.black));
			} else {
				AlertDialog.Builder builder = AlertDialogCompat.getAlertBuilder(InfosDashboardActivity.this);
				builder.setMessage("Permission not granted")
			       .setTitle("Sorry, permission is not granted for your account");
				builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
				      public void onClick(DialogInterface dialog, int which) {
				    	  dialog.dismiss();
				    } });
				
				AlertDialog dialog = builder.create();
				dialog.show();
			}
	    }
	}
	
	private OnClickListener login_listener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			if (!TSLApplicationContext.getInstance().checkIfUserIsLoggedIn()) {
				//Going to sign in
				userId = email.getText().toString();
				
				if (userId.contains("@")) {
					//Sign in if it is a valig gmail account
					TSLApplicationContext.getInstance().injectEmailAddressOnceUserLoggedIn(userId);
			        email.setTextColor(getResources().getColor(android.R.color.darker_gray));
					signInText.setText("You are already signed in!");					
					login.setText("Sign out");
					signInItem.setTitle("Sign out");
				} else {
					searchGoogleAccount();
				}
				
			} else {
				TSLApplicationContext.getInstance().injectEmailAddressOnceUserLoggedIn(null);
		        email.setTextColor(getResources().getColor(android.R.color.black));
		        login.setText("Sign in");
				signInText.setText("Sign in with Gmail");
				signInItem.setTitle("Sign in");
			}
		}
	};
}
