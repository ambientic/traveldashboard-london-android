package traveldashboardlondon.compat;

import android.app.ProgressDialog;
import android.content.Context;

public class ProgressDialogCompat {

	public static ProgressDialog getProgressDialog(Context context, String title) {
		ProgressDialog progressDialog;
		progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(title);
        progressDialog.setCancelable(true);     
        return progressDialog;
	}
}
