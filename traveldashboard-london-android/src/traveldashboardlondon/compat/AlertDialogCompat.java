package traveldashboardlondon.compat;


import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;

public class AlertDialogCompat {
	
	public static Builder getAlertBuilder(Context context) {
		Builder builder;
        builder = new AlertDialog.Builder(context);
        return builder;
	}

	
	public static AlertDialog getAlertDialog(Builder builder) {
		AlertDialog  ad = builder.create();
		return ad;
	}
}
